<?php

use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Domain\DomainController;
use App\Http\Controllers\Hosting\HostingController;
use App\Http\Controllers\Invoice\InvoiceController;
use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\Setting\SettingController;
use App\Http\Controllers\TestScheduleInvoiceController;
use App\Http\Controllers\Tld\TldController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::route('user.index');
})->middleware(['auth', 'verified']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';

Route::resource('user',UserController::class);
Route::get('userSearch', [UserController::class, 'search']);
Route::get('userBatchReg', [UserController::class, 'batchRegOp']);
Route::get('user/{id}/detail', [UserController::class, 'detail']);

Route::resource('invoice', InvoiceController::class);
Route::get('invoice/showByDebtor/{debtorCode}', [ InvoiceController::class, 'listByDebtor' ]);
Route::get('invoiceStatus', [ InvoiceController::class, 'searchByStatus' ]);
Route::get('invoiceSearch', [ InvoiceController::class, 'searchById' ]);
Route::get('invoiceDownload/{invoiceCode}', [InvoiceController::class, 'download']);
Route::get('testInvoice/{invoiceCode}', [InvoiceController::class, 'testEditInvoice']);
Route::get('testInvoiceFromDb', [InvoiceController::class, 'testInvoiceDb']);
Route::get('testDate/{cs}/{nd}/{bcl}/{bcp}', [InvoiceController::class, 'testDate']);
Route::get('previewInvoice', [InvoiceController::class, 'previewPDF']);

Route::resource('domain', DomainController::class);
Route::get('domain/nameserver/{domainId}', [DomainController::class, 'nameserver'])->name('domain.nameserver');
Route::get('domain/contactInformation/{domainId}', [DomainController::class, 'contactInformation'])->name('domain.information');
Route::get('domainByStatus', [DomainController::class, 'domainByStatus']);
Route::get('domainAuthCode', [DomainController::class, 'domainAuthCode']);
Route::post('domainCheck', [DomainController::class, 'domainCheck']);
Route::get('domain/dns/template',[DomainController::class, 'domainDnsTemplate']);
Route::get('domain/dns/template/{dnsTemplateId}',[DomainController::class, 'showDnsTemplate']);

Route::resource('product', ProductController::class);
Route::get('productSearch', [ProductController::class, 'search']);

Route::resource('customer', CustomerController::class);

Route::resource('tld', TldController::class);
Route::get('tldByName', [TldController::class, 'getByName']);

Route::resource('hosting', HostingController::class);

Route::get('availablePeriod', [SettingController::class, 'getPeriod']);

Route::get('testScheduleInvoice', [TestScheduleInvoiceController::class, 'index']);
Route::post('testGenerateInvoice', [TestScheduleInvoiceController::class, 'generate'])->name('invoice.test');



// View Only (Slicing Purpose)
Route::get("domain/detail-domain", function(){
   return view("admin.domain.detail-domain");
});

Route::get('/detail-domain', function () {
    return view('admin.domain.detail-domain');
});
Route::get('/detail-invoice', function () {
    return view('admin.invoice.detail-invoice');
});
Route::get('/manage-product', function () {
    return view('admin.manage-product.index');
});
Route::get('/add-product', function () {
    return view('admin.manage-product.add-new-product');
});
Route::get('/my-account', function () {
    return view('admin.my-account-admin.index');
});

Route::get('scheduleInvoice', [InvoiceController::class, 'schedule']);

// Manage Service
Route::get('/manage-service/registrar/overview', function () {
    return view('admin.manage-services.registrar.overview');
})->name('manage-service.registrar');
Route::get('/manage-service/registrar/add', function () {
    return view('admin.manage-services.registrar.registrar-add');
})->name('manage-service.registrar.create');
Route::get('/manage-service/registrar/edit', function () {
    return view('admin.manage-services.registrar.registrar-edit');
})->name('manage-service.registrar.edit');
Route::get('/manage-service/registrar/edit/import-contact', function () {
    return view('admin.manage-services.registrar.top-menu-page.import-contact');
})->name('manage-service.registrar.edit.import-contact');
Route::get('/manage-service/registrar/edit/import-domain', function () {
    return view('admin.manage-services.registrar.top-menu-page.import-domain');
})->name('manage-service.registrar.edit.import-domain');


Route::get('/manage-service/domain-contacts/overview', function () {
    return view('admin.manage-services.domain-contacts.overview');
})->name('manage-service.domain-contacts');
Route::get('/manage-service/domain-contacts/add', function () {
    return view('admin.manage-services.domain-contacts.domain-contacts-add');
})->name('manage-service.domain-contacts.create');
Route::get('/manage-service/domain-contacts/edit', function () {
    return view('admin.manage-services.domain-contacts.domain-contacts-edit');
})->name('manage-service.domain-contacts.edit');


Route::get('/manage-service/tld/overview', function () {
    return view('admin.manage-services.tld.overview');
})->name('manage-service.tld');
Route::get('/manage-service/tld/add', function () {
    return view('admin.manage-services.tld.tld-add');
})->name('manage-service.tld.create');
Route::get('/manage-service/tld/edit', function () {
    return view('admin.manage-services.tld.tld-edit');
})->name('manage-service.tld.edit');


Route::get('/manage-service/nameserver-groups/overview', function () {
    return view('admin.manage-services.nameserver-groups.overview');
})->name('manage-service.nameserver');
Route::get('/manage-service/nameserver-groups/add', function () {
    return view('admin.manage-services.nameserver-groups.nameserver-groups-add');
})->name('manage-service.nameserver.create');
Route::get('/manage-service/nameserver-groups/edit', function () {
    return view('admin.manage-services.nameserver-groups.nameserver-groups-edit');
})->name('manage-service.nameserver.edit');


Route::get('/manage-service/server/overview', function () {
    return view('admin.manage-services.servers.overview');
})->name('manage-service.server');
Route::get('/manage-service/server/add', function () {
    return view('admin.manage-services.servers.server-add');
})->name('manage-service.server.create');
Route::get('/manage-service/server/edit', function () {
    return view('admin.manage-services.servers.server-edit');
})->name('manage-service.server.edit');
Route::get('/manage-service/server/edit/send-mail', function () {
    return view('admin.manage-services.servers.top-menu-page.send-mail');
})->name('manage-service.server.edit.send-mail');
Route::get('/manage-service/server/edit/import-account', function () {
    return view('admin.manage-services.servers.top-menu-page.import-account');
})->name('manage-service.server.edit.import-account');


Route::get('/manage-service/package/overview', function () {
    return view('admin.manage-services.packages.overview');
})->name('manage-service.package');
Route::get('/manage-service/package/add', function () {
    return view('admin.manage-services.packages.package-add');
})->name('manage-service.package.create');
Route::get('/manage-service/package/edit', function () {
    return view('admin.manage-services.packages.package-edit');
})->name('manage-service.package.edit');
