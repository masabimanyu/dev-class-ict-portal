<?php

namespace App\Services;

use App\Repositories\TransactionRepository;

class TransactionService{

    protected $transactionRepository;

    public function __construct( TransactionRepository $transactionRepository )
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function all()
    {
        return $this->transactionRepository->all();
    }

    public function transactionDetail( $transactionId )
    {
        return $this->transactionRepository->get( $transactionId );
    }

    public function clientTransactions( $clientId )
    {
        return $this->transactionRepository->transactions( $clientId );
    }

    // public function adjustTransaction( $transactions )
    // {
    //     foreach ($transactions as $transaction) {
    //         $transactionId = $transaction["TransactionId"];
    //         $this->transactionRepository->adjustPeriod( $transactionId );
    //         $this->transactionRepository->adjustNextdate( $transactionId );
    //         $this->transactionRepository->adjustTotalInvoice( $transactionId );
    //     }
    // }

    public function adjustTransaction( $transactionId, $section )
    {
        switch ($section) {
            case 'period':
                $this->transactionRepository->adjustPeriod( $transactionId );
                break;
            case 'total_invoice':
                $this->transactionRepository->adjustTotalInvoice( $transactionId );
                break;
            case 'billing_cycle':
                $this->transactionRepository->adjustNextdate( $transactionId );
                break;
        }
    }
}
