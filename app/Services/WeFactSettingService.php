<?php

namespace App\Services;

use App\Exceptions\WeFactException;
use App\Repositories\WeFactSettingRepository;
use Illuminate\Support\Facades\Log;

class WeFactSettingService
{
    protected $weFactSettingRepository;

    public function __construct(WeFactSettingRepository $weFactSettingRepository)
    {
        $this->weFactSettingRepository = $weFactSettingRepository;
    }

    public function getWeFactTax( $taxtType = 'sale' )
    {
        $taxes = $this->weFactSettingRepository->getSettingTaxCode();

        if( $taxtType == "sale" )
        {
            return $taxes["Sale"];
        }else{
            return $taxes["Purchase"];
        }

        return $taxes;
    }

    public function getWeFactLanguageCodes()
    {
        $languageCodes = $this->weFactSettingRepository->getLanguageCode();

        Log::debug(json_encode($languageCodes));

        if($languageCodes['status'] != "error")
        {
            return $languageCodes['settings']['CorporateIdentity'];
        }else{
            Log::debug("error getting setting wefact");

            return throw new WeFactException("lorem exception");
        }
    }
}
