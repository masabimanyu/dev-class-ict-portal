<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ProductService
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getProducts( $order = null, $status = null, $id = null )
    {
        $parameters = [];

        if(isset($order))
        {
            $parameters["order"] = $order;
        }

        if(isset($status))
        {
            $parameters["status"] = (int) $status;
        }

        if(isset($id))
        {
            $parameters["searchat"] = "ProductCode";
            $parameters["searchfor"] = $id;
        }

        Log::debug("trying get product with parameters ". json_encode($parameters) );

        $products = $this->productRepository->lists( $parameters );

        return $products;
    }

    public function save(Request $request   )
    {
        $validator = Validator::make($request->all(), [
            // general product validation
            "product_type" => "required",
            "product_code" => "nullable|string|max:50",
            "product_name" => "required|string|max:255",
            "invoice_description" => "nullable|string",
            "product_unit" => "nullable|string|max:50",
            "price_exclude" => "numeric|nullable",
            "tax" => "nullable|string|max:50",
            "price_per" => "nullable|string|max:5",
            "cost_price_exclude" => "nullable|numeric",
            "contract_period" => "nullable|numeric",
            "custom_price" => "required_if:contract_period,==,1",

            // product domain validation
            "registrar" => "nullable|string",
            "charge" => "nullable|string",
            "whois" => "nullable|ip",
            "no_match_check" => "nullable|string",
            "need_auth_code" => "nullable|string",

            // product hosting validation
            "webhosting" => "nullable|string",
            "hosting_server" => "nullable|string",
            "hosting_template" => "nullable|string",
            "hosting_email_template" => "nullable|string",
            "send_hosting_direct" => "nullable|string",
            "hosting_pdf_template" => "nullable|string",
        ]);

        if($validator->fails())
        {
            Log::debug($validator->errors()->toJson());
            return Redirect::back()->withErrors($validator->messages()->toJson())->withInput();
        }

        $product = $this->productRepository->store( $request);

        if( $request->contract_period == "0")
        {
            $this->productRepository->setProductCustomPrice( $request->custom_price, $product->id );
        }

        return $product;
    }

    public function lists()
    {
        $products = $this->productRepository->getAll();
        return $products;
    }

    public function products( $parameter )
    {
        $products = $this->productRepository->getProducts( $parameter );

        return $products;
    }

    public function productDetail( $productId )
    {
        $product = $this->productRepository->get( $productId );

        return $product;
    }
}
