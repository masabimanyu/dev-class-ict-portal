<?php

namespace App\Services;

use App\Repositories\SubscriptionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SubscriptionService
{
    protected $subscriptionRepository;

    protected $productService;

    public function __construct(SubscriptionRepository $subscriptionRepository, ProductService $productService)
    {
        $this->subscriptionRepository = $subscriptionRepository;
        $this->productService = $productService;
    }

    public function addSubscription( $transaction, $domainDetail )
    {

        // return $transaction->contractPeriod->contractPeriod;
        $product = $this->productService->productDetail( $transaction->product_id );
        $nextDate = Carbon::createFromFormat('Y-m-d H:i:s', $transaction->contractPeriod->contract_period_start );

        $parameters = [
            'DebtorCode' => $transaction->client_id,
            'Subscription' => [
                'Number' => 1,
                'NumberSuffix' => $product->product_unit,
                'Description' => $product->product_name,
                'PriceExcl' => $transaction->price_exclude,
                'TaxCode' => $transaction->tax_rate,
                'DiscountPercentage'=> $transaction->discount_recurring ? $transaction->discount_recurring : 0,
                'Periods' => $transaction->billing_cycle_length,
                'Periodic' => $transaction->billing_cycle_per,
                'StartDate' => Carbon::createFromFormat('Y-m-d H:i:s', $transaction->contractPeriod->contract_period_start)->format('Y-m-d'),
                // 'TerminationDate' => $transaction->billing_period_end,
                'NextDate' => $nextDate->format("Y-m-d"),
                'TerminateAfter' => $this->setTerminationDate(
                    $transaction->billing_cycle_length,
                    $transaction->billing_cycle_per,
                    $transaction->contractPeriod->contract_length,
                    $transaction->contractPeriod->contract_method,
                 ),
                'Comment' => $transaction->id
            ]
        ];

        Log::debug(" parameters add subscription ". json_encode($parameters) );

        return $this->subscriptionRepository->add( $parameters );
    }

    public function setTerminationDate( $billing_period_length, $billing_period_per, $contract_length, $contract_per)
    {
        $monthPeriod = config('constant.wefact.period_month');

        return ( $contract_length * $monthPeriod[$contract_per] ) / ($billing_period_length * $monthPeriod[$billing_period_per]);
    }

    public function show( $subscriptionCode )
    {
        $parameters = [
            "Identifier" => $subscriptionCode
        ];

        return $this->subscriptionRepository->get( $parameters );
    }
}
