<?php

namespace App\Services;

use App\Repositories\TldRepository;
use Illuminate\Http\Request;

class TldService
{
    protected $tldRepository;

    public function __construct( TldRepository $tldRepository)
    {
        $this->tldRepository = $tldRepository;
    }

    public function getAll()
    {
        $tlds = $this->tldRepository->lists();

        return $tlds;
    }

    public function getByName( Request $request )
    {
        $name = $request->query('name');

        return $name;

        if($name == "")
        {
            return false;
        }

        $tld = $this->tldRepository->getByName( $name );

        return $tld;

        // $tld = $this->tldRepository->getByName(  )
    }

}
