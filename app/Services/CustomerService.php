<?php

namespace App\Services;

use App\Repositories\CustomerRepository;
use App\Repositories\UserRepository;

class CustomerService
{
    protected $customerRepository, $userRepository;

    public function __construct(CustomerRepository $customerRepository, UserRepository $userRepository)
    {
        $this->customerRepository = $customerRepository;
        $this->userRepository = $userRepository;
    }

    public function create( $user , $request = null)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        $phoneNumber = $request ? $request->contact_phone_number : $user->userContact->phone;
        $land = $request ? $request->contact_country : $user->userContact->phone;

        $pn = $phoneUtil->parse( $phoneNumber, $land);

        $userInformationOpenProvider = [
            "address" => [
                "city" => $request ? $request->contact_city : $user->userInformation->place,
                "country" => $request ? $request->contact_country : $user->userInformation->land,
                "number" => "",
                "state" => "",
                "street" => $request ? $request->contact_address :  $user->userInformation->address,
                "suffix" => "",
                "zipcode" => $request ? $request->contact_postcode : $user->userInformation->postcode,
            ],
            "locale" => "nl_NL",
            "company_name" => $request ? $request->contact_company_name : $user->userInformation->company_name,
            "email" => $request ? $request->contact_email : $user->email,
            "name" => [
                "first_name" => $request ? $request->contact_first_name : $user->userInformation->first_name,
                "full_name" => $request ? $request->contact_first_name.' '.$request->contact_last_name : $user->userInformation->first_name.' '. $user->userInformation->last_name,
                "initials" => $request ? $request->contact_first_name : $user->userInformation->first_name,
                "last_name" => $request ? $request->contact_last_name : $user->userInformation->last_name,
            ],
            "vat" => $request ? $request->contact_vat_number : $user->userInformation->vat_number,
            "phone" => [
                "area_code"=> "97",
                "country_code" => (string) $pn->getCountryCode(),
                "subscriber_number" => $pn->getNationalNumber(),
            ]
        ];

        // if($request->client_contact_new == "1")
        // {
            // return $this->customerRepository->add( $userInformationOpenProvider );
        // }else{
            return $this->customerRepository->add( $userInformationOpenProvider );
        // }
    }
}
