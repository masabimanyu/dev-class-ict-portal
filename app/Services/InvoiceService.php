<?php

namespace App\Services;

use App\Repositories\InvoiceRepository;
use App\Repositories\WeFactSettingRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class InvoiceService
{
    protected $invoiceRepository, $weFactSettingRepository, $transactionService, $periodicMonth;

    public function __construct(InvoiceRepository $invoiceRepository, WeFactSettingRepository $weFactSettingRepository, TransactionService $transactionService)
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->weFactSettingRepository = $weFactSettingRepository;
        $this->transactionService = $transactionService;
        $this->periodicMonth = config('constant.wefact.period_month');
    }

    public function addInvoice( $debtorCode, $invoiceLine )
    {
        return $this->invoiceRepository->storeInvoice( $debtorCode, $invoiceLine );
    }

    public function list()
    {
        $invoices = $this->invoiceRepository->lists();

        return $invoices;
    }

    public function listByDebtor( $debtorCode )
    {
        $invoices = $this->invoiceRepository->lists( $debtorCode );

        return $invoices["invoices"];
    }

    public function listByStatus( $status )
    {
        $invoices = $this->invoiceRepository->lists( null, null, $status);

        return $invoices;
    }

    public function listById( $id )
    {
        $invoices = $this->invoiceRepository->lists( null, null, null, $id);

        if(isset($invoices["invoices"]))
        {
            return response()->json($invoices, 200);
        }else{
            return response()->json(["message" => "your search parameters do not match", "data" => $invoices], 400);
        }

        return $invoices;
    }

    public function detail( $invoiceCode )
    {
        $invoiceDetail = $this->invoiceRepository->invoiceDetail( $invoiceCode );
        $weFactTax = $this->weFactSettingRepository->getSettingTaxCode();

        foreach ($invoiceDetail["invoice"]["InvoiceLines"] as $key => $lineItem) {
            $invoiceDetail["invoice"]["InvoiceLines"][$key]["TaxDetail"] = $weFactTax["Sale"][$lineItem["TaxCode"]];
        }

        return $invoiceDetail;
    }

    public function calculateAvailableTax( $invoice )
    {
        $taxList = [];

        if(isset($invoice["invoice"]))
        {
            foreach ($invoice["invoice"]["InvoiceLines"] as $key => $lineItem) {
                $tax = [
                    "name" => $lineItem["TaxDetail"]["Name"],
                ];

                if(array_key_exists($lineItem["TaxCode"], $taxList))
                {
                    $tax["value"] = $taxList[$lineItem["TaxCode"]]["value"] + ((int) $lineItem["TaxPercentage"]/100) * (int) $lineItem["PriceExcl"];

                }else{
                    $tax["value"] = ((int) $lineItem["TaxPercentage"]/100) * (int) $lineItem["PriceExcl"];
                }

                $taxList[$lineItem["TaxCode"]] = $tax;
            }
        }

        return $taxList;
    }

    public function downloadInvoice( $invoiceCode )
    {
        return $this->invoiceRepository->download( $invoiceCode );
    }

    public function scheduleCron()
    {
        return $this->invoiceRepository->schedule();
    }

    public function generateInvoice( $debtorCode )
    {
        Log::debug($debtorCode);
        // Carbon::setTestNow(Carbon::create(2023,2,1));

        $invoicePeriodStart = Carbon::parse('first day of last month');
        // $invoicePeriodStart = $invoicePeriodStart->day(15)->format('Y-m-d');
        $invoicePeriodStart = $invoicePeriodStart->format('Y-m-d');
        $invoicePeriodEnd = Carbon::parse('first day of this month');
        $invoicePeriodEnd = $invoicePeriodEnd->day(14)->format('Y-m-d');

        $transactions = $this->transactionService->clientTransactions( $debtorCode );

        $invoiceLines = [];

        foreach ($transactions as $transaction) {
            $invoicedOn =  $transaction->next_date == null ? $transaction->contractPeriod->contract_period_start : $transaction->next_date;
            $invoicedOn = Carbon::parse( $invoicedOn );

            if($transaction->contract_period == 0)
            {
                $contractPeriodStart = Carbon::parse( $transaction->contractPeriod->contract_period_start );
                $contractPeriodEnd = Carbon::parse( $transaction->contractPeriod->contract_period_end );

                if( !$invoicedOn->gte( $contractPeriodStart ) && !$invoicedOn->lte( $contractPeriodEnd ) ) continue;
            }

            $invoiceLine = $this->transactionToInvoiceLine( $transaction );

            // for logging purposes only, not necessary in wefact
            $invoiceLine["TransactionId"] = $transaction->id;
            $invoiceLine["InvoiceAttempt"] = $transaction->invoice_total;

            Log::debug($invoicedOn);
            Log::debug($invoicePeriodStart);
            Log::debug($invoicePeriodEnd);
            Log::debug($transaction);

            if(
                $invoicedOn->gte($invoicePeriodStart) &&
                $invoicedOn->lte( $invoicePeriodEnd ) &&
                $transaction->is_billed == 1
            )
            {
                array_push($invoiceLines, $invoiceLine);
                Log::debug( "invoice Lines ". json_encode($invoiceLines) );

                $this->transactionService->adjustTransaction( $transaction->id , 'period' );
                $this->transactionService->adjustTransaction( $transaction->id , 'total_invoice' );
                $this->transactionService->adjustTransaction( $transaction->id , 'billing_cycle' );

                if($transaction->billing_cycle_per == 'm')
                {
                    if($transaction->next_date == null)
                    {
                        // if transaction first time billed the period must included frist period and second period

                        $doubleLineItem = $invoiceLine;
                        $doubleLineItem["Date"] = $this->nextDate( $transaction->contractPeriod->contract_period_start, (int) $invoiceLine['Periods'], $invoiceLine['Periodic'], $invoiceLine["Date"]);
                        $doubleLineItem["StartDate"] = $this->nextDate( $transaction->contractPeriod->contract_period_start, (int) $invoiceLine['Periods'], $invoiceLine['Periodic'], $invoiceLine["StartDate"]);
                        $doubleLineItem["EndDate"] = $this->nextDate( $transaction->contractPeriod->contract_period_start, (int) $invoiceLine['Periods'], $invoiceLine['Periodic'], $invoiceLine["EndDate"]);
                        array_push( $invoiceLines, $doubleLineItem );
                        Log::debug( "invoice Lines ". json_encode($invoiceLines) );

                        $this->transactionService->adjustTransaction( $transaction->id , 'period' );
                    }
                }
            }
        }

        if(count($invoiceLines) > 0)
        {
            Log::debug( "generate invoice with line item ". json_encode($invoiceLines) );

            $invoice = $this->invoiceRepository->storeInvoice( $debtorCode, $invoiceLines );

            Log::debug("invoice ". json_encode($invoice));

            return $invoice;
        }

        // if(!$invoice["status"] == "error")
        // {
        //     $this->transactionService->adjustTransaction( $invoiceLines );
        //     foreach( $invoiceLines as $invoiceLine )
        //     {
        //         $this->invoiceRepository->storeInvoiceLog( $invoice["invoice"]["InvoiceCode"], $invoiceLine["TransactionId"] );
        //     }
        // }

        return false;
    }

    public function transactionToInvoiceLine( $transaction )
    {
        $invoiceLine = [
            "Date" => $transaction->next_date == null ? Carbon::parse($transaction->contractPeriod->contract_period_start)->format('Y-m-d') : Carbon::parse($transaction->next_date)->format('Y-m-d'),
            "Number" => 1,
            "NumberSuffix" => $transaction->product->product_unit,
            "Description" => $transaction->product->product_name,
            "PriceExcl" => $this->calculatePrice($transaction),
            "DiscountPercentage" => $transaction->discount_recurring,
            "PeriodicType" => "once",
            "StartDate" => $transaction->next_date == null ?
                                Carbon::parse($transaction->contractPeriod->contract_period_start)->format('Y-m-d')
                                :
                                Carbon::parse($transaction->billing_period_start)->format('Y-m-d'),
            "EndDate" => $this->nextDate( $transaction->contractPeriod->contract_period_start, (int) $transaction->billing_cycle_length, $transaction->billing_cycle_per , ($transaction->next_date != null ? $transaction->billing_period_start : null) ),
            "Periods" => $transaction->billing_cycle_length,
            "Periodic" => $transaction->billing_cycle_per,
        ];

        return $invoiceLine;
    }

    public function nextDate($contractStart, $billingCycleLength, $billingCyclePer, $providedDate = null )
    {
        $date = ($providedDate == null || $providedDate == "null") ? $contractStart : $providedDate;

        $date = Carbon::parse($date);

        $isLastDayOfMonth = $date->format('Y-m-d') == Carbon::parse( Carbon::create($date->year, $date->month, $date->day )->lastOfMonth() )->format('Y-m-d') ? true : false;

        $daydate = $date->day;

        $futureContract = $date->setDay(1)->addMonth((int) $billingCycleLength * $this->periodicMonth[$billingCyclePer])->lastOfMonth();

        $dayFutureContract = $futureContract->day;

        if($daydate > $dayFutureContract)
        {
            $futureContract->setDay($dayFutureContract);
        }else if( $daydate < $dayFutureContract && $isLastDayOfMonth )
        {
            $futureContract->setDay($dayFutureContract);
        }
        else{
            $futureContract->setDay($daydate);
        }

        return $futureContract->format('Y-m-d');
    }

    public function calculatePrice( $transaction )
    {
        $price = 0;

        if($transaction->product->price_per != $transaction->billing_cycle_per)
        {
            $price = $transaction->billing_cycle_length * ($this->periodicMonth[$transaction->billing_cycle_per] / $this->periodicMonth[$transaction->product->price_per]) * $transaction->price_exclude;
        }else{
            $price = $transaction->billing_cycle_length * $transaction->price_exclude;
        }

        return $price;
    }
}
