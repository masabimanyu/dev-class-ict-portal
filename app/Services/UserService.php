<?php

namespace App\Services;

use App\Mail\UserWelcomeMail;
use App\Models\User;
use App\Repositories\InvoiceRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class UserService
{
    protected $userRepository;

    protected $invoiceRepository;

    public function __construct(UserRepository $userRepository, InvoiceRepository $invoiceRepository)
    {
        $this->userRepository = $userRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function store( Request $request )
    {
        Log::debug( 'UserService');
        $validator = Validator::make($request->all(),[
            'company_name' => 'required|string|max:255|nullable',
            'company_legal_form' => 'required|string|max:255|nullable',
            'chamber_of_commerce_number' => 'numeric|max:11|nullable',
            'vat_number' => 'string|nullable',
            'user_legal_form' => 'string|max:11|nullable',
            'first_name' => 'required|string|max:255',
            'last_name'=> 'required|string|max:255',
            'address' => 'required',
            'postcode' => 'required|string|max:25',
            'place'=> 'required|string',
            'land'=> 'required|string',
            'email' => 'required|unique:App\Models\User,email|email',
            'phone_number' => 'required|string|max:255',
            'mobile_number' => 'required|string|max:255',
            'fax_number' => 'required|string|max:255',
            'website' => 'nullable|string|max:255',
            'is_mailing' => 'boolean|nullable',
            'username' => 'required|string|max:255',
            'temporary_password' => 'string|max:255',
            'language' => 'nullable|string|max:25',
            'profile' => 'nullable',
            'send_welcome_page' => 'nullable',
            'alt_invoicing_and_payment' => 'nullable',
            'is_login_and_order_form' => 'nullable',
            'processing_agreement' => 'required',
            'term_and_condition' => 'required',
            'privacy_policy' => 'required',
            'bank_account_number' => 'numeric|nullable',
            'account_holder' => 'string|nullable|max:255',
            'bank' => 'string|nullable',
            'bank_city' => 'string|nullable',
            'bank_code' => 'string|nullable|max:30',
            'direct_debit' => 'string|nullable',
            'mandate_reference' => 'string|nullable',
            'mandate_sign_date' => 'date|nullable',
        ]);

        if($validator->fails())
        {
            Log::debug($validator->errors()->toJson());
            return Redirect::back()->withErrors($validator->messages()->toJson())->withInput();
        }

        Log::debug("pass validation");

        $user = $this->userRepository->save( $request );

        if($user && $user->userClientArea->send_welcome == 1 && $user->userClientArea->is_login_and_order_form == 1)
        {
            $this->sendWelcomeEmail( $user );
        }

        return $user;
    }

    public function update( Request $request, $id )
    {
        Log::debug( 'UserService update');
        $validator = Validator::make($request->all(),[
            'company_name' => 'required|string|max:255|nullable',
            'company_legal_form' => 'required|string|max:255|nullable',
            'chamber_of_commerce_number' => 'string|max:11|nullable',
            'vat_number' => 'string|nullable',
            'user_legal_form' => 'string|max:11|nullable',
            'first_name' => 'required|string|max:255',
            'last_name'=> 'required|string|max:255',
            'address' => 'required',
            'postcode' => 'required|string|max:25',
            'place'=> 'required',
            'land'=> 'required',
            'email' => 'required|email',
            'phone_number' => 'required|string|max:255',
            'mobile_number' => 'required|string|max:255',
            'fax_number' => 'required|string|max:255',
            'website' => 'nullable|string|max:255',
            'is_mailing' => 'boolean|nullable',
            'username' => 'required|string|max:255',
            'temporary_password' => 'string|max:255|nullable',
            'language' => 'nullable|string|max:25',
            'profile' => 'nullable',
            'send_welcome_page' => 'nullable',
            'alt_invoicing_and_payment' => 'required',
            'is_login_and_order_form' => 'required',
            'processing_agreement' => 'required',
            'term_and_condition' => 'required',
            'privacy_policy' => 'required',
            'bank_account_number' => 'numeric|nullable',
            'account_holder' => 'string|nullable|max:255',
            'bank' => 'string|nullable',
            'bank_city' => 'string|nullable',
            'bank_code' => 'string|nullable|max:30',
            'direct_debit' => 'string|nullable',
            'mandate_reference' => 'string|nullable',
            'mandate_sign_date' => 'date|nullable',
        ]);

        if($validator->fails())
        {
            Log::debug($validator->errors()->toJson());
            return Redirect::back()->withErrors($validator->messages()->toJson());
        }

        Log::debug("pass validation update");

        $user = $this->userRepository->update( $request, $id );

        return $user;
    }

    public function findByUsername( $username )
    {
        return $this->userRepository->findByUsername( $username );
    }

    public function list( $offset = 2, $limit = 1000, $order = 'DESC')
    {


        $parameters = [
            'offset' => $offset,
            'limit' => $limit,
            'order' =>  $order,
        ];

        $users = $this->userRepository->list( $parameters );

        if(isset($users["debtors"]))
        {
            foreach ($users["debtors"] as $key => $debtor) {

                $invoices = $this->invoiceRepository->lists($debtor["DebtorCode"]);

                $totalInvoices = 0;

                if(isset($invoices["invoices"]))
                {
                    foreach ($invoices["invoices"] as $invoice) {
                        $totalInvoices += (float) $invoice["AmountOutstanding"];
                    }
                }

                $users["debtors"][$key]["OutstandingAmount"] = $totalInvoices;

                Log::debug($debtor["DebtorCode"]);
            }
        }

        Log::debug("response from user service ". json_encode($users));

        return $users;
    }

    public function userDetail( string $debtorCode )
    {
        $userDetail = $this->userRepository->show( $debtorCode );

        return $userDetail;
    }

    public function searchUser( string $parameter )
    {
        $users = $this->userRepository->search( $parameter );

        if(isset($users['debtors']))
        {
            foreach ($users["debtors"] as $key => $debtor) {

                $invoices = $this->invoiceRepository->lists($debtor["DebtorCode"]);

                $totalInvoices = 0;

                if(isset($invoices["invoices"]))
                {
                    foreach ($invoices["invoices"] as $invoice) {
                        $totalInvoices += (float) $invoice["AmountOutstanding"];
                    }
                }

                $users["debtors"][$key]["OutstandingAmount"] = $totalInvoices;

                Log::debug($debtor["DebtorCode"]);
            }

            return response()->json($users, 200);
        }else{
            return response()->json(["message" => "your search parameters do not match", "data" => $users], 400);
        }
    }

    public function findByDebtorCode( string $debtorCode )
    {
        $user = $this->userRepository->findByDebtorCode( $debtorCode );
        return $user;
    }

    public function batchRegisterOp()
    {
        $users = User::whereNotNull('wefact_user_id')->with(['userInformation', 'userContact'])->get();

        return $this->userRepository->registerOpenProvider( $users );
    }

    public function getLegalFormList()
    {
        return $this->userRepository->legalForm();
    }

    public function getCountryList()
    {
        return $this->userRepository->country();
    }

    public function sendWelcomeEmail( $user )
    {
        $user = $this->userRepository->findByDebtorCode( $user->wefact_user_id );

        Mail::to( $user->email )->queue( new UserWelcomeMail( $user ) );
    }

    public function users()
    {
        return $this->userRepository->userDB();
    }
}
