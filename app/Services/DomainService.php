<?php

namespace App\Services;

use App\Api\WeFactAPI;
use App\Jobs\RegisterOpenProvider;
use App\Repositories\CustomerRepository;
use App\Repositories\DomainRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class DomainService
{
    protected $domainRepository, $userRepository, $customerRepository, $customerService, $transactionRepository, $subscriptionService;

    public function __construct(
        DomainRepository $domainRepository,
        UserRepository $userRepository,
        CustomerRepository $customerRepository,
        CustomerService $customerService,
        TransactionRepository $transactionRepository,
        SubscriptionService $subscriptionService
    )
    {
        $this->domainRepository = $domainRepository;
        $this->userRepository = $userRepository;
        $this->customerRepository = $customerRepository;
        $this->customerService = $customerService;
        $this->transactionRepository = $transactionRepository;
        $this->subscriptionService = $subscriptionService;
    }

    public function lists()
    {
        $domains = $this->domainRepository->list();

        return $domains['data'];
    }

    public function store( Request $request)
    {
        // return $request->all();
        Log::debug('domain service');
        $validator = Validator::make($request->all(), [
            'client_id' => "required|string",
            'is_billed' => "nullable|boolean",
            'search_domain_name' => "required|string",
            'select_domain_extension' => "required|string",
            'hosting' => "string|nullable",
            'inter_note' => "string|nullable",
            'auth' => "string|nullable",
            'status' => "required|numeric",
            'domain_registration_direct' => 'nullable|string',
            'registrar' => "nullable|string",
            'name_server_1' => 'nullable|string',
            'name_server_2' => 'nullable|string',
            'name_server_3' => 'nullable|string',
            'ip_1' => 'nullable|ip',
            'ip_2' => 'nullable|ip',
            'ip_3' => 'nullable|ip',
            'dns_template' => "string|nullable",
            'tech_contact' => "string|nullable",
            'admin_contact' => "string|nullable",
            "domain_owner" => "string|nullable",
            "search_product" => "string|nullable",
            "description" => "string|nullable",
            "price_exclude" => "string|nullable",
            "tax" => "string|nullable",
            "discount_recurring" => "numeric|nullable",
            "contract_period" => "boolean|nullable",
            "contract_length" => "required_if:contract_period,0|numeric|nullable",
            "contract_per" => "required_if:contract_period,0|string|nullable",
            "contract_date_start" => "required_if:contract_period,0|date|nullable",
            "contract_date_end" => "required_if:contract_period,0|date|nullable",
            "subscription" => "numeric|nullable",
            "billing_cycle_length" => "nullable|numeric",
            "billing_cycle_per" => "nullable|string",
            "billing_period_start" => "nullable|date",
            "billing_period_end" => "nullable|date",
            "bill_recurring"=> "nullable|boolean"
        ]);

        if($validator->fails())
        {
            Log::debug("error validate". $validator->errors()->toJson());
            return $validator->errors();
            return Redirect::back()->withErrors($validator->messages()->toJson());
        }

        Log::debug("pass domain service validation");

        $currentOpenProviderId = '';

        if($request->domain_owner == "1")
        {
            $client = $this->userRepository->findByDebtorCode( $request->client_id );
            if($client->open_provider_id != null && $client->open_provider_id != "")
            {
                $currentOpenProviderId = $client->open_provider_id;
            }else{
                $newCustomer = $this->customerService->create( $client );

                if($newCustomer)
                {
                    $currentOpenProviderId = $newCustomer["data"]["handle"];
                    $client->open_provider_id = $newCustomer["data"]["handle"];
                    $client->save();
                }
            }
        }

        // $parameters = [
        //     "admin_handle" => $request->admin_contact == "1" ? $currentOpenProviderId : $request->admin_contact,
        //     "billing_handle" => $request->domain_owner == "1" ? $currentOpenProviderId : $request->domain_owner,
        //     "tech_handle" => $request->tech_contact == "1" ? $currentOpenProviderId : $request->tech_contact,
        //     "owner_handle" => $request->domain_owner == "1" ? $currentOpenProviderId : $request->domain_owner,
        //     "ns_template_id" => $request->dns_template,
        //     "domain" => [
        //         "extension" => $request->select_domain_extension,
        //         "name" => $request->search_domain_name,
        //     ],
        //     "period" => 1,
        // ];

        $parameters = [
            "admin_handle" => $request->admin_contact,
            "billing_handle" => $request->domain_owner,
            "tech_handle" => $request->tech_contact,
            "owner_handle" => $request->domain_owner,
            "ns_template_id" => $request->dns_template,
            "domain" => [
                "extension" => $request->select_domain_extension,
                "name" => $request->search_domain_name,
            ],
            "period" => 1,
        ];

        $domainContacts = [
            "admin_handle" => $request->admin_contact == "1" ? $currentOpenProviderId : $request->admin_contact,
            "billing_handle" => $request->domain_owner == "1" ? $currentOpenProviderId : $request->domain_owner,
            "tech_handle" => $request->tech_contact == "1" ? $currentOpenProviderId : $request->tech_contact,
            "owner_handle" => $request->domain_owner == "1" ? $currentOpenProviderId : $request->domain_owner,
        ];

        $transaction = $this->transactionRepository->storeTransaction( $request );

        $this->transactionRepository
            ->storeTransactionContractPeriod( $request, $transaction->id );


        $domainDB = $this->domainRepository
                        ->store(
                            $request,
                            $parameters,
                            $transaction->id,
                            $domainContacts
                        );

        if($request->status == "1" && $request->domain_registration_direct != "on")
        {
            Log::debug("dispatch job make domain");
            RegisterOpenProvider::dispatch( $domainDB )->delay(now()->addSeconds(10));
        }

        if($request->domain_registration_direct == "on")
        {
            Log::debug("directly register domain here");
        }

        // if($transaction->billing_cycle_per != 'empty')
        // {
        //     $this->subscriptionService->addSubscription( $transaction, $domainDB );
        // }

        return Redirect::back()->with('success', 'success add domain');
    }


    public function status( $status )
    {
        $domains = $this->domainRepository->filterStatus( $status );

        return $domains;
    }

    public function detail( $domainId )
    {
        $domain = $this->domainRepository->detail( $domainId );

        return json_decode($domain);
    }

    public function getContactDetail($contacts)
    {
        $results = [];
        foreach ($contacts as $key => $contact) {
            $customer = $this->domainRepository->contact( $contact );

            $results[$key] = json_decode($customer)->data;
        }

        return $results;
    }

    public function getAuthCode( $domainId )
    {
        return $this->domainRepository->authCode( $domainId );
    }

    public function domainCheck( $name , $extension )
    {
        $request = [
            'additional_data' => [
                "idn_script" => "cyrl"
            ],
            'application_mode' => "preregistration",
            'domains' => [
                [
                    "extension" => $extension,
                    "name" => $name,
                ]
            ],
            'with_price' => false
        ];

        return $this->domainRepository->check( $request );
    }

    public function getDnsTemplates()
    {
        return $this->domainRepository->dnsTemplate();
    }

    public function getDnsTemplate( $dnsTemplateId )
    {
        return $this->domainRepository->showDnsTemplate( $dnsTemplateId );
    }
}
