<?php

namespace App\Services;

use App\Jobs\RegisterPlesk;
use App\Repositories\HostingRepository;
use App\Repositories\TransactionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class HostingService
{
    protected $hostingRepository, $transactionRepository, $subscriptionService, $userService;

    public function __construct(
        HostingRepository $hostingRepository,
        TransactionRepository $transactionRepository,
        SubscriptionService $subscriptionService,
        UserService $userService
    )
    {
        $this->hostingRepository = $hostingRepository;
        $this->transactionRepository = $transactionRepository;
        $this->subscriptionService = $subscriptionService;
        $this->userService = $userService;
    }

    public function save( Request $request )
    {
        $validator = Validator::make($request->all(), [
            'client_id' => "required|string",
            'is_billed' => "nullable|boolean",
            'domain_name' => "string|nullable",
            'direct_create_domain' => "string|nullable",
            'random_password' => "string|nullable",
            'hosting_account_name' => "string|nullable",
            'status' => "string|nullable",
            'direct_create_hosting' => "string|nullable",
            'hosting_server' => "numeric|nullable",
            'hosting_package' => "numeric|nullable",
            "search_product" => "string|nullable",
            "description" => "string|nullable",
            "price_exclude" => "string|nullable",
            'internal_note' => "string|nullable",
            "tax" => "string|nullable",
            "discount_recurring" => "numeric|nullable",
            "billing_cycle_length" => "nullable|numeric",
            "billing_cycle_per" => "nullable|string",
            "billing_period_start" => "nullable|date",
            "billing_period_end" => "nullable|date",
            "bill_recurring"=> "nullable|boolean",
            "contract_period" => "boolean|nullable",
            "contract_length" => "required_if:contract_period,0|numeric|nullable",
            "contract_per" => "required_if:contract_period,0|string|nullable",
            "contract_date_start" => "required_if:contract_period,0|date|nullable",
            "contract_date_end" => "required_if:contract_period,0|date|nullable",
            "client_send_notification" => "numeric|nullable",
            "bill_direct_debit" => "boolean|nullable",
            "subscription" => "numeric|nullable",
        ]);

        if($validator->fails())
        {
            Log::debug("fail hosting store validation");
            Log::debug("error validation : ". json_encode($validator->errors()));
            return Redirect::back()->withErrors( $validator->errors()->toJson() )->withInput();
        }

        Log::debug("pass hosting store validation");

        $transaction = $this->transactionRepository->storeTransaction( $request );

        $this->transactionRepository
                ->storeTransactionContractPeriod( $request, $transaction->id );

        $hostingDB = $this->hostingRepository->store( $request, $transaction);

        if($request->status == "1" && $request->domain_registration_direct != "on")
        {
            $user = $this->userService->findByDebtorCode( $request->client_id );
            Log::debug("dispatch job make hosting");
            RegisterPlesk::dispatch( $hostingDB, $user->email )->delay( now()->addSeconds(10) );
        }

        if($request->direct_create_hosting == "on")
        {
            Log::debug("directly register hosting here");
        }

        // if($transaction->billing_cycle_per != 'empty')
        // {
        //     $this->subscriptionService->addSubscription( $transaction, $hostingDB );
        // }

        return Redirect::back()->with('success', "success add hosting");
    }
}
