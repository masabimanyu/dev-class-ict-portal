<?php

namespace App\Exceptions;

use Exception;

class WeFactException extends Exception
{
    protected $message;

    public function __construct( $message )
    {
        $this->message = $message;
    }

    public function render()
    {
        return $this->message;
    }
}
