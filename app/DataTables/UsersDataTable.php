<?php

namespace App\DataTables;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\CollectionDataTable;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{

    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\CollectionDataTable
     */
    public function dataTable(): CollectionDataTable
    {
        $users = $this->userService->list(1, 1000);
        $debtors = [];

        if($users['debtors'])
        {
            $debtors = collect($users['debtors']);
        }

        return DataTables::collection( $debtors )
            ->editColumn('Name', function( $debtor ){
                return $debtor['CompanyName'];
            })
            ->editColumn('Email', function( $debtor ){
                return $debtor['EmailAddress'];
            })
            ->editColumn('Outstanding Amount', function( $debtor ){
                return $debtor['OutstandingAmount'];
            })
            ->editColumn('Group', function( $debtor ){
                return '-';
            });
    }

    // /**
    //  * Get query source of dataTable.
    //  *
    //  * @param \App\Models\User $model
    //  * @return \Illuminate\Database\Eloquent\Builder
    //  */
    // public function query(User $model): QueryBuilder
    // {
    //     return $model->newQuery();
    // }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            // Column::computed('action')
            //       ->exportable(false)
            //       ->printable(false)
            //       ->width(60)
            //       ->addClass('text-center'),
            // Column::make('id'),
            Column::make('Name'),
            Column::make('Email'),
            Column::make('Group'),
            Column::make('Outstanding Amount'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Users_' . date('YmdHis');
    }
}
