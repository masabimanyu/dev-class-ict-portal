<?php

namespace App\Api;

use Openprovider\Api\Rest\Client\Auth\Model\AuthLoginRequest;
use Openprovider\Api\Rest\Client\Base\Configuration;
use Openprovider\Api\Rest\Client\Client;
use GuzzleHttp\Client as HttpClient;

class OpenProvider
{
    public function createClient()
    {
        $httpClient = new HttpClient();

        // Create new configuration.
        $configuration = new Configuration();

        $configuration->setHost("http://api.sandbox.openprovider.nl:8480");

        // Build api client for using created client & configuration.
        $client = new Client($httpClient, $configuration);

        // Our credentials;
        $username = 'anggit@madeindonesia.com';
        $password = 'Masdazaii7';

        // Retrieve token for further using.
        $loginResult = $client->getAuthModule()->getAuthApi()->login(
            new AuthLoginRequest([
                'username' => $username,
                'password' => $password,
            ])
        );

        $configuration->setAccessToken($loginResult->getData()->getToken());

        return $client;
    }
}
