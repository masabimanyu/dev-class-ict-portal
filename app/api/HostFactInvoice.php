<?php

namespace App\Api;

use App\Api\HostFactAPI;

/**
 * end function hostfact
 * */


class HostFactInvoice
{
	private $from;
	private $to;
	private $api;
	private $status_invoice;

	/**
	 * inisiasi private variable
	 * */
	public function __construct($from, $to, $status_invoice = 0)
	{
		$this->from 			= $from;
		$this->to 				= $to;
		$this->status_invoice 	= $status_invoice;
		$this->api 				= new HostFactAPI();
	}


	/**
	 * invoice status
	 * */
	public function invoiceStatus()
	{
		$status = [
			'0'	=> 'Draft invoice',
			'1'	=> 'Queue invoice',
			'2'	=> 'Sent',
			'3'	=> 'Paid in part',
			'4'	=> 'Paid',
			'8'	=> 'Credit invoice',
			'9'	=> 'Cancelled',
		];

		return $status;
	}

	/**
	 * get detail invoice
	 * */
	private function setStatusInvoices($data_invoice)
	{
		if(! isset($data_invoice['invoices']) ) return $data_invoice;

		$statusName = $this->invoiceStatus();
		$invoices 	= $data_invoice['invoices'];
		$result		= [];

		$idx = 0;
		foreach ($invoices as $key => $data) {

			$statusIndex = $data['Status'];

			if( $statusIndex != $this->status_invoice) continue;

			$result[$idx] = $data;
			$result[$idx]['StatusName'] = $statusName[$statusIndex];

			$idx++;
		}

		$data_invoice['invoices'] = $result;
		$data_invoice['filterInvoiceStatus'] = $this->status_invoice;
		$data_invoice['filterInvoiceName'] = $statusName[$this->status_invoice];
		$data_invoice['totalAfterFilter'] = count($result);

		return $data_invoice;
	}


	/**
	 * get data invoice
	 * */
	public function getInvoice()
	{
		$param = [
			'searchat' => 'Date',
			'created' => [
				'from' 	=> $this->from, // filter_date
				'to' 	=> $this->to,
			]
		];

		$respone = $this->api->sendRequest('invoice', 'list', $param);
		$respone = $this->setStatusInvoices($respone);

		return $respone;
	}

	/**
	 * get detail invoice group by
	 * */
	public function getDetailInvoiceGroupBy($group_by = 'DebtorCode')
	{
		$data_invoice = $this->getInvoice();
		if(! isset($data_invoice['invoices']) ) return $data_invoice;

		$result 	= [];
		$invoices 	= $data_invoice['invoices'];
		foreach($invoices as $key => $data){

			$param = array(
				'InvoiceCode'	=> $data['InvoiceCode']
			);

			$response = $this->api->sendRequest('invoice', 'show', $param);
			$DebtorCode = $response['invoice'][$group_by];

			$result[$DebtorCode][] = $response;
		}

		return $result;
	}
}

// $invoices = new DataInvoices('2022-01-01', '2022-12-30', 0);
// $data = $invoices->getDetailInvoiceGroupBy();

// print_r($data);
