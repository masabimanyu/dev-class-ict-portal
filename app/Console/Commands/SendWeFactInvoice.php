<?php

namespace App\Console\Commands;

use App\Api\HostFactInvoice;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendWeFactInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wefact:invoice:push';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::debug("send invoice");
        $start = new Carbon('first day of last month');
        $start=  $start->startOfMonth()->format("Y-m-d");
        $end = new Carbon('last day of last month');
        $end = $end->endOfMonth()->format("Y-m-d");

        $invoiceUser = new HostFactInvoice('2022-01-01', '2022-12-30', 0);
        $invoiceUser = $invoiceUser->getDetailInvoiceGroupBy();

        foreach ($invoiceUser as $debtor => $invoices) {
            $payload = [
                "DebtorCode" => $debtor,
                "InvoiceLines" => []
            ];

            $productLine = [];

            foreach ($invoices as $invoice ) {

                foreach ($invoice["invoice"]["InvoiceLines"] as $key => $InvoiceLine) {
                    $productLine["Date"] = $InvoiceLine["Date"];
                    $productLine["Number"] = $InvoiceLine["Number"];
                    $productLine["NumberSuffix"] = $InvoiceLine["NumberSuffix"];
                    $productLine["Description"] = $InvoiceLine["Description"];
                    $productLine["PriceExcl"] = $InvoiceLine["PriceExcl"];
                    $productLine["DiscountPercentage"] = $InvoiceLine["DiscountPercentage"];
                    $productLine["DiscountPercentageType"] = $InvoiceLine["DiscountPercentageType"];
                    $productLine["DiscountPercentage"] = $InvoiceLine["DiscountPercentage"];
                    $productLine["StartDate"] = $InvoiceLine["StartPeriod"];
                    $productLine["EndDate"] = $InvoiceLine["EndPeriod"];

                    array_push($payload["InvoiceLines"], $productLine);
                }

            }

            Log::debug(json_encode($productLine));

            if($debtor == "DB10074")
            {
                $newInvoice = $this->weFactApi->sendRequest( 'invoice', 'add', $payload );

                Log::debug( json_encode($newInvoice) );
            }
        }

        return Command::SUCCESS;
    }
}
