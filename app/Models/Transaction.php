<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $table = 'transaction';

    public $timestamps = true;

    protected $appends = ['period'];

    protected $fillable = [
        'client_id',
        'transaction_code',
        'is_billed',
        'internal_note',
        'product_id',
        'description',
        'status',
        'price_exclude',
        'tax_rate',
        'discount_recurring',
        'contract_period',
        'billing_cycle_length',
        'billing_cycle_per',
        'bill_recurring',
        'billing_period_start',
        'billing_period_end',
        'invoice_total',
        'notification_new_billing',
        'bill_direct_debit',
        'next_date',
    ];

    public function contractPeriod()
    {
        return $this->hasOne( TransactionContractPeriod::class, 'transaction_id', 'id' );
    }

    public function product()
    {
        return $this->belongsTo( Product::class, 'product_id', 'id' );
    }

    public function scopeClient( $query, $clientId )
    {
        return $query->where('client_id', $clientId);
    }

    protected function period() : Attribute
    {
        return new Attribute(
            get: fn () => config('constant.wefact.periodic')[$this->billing_cycle_per]
        );
    }
}
