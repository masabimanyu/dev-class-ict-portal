<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    protected $table = 'user_contact';

    public $timestamps = false;

    protected $fillable = [
        'email',
        'phone',
        'mobile',
        'fax_number',
        'website',
        'is_mailing',
    ];
}
