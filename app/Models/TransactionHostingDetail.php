<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionHostingDetail extends Model
{
    use HasFactory;

    protected $table = 'transaction_hosting_detail';

    public $timestamps = true;

    protected $fillable = [
        'transaction_id',
        'hosting_id',
        'domain_name',
        'direct_create_domain',
        'account_password',
        'account_name',
        'status',
        'create_account_direct',
        'server',
        'package'
    ];
}
