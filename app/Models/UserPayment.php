<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model
{
    use HasFactory;

    protected $table = 'user_payment';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'bank_account_number',
        'account_holder',
        'bank',
        'bank_city',
        'bank_code',
        'direct_debit',
        'mandate_reference',
        'mandate_sign_date',
    ];
}
