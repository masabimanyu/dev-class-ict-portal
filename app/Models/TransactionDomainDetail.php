<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDomainDetail extends Model
{
    use HasFactory;

    protected $table = 'transaction_domain_detail';

    public $timestamps = true;

    protected $fillable = [
        'transaction_id',
        'domain_name',
        'domain_extension',
        'hosting',
        'status',
        'is_registration_direct',
        'registrar',
        'server_name_1',
        'server_name_2',
        'server_name_3',
        'ip_address_1',
        'ip_address_2',
        'ip_address_3',
        'domain_owner',
        'admin_contact',
        'tech_contact',
    ];
}
