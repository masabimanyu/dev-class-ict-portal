<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'product';

    public $timestamps = true;

    protected $appends = ['period'];

    protected $fillable = [
        'wefact_product_id',
        'product_code',
        'product_type',
        'product_name',
        'invoice_description',
        'product_unit',
        'price_exclude',
        'cost_price_exclude',
        'tax',
        'price_per',
        'contract_period',
    ];

    protected function period() : Attribute
    {
        return new Attribute(
            get: fn () => config('constant.wefact.periodic')[$this->price_per]
        );
    }
}
