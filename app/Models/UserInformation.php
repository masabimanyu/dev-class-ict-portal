<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    use HasFactory;

    protected $table = 'user_information';

    public $timestamps = false;

    protected $fillable = [
        'company_name',
        'company_legal_form',
        'chamber_of_commerce',
        'vat_number',
        'user_legal_form',
        'first_name',
        'last_name',
        'address',
        'postcode',
        'place',
        'land'
    ];
}
