<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserClientArea extends Model
{
    use HasFactory;

    protected $table = 'user_client_area';

    public $timestamps = false;

    protected $hidden = ['temporary_password'];

    protected $fillable = [
        'username',
        'temporary_password',
        'language',
        'profile',
        'send_welcome',
        'alt_invoicing_and_payment',
        'is_login_and_order_form'
    ];
}
