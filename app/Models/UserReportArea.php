<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserReportArea extends Model
{
    use HasFactory;

    protected $table = 'user_report_area';

    public $timestamps = false;

    protected $fillable = [
        'processing_agreement',
        'term_and_condition',
        'privacy_policy',
    ];
}
