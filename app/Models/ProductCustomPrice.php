<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCustomPrice extends Model
{
    use HasFactory;

    protected $table = 'product_custom_price';

    public $timestamps = false;

    protected $fillable = [
        'product_id',
        'period',
        'periodic',
        'price_exclude',

    ];

    protected function periodic() : Attribute
    {
        return Attribute::make(
            get: fn ($value) => config('constant.wefact.periodic')[$value]
        );
    }
}
