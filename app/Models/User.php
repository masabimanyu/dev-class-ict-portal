<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'wefact_user_id',
        'open_provider_id',
        'role_id',
        'username'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userClientArea()
    {
        return $this->hasOne(UserClientArea::class, 'user_id', 'id');
    }

    public function userContact()
    {
        return $this->hasOne(UserContact::class, 'user_id', 'id');
    }

    public function userInformation()
    {
        return $this->hasOne(UserInformation::class, 'user_id', 'id');
    }

    public function userReportArea()
    {
        return $this->hasOne(UserReportArea::class, 'user_id', 'id');
    }

    public function userRole()
    {
        return $this->hasOne(UserRole::class, 'user_id', 'id');
    }

    public function userPayment()
    {
        return $this->hasOne(UserPayment::class, 'user_id', 'id');
    }

    public function scopeUsername( $query, $username )
    {
        return $query->where('username', $username);
    }

    public function scopeWeFactUser( $query, $debtorCode)
    {
        return $query->where('wefact_user_id', $debtorCode);
    }

    public function transaction()
    {
        return $this->hasMany( Transaction::class, 'client_id', 'wefact_user_id');
    }
}
