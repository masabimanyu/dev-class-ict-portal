<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LegalForm extends Model
{
    use HasFactory;

    protected $table = 'legal_form';

    public $timestamps = false;

    protected $fillable = [
        'legal_form_code',
        'legal_form_name',
    ];
}
