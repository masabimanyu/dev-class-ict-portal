<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DomainTransaction extends Model
{
    use HasFactory;

    protected $table = 'domain_transaction';

    public $timestamps = false;

    protected $fillable = [
        'is_billed',
        'internal_note',
        'product_code',
        'description',
        'price_exclude',
        'tax_rate',
        'discount_recurring',
        'contract_period',
        'payload',
        'response',
    ];
}
