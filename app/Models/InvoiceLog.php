<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceLog extends Model
{
    use HasFactory;

    protected $table = 'invoice_log';

    public $timestamps = true;

    protected $fillable = [
        'transaction_id',
        'invoice_id',
        'invoice_attempt',
        'invoice_period_start',
        'invoice_period_end',
    ];

    public function transaction()
    {
        return $this->belongsTo( Transaction::class, 'transaction_id', 'id' );
    }
}
