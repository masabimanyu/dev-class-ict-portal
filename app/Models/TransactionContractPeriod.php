<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionContractPeriod extends Model
{
    use HasFactory;

    protected $table = 'transaction_contract_period';

    public $timestamps = true;

    protected $fillable = [
        'transaction_id',
        'contract_length',
        'contract_method',
        'contract_period_start',
        'contract_period_end',
    ];
}
