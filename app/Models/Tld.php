<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tld extends Model
{
    use HasFactory;

    protected $table = 'tld';

    public $timestamps = true;

    protected $fillable = [
        'tld',
        'registrar',
        'auth_code_transfer',
        'charge_cost',
        'public_whois',
        'number_match_check',
        'idn_support',
        'idn_support_character',
    ];

    public function scopeName( $query, $name)
    {
        return $query->where('name', '=', $name);
    }
}
