<?php

namespace App\Repositories;

use App\Api\OpenProvider;

class CustomerRepository
{

    public function get( $customerId )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $customer = $client->getPersonModule()->getCustomerApi()->getCustomer( $customerId );

        return $customer;
    }

    public function add( $parameters )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $customer = $client->getPersonModule()->getCustomerApi()->createCustomer( $parameters );

        return $customer;
    }

    public function update( $contactId, $parameters )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $customer = $client->getPersonModule()->getCustomerApi()->updateCustomer( $contactId, $parameters );

        return $customer;
    }
}
