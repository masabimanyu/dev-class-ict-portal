<?php

namespace App\Repositories;

use App\Models\TransactionHostingDetail;
use Illuminate\Http\Request;

class HostingRepository
{
    protected $transactionHostingDetail;

    public function __construct(TransactionHostingDetail $transactionHostingDetail)
    {
        $this->transactionHostingDetail = $transactionHostingDetail;
    }

    public function store( Request $request, $transaction)
    {
        $transactionHostingDetail = new $this->transactionHostingDetail;
        $transactionHostingDetail->transaction_id = $transaction->id;
        $transactionHostingDetail->domain_name = $request->domain_name;
        $transactionHostingDetail->direct_create_domain = $request->direct_create_domain == "on" ? 1 : 0;
        $transactionHostingDetail->account_password = $request->random_password;
        $transactionHostingDetail->account_name = $request->hosting_account_name;
        $transactionHostingDetail->status = $request->status;
        $transactionHostingDetail->create_account_direct = $request->direct_create_hosting == "on" ? 1 : 0;
        $transactionHostingDetail->server = $request->hosting_server;
        $transactionHostingDetail->package = $request->hosting_package;
        $transactionHostingDetail->save();

        return $transactionHostingDetail;
    }
}
