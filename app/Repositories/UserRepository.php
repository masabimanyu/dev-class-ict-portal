<?php

namespace App\Repositories;

use App\Api\OpenProvider;
use App\Api\WeFactAPI;
use App\Models\Country;
use App\Models\LegalForm;
use App\Models\User;
use App\Models\UserClientArea;
use App\Models\UserContact;
use App\Models\UserInformation;
use App\Models\UserPayment;
use App\Models\UserReportArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Propaganistas\LaravelPhone\PhoneNumber;


class UserRepository
{
    protected $user, $legalForm, $country;

    public function __construct(User $user, LegalForm $legalForm, Country $country )
    {
        $this->user = $user;
        $this->legalForm = $legalForm;
        $this->country = $country;
    }

    /**
     * save
     *
     * @param  mixed $request
     * @return User
     */
    public function save(Request $request)
    {
        Log::debug('userrepository save');
        $user = new $this->user;
        $user->name = $request->username ;
        $user->email = $request->email;
        $user->password = bcrypt($request->temporary_password);
        $user->username = $request->username;
        $user->save();

        $userInformation = new UserInformation();
        $userInformation->user_id = $user->id;
        $userInformation->company_name = $request->company_name;
        $userInformation->company_legal_form = $request->company_legal_form;
        $userInformation->chamber_of_commerce = $request->chamber_of_commerce;
        $userInformation->vat_number = $request->vat_number;
        $userInformation->user_legal_form = $request->user_legal_form;
        $userInformation->first_name = $request->first_name;
        $userInformation->last_name = $request->last_name;
        $userInformation->address = $request->address;
        $userInformation->postcode = $request->postcode;
        $userInformation->place = $request->place;
        $userInformation->land = $request->land;
        $userInformation->save();

        $userContact = new UserContact();
        $userContact->user_id = $user->id;
        $userContact->email = $request->email;
        $userContact->phone = $request->phone_number;
        $userContact->mobile = $request->mobile_number;
        $userContact->website = $request->website;
        $userContact->is_mailing = $request->receive_mailing;
        $userContact->save();

        $userClientArea = new UserClientArea();
        $userClientArea->user_id = $user->id;
        $userClientArea->username = $request->username;
        $userClientArea->temporary_password = $request->temporary_password;
        $userClientArea->language = $request->language;
        $userClientArea->profile =  $request->profile == "true" ? 1 : 0;
        $userClientArea->send_welcome = $request->send_welcome_page == "true" ? 1 : 0;
        $userClientArea->alt_invoicing_and_payment = $request->alt_invoicing_and_payment == 'on' ? 1 : 0;
        $userClientArea->is_login_and_order_form = $request->is_login_and_order_form == 'on' ? 1 : 0;
        $userClientArea->save();

        $userReportArea = new UserReportArea();
        $userReportArea->user_id = $user->id;
        $userReportArea->processing_agreement = $request->processing_agreement;
        $userReportArea->term_and_condition = $request->term_and_condition;
        $userReportArea->privacy_policy = $request->privacy_policy;
        $userReportArea->save();

        $userPayment = new UserPayment;
        $userPayment->user_id = $user->id;
        $userPayment->bank_account_number = $request->bank_account_number;
        $userPayment->account_holder = $request->account_holder;
        $userPayment->bank = $request->bank;
        $userPayment->bank_city = $request->bank_city;
        $userPayment->bank_code = $request->bank_code;
        $userPayment->direct_debit = $request->direct_debit == "on" ? 1 : 0;
        $userPayment->mandate_reference = $request->mandate_reference;
        $userPayment->mandate_sign_date = $request->mandate_sign_date;
        $userPayment->save();

        $userInformationWeFact = [
            'CompanyName' => $request->company_name,
            'CompanyNumber' => $request->chamber_of_commerce,
            'TaxNumber' => $request->vat_number,
            'initials' => $request->first_name,
            'SurName' => $request->last_name,
            'Address' => $request->address,
            'ZipCode' => $request->postcode,
            'City' => $request->place,
            'Country' => $request->land,
            'EmailAddress' => $request->email,
            'PhoneNumber' => $request->phone_number,
            'MobileNumber' => $request->mobile_number,
            'FaxNumber' => $request->fax_number,
            'mailing' => $request->is_mailing == "1" ? 'yes' : 'no',
            'MandateID' => $request->mandate_reference,
            'MandateDate' => $request->mandate_sign_date,
            'AccountNumber' => $request->bank_account_number,
            'AccountBank' => $request->bank,
            'AccountCity' => $request->bank_city,
            'AccountBIC' => $request->bank_code,
            'AccountName' => $request->account_holder,
            'InvoiceAuthorisation' => $request->direct_debit == "on" ? "yes" : "no"
        ];

        Log::debug("trying creating user with data : ". json_encode($userInformationWeFact));

        $api = new WeFactAPI();
        $weFactUser = $api->sendRequest('debtor', 'add' , $userInformationWeFact);

        Log::debug("Add Debtor : ". json_encode($weFactUser));

        $user = User::where('id', $user->id)->first();
        $user->wefact_user_id = $weFactUser['debtor']['DebtorCode'];

        // $userInformationOpenProvider = [
        //     "address" => [
        //         "city" => $request->place,
        //         "country" => $request->country,
        //         "number" => null,
        //         "state" => null,
        //         "street" => null ,
        //         "suffix" => null,
        //         "zipcode" => $request->postcode,
        //     ],
        //     "company_name" => $request->company_name,
        //     "email" => $request->email,
        //     "name" => [
        //         "first_name" => $request->first_name,
        //         "full_name" => $request->first_name.' '. $request->last_name,
        //         "initials" => $request->first_name,
        //         "last_name" => $request->last_name,
        //     ],
        //     "vat" => $request->vat_number,
        //     "phone" => $request->phone_number,
        // ];
        $user->save();

        return $user;
    }

    public function update( Request $request, $id)
    {
        Log::debug('userrepository update');

        $user = $this->user::weFactUser( $id )->first();
        $user->name = $request->username;
        $user->email = $request->email;

        if($request->temporary_password)
        {
            $user->password = bcrypt($request->temporary_password);
        }

        $user->username = $request->username;
        $user->save();

        $userInformation = UserInformation::where('user_id', $user->id)->first();
        $userInformation->company_name = $request->company_name;
        $userInformation->company_legal_form = $request->company_legal_form;
        $userInformation->chamber_of_commerce = $request->chamber_of_commerce;
        $userInformation->vat_number = $request->vat_number;
        $userInformation->user_legal_form = $request->user_legal_form;
        $userInformation->first_name = $request->first_name;
        $userInformation->last_name = $request->last_name;
        $userInformation->address = $request->address;
        $userInformation->postcode = $request->postcode;
        $userInformation->place = $request->place;
        $userInformation->land = $request->land;
        $userInformation->save();

        $userContact = UserContact::where('user_id', $user->id)->first();
        $userContact->email = $request->email;
        $userContact->phone = $request->phone_number;
        $userContact->mobile = $request->mobile_number;
        $userContact->website = $request->website;
        $userContact->is_mailing = $request->receive_mailing;
        $userContact->save();

        $userClientArea = UserClientArea::where('user_id', $user->id)->first();
        $userClientArea->username = $request->username;
        $userClientArea->temporary_password = $request->temporary_password;
        $userClientArea->language = $request->language;
        $userClientArea->profile =  $request->profile == "true" ? 1 : 0;
        $userClientArea->send_welcome = $request->send_welcome_page = "true" ? 1 : 0;
        $userClientArea->alt_invoicing_and_payment = $request->alt_invoicing_and_payment == 'on' ? 1 : 0;
        $userClientArea->is_login_and_order_form = $request->is_login_and_order_form == 'on' ? 1 : 0;
        $userClientArea->save();

        $userReportArea = UserReportArea::where('user_id', $user->id)->first();
        $userReportArea->processing_agreement = $request->processing_agreement;
        $userReportArea->term_and_condition = $request->term_and_condition;
        $userReportArea->privacy_policy = $request->privacy_policy;
        $userReportArea->save();

        $userPayment = UserPayment::where('user_id', $user->id)->first();
        $userPayment->user_id = $user->id;
        $userPayment->bank_account_number = $request->bank_account_number;
        $userPayment->account_holder = $request->account_holder;
        $userPayment->bank = $request->bank;
        $userPayment->bank_city = $request->bank_city;
        $userPayment->bank_code = $request->bank_code;
        $userPayment->direct_debit = $request->direct_debit == "on" ? 1 : 0;
        $userPayment->mandate_reference = $request->mandate_reference;
        $userPayment->mandate_sign_date = $request->mandate_sign_date;
        $userPayment->save();

        $userInformationWeFact = [
            "DebtorCode" => $id,
            'CompanyName' => $request->company_name,
            'CompanyNumber' => $request->chamber_of_commerce,
            'TaxNumber' => $request->vat_number,
            'initials' => $request->first_name,
            'SurName' => $request->last_name,
            'Address' => $request->address,
            'ZipCode' => $request->postcode,
            'City' => $request->place,
            'Country' => $request->land,
            'EmailAddress' => $request->email,
            'PhoneNumber' => $request->phone_number,
            'MobileNumber' => $request->mobile_number,
            'FaxNumber' => $request->fax_number,
            'Mailing' =>  $request->receive_mailing == "1" ? 'yes' : 'no',
            'MandateID' => $request->mandate_reference,
            'MandateDate' => $request->mandate_sign_date,
            'AccountNumber' => $request->bank_account_number,
            'AccountBank' => $request->bank,
            'AccountCity' => $request->bank_city,
            'AccountBIC' => $request->bank_code,
            'AccountName' => $request->account_holder,
            'InvoiceAuthorisation' => $request->direct_debit == "on" ? "yes" : "no"
        ];

        Log::debug("trying edit user with data : ". json_encode($userInformationWeFact));

        $api = new WeFactAPI();
        $weFactUser = $api->sendRequest('debtor', 'edit' , $userInformationWeFact);
        Log::debug("Edit Debtor : ". json_encode($weFactUser));

        $user = User::where('id', $user->id)->first();
        $user->save();

        return $user;
    }

    public function list( $parameters )
    {
        Log::debug('start get users data wefact');

        $api = new WeFactAPI();
        $users = $api->sendRequest('debtor', 'list', $parameters);

        Log::debug("get users from wefact : ". json_encode($users) );

        foreach ($users['debtors'] as $key => $user) {
            $users['debtors'][$key] = $this->show( $user['DebtorCode'] );
        }

        return $users;
    }

    public function findByUsername( $request )
    {
        $username = $request['username'];
        Log::debug('geting username using parameters'. $username);
        $user = $this->user;

        $username = $user->username( $username )->first();

        return $username;
    }

    public function findByDebtorCode( $debtorCode )
    {
        Log::debug('getting parameter using we_fact_id '. $debtorCode);
        $user = $this->user;

        $userWeFact = $user
                        ->with(['userClientArea', 'userContact', 'userInformation', 'userReportArea', 'userPayment'])
                        ->weFactUser($debtorCode)
                        ->first();

        return $userWeFact;

    }

    public function show( $debtorCode )
    {
        $debtorCode = [
            'DebtorCode' => $debtorCode
        ];

        Log::debug('getting user detail with debtor code '. json_encode($debtorCode));

        $api = new WefactAPI();
        $userDetail = $api->sendRequest('debtor', 'show', $debtorCode);

        return $userDetail['debtor'];
    }

    public function showDB( $debtorCode )
    {

    }

    public function search( $parameter )
    {
        $parameters = [];

        if($parameter != "all")
        {
            $parameters["searchat"] = "CompanyName";
            $parameters["searchfor"] = $parameter;
        }

        $api = new WefactAPI();
        $users = $api->sendRequest('debtor', 'list', $parameters);

        if(isset($users["debtors"]))
        {
            foreach ($users['debtors'] as $key => $user) {
                $users['debtors'][$key] = $this->show( $user['DebtorCode'] );
            }
        }

        return $users;
    }

    public function registerOpenProvider( $users )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        foreach ($users as $key => $user) {

            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

            $pn = $phoneUtil->parse( "+31 71 387 6754", "nl");

            $userInformationOpenProvider = [
                "address" => [
                    "city" => $user->userInformation->place,
                    "country" => $user->userInformation->land,
                    "number" => "",
                    "state" => "",
                    "street" => $user->userInformation->address,
                    "suffix" => "",
                    "zipcode" => $user->userInformation->postcode,
                ],
                "locale" => "nl_NL",
                "company_name" => $user->userInformation->company_name,
                "email" => $user->email,
                "name" => [
                    "first_name" => $user->userInformation->first_name,
                    "full_name" => $user->userInformation->first_name.' '. $user->userInformation->last_name,
                    "initials" => $user->userInformation->first_name,
                    "last_name" => $user->userInformation->last_name,
                ],
                "vat" => $user->userInformation->vat_number,
                "phone" => [
                    "area_code"=> "97",
                    "country_code" => (string) $pn->getCountryCode(),
                    "subscriber_number" => $pn->getNationalNumber(),
                ]
            ];

            $customer = $client->getPersonModule()->getCustomerApi()->createCustomer( $userInformationOpenProvider );

            $user->open_provider_id = $customer["data"]["handle"];
            $user->save();
        }
    }

    public function legalForm()
    {
        $legalForm = $this->legalForm::all();

        return $legalForm;
    }

    public function country()
    {
        $country = $this->country::all();

        return $country;
    }

    public function userDB()
    {
        return $this->user->all();
    }
}
