<?php

namespace App\Repositories;

use App\Api\WeFactAPI;

class WeFactSettingRepository
{
    protected $weFactApi;

    public function __construct()
    {
        $this->weFactApi = new WeFactAPI();
    }

    public function getSettingTaxCode()
    {
        $parameters = [];

        $weFactApi = new WeFactAPI;
        $setting = $weFactApi->sendRequest('settings', 'list', $parameters);

        return $setting["settings"]["Tax"]["Codes"];
    }

    public function getLanguageCode()
    {
        $parameters = [];
        $languageCodes = $this->weFactApi->sendRequest( 'settings', 'list', $parameters );

        return $languageCodes;
    }

}
