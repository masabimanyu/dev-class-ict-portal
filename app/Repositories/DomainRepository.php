<?php

namespace App\Repositories;

use App\Api\OpenProvider;
use App\Models\TransactionDomainDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DomainRepository
{
    protected $transactionDomainDetail;

    public function __construct( TransactionDomainDetail $transactionDomainDetail)
    {
        $this->transactionDomainDetail = $transactionDomainDetail;
    }

    public function list()
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $domains = $client->getDomainModule()->getDomainServiceApi()->listDomains(
        );

        Log::debug('try search domain result' . json_encode($domains));

        return $domains;
    }

    /**
     * store
     *
     * @param  mixed $requestis request coming from the frontend part
     * @param  mixed $parameters payload who already send to the Open Provider
     * @param  mixed $transactionId the id transaction of the transaction already made
     * @param  mixed $domainContact array associative of domain contact information
     * @return TransactionDomainDetail
     */
    public function store( Request $request, $parameters, $transactionId, $domainContact)
    {
        Log::debug("trying to create domain with parameter ". json_encode($parameters));

        $transactionDomainDetail = new $this->transactionDomainDetail;
        $transactionDomainDetail->transaction_id = $transactionId;
        $transactionDomainDetail->domain_name = $request->search_domain_name;
        $transactionDomainDetail->domain_extension = $request->search_domain_extension;
        $transactionDomainDetail->hosting = $request->hosting;
        $transactionDomainDetail->status = $request->status;
        $transactionDomainDetail->auth_code = $request->auth;
        $transactionDomainDetail->is_registration_direct = $request->domain_registration_direct == "on" ? 1 : 0;
        $transactionDomainDetail->registrar = $request->registrar;
        $transactionDomainDetail->dns_template = $request->dns_template;
        $transactionDomainDetail->server_name_1 = $request->name_server_1;
        $transactionDomainDetail->server_name_2 = $request->name_server_2;
        $transactionDomainDetail->server_name_3 = $request->name_server_3;
        $transactionDomainDetail->ip_address_1 = $request->ip_1;
        $transactionDomainDetail->ip_address_2 = $request->ip_2;
        $transactionDomainDetail->ip_address_3 = $request->ip_3;
        $transactionDomainDetail->domain_owner = $domainContact["owner_handle"];
        $transactionDomainDetail->admin_contact = $domainContact["admin_handle"];
        $transactionDomainDetail->tech_contact = $domainContact["tech_handle"];
        $transactionDomainDetail->save();

        return $transactionDomainDetail;


        // $openProvider = new OpenProvider;
        // $client = $openProvider->createClient();
        // $domain = $client->getDomainModule()->getDomainServiceApi()->createDomain( $parameters );

        // if(isset($domain["data"]["id"]))
        // {
        //     $domainTransaction = new DomainTransaction();
        //     $domainTransaction->domain_id = $domain['data']['id'];
        //     $domainTransaction->is_billed = (int) $request->is_billed;
        //     $domainTransaction->internal_note = $request->internal_note;
        //     $domainTransaction->product_code = $request->search_product;
        //     $domainTransaction->description = $request->description;
        //     $domainTransaction->price_exclude = $request->price_exclude;
        //     $domainTransaction->tax_rate = $request->tax;
        //     $domainTransaction->discount_recurring = $request->discount_recurring;
        //     $domainTransaction->contract_period = $request->contract_period == "1" ? true : false;
        //     $domainTransaction->payload = json_encode( $request->all() );
        //     $domainTransaction->response = json_encode( $domain );
        //     $domainTransaction->save();

        //     $userDomain = new UserDomain;
        //     $userDomain->user_id = $request->client_id;
        //     $userDomain->transaction_id = $domainTransaction->id;
        //     $userDomain->save();
        // }

        // Log::debug("get domain after create ". $domain);

        // return $domain;
    }

    public function filterStatus( $status )
    {
        if($status == 'all')
        {
            $status = null;
        }

        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $domains = $client->getDomainModule()->getDomainServiceApi()->listDomains(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,null,
            null,
            null,
            null,
            null,
            $status,
        );

        return $domains;
    }

    public function detail( $domainId )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $domain = $client->getDomainModule()->getDomainServiceApi()->getDomain( $domainId );

        return $domain;
    }

    public function contact( $customerId )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $customer = $client->getPersonModule()->getCustomerApi()->getCustomer( $customerId);

        return $customer;
    }

    public function authCode( $domainId )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $authCode = $client->getDomainModule()->getAuthCodeApi()->getAuthCode( $domainId );

        return json_decode($authCode)->data->auth_code;
    }

    public function check( $body )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $checkDomain = $client->getDomainModule()->getDomainServiceApi()->checkDomain($body);

        return $checkDomain;

    }

    public function dnsTemplate( )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $dnsTemplates = $client->getDnsModule()->getTemplateServiceApi()->listTemplates();

        return $dnsTemplates;
    }

    public function showDnsTemplate( $dnsTemplateId )
    {
        $openProvider = new OpenProvider;
        $client = $openProvider->createClient();

        $template = $client->getDnsModule()->getTemplateServiceApi()->getTemplate( $dnsTemplateId );

        return $template;
    }

    public function getDetailDomain( $transactionDomainDetailId )
    {
        $transactionDomainDetail = $this->transactionDomainDetail::where('id', $transactionDomainDetailId)->first();

        return $transactionDomainDetail;
    }
}
