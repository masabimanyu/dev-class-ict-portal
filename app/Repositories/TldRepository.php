<?php

namespace App\Repositories;

use App\Models\Tld;

class TldRepository
{
    protected $tld;

    public function __construct( Tld $tld )
    {
        $this->tld = $tld;
    }

    public function lists()
    {
        $tlds = Tld::all();

        return $tlds;
    }

    public function getByName( $name )
    {
        $tld = $this->tld::name($name);

        return $tld;
    }
}
