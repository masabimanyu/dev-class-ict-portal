<?php

namespace App\Repositories;

use App\Api\WeFactAPI;
use App\Models\Product;
use App\Models\ProductCustomPrice;
use Illuminate\Http\Request;

class ProductRepository
{
    protected $product,$productCustomPrice;

    public function __construct( Product $product, ProductCustomPrice $productCustomPrice)
    {
        $this->product = $product;
        $this->productCustomPrice = $productCustomPrice;
    }

    public function lists( $parameters )
    {
        $api = new WeFactAPI;
        $product = $api->sendRequest('product', 'list', $parameters);

        return $product;
    }

    public function store( Request $request )
    {
        $product = new $this->product;
        $product->product_code = $request->product_code;
        $product->product_type = $request->product_type;
        $product->product_name = $request->product_name;
        $product->invoice_description = $request->invoice_description;
        $product->product_unit = $request->product_unit;

        if($request->product_type == "domain" )
        {
            $product->domain_tld = $request->product_domain_tld;
            $product->registrar = $request->registrar;
            $product->charge_cost = $request->charge;
            $product->public_who_is_server = $request->whois;
            $product->no_match_check = $request->no_match_check;
            $product->is_need_auth_code = $request->need_auth_code == "on" ? "1" : "0";
        }

        if($request->product_type = "hosting")
        {
            $product->hosting_template = $request->hosting_template;
            $product->webhosting_type = $request->webhosting;
            $product->server = $request->hosting_server;
            $product->hosting_account_email_template = $request->hosting_email_template;
            $product->is_send_hosting_directly = $request->send_hosting_direct == "on" ? "1" : "0";
            $product->hosting_account_pdf_template = $request->hosting_pdf_template;
        }

        $product->price_exclude = $request->price_exclude;
        $product->cost_price_exclude = $request->cost_price_exclude;
        $product->tax = $request->tax;
        $product->price_per = $request->price_per;
        $product->contract_period = $request->contract_period;
        $product->save();

        return $product;
    }

    public function getAll()
    {
        $products = $this->product::all();

        return $products;
    }

    public function setProductCustomPrice( $customPrices, $productId, $method = "store" )
    {
        if( $method == "store" )
        {
            foreach ($customPrices as $customPrice) {
                $productCustomPrice = new $this->productCustomPrice;
                $productCustomPrice->product_id = $productId;
                $productCustomPrice->period = $customPrice['periods'];
                $productCustomPrice->periodic = $customPrice['periodic'];
                $productCustomPrice->price_exclude = $customPrice['price_exclude'];
                $productCustomPrice->save();
            }
        }

        return true;
    }

    public function getProducts( $parameter )
    {
        $products = $this->product::all(['product_id','product_code', 'product_type', 'price_exclude', 'price_per'])
                    ->where('product_name', 'LIKE', `%${$parameter}%`);

        return $products;
    }

    public function get( $productId )
    {
        $product = $this->product::find($productId);

        return $product;
    }
}
