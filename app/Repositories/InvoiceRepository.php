<?php

namespace App\Repositories;

use App\Api\HostFactInvoice;
use App\Api\WeFactAPI;
use App\Models\InvoiceLog;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class InvoiceRepository
{
    protected $weFactApi, $invoiceLog;

    public function __construct( InvoiceLog $invoiceLog )
    {
        $this->weFactApi = new WeFactAPI;
        $this->invoiceLog = $invoiceLog;
    }

    public function lists( $debtor = null , $order = null, $status = null, $id = null)
    {
        $parameters = [];

        if(isset($debtor))
        {
            $parameters["searchat"] = "DebtorCode";
            $parameters["searchfor"] = $debtor;
        }

        if(isset($order))
        {
            $parameters["order"] = $order;
        }

        if(isset($status))
        {
            $parameters["status"] = (int) $status;
        }

        if(isset($id))
        {
            $parameters["searchat"] = "InvoiceCode";
            $parameters["searchfor"] = $id;
        }

        Log::debug("trying get invoices with parameters ". json_encode($parameters) );

        $api = new WeFactAPI();
        $invoices = $api->sendRequest('invoice', 'list', $parameters);

        Log::debug("invoices result " . json_encode($invoices));

        return $invoices;
    }


    public function invoiceDetail( $invoiceCode )
    {
        $parameters = [
            "InvoiceCode" => $invoiceCode
        ];

        Log::debug("trying to get invoice detail where invoiceCode : ". json_encode($parameters));

        $api = new WeFactAPI();
        $invoiceDetail = $api->sendRequest('invoice', 'show', $parameters);

        Log::debug("invoice detail for user" . json_encode($invoiceDetail));

        return $invoiceDetail;
    }

    public function download( $invoiceCode )
    {

        $invoiceDetail = $this->invoiceDetail( $invoiceCode );
        // $parameters = [
        //     "InvoiceCode" => $invoiceCode
        // ];

        // Log::debug("trying to get file download with invoiceCode : ". json_encode($parameters));

        // $api = new WeFactAPI();
        // $invoice = $api->sendRequest('invoice', 'download', $parameters);

        // Log::debug('response for download request ' . json_encode($invoice) );

        // // return $invoice;
        // $pdfData = base64_decode($invoice['invoice']['Base64']);

        // Storage::disk('local')->put('public/invoice/'.$invoice["invoice"]["Filename"], $pdfData);

        // return Storage::download('public/invoice/'.$invoice["invoice"]["Filename"]);

        $invoiceDetail = $invoiceDetail['invoice'];

        // return $invoiceDetail;

        $pdf = Pdf::loadView('admin.invoice.download', compact('invoiceDetail'))->setPaper('a4', 'potrait');

        return $pdf->download('test_invoice.pdf');
    }

    public function schedule()
    {
        $start = new Carbon('first day of last month');
        $start=  $start->startOfMonth()->format("Y-m-d");
        $end = new Carbon('last day of last month');
        $end = $end->endOfMonth()->format("Y-m-d");

        $invoiceUser = new HostFactInvoice('2022-01-01', '2022-12-30', 0);
        $invoiceUser = $invoiceUser->getDetailInvoiceGroupBy();

        // return $invoiceUser;

        foreach ($invoiceUser as $debtor => $invoices) {
            $payload = [
                "DebtorCode" => $debtor,
                "InvoiceLines" => []
            ];

            $productLine = [];

            foreach ($invoices as $invoice ) {

                foreach ($invoice["invoice"]["InvoiceLines"] as $key => $InvoiceLine) {
                    $productLine["Date"] = $InvoiceLine["Date"];
                    $productLine["Number"] = $InvoiceLine["Number"];
                    $productLine["NumberSuffix"] = $InvoiceLine["NumberSuffix"];
                    $productLine["Description"] = $InvoiceLine["Description"];
                    $productLine["PriceExcl"] = $InvoiceLine["PriceExcl"];
                    $productLine["DiscountPercentage"] = $InvoiceLine["DiscountPercentage"];
                    $productLine["DiscountPercentageType"] = $InvoiceLine["DiscountPercentageType"];
                    $productLine["DiscountPercentage"] = $InvoiceLine["DiscountPercentage"];
                    $productLine["StartDate"] = $InvoiceLine["StartPeriod"];
                    $productLine["EndDate"] = $InvoiceLine["EndPeriod"];

                    array_push($payload["InvoiceLines"], $productLine);
                }

            }

            Log::debug(json_encode($productLine));

            if($debtor == "DB10074")
            {
                $newInvoice = $this->weFactApi->sendRequest( 'invoice', 'add', $payload );

                Log::debug( json_encode($newInvoice) );
            }
        }

        return "can";
    }

    public function storeInvoice( $debtorCode, $invoiceLines )
    {
        $parameters = [
            "DebtorCode" => $debtorCode,
            "InvoiceLines" => $invoiceLines
        ];

        $invoice = $this->weFactApi->sendRequest( 'invoice', 'add', $parameters );

        return $invoice;
    }

    public function storeInvoiceLog( $weFactInvoice, $lineItem )
    {
        // $invoiceLog = new $this->invoiceLog;
        // $invoiceLog->transaction_id = $lineItem["TransactionId"];
        // $invoiceLog->invoice_id = $weFactInvoice["Invoice"]["InvoiceCode"];
        // $invoiceLog->
    }
}
