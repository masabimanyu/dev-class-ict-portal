<?php

namespace App\Repositories;

use App\Api\WeFactAPI;

class SubscriptionRepository
{
    protected $weFactApi;

    public function __construct()
    {
        $this->weFactApi = new WeFactAPI();
    }

    public function add( $parameters )
    {
        $subscription = $this->weFactApi->sendRequest( 'subscription', 'add', $parameters );

        return $subscription;
    }

    public function get( $parameters )
    {
        $subscription = $this->weFactApi->sendRequest('subscription', 'show', $parameters);

        return $subscription;
    }
}
