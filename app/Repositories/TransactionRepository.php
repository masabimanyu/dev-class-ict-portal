<?php

namespace App\Repositories;

use App\Models\Transaction;
use App\Models\TransactionContractPeriod;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionRepository
{
    protected $transaction, $transactionContractPeriod, $periodicMonth;

    public function __construct(
        Transaction $transaction,
        TransactionContractPeriod $transactionContractPeriod
    ){
        $this->transaction = $transaction;
        $this->transactionContractPeriod = $transactionContractPeriod;
        $this->periodicMonth = config('constant.wefact.period_month');
    }

    public function all()
    {
        $transactions = $this->transaction::all();

        return $transactions;
    }

    public function storeTransaction( Request $request )
    {
        $transaction = new $this->transaction;
        $transaction->is_billed = $request->is_billed == "1" ? 1 : 0;
        $transaction->internal_note = $request->internal_note;
        $transaction->product_id = $request->search_product;
        $transaction->client_id = $request->client_id;
        $transaction->description = $request->description;
        $transaction->status = $request->status;
        $transaction->price_exclude = $request->price_exclude;
        $transaction->tax_rate = $request->tax;
        $transaction->discount_recurring = $request->discount_recurring;
        $transaction->contract_period = $request->contract_period;
        $transaction->billing_cycle_length = $request->billing_cycle_length;
        $transaction->billing_cycle_per = $request->billing_cycle_per;
        $transaction->bill_recurring = $request->bill_recurring;
        $transaction->billing_period_start = $request->billing_period_start;
        $transaction->billing_period_end = $request->billing_period_end;
        $transaction->notification_new_billing = $request->client_send_notification;
        $transaction->bill_direct_debit = $request->bill_direct_debit;
        $transaction->save();

        return $transaction;
    }

    public function storeTransactionContractPeriod( Request $request, $transactionId )
    {
        $transactionContract = new $this->transactionContractPeriod;
        $transactionContract->contract_length = $request->contract_period == "0" ? $request->contract_length : $request->billing_cycle_length;
        $transactionContract->transaction_id = $transactionId;
        $transactionContract->contract_method = $request->contract_period == "0" ?  $request->contract_per : $request->billing_cycle_per;
        $transactionContract->contract_period_start = $request->contract_period == "0" ? $request->contract_date_start : $request->billing_period_start;
        $transactionContract->contract_period_end = $request->contract_period == "0" ? $request->contract_date_end : $request->billing_period_end;
        $transactionContract->save();

        return $transactionContract;
    }

    public function get( $transactionId )
    {
        $transaction = $this->transaction->where('id', $transactionId)->first();

        return $transaction;
    }

    public function transactions( $debtorCode )
    {
        $transactions = $this->transaction->client( $debtorCode )->get();

        return $transactions;
    }

    public function adjustPeriod( $transactionId )
    {
        $monthPeriod = config('constant.wefact.period_month');

        $transaction = $this->transaction::where('id', $transactionId)->first();

        $period = $transaction->billing_cycle_per;
        $periodLength = $transaction->billing_cycle_length;

        // $adjustedBillingPeriodStart = Carbon::parse( $transaction->billing_period_start )->addMonth( (int) $periodLength * $monthPeriod[$period] );
        // $adjustedBillingPeriodEnd = Carbon::parse( $transaction->billing_period_end )->addMonth( (int) $periodLength * $monthPeriod[$period] );

        $adjustedBillingPeriodStart = $this->nextDate( $transaction->billing_period_start, null, $periodLength, $period );
        $adjustedBillingPeriodEnd = $this->nextDate($adjustedBillingPeriodStart , null, $periodLength, $period);

        $transaction->billing_period_start = $adjustedBillingPeriodStart;
        $transaction->billing_period_end = $adjustedBillingPeriodEnd;
        $transaction->save();
    }

    public function adjustTotalInvoice( $transactionId )
    {
        $transaction = $this->transaction::where('id', $transactionId)->first();
        $transaction->invoice_total += 1;
        $transaction->save();
    }

    public function adjustNextdate( $transactionId )
    {
        $monthPeriod = config('constant.wefact.period_month');

        $transaction = $this->transaction::where('id', $transactionId)->first();

        $period = $transaction->billing_cycle_per;
        $periodLength = (int) $transaction->billing_cycle_length;

        // $nextDate = $transaction->next_date == null ?
        //                 Carbon::parse($transaction->contractPeriod->contract_period_start)->addMonth( $periodLength * $monthPeriod[$period])
        //                 :
        //                 Carbon::parse($transaction->next_date)->addMonth( $periodLength * $monthPeriod[$period]);

        $nextDate = $this->nextDate( $transaction->contractPeriod->contract_period_start, $transaction->next_date, $periodLength, $period );
        // $transaction->next_date = Carbon::parse( $transaction->billing_period_start );

        $transaction->next_date = $nextDate ;
        $transaction->save();
    }

    public function nextDate($contractStart, $nextDate, $billingCycleLength, $billingCyclePer )
    {
        $date = ($nextDate == null || $nextDate == "null") ? $contractStart : $nextDate;

        $date = Carbon::parse($date);

        $isLastDayOfMonth = $date->format('Y-m-d') == Carbon::parse( Carbon::create($date->year, $date->month, $date->day )->lastOfMonth() )->format('Y-m-d') ? true : false;

        $daydate = $date->day;

        $futureContract = $date->setDay(1)->addMonth((int) $billingCycleLength * $this->periodicMonth[$billingCyclePer])->lastOfMonth();

        $dayFutureContract = $futureContract->day;

        if($daydate > $dayFutureContract)
        {
            $futureContract->setDay($dayFutureContract);
        }else if( $daydate < $dayFutureContract && $isLastDayOfMonth )
        {
            $futureContract->setDay($dayFutureContract);
        }
        else{
            $futureContract->setDay($daydate);
        }

        return $futureContract->format('Y-m-d');
    }
}
