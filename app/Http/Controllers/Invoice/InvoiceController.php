<?php

namespace App\Http\Controllers\Invoice;

use App\Http\Controllers\Controller;
use App\Services\InvoiceService;
use App\Services\SubscriptionService;
use App\Services\TransactionService;
use App\Services\UserService;
use Barryvdh\DomPDF\Facade\Pdf as FacadePdf;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{

    protected $invoiceService, $subscriptionService, $userService;

    public function __construct(
        InvoiceService $invoiceService,
        SubscriptionService $subscriptionService,
        UserService $userService
    )
    {
        $this->invoiceService = $invoiceService;
        $this->subscriptionService = $subscriptionService;
        $this->userService = $userService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = $this->invoiceService->list();

        return view('admin.invoice.index', compact('invoices'));
    }

    public function listByDebtor( $debtorCode )
    {
        $invoices = $this->invoiceService->listByDebtor( $debtorCode );

        return $invoices;
    }

    public function searchByStatus(Request $request)
    {
        $status = $request->query('status');
        $invoices = $this->invoiceService->listByStatus( $status );

        return $invoices;
    }

    public function searchById(Request $request)
    {
        $id = $request->query('parameter');

        $invoices = $this->invoiceService->listById( $id );

        return $invoices;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $invoiceCode
     * @return \Illuminate\Http\Response
     */
    public function show($invoiceCode)
    {
        $invoice = $this->invoiceService->detail( $invoiceCode );
        $taxes = $this->invoiceService->calculateAvailableTax( $invoice );

        return view('admin.invoice.detail-invoice', compact('invoice', 'taxes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download( $invoiceCode )
    {
        // $pdf = FacadePdf::loadView('admin.invoice.download');

        // return $pdf->download('test_invoice.pdf');

        return $this->invoiceService->downloadInvoice( $invoiceCode );
        return view('admin.invoice.download');
    }

    public function schedule()
    {
        return $this->invoiceService->scheduleCron();
    }

    // public function testEditInvoice( $invoiceCode )
    // {

    //     $invoiceCode = '[concept]0005';
    //     $invoice = $this->invoiceService->detail( $invoiceCode );
    //     $taxes = $this->invoiceService->calculateAvailableTax( $invoice );

    //     $invoiceLines = $invoice["invoice"]["InvoiceLines"];

    //     Carbon::setTestNow( Carbon::create(2023, 1, 1) );

    //     $invoicePeriodStart = Carbon::parse('first day of last month');
    //     $invoicePeriodStart = $invoicePeriodStart->day(15)->format('Y-m-d');
    //     $invoicePeriodEnd = Carbon::parse('first day of this month');
    //     $invoicePeriodEnd = $invoicePeriodEnd->day(14)->format('Y-m-d');

    //     $newInvoiceLines = [];
    //     foreach ($invoiceLines as $key => $lineItem) {
    //         $invoiceLineDate = Carbon::createFromFormat('Y-m-d', $lineItem['Date']);

    //         if( $invoiceLineDate->gte($invoicePeriodStart) && $invoiceLineDate->lte( $invoicePeriodEnd ) )
    //         {
    //             array_push($newInvoiceLines, $lineItem);
    //             if($lineItem["Periodic"] == "m")
    //             {
    //                 $subscription = $lineItem['PeriodicID'] != "0" ? $this->subscriptionService->show($lineItem['PeriodicID'] ) : "0";
    //                 $transactionId = $subscription != "0" ? $subscription["subscription"]["Comment"] : "";
    //                 $transaction = $transactionId != "" ? $this->transactionService->transactionDetail( $transactionId ) : "";

    //                 $transactionContractPeriod = Carbon::createFromFormat( 'Y-m-d H:i:s', $transaction->contractPeriod->contract_period_start );
    //                 if( $invoiceLineDate->eq( $transactionContractPeriod ) )
    //                 {
    //                     // if the invoice is first billed make it double
    //                     $doubleLineItem = $lineItem;
    //                     $doubleLineItem["Date"] = Carbon::parse($lineItem["Date"])->addMonths( (int) $lineItem['Periods'] )->format('Y-m-d');
    //                     $doubleLineItem["StartDate"] = Carbon::parse($lineItem["StartDate"])->addMonths( (int) $lineItem['Periods'] )->format('Y-m-d');
    //                     $doubleLineItem["EndDate"] = Carbon::parse($lineItem["EndDate"])->addMonths( (int) $lineItem['Periods'] )->format('Y-m-d');
    //                     $doubleLineItem["StartPeriod"] = Carbon::parse($lineItem["StartPeriod"])->addMonths( (int) $lineItem['Periods'] )->format('Y-m-d');
    //                     $doubleLineItem["EndPeriod"] = Carbon::parse($lineItem["EndPeriod"])->addMonths( (int) $lineItem['Periods'] )->format('Y-m-d');

    //                     array_push( $newInvoiceLines, $doubleLineItem );
    //                 }
    //             }
    //         }
    //     }

    //     return $newInvoiceLines;

    //     return $invoice;
    // }

    public function testInvoiceDb()
    {
        // Carbon::setTestNow( Carbon::create(2023, 1, 1) );

        // $invoicePeriodStart = Carbon::parse('first day of last month');
        // $invoicePeriodStart = $invoicePeriodStart->day(15)->format('Y-m-d');
        // $invoicePeriodEnd = Carbon::parse('first day of this month');
        // $invoicePeriodEnd = $invoicePeriodEnd->day(14)->format('Y-m-d');

        // $transactions = $this->transactionService->clientTransactions( $debtorCode );

        // $invoiceLines = [];

        // foreach ($transactions as $transaction) {

        //     $invoicedOn =  $transaction->next_date == null ? $transaction->contractPeriod->contract_period_start : $transaction->next_date;
        //     $invoicedOn = Carbon::parse( $invoicedOn );

        //     $invoiceLine = $this->transactionToInvoiceLine( $transaction );

        //     // $transactionContractDate = Carbon::parse($transaction->contractPeriod->contract_period_start);

        //     if(
        //         $invoicedOn->gte($invoicePeriodStart) &&
        //         $invoicedOn->lte( $invoicePeriodEnd ) &&
        //         $transaction->is_billed == 1
        //     )
        //     {

        //         array_push($invoiceLines, $invoiceLine);
        //         if($transaction->billing_cycle_per == 'm')
        //         {
        //             if($transaction->next_date == null)
        //             {
        //                 // if transaction if the first time billed the period must included frist period and second period

        //                 $doubleLineItem = $invoiceLine;
        //                 $doubleLineItem["Date"] = Carbon::parse($invoiceLine["Date"])->addMonths( (int) $invoiceLine['Periods'] )->format('Y-m-d');
        //                 $doubleLineItem["StartDate"] = Carbon::parse($invoiceLine["StartDate"])->addMonths( (int) $invoiceLine['Periods'] )->format('Y-m-d');

        //                 array_push( $invoiceLines, $doubleLineItem );
        //             }else{
        //                 // if transaction not first time billed, it needs to be next period
        //                 // $futureLineItem["Date"] = Carbon::parse


        //                 // array_push($invoiceLines, $invoiceLine);
        //             }
        //         }
        //     }
        // }

        // return $invoiceLines;

        // $invoice = $this->invoiceService->addInvoice( $debtorCode, $invoiceLines );

        // return $invoice;

        $users = $this->userService->users();

        foreach ($users as $key => $user) {
            if($user->wefact_user_id != null || $user->wefact_user_id != "")
            {
                $this->invoiceService->generateInvoice( $user->wefact_user_id );
            }
        }

        return $users;
    }

    public function testDate($cs, $nd, $bcl, $bcp)
    {
        return $this->invoiceService->nextDate( $cs, $nd,$bcl, $bcp );
    }

    public function previewPDF()
    {
        return view('admin.invoice.download');
    }
}
