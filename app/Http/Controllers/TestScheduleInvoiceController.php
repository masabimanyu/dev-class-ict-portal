<?php

namespace App\Http\Controllers;

use App\Services\InvoiceService;
use App\Services\TransactionService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class TestScheduleInvoiceController extends Controller
{
    protected $userService, $invoiceService, $transactionService;

    public function __construct( UserService $userService, InvoiceService $invoiceService, TransactionService $transactionService )
    {
        $this->userService = $userService;
        $this->invoiceService = $invoiceService;
        $this->transactionService = $transactionService;
    }

    public function index()
    {
        $transactions = $this->transactionService->all();

        // return $transactions;
        $invoiceDate = [];

        for ($i=1; $i <= 12; $i++) {
            $date = Carbon::create( Carbon::now()->year, $i, 1)->format('Y-m-d');
            array_push($invoiceDate, $date);
        }

        return view( 'admin.test-schedule.index', compact('invoiceDate', 'transactions'));
    }

    public function generate( Request $request )
    {
        $date = $request->schedule_date;

        $invoiceDate = Carbon::parse( $date );

        Carbon::setTestNow( $invoiceDate );

        $users = $this->userService->users();

        $generatedInvoice = [];

        foreach ($users as $key => $user) {
            if($user->wefact_user_id != null || $user->wefact_user_id != "")
            {
                $invoice = $this->invoiceService->generateInvoice( $user->wefact_user_id );

                if($invoice)
                {
                    array_push( $generatedInvoice, $invoice );
                }
            }
        }

        return Redirect::back()->withInput()->with(['generatedInvoice' => $generatedInvoice]);
    }
}
