<?php

namespace App\Http\Controllers\User;

use App\Api\WeFactAPI;
use App\DataTables\UsersDataTable;
use App\Exceptions\WeFactException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\WeFactSettingService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
class UserController extends Controller
{

    protected $userService, $weFactSettingService;

    public function __construct(UserService $userService, WeFactSettingService $weFactSettingService )
    {
        $this->middleware('auth');
        $this->userService  = $userService;
        $this->weFactSettingService = $weFactSettingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( UsersDataTable $dataTable )
    {
        // return $dataTable->render('admin.user.index'); //use if datatable
        try {
            $users = $this->userService->list( 1, 200 );
            return view('admin.user.index', compact('users'));
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }

    public function lists()
    {
        try {
            $users = $this->userService->list( 8, 200 );
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $legalForms = $this->userService->getLegalFormList();
        $countries = $this->userService->getCountryList();
        $languageCodes = $this->weFactSettingService->getWeFactLanguageCodes();

        return view('admin.user.create', compact('legalForms', 'countries', 'languageCodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            $this->userService->store($request);

            DB::commit();
            Log::debug("success add user");
            return Redirect::route('user.create')->with('success', 'Success create user');

        }catch(WeFactException $e){
            DB::rollBack();
            Log::error("error :" . $e->getMessage());
            return Redirect::back()->with('error', __( 'something error while adding new user' ))->withInput();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error("error :" . $th->getMessage());
            return Redirect::back()->with('error', __( 'something error while adding new user' ))->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = $this->userService->userDetail( $id );
            if(request()->ajax())
            {
                return response()->json($user, 200);
            }

            return $user;
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $user = $this->userService->userDetail( $id );
            $userDB = $this->userService->findByDebtorCode( $id );

            $legalForms = $this->userService->getLegalFormList();
            $countries = $this->userService->getCountryList();
            $languageCodes = $this->weFactSettingService->getWeFactLanguageCodes();

            return view('admin.user.edit', compact('user', 'userDB', 'legalForms', 'countries', 'languageCodes'));
        } catch (\Throwable $th) {
            Log::error( $th->getMessage());
        }
    }

    public function search( Request $request)
    {
        $parameter = $request->query('parameter');

        try {
            $users = $this->userService->searchUser( $parameter );

            return $users;
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        DB::beginTransaction();
        try {
            $this->userService->update( $request, $id );

            DB::commit();
            Log::debug("success update user");
            return Redirect::route('user.edit', $id)->with('success', 'Success update user');

        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
            return Redirect::back()->with('error', $th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // DB::beginTransaction();
        // try {

        //     $user = User::where('id', $id)->first();
        //     $user->delete();

        //     $userInformation = UserInformation::where('user_id', $user->id);
        //     $userInformation->delete();

        //     $userContact = UserContact::where('user_id', $user->id);
        //     $userContact->delete();

        //     $userClientArea = UserClientArea::where('user_id', $user->id);
        //     $userClientArea->delete();

        //     $userReportArea = UserReportArea::where('user_id', $user->id);
        //     $userReportArea->delete();

        //     DB::commit();
        //     return view('user.index')->with('success', 'Success delete user');

        // } catch (\Throwable $th) {
        //     DB::rollBack();
        //     return Redirect::back(500)->with('error', $th->getMessage());
        // }
    }


    public function batchRegOp()
    {
        return $this->userService->batchRegisterOp();
    }

    public function detail( $id )
    {
        try {
            $user = $this->userService->userDetail( $id );
            $userDB = $this->userService->findByDebtorCode( $id );

            return response()->json(["user" => $user, "userDB" => $userDB], 200);
        } catch (\Throwable $th) {
            throw $th;
            Log::error( $th->getMessage());
        }
    }
}
