<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Services\ProductService;
use App\Services\TldService;
use App\Services\WeFactSettingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;

class ProductController extends Controller
{

    protected $productService, $weFactSettingService, $tldService;

    public function __construct(
        ProductService $productService,
        WeFactSettingService $weFactSettingService,
        TldService $tldService
    )
    {
        $this->middleware('auth');
        $this->middleware('adminRole')->only(['create', 'store', 'edit', 'update', 'destroy']);
        $this->productService = $productService;
        $this->weFactSettingService = $weFactSettingService;
        $this->tldService = $tldService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productService->lists();

        $taxes = $this->weFactSettingService->getWeFactTax( 'sale' );

        return view('admin.manage-product.index', compact('products', 'taxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $periods = config('constant.wefact.periodic');
        $taxes = $this->weFactSettingService->getWeFactTax( 'sale' );
        $tlds = $this->tldService->getAll();
        $products = $this->productService->lists();

        return view('admin.manage-product.add-new-product', compact('periods', 'taxes', 'tlds', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->productService->save( $request);
            DB::commit();

            return Redirect::route('product.create')->with('success', "success add product")->withInput();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::debug("error adding product : ". $th->getMessage());
            return Redirect::back()->with('error', "Something error while create product")->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * search by product name
     *
     * @return void
     */
    public function search(Request $request)
    {
        // return $request->query('parameter');

        // $products = $this->productService->products( $param );

        // return $products;
    }
}
