<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function getPeriod(Request $request)
    {
        // $periods = config('constant.wefact.billing_period');
        $currentPeriod = $request->query('period');

        $keyPeriod = config('constant.wefact.key_period');
        $periods = config('constant.wefact.billing_period');

        // return $periods;

        $availablePeriods = array_slice( $periods, array_search($currentPeriod, $keyPeriod), count($periods) );

        return $availablePeriods;
    }
}
