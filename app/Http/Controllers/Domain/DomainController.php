<?php

namespace App\Http\Controllers\Domain;

use App\Http\Controllers\Controller;
use App\Services\DomainService;
use App\Services\ProductService;
use App\Services\UserService;
use App\Services\WeFactSettingService;
use Illuminate\Http\Request;

class DomainController extends Controller
{
    protected $domainService;

    protected $userService;

    protected $productService;

    protected $weFactSettingService;

    public function __construct(
        DomainService $domainService,
        UserService $userService,
        ProductService $productService,
        WeFactSettingService $weFactSettingService,
    )
    {
        $this->middleware('auth');
        $this->middleware('adminRole');
        $this->domainService = $domainService;
        $this->userService = $userService;
        $this->productService = $productService;
        $this->weFactSettingService = $weFactSettingService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $domains = $this->domainService->lists();
        // $domains = !is_array($this->domainService->lists()) ? json_decode($this->domainService->lists()) : $this->domainService->lists() ;
        $domains = json_decode($this->domainService->lists()); // use json_decode only for sandbox

        return view('admin.domain.index', compact('domains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = $this->userService->list(1, 2);
        $dnsTemplates = $this->domainService->getDnsTemplates()["data"];
        $products = $this->productService->lists();
        $taxes = $this->weFactSettingService->getWeFactTax();
        $domainStatus = config('constant.hostfact.domain_status.domain_create');
        $periodic = config('constant.wefact.billing_period');
        $hostingStatus = config('constant.hostfact.hosting_status.hosting_create');

        return view('admin.domain.create', compact('users', 'dnsTemplates', 'products', 'taxes', 'domainStatus', 'periodic', 'hostingStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        // DB::beginTransaction();
        // try {
            return $this->domainService->store( $request );
        //     DB::commit();
        //     return Redirect::route('domain.index')->with("success", __("success registering domain"));
        // } catch (\Throwable $th) {
        //     DB::rollBack();
        //     Log::debug("error creating domain with error : " . $th->getMessage());
        //     return Redirect::back()->withErrors($th->getMessage());
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $domain = $this->domainService->detail( $id );

        return view('admin.domain.detail-domain', compact('domain'));
    }

    public function domainByStatus( Request $request )
    {
        $status = $request->query('status');

        $domains = json_decode($this->domainService->status( $status )); // json_decode only for sandbox

        return $domains->data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return view('admin.domain.detail-domain');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function contactInformation( $domainId )
    {
        $domainDetail = $this->domainService->detail( $domainId );

        $contacts = [
            'admin_handle' => $domainDetail->data->admin_handle,
            'billing_handle' => $domainDetail->data->billing_handle,
            'owner_handle' => $domainDetail->data->owner_handle,
            'tech_handle' => $domainDetail->data->tech_handle
        ];

        $contacts = $this->domainService->getContactDetail( $contacts );

        return $contacts;

        return view('admin.domain.manage.contactinformation', $contacts);
    }

    public function domainAuthCode( Request $request)
    {
        $domainId = $request->query('domain');

        $authCode = $this->domainService->getAuthCode( $domainId );

        return $authCode;
    }

    public function domainCheck( Request $request )
    {
        $name = $request->domain_name;
        $extension = $request->domain_extension;

        $check = $this->domainService->domainCheck($name, $extension);

        return $check;
    }

    public function domainDnsTemplate()
    {
        $domain = $this->domainService->getDnsTemplates();

        if(request()->ajax())
        {
            return response()->json($domain, 200);
        }

        return $domain;
    }

    public function showDnsTemplate( $dnsTemplateId )
    {
        $dnsTemplate = $this->domainService->getDnsTemplate( $dnsTemplateId );

        return $dnsTemplate["data"];
    }

}
