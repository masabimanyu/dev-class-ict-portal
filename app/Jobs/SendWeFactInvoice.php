<?php

namespace App\Jobs;

use App\Api\HostFactInvoice;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendWeFactInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Log::debug("send invoice");
        $start = new Carbon('first day of last month');
        $start=  $start->startOfMonth()->format("Y-m-d");
        $end = new Carbon('last day of last month');
        $end = $end->endOfMonth()->format("Y-m-d");

        $invoiceUser = new HostFactInvoice('2022-01-01', '2022-12-30', 0);
        $invoiceUser = $invoiceUser->getDetailInvoiceGroupBy();

        foreach ($invoiceUser as $debtor => $invoices) {
            $payload = [
                "DebtorCode" => $debtor,
                "InvoiceLines" => []
            ];

            $productLine = [];

            foreach ($invoices as $invoice ) {

                foreach ($invoice["invoice"]["InvoiceLines"] as $key => $InvoiceLine) {
                    $productLine["Date"] = $InvoiceLine["Date"];
                    $productLine["Number"] = $InvoiceLine["Number"];
                    $productLine["NumberSuffix"] = $InvoiceLine["NumberSuffix"];
                    $productLine["Description"] = $InvoiceLine["Description"];
                    $productLine["PriceExcl"] = $InvoiceLine["PriceExcl"];
                    $productLine["DiscountPercentage"] = $InvoiceLine["DiscountPercentage"];
                    $productLine["DiscountPercentageType"] = $InvoiceLine["DiscountPercentageType"];
                    $productLine["DiscountPercentage"] = $InvoiceLine["DiscountPercentage"];
                    $productLine["StartDate"] = $InvoiceLine["StartPeriod"];
                    $productLine["EndDate"] = $InvoiceLine["EndPeriod"];

                    array_push($payload["InvoiceLines"], $productLine);
                }

            }

            Log::debug(json_encode($productLine));

            if($debtor == "DB10074")
            {
                $newInvoice = $this->weFactApi->sendRequest( 'invoice', 'add', $payload );

                Log::debug( json_encode($newInvoice) );
            }
        }
    }
}
