<?php

namespace App\Jobs;

use App\Repositories\DomainRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class RegisterOpenProvider implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $domainDetail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $domainDetail )
    {
        $this->domainDetail = $domainDetail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug( json_encode($this->domainDetail) );
    }
}
