<?php

namespace App\Jobs;

use App\Mail\SendHostingCredential;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RegisterPlesk implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $hostingDetail, $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $hostingDetail, $email )
    {
        $this->hostingDetail = $hostingDetail;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug("dispatching job with detail : " . json_encode($this->hostingDetail));

        //rest of code that register to plesk

        // mail to user when hosting already created
        Mail::to( $this->email )->queue( new SendHostingCredential($this->hostingDetail) );
    }
}
