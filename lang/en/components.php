<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'sidebar' => [
        'menu-1'                 => 'Client Overview',
        'menu-2'                 => 'Domain',
        'menu-3'                 => 'Hosting accounts',
        'menu-4'                 => 'Other services',
        'menu-5'                 => 'Invoices',
        'menu-6'                 => 'My Account',
        'language'              => 'Language:'
    ],
    'navbar' => [
        'logout'                 => 'Log out',
        'language'              => 'Language:'
    ],
];
