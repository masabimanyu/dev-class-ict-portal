<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' => [
        'title'                 => 'Inloggen',
        'placeholder_type_here' => 'Typ hier',
        'label_email'           => 'Email',
        'label_password'        => 'Password',
        'forget_password'       => 'Wachtwoord vergeten?',
        'remember_me'           => 'Remember me',
        'button_label'          => 'Inloggen',
        'language'              => 'Language:'
    ],
    'forgot' => [
        'title'                 => 'Wachtwoord vergeten',
        'label_username'        => 'Gebruikersnaam',
        'placeholder_type_here' => 'Typ hier',
        'label_email'           => 'E-mailadres',
        'login'                 => 'Terug naar de login pagina',
        'button_label'          => 'Verzenden',
        'language'              => 'Language:'
    ],
    'sidebar' => [
        'menu1'                 => 'Overview',
        'menu2'                 => 'Domain',
        'menu3'                 => 'Hosting accounts',
        'menu4'                 => 'Other services',
        'menu5'                 => 'Invoices',
        'menu6'                 => 'My Account',
        'language'              => 'Language:'
    ]
];
