<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'user-overview' => [
        'title'                 => 'Overview',
        'contact'               => 'Contact', 
        'no-contact'            => 'No contact Found', 
        'add-contact'           => 'Add new contact', 
        'support'               => 'Support', 
        'invoice'               => 'Invoices', 
        'date'                  => 'Date', 
        'amount'                => 'Amount', 
        'open invoices'         => 'Open invoices', 
        'services'              => 'Services', 
        'tickets'               => 'Tickets', 
        'shortcut'              => 'Shortcut',
        'detail'                => 'Detail',
        'more-information'      => 'More Information',
        'edit-information'      => 'Edit information',
        'order-service'         => 'Order new services',
        'register-domain'       => 'Register new domain',
        'logout'                => 'Logout',
        'language'              => 'Language:'
    ],
    'client-overview' => [
        'title'                 => 'Clients Overview',
        'all-clients'           => 'All Clients',
        'debit-clients'         => 'Client with direct debit',
        'active-clients'        => 'Active clients',
        'archived-clients'      => 'Archived clients',
        'search-clients'        => 'Search Clients',
        'add-new-clients'       => 'Add new clients',  
        'id-clients'            => 'ID Clients',  
        'name'                  => 'Name',   
        'email'                 => 'E-mail',   
        'group'                 => 'Group',   
        'outstand'              => 'Outstanding Amount',   
        'filters'               => 'Filters', 
        'detail'                => 'Detail',
        'language'              => 'Language:'
    ],
    'client-create' => [
        'title'                 => 'Clients Overview',
        'information-clients'   => 'Client information',
        'debit-clients'         => 'Bank and direct debit',
        'company-clients'       => 'Company name',
        'legal-form'            => 'Legal form',   
        'first-name'            => 'First name',   
        'last-name'             => 'Last name',   
        'address'               => 'Address',    
        'post-code'             => 'Post code',     
        'place'                 => 'Place',     
        'land'                  => 'Land',     
        'choose-here'           => 'Choose here',     
        'contact-information'   => 'Contact information',   
        'email'                 => 'E-mail address',   
        'phone'                 => 'Phone number',   
        'mobile-number'         => 'Mobile number',   
        'fax'                   => 'Fax number',   
        'website'               => 'Website',   
        'receive'               => 'Receive mailing?',   
        'invoicing-and-payment' => 'Invoicing and payment',   
        'alternative'           => 'Alternative invoice address',   
        'client-area'           => 'Client area',   
        'active-login'          => 'Activate login for client area and order form',   
        'username'              => 'Username',   
        'temporary-pass'        => 'Temporary password',   
        'language'              => 'Language',   
        'profile'               => 'Profile',   
        'send-welcome'          => 'Send welcome message',   
        'report-area'           => 'Report area',   
        'verwerker'             => 'Verwerkersovereenkomst',   
        'algemene'              => 'Algemene Voorwaarden',   
        'privacy'               => 'Privacy Policy',   
        'save'                  => 'Save',   
        'bank-account'          => 'Bank account information',   
        'back-number'           => 'Bank account number',   
        'account-number'        => 'Account holder',   
        'bank-city'             => 'Bank city',   
        'bank-code'             => 'Bank Code (BIC)',   
        'bank'                  => 'Bank',   
        'direct-debit'          => 'Direct debit',   
        'direct-debit-client'   => 'This client wants to pay by direct debit',   
        'mandate-reference'     => 'Mandate reference',   
        'mandate-sign-date'     => 'Mandate sign date',   
        'back'                  => 'Back',   
        'language'              => 'Language:'
    ],
];
