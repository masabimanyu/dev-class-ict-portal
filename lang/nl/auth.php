<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login' => [
        'title'                 => 'Inloggen',
        'placeholder_type_here' => 'Typ hier',
        'label_email'           => 'Email',
        'label_password'        => 'Password',
        'forget_password'       => 'Wachtwoord vergeten?',
        'remember_me'           => 'Remember me',
        'button_label'          => 'Inloggen',
        'language'              => 'Language:'
    ],
    'login' => [
        'title'                 => 'Inloggen',
        'placeholder_type_here' => 'Typ hier',
        'label_email'           => 'Email',
        'label_password'        => 'Password',
        'forget_password'       => 'Wachtwoord vergeten?',
        'remember_me'           => 'Remember me',
        'button_label'          => 'Inloggen',
        'language'              => 'Language:'
    ]
];
