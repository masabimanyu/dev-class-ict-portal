<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserReportAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_report_area', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->index('user_id');
            $table->boolean('processing_agreement')->nullable()->default(false);
            $table->boolean('term_and_condition')->nullable()->default(false);
            $table->boolean('privacy_policy')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_report_area');
    }
}
