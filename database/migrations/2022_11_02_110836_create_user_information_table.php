<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_information', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable()->index('user_id');
            $table->string('company_name')->nullable();
            $table->string('company_legal_form')->nullable();
            $table->integer('chamber_of_commerce')->nullable();
            $table->integer('vat_number')->nullable();
            $table->string('user_legal_form', 11)->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->text('address')->nullable();
            $table->string('postcode', 25)->nullable();
            $table->text('place')->nullable();
            $table->string('land')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_information');
    }
}
