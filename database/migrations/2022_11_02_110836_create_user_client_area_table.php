<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserClientAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_client_area', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable()->index('user_id');
            $table->string('username');
            $table->string('temporary_password');
            $table->string('language');
            $table->integer('profile');
            $table->integer('send_welcome');
            $table->tinyInteger('alt_invoicing_and_payment')->default(0);
            $table->tinyInteger('is_login_and_order_form')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_client_area');
    }
}
