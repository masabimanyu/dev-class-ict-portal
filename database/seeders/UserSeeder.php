<?php

namespace Database\Seeders;

use App\Api\WeFactAPI;
use App\Models\User;
use App\Models\UserClientArea;
use App\Models\UserContact;
use App\Models\UserInformation;
use App\Models\UserPayment;
use App\Models\UserReportArea;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    protected $user, $weFactApi, $userInformation, $userClientArea, $userReportArea, $userPayment, $userContact;

    public function __construct()
    {
        $this->user = new User;
        $this->weFactApi = new WeFactAPI();
        $this->userInformation = new UserInformation();
        $this->userClientArea = new UserClientArea();
        $this->userReportArea = new UserReportArea();
        $this->userPayment = new UserPayment();
        $this->userContact = new UserContact();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new $this->user;
        $user->name = Str ::random('Test Account') ;
        $user->email = "test@gmail.com";
        $user->password = Hash::make('00000000');
        $user->username = 'admin';
        $user->save();

        $userInformation = new $this->userInformation;
        $userInformation->user_id = $user->id;
        $userInformation->company_name = "Test Company";
        $userInformation->company_legal_form = "NV";
        $userInformation->chamber_of_commerce = "ICS 90988";
        $userInformation->vat_number = "IU9872231";
        $userInformation->user_legal_form = "Dhr.";
        $userInformation->first_name = "Lorem";
        $userInformation->last_name = "Ipsum";
        $userInformation->address = "Kazernelaan 3";
        $userInformation->postcode = "6401 NE";
        $userInformation->place = "Utrecth";
        $userInformation->land = "NL";
        $userInformation->save();

        $userContact = new $this->userContact;
        $userContact->user_id = $user->id;
        $userContact->email = "test@gmail.com";
        $userContact->phone = "+31 97 3624";
        $userContact->mobile = "+31 97 3624";
        $userContact->website = "lorem.com";
        $userContact->is_mailing = 0;
        $userContact->save();

        $userClientArea = new $this->userClientArea;
        $userClientArea->user_id = $user->id;
        $userClientArea->username = "admin";
        $userClientArea->temporary_password = "00000000";
        $userClientArea->language = "nl_nl";
        $userClientArea->profile = 1;
        $userClientArea->send_welcome = 1;
        $userClientArea->alt_invoicing_and_payment = 1;
        $userClientArea->is_login_and_order_form = 1;
        $userClientArea->save();

        $userReportArea = new $this->userReportArea;
        $userReportArea->user_id = $user->id;
        $userReportArea->processing_agreement = "lorem ipsum dolor sit amet, consectetur adipiscing elit";
        $userReportArea->term_and_condition = "lorem ipsum dolor sit amet, consectetur adipiscing elit";
        $userReportArea->privacy_policy = "lorem ipsum dolor sit amet, consectetur adipiscing elit";
        $userReportArea->save();

        $userPayment = new $this->userPayment;
        $userPayment->user_id = $user->id;
        $userPayment->bank_account_number = "0379598772";
        $userPayment->account_holder = "Lorem Test Account Holder";
        $userPayment->bank = "Lorem Bank";
        $userPayment->bank_city = "Amsterdam";
        $userPayment->bank_code = "3311";
        $userPayment->direct_debit = 1;
        $userPayment->mandate_reference = "DB100000";
        $userPayment->mandate_sign_date = "2023-01-01";
        $userPayment->save();

        $userInformationWeFact = [
            'CompanyName' => "Test Company",
            'CompanyNumber' => "ICS 90988",
            'TaxNumber' => "IU9872231",
            'initials' => "Lorem",
            'SurName' => "ipsum",
            'Address' => "Kazernelaan 3",
            'ZipCode' => "6401 NE",
            'City' => "Utrecth",
            'Country' => "NL",
            'EmailAddress' => "test@gmail.com",
            'PhoneNumber' => "+31 97 3624",
            'MobileNumber' => "+31 97 3624",
            'FaxNumber' => "+31 97 3624",
            'mailing' => 'yes',
            'MandateID' => "",
            'MandateDate' => "2023-01-01",
            'AccountNumber' => "0379598772",
            'AccountBank' => "Lorem Bank",
            'AccountCity' => "Amsterdam",
            'AccountBIC' => "3311",
            'AccountName' => "Lorem Test Account Holder",
            'InvoiceAuthorisation' => "yes"
        ];

        $userWeFact = $this->weFactApi->sendRequest('debtor', 'add' , $userInformationWeFact);
        $user = $this->user::where('id', $user->id)->first();
        $user->wefact_user_id = $userWeFact['debtor']['DebtorCode'];
        $user->save();
    }
}
