<?php

return [
    'wefact' => [
        'periodic' => [
            'empty' => 'No subscription',
            'd' => 'Day',
            'w' => 'Week',
            'm' => 'Month',
            'k' => 'Quarter',
            'h' => 'Half year',
            'j' => 'Year',
            't' => 'Two years',
        ],
        'billing_period' => [
            'd' => 'Day',
            'w' => 'Week',
            'm' => 'Month',
            'k' => 'Quarter',
            'h' => 'Half year',
            'j' => 'Year',
            't' => 'Two years',
        ],
        'key_period' => [
            'd',
            'w',
            'm',
            'k',
            'h',
            'j',
            't'
        ],
        'period_month' => [
            'm' => 1,
            'k' => 3,
            'h' => 6,
            'j' => 12,
            't' => 24,
        ],
        'period_month_day' => [
            'm' => 1 * 30,
            'k' => 3 * 30,
            'h' => 6 * 30,
            'j' => 12 * 30,
            't' => 24 * 30,
        ],
    ],
    'hostfact' => [
        'domain_status' => [
            "domain_create" => [
                "1" => "Waiting for Action",
                "3" => "Request",
                "4" => "Active",
                "8" => "Cancelled",
            ],
            "all" => [
                "-1" => "On Process",
                "1" => "Waiting for Action",
                "3" => "Request",
                "4" => "Active",
                "5" => "Expired",
                "6" => "Pending",
                "7" => "Error occured",
                "8" => "Cancelled",
            ]
        ],
        'hosting_status' => [
            "hosting_create" => [
                "1" => "Waiting for Action",
                "3" => "Create",
                "4" => "Active",
                "9" => "Suspended",
            ],
            "all" => [
                "-1" => "On Process",
                "1" => "Waiting for Action",
                "3" => "Create",
                "4" => "Active",
                "5" => "Blocked",
                "6" => "Pending",
                "7" => "Error occured",
                "8" => "Suspended",
            ]
        ]
    ]
];
