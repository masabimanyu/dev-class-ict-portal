import $ from "jquery";

const billing = () => {
    const init = () => {
        menu()
    };

    const menu = () => {
        const el = {
            menuItem: ".js-billing-menu-item",
            colForm: '.js-billing-forms'
        };

        if ($(el.menuItem).length === 0) return;
        $(el.menuItem).on('click', function(){
            // Check for active menu
            $(el.menuItem).removeClass('active');
            $(this).addClass('active');

            // Display active tab
            let currentTab = $(this).attr('data-target');
            
            $(el.colForm).removeClass('active');
            $(`#${currentTab}`).addClass('active');
        })
    };

    init();
};

export default billing;
