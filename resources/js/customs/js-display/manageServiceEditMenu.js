import $ from "jquery";

const manageServiceEditMenu = () => {
    const init = () => {
        menu()
    };

    const menu = () => {
        const el = {
            menuItem: ".js-menu-item",
            colContent: '.js-manage-service-edit-menu-content'
        };

        if ($(el.menuItem).length === 0) return;
        $(el.menuItem).on('click', function(){
            // Check for active menu
            $(el.menuItem).removeClass('active');
            $(this).addClass('active');

            // Display active tab
            let currentTab = $(this).attr('data-target');
            
            $(el.colContent).removeClass('active');
            $(`#${currentTab}`).addClass('active');

        })
    };

    init();
};

export default manageServiceEditMenu;
