@extends('layouts.auth')
@section('content')
    <form class="form" action="{{ route('password.email') }}" method="post">
        <h1 class="h2">{{ __('auth.forgot.title') }}</h1>

        @csrf

        <div class="form-group">
            <label for="text">{{ __('auth.forgot.label_username') }}</label>
            <input id="text" type="text" name="username" placeholder="{{ __('auth.forgot.placeholder_type_here') }}">

            @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="email">{{ __('auth.forgot.label_email') }}</label>
            <input id="email" type="email" name="email"
                placeholder="{{ __('auth.forgot.placeholder_type_here') }}">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group">
            <a href="{{ route('login') }}"
                class="form-group__link">{{ __('auth.forgot.login') }}</a>
        </div>

        <button class="btn btn--primary" type="submit">{{ __('auth.forgot.button_label') }}</button>
    </form>
@endsection

