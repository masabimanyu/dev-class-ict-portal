@extends('layouts.auth')
@section('content')
    <form class="form" action="{{ route('login') }}" method="post">
        <h1 class="h2">{{ __('auth.login.title') }}</h1>

        @csrf

        <div class="form-group">
            <label for="email">{{ __('auth.login.label_email') }}</label>
            <input id="email" type="email" name="email" placeholder="{{ __('auth.login.placeholder_type_here') }}">

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input id="password" type="password" name="password"
                placeholder="{{ __('auth.login.placeholder_type_here') }}">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        @if (Route::has('password.request'))
            <div class="form-group">
                <a href="{{ route('password.request') }}"
                    class="form-group__link">{{ __('auth.login.forget_password') }}</a>
            </div>
        @endif

        <div class="form-group checkbox">
            <label for="remember_me">{{ __('auth.login.remember_me') }}</label>
            <input type="checkbox" name="remember_me" id="remember_me" placeholder="Typ hier"
                {{ old('remember') ? 'checked' : '' }}>
        </div>
        <button class="btn btn--primary" type="submit">{{ __('auth.login.button_label') }}</button>
    </form>
@endsection
