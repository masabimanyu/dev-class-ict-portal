<nav class="navbar">

    <div class="navbar-content-start">
        <img src="{{ asset('/assets/images/ict-sb-logo.webp') }}" alt="">
    </div>

    <div class="navbar-content-end">
        <div class="navbar-lang">
            <div class="lang">
                <div class="lang__select">
                    <ul class="lang__select-list js-lang-list">
                        <li class="active"><img src="{{ asset('/assets/images/nl.svg') }}" alt="Lang NL" width="32"
                                height="22"> NL
                        </li>
                        <li><img src="{{ asset('/assets/images/en.svg') }}" alt="Lang EN" width="32" height="22"> EN
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="navbar-log-out">
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <div class="icon">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.25 2.25V1.5H1.5V2.25H2.25ZM9.21967 10.2803C9.51256 10.5732 9.98744 10.5732 10.2803 10.2803C10.5732 9.98744 10.5732 9.51256 10.2803 9.21967L9.21967 10.2803ZM3 8.25V2.25H1.5V8.25H3ZM2.25 3H8.25V1.5H2.25V3ZM1.71967 2.78033L9.21967 10.2803L10.2803 9.21967L2.78033 1.71967L1.71967 2.78033Z" fill="currentColor"/>
                        <path d="M3 11.25V11.25C3 11.947 3 12.2955 3.05764 12.5853C3.29436 13.7753 4.22466 14.7056 5.41473 14.9424C5.70453 15 6.05302 15 6.75 15H9C11.8284 15 13.2426 15 14.1213 14.1213C15 13.2426 15 11.8284 15 9V6.75C15 6.05302 15 5.70453 14.9424 5.41473C14.7056 4.22466 13.7753 3.29436 12.5853 3.05764C12.2955 3 11.947 3 11.25 3V3" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>
                {{ __('components.navbar.logout') }}
            </a>
        </div>
        <div class="navbar-hamburger">
            <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                    <path d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0
                    76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837
                    0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837
                    0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"/>
                </svg>
            </div>
        </div>
    </div>
</nav>
