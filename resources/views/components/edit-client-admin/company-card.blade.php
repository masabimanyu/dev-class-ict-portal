<div class="col col-card-company">
    <div class="card-company-wrapper">
        <svg class="icon-top" width="68" height="57" viewBox="0 0 68 57" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M12.6897 31.374C12.2234 30.5215 11.9788 29.5623 11.9788 28.5871C11.9788 27.612 12.2234 26.6528 12.6897 25.8003L23.6794 5.70965C24.169 4.81435 24.8856 4.06837 25.7549 3.54907C26.6242 3.02976 27.6144 2.75606 28.623 2.75633L50.2348 2.75633C51.2429 2.75657 52.2326 3.03052 53.1013 3.54979C53.9701 4.06907 54.6862 4.81477 55.1757 5.70965L66.171 25.8003C66.6372 26.6528 66.8819 27.612 66.8819 28.5871C66.8819 29.5623 66.6372 30.5215 66.171 31.374L55.1757 51.4646C54.6865 52.3591 53.9707 53.1045 53.1026 53.6238C52.2344 54.143 51.2453 54.4172 50.2377 54.418L28.623 54.418C27.6144 54.4182 26.6242 54.1445 25.7549 53.6252C24.8856 53.1059 24.169 52.3599 23.6794 51.4646L12.6897 31.374Z" stroke="url(#paint0_linear_1212_31030)" stroke-width="2"/>
            <path d="M0.762659 31.565C0.262474 30.654 -3.11136e-07 29.629 -3.23343e-07 28.5869C-3.35551e-07 27.5448 0.262474 26.5198 0.762659 25.6088L12.5516 4.13938C13.0768 3.18264 13.8456 2.38546 14.7781 1.83052C15.7105 1.27558 16.7728 0.983089 17.8547 0.983375L41.0383 0.983375C42.1197 0.983634 43.1814 1.27639 44.1133 1.8313C45.0452 2.38621 45.8135 3.18309 46.3385 4.13938L58.1334 25.6088C58.6336 26.5198 58.8961 27.5448 58.8961 28.5869C58.8961 29.629 58.6336 30.654 58.1334 31.565L46.3385 53.0344C45.8137 53.9903 45.0459 54.7868 44.1146 55.3417C43.1833 55.8966 42.1223 56.1896 41.0414 56.1904L17.8547 56.1904C16.7728 56.1907 15.7105 55.8982 14.7781 55.3433C13.8456 54.7883 13.0768 53.9912 12.5516 53.0344L0.762659 31.565Z" fill="url(#paint1_linear_1212_31030)"/>
            <path d="M26.9819 27.1749H27.902C28.1782 27.1749 28.402 27.3987 28.402 27.6749V33.8284C28.402 34.1046 28.6259 34.3284 28.902 34.3284H29.8222M28.4021 24.3135H28.4163" stroke="#E6007E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M30.3749 40.5924C31.8441 40.3314 33.248 39.7814 34.5063 38.9738C35.7645 38.1662 36.8526 37.1168 37.7083 35.8856C38.5641 34.6544 39.1707 33.2654 39.4936 31.7979C39.8166 30.3305 39.8494 28.8134 39.5903 27.3331C39.3313 25.8529 38.7853 24.4386 37.9837 23.1709C37.1821 21.9032 36.1405 20.807 34.9184 19.9449C33.6963 19.0828 32.3176 18.4716 30.861 18.1463C29.4044 17.821 27.8985 17.7879 26.4293 18.0489C24.96 18.3099 23.5562 18.8599 22.2979 19.6675C21.0396 20.4751 19.9515 21.5245 19.0958 22.7557C18.2401 23.9869 17.6334 25.3759 17.3105 26.8434C16.9876 28.3108 16.9547 29.8279 17.2138 31.3082C17.4729 32.7884 18.0188 34.2027 18.8204 35.4704C19.622 36.7381 20.6636 37.8343 21.8857 38.6964C23.1079 39.5585 24.4866 40.1697 25.9431 40.495C27.3997 40.8203 28.9056 40.8534 30.3749 40.5924L30.3749 40.5924Z" stroke="#E6007E" stroke-width="2"/>
            <defs>
            <linearGradient id="paint0_linear_1212_31030" x1="11.9788" y1="28.5871" x2="66.8819" y2="28.5871" gradientUnits="userSpaceOnUse">
            <stop stop-color="white" stop-opacity="0"/>
            <stop offset="1" stop-color="white"/>
            </linearGradient>
            <linearGradient id="paint1_linear_1212_31030" x1="-3.23343e-07" y1="28.5869" x2="58.8961" y2="28.5869" gradientUnits="userSpaceOnUse">
            <stop stop-color="white" stop-opacity="0.22"/>
            <stop offset="1" stop-color="white"/>
            </linearGradient>
            </defs>
        </svg>

        <h2 class="title">{{ $user['CompanyName'] ? $user['CompanyName'] : '' }}</h2>
        <p class="name">{{ $userDB->userInformation->user_legal_form }} {{ $user['Initials'] != "" ? $user['Initials'] : '' }}  {{ $user["SurName"] != "" ? $user["SurName"] : '' }} </p>
        <p class="address">{{ $user['Address'] != "" ? $user['Address'] : '' }}</p>
        <a class="email" href="mailto: yourname@domain.com">
            <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.41807 7L12.3719 12.5978L19.3257 7H5.41807ZM4.5 7.73193V17.3679H20.244V7.73193L12.732 13.7799L12.7321 13.7801C12.6299 13.8618 12.503 13.9064 12.3721 13.9064C12.2413 13.9064 12.1143 13.8618 12.0121 13.7801L4.5 7.73193Z" fill="white"/>
            </svg>
            {{ $user["EmailAddress"] }}
        </a>
        <a class="telp" href="tel: 0619007293">
            <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16.155 3.11133H8.85902C8.16782 3.11133 7.61108 3.66806 7.61108 4.35926V19.9113C7.61108 20.6025 8.16782 21.1592 8.85902 21.1592H16.155C16.8462 21.1592 17.403 20.6025 17.403 19.9113V4.35926C17.403 3.66806 16.8461 3.11133 16.1549 3.11133H16.155ZM12.123 4.64736H12.891C13.1597 4.64736 13.371 4.85857 13.371 5.12736C13.371 5.39615 13.1597 5.60736 12.891 5.60736H12.123C11.8542 5.60736 11.643 5.39615 11.643 5.12736C11.643 4.85857 11.8542 4.64736 12.123 4.64736ZM13.275 19.4314H11.7389C11.4701 19.4314 11.2589 19.2201 11.2589 18.9514C11.259 18.6826 11.4703 18.4714 11.7391 18.4714H13.2751C13.5439 18.4714 13.7551 18.6826 13.7551 18.9514C13.755 19.2201 13.5437 19.4314 13.275 19.4314ZM16.443 17.3193H8.57102V6.56729H16.443V17.3193Z" fill="white"/>
            </svg>
            {{ $user["PhoneNumber"] }}
        </a>
        <div class="bg-icon-tr">
            <svg width="126" height="170" viewBox="0 0 126 170" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle cx="146.317" cy="80.0313" r="89.0335" transform="rotate(54.9163 146.317 80.0313)" fill="url(#paint0_linear_1220_42433)"/>
                <rect x="1" y="-1" width="173" height="148" rx="38" transform="matrix(1 -8.74228e-08 -8.74228e-08 -1 58 67)" stroke="url(#paint1_radial_1220_42433)" stroke-width="2"/>
                <circle cx="89.5469" cy="-6.45321" r="89.0335" transform="rotate(-7.24205 89.5469 -6.45321)" fill="url(#paint2_linear_1220_42433)"/>
                <rect x="88" y="25" width="160" height="137" rx="38" stroke="url(#paint3_linear_1220_42433)" stroke-width="2"/>
                <circle r="3" transform="matrix(-1 0 0 1 97 38)" fill="#FFC700"/>
                <circle r="3" transform="matrix(-1 0 0 1 117 68)" fill="#8CB0FF"/>
                <defs>
                <linearGradient id="paint0_linear_1220_42433" x1="178.962" y1="-167.037" x2="146.317" y2="169.065" gradientUnits="userSpaceOnUse">
                <stop stop-color="white"/>
                <stop offset="1" stop-color="white" stop-opacity="0"/>
                </linearGradient>
                <radialGradient id="paint1_radial_1220_42433" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(22.5 -21) rotate(76.5571) scale(105.387 555.808)">
                <stop stop-color="#8B9BFF"/>
                <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                </radialGradient>
                <linearGradient id="paint2_linear_1220_42433" x1="122.192" y1="-253.521" x2="89.5469" y2="82.5802" gradientUnits="userSpaceOnUse">
                <stop stop-color="white"/>
                <stop offset="1" stop-color="white" stop-opacity="0"/>
                </linearGradient>
                <linearGradient id="paint3_linear_1220_42433" x1="72.8022" y1="-1.4712" x2="160.317" y2="79.7137" gradientUnits="userSpaceOnUse">
                <stop stop-color="#FFC700"/>
                <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                </linearGradient>
                </defs>
            </svg>
        </div>
        <div class="bg-icon-br">
            <svg width="130" height="145" viewBox="0 0 130 145" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="34.9381" y="120.074" width="162.288" height="138.836" rx="35.6471" stroke="url(#paint0_linear_1220_43608)" stroke-width="1.87616"/>
                <circle cx="89.7283" cy="196.728" r="89.0335" transform="rotate(137.318 89.7283 196.728)" fill="url(#paint1_linear_1220_43608)"/>
                <rect x="95.9132" y="136.022" width="162.288" height="138.836" rx="35.6471" stroke="url(#paint2_linear_1220_43608)" stroke-width="1.87616"/>
                <path d="M127.808 0V85.4058C127.808 95.7676 119.408 104.167 109.046 104.167H99.6657C89.3039 104.167 80.9041 112.567 80.9041 122.929V303" stroke="url(#paint3_radial_1220_43608)" stroke-width="1.87616"/>
                <circle cx="118.427" cy="101.312" r="2.81424" fill="#8CB0FF"/>
                <circle cx="75.2757" cy="120.074" r="2.81424" fill="#FFC700"/>
                <defs>
                <linearGradient id="paint0_linear_1220_43608" x1="19.6126" y1="93.3508" x2="108.212" y2="175.627" gradientUnits="userSpaceOnUse">
                <stop stop-color="#FFC700"/>
                <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                </linearGradient>
                <linearGradient id="paint1_linear_1220_43608" x1="122.374" y1="-50.3399" x2="89.7283" y2="285.761" gradientUnits="userSpaceOnUse">
                <stop stop-color="white"/>
                <stop offset="1" stop-color="white" stop-opacity="0"/>
                </linearGradient>
                <linearGradient id="paint2_linear_1220_43608" x1="80.5877" y1="109.299" x2="169.187" y2="191.575" gradientUnits="userSpaceOnUse">
                <stop stop-color="#FFC700"/>
                <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                </linearGradient>
                <radialGradient id="paint3_radial_1220_43608" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(104.356 151.5) rotate(90) scale(151.5 106.016)">
                <stop stop-color="#8B9BFF"/>
                <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                </radialGradient>
                </defs>
            </svg>
        </div>
    </div>
</div>