<div class="container-card-1 client-area-container">
    <h2>{{ __('Report Area') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="#processing_agreement">{{ __('Verwerkersovereenkomst') }}</label>
            <input name="processing_agreement" id="processing_agreement" value={{ $userDB->userReportArea->processing_agreement }}>
            {{-- <div class="select-wrapper">
                <select name="processing_agreement" id="processing_agreement">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="true" {{ $userDB->userReportArea->processing_agreement == 1 ? 'selected' : ''}} >Yes</option>
                    <option value="false" {{ $userDB->userReportArea->processing_agreement == 0 ? 'selected' : ''}} >No</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div> --}}
        </div>
        <div class="form-group w-1/2">
            <label for="#term_and_condition">{{ __('Algemene Voorwaarden') }}</label>
            <input name="term_and_condition" id="term_and_condition" value="{{ $userDB->userReportArea->term_and_condition }}" >
            {{-- <div class="select-wrapper">
                <select name="term_and_condition" id="term_and_condition">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="true" {{ $userDB->userReportArea->term_and_condition == 1 ? 'selected' : ''}} >Yes</option>
                    <option value="false" {{ $userDB->userReportArea->term_and_condition == 0 ? 'selected' : ''}} >No</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div> --}}
        </div>
        <div class="form-group w-1/2">
            <label for="#privacy_policy">{{ __('Privacy Policy') }}</label>
            <input name="privacy_policy" id="privacy_policy" value="{{ $userDB->userReportArea->privacy_policy }}">
            {{-- <div class="select-wrapper">
                <select name="privacy_policy" id="privacy_policy">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="true" {{ $userDB->userReportArea->privacy_policy == 1 ? 'selected' : ''}} >Send</option>
                    <option value="false" {{ $userDB->userReportArea->privacy_policy == 0 ? 'selected' : ''}}>Do not send</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div> --}}
        </div>
    </div>
</div>
