<div class="container-card-1 invoicing-and-payment-container">
    <h2>{{ __('Invoicing and payment') }}</h2>
    <div class="input-container form">
        <div class="form-group checkbox">
            <label for="#alt_invoicing_and_payment">{{ __('Alternate Invoice Address') }}</label>
            <input
                type="checkbox"
                name="alt_invoicing_and_payment"
                id="alt_invoicing_and_payment"
                {{ $userDB->userClientArea->alt_invoicing_and_payment == 1 ? 'checked' : '' }}
                >
        </div>
    </div>
</div>