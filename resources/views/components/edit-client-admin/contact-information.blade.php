<div class="container-card-1 contact-information-container">
    <h2>{{ __('Contact information') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="#email">{{ __('E-mail address') }}</label>
            <input type="email" name="email" id="email" value=" {{ $user["EmailAddress"] != "" ? $user["EmailAddress"] : "" }} " >
        </div>
        <div class="form-group w-1/2">
            <label for="#phone_number">{{ __('Phone number') }}</label>
            <input type="text" name="phone_number" id="phone_number" value=" {{ $user["PhoneNumber"] != "" ? $user["PhoneNumber"] : "" }} " >
        </div>
        <div class="form-group w-1/2">
            <label for="#mobile_number">{{ __('Mobile number') }}</label>
            <input type="text" name="mobile_number" id="mobile_number" value=" {{ $user["MobileNumber"] != "" ? $user["MobileNumber"] : "" }} ">
        </div>
        <div class="form-group w-1/2">
            <label for="#fax_number">{{ __('Fax number') }}</label>
            <input type="text" name="fax_number" id="fax_number" value="{{ $user["FaxNumber"] != "" ? $user["FaxNumber"] : "" }}" >
        </div>
        <div class="form-group w-1/2">
            <label for="#website">{{ __('Website') }}</label>
            <input type="text" name="website" id="website" value="{{ $userDB->userContact->website != "" ? $userDB->userContact->website : "" }}" >
        </div>
        <div class="form-group w-1/2">
            <label for="#receive_mailing">{{ __('Receive mailing') }}</label>
            <div class="select-wrapper">
                <select name="receive_mailing" id="receive_mailing">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="0" {{ $user["Mailing"] == "no" ? "selected" : "" }} >{{ __('Not received') }}</option>
                    <option value="1" {{ $user["Mailing"] == "yes" ? "selected" : "" }}  >{{ __('Well received') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
    </div>
</div>
