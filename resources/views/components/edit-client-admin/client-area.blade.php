<div class="container-card-1 client-area-container">
    <h2>{{ __('Client Area') }}</h2>
    <div class="input-container form">
        <div class="form-group checkbox">
            <label for="#is_login_and_order_form">{{ __('Activate login for client area and order form') }}</label>
            <input
                type="checkbox"
                name="is_login_and_order_form"
                id="is_login_and_order_form"
                {{ $userDB->userClientArea->is_login_and_order_form == 1 ? 'checked' : '' }}
                >
        </div>
        <div class="form-group w-1/2">
            <label for="#username">{{ __('Username') }}</label>
            <input type="text" name="username" id="username" value="{{ $userDB->username != ""  ? $userDB->username : "" }}">
        </div>
        <div class="form-group w-1/2">
            <label for="#temporary_password">{{ __('Temporary password') }}</label>
            <input type="password" name="temporary_password" id="temporary_password" value="" >
        </div>
        <div class="form-group w-1/2">
            <label for="#language">{{ __('Language') }}</label>
            <div class="select-wrapper">
                <select name="language" id="language">
                    <option value="">{{ __('Choose here') }}</option>
                    @if(count($languageCodes) > 0)
                        @foreach ($languageCodes as $languageCode)
                            <option value='{{ $languageCode['LanguageCode'] }}' {{ $user['LanguageCode'] == $languageCode['LanguageCode'] ? 'selected' : '' }} >{{ $languageCode['Name'] }}</option>
                        @endforeach
                    @else
                        <option value="">{{ __('No Country Available') }}</option>
                    @endif
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
        <div class="form-group w-1/2">
            <label for="#language">{{ __('Profile') }}</label>
            <div class="select-wrapper">
                <select name="profile" id="profile">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="true" {{ $userDB->userClientArea->profile == 1 ? 'selected' : '' }} >Yes</option>
                    <option value="false" {{ $userDB->userClientArea->profile == 0 ? 'selected' : '' }} >No</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
        <div class="form-group w-1/2">
            <label for="#send_welcome_page">{{ __('Send welcome message') }}</label>
            <div class="select-wrapper">
                <select name="send_welcome_page" id="send_welcome_page">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="true" {{ $userDB->userClientArea->send_welcome == 1 ? 'selected' : ''}} >Send</option>
                    <option value="false" {{ $userDB->userClientArea->send_welcome == 0 ? 'selected' : ''}} >Do not send</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
    </div>
</div>
