<div class="container-card-1 direct-debit">
    <h2>{{ __('Direct Debit') }}</h2>
    <div class="input-container form">
        <div class="form-group checkbox">
            <label for="#direct_debit">{{ __('This client wants to pay by direct debit') }}</label>
            <input type="checkbox" name="direct_debit" id="direct_debit" {{ $user["InvoiceAuthorisation"] == "yes" ? 'checked' : '' }} >
        </div>
        <div class="form-group">
            <label for="#mandate_reference">{{ __('Mandate reference') }}</label>
            <input type="text" name="mandate_reference" id="mandate_reference" value="{{ $user["MandateID"] }}" >
        </div>
        <div class="form-group">
            <label for="#mandate_sign_date">{{ __('Mandate sign date') }}</label>
            <input type="datetime-local" name="mandate_sign_date" id="mandate_sign_date"  value="{{ $user["MandateDate"] }}" >
        </div>
    </div>
</div>