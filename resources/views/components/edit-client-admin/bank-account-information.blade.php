<div class="container-card-1 bank-account-information-container">
    <h2>{{ __('Bank Account Information') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="#bank_account_number">{{ __('Bank account number') }}</label>
            <input type="number" name="bank_account_number" id="bank_account_number" value="{{ $user["AccountNumber"] }}" >
        </div>
        <div class="form-group w-1/2">
            <label for="#account_holder">{{ __('Account Holder') }}</label>
            <input type="text" name="account_holder" id="account_holder" value="{{ $user["AccountNumber"] }}">
        </div>
        <div class="form-group w-1/2">
            <label for="#bank">{{ __('Bank') }}</label>
            <input type="text" name="bank" id="bank" value="{{ $user["AccountBank"] }}">
        </div>
        <div class="form-group w-1/2">
            <label for="#bank_city">{{ __('Land') }}</label>
            <input type="text" name="bank_city" id="bank_city" value="{{ $user["AccountCity"] }}">
            {{-- <div class="select-wrapper">
                <select name="bank_city" id="bank_city">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="NL" {{ $user["AccountCity"] == "NL" ? 'selected' : ''}}>Nederland</option>
                    <option value="BE" {{ $user["AccountCity"] == "BE" ? 'selected' : '' }} >Belgie</option>
                    <option value="BG" {{ $user["AccountCity"] == "BG" ? 'selected' : '' }} >Bulgarije</option>
                    <option value="CY" {{ $user["AccountCity"] == "CY" ? 'selected' : '' }} >Cyprus</option>
                    <option value="DK" {{ $user["AccountCity"] == "DK" ? 'selected' : '' }} >Denemarken</option>
                    <option value="THE" {{ $user["AccountCity"] == "THE" ? 'selected' : '' }} >Duitsland</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div> --}}
        </div>
        <div class="form-group w-1/2">
            <label for="#bank_code">{{ __('Bank Code (BIC)') }}</label>
            <input type="text" name="bank_code" id="bank_code" value="{{ $user["AccountIban"] }}" >
        </div>
    </div>
</div>
