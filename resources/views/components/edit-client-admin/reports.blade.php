<div class="container-card reports">

    <div class="row row-1">
        <div class="col container-card-2">
            <div class="container-card-2-wrapper">
                <svg class="icon-top" width="68" height="57" viewBox="0 0 68 57" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.5535 29.6865C11.129 28.8813 10.9062 27.9754 10.9062 27.0544C10.9062 26.1334 11.129 25.2276 11.5535 24.4224L21.559 5.44789C22.0048 4.60233 22.6573 3.8978 23.4487 3.40735C24.2401 2.91689 25.1417 2.65839 26.06 2.65865L45.7364 2.65864C46.6542 2.65887 47.5553 2.91761 48.3462 3.40803C49.1371 3.89846 49.7892 4.60274 50.2348 5.44789L60.2454 24.4224C60.6699 25.2276 60.8927 26.1334 60.8927 27.0544C60.8927 27.9754 60.6699 28.8813 60.2454 29.6865L50.2348 48.6609C49.7894 49.5057 49.1378 50.2097 48.3473 50.7001C47.5569 51.1905 46.6564 51.4495 45.739 51.4502L26.06 51.4502C25.1417 51.4504 24.2401 51.1919 23.4487 50.7015C22.6573 50.211 22.0048 49.5065 21.559 48.6609L11.5535 29.6865Z" stroke="url(#paint0_linear_1608_40591)" stroke-width="2"/>
                    <path d="M0.694361 29.8672C0.238969 29.0067 -2.83273e-07 28.0387 -2.94387e-07 27.0545C-3.05501e-07 26.0703 0.238969 25.1023 0.694361 24.2419L11.4275 3.96519C11.9058 3.0616 12.6057 2.30871 13.4546 1.7846C14.3036 1.26049 15.2708 0.984248 16.2558 0.984521L37.3633 0.98452C38.3478 0.984765 39.3144 1.26125 40.1629 1.78534C41.0113 2.30942 41.7108 3.06203 42.1888 3.96519L52.9275 24.2419C53.3829 25.1023 53.6218 26.0703 53.6218 27.0545C53.6218 28.0387 53.3829 29.0067 52.9275 29.8672L42.1888 50.1438C41.711 51.0466 41.012 51.7989 40.164 52.323C39.3161 52.847 38.3501 53.1238 37.366 53.1245L16.2558 53.1245C15.2708 53.1248 14.3036 52.8485 13.4546 52.3244C12.6057 51.8003 11.9058 51.0474 11.4275 50.1438L0.694361 29.8672Z" fill="url(#paint1_linear_1608_40591)"/>
                    <rect x="14.2222" y="19.6401" width="23.2728" height="16.2147" rx="2" stroke="white" stroke-width="2"/>
                    <path d="M16.8081 23.6938H20.6869" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <path d="M31.0303 31.8013H34.9091" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <ellipse cx="25.8583" cy="27.7474" rx="2.58587" ry="2.70245" stroke="white" stroke-width="2"/>
                    <defs>
                    <linearGradient id="paint0_linear_1608_40591" x1="62.5" y1="25" x2="14" y2="30.5" gradientUnits="userSpaceOnUse">
                    <stop stop-color="white"/>
                    <stop offset="1" stop-color="white" stop-opacity="0"/>
                    </linearGradient>
                    <linearGradient id="paint1_linear_1608_40591" x1="-24.6396" y1="61.5697" x2="52.7587" y2="22.5869" gradientUnits="userSpaceOnUse">
                    <stop offset="0.160122" stop-color="white" stop-opacity="0"/>
                    <stop offset="1" stop-color="white"/>
                    </linearGradient>
                    </defs>
                </svg>
        
               <p class="title">Outstanding amount</p>  
               <p class="amount">€ 199,65 incl. tax</p>  
                <div class="bg-icon-tl">
                    <svg width="96" height="161" viewBox="0 0 96 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="49" y="33" width="173" height="148" rx="38" transform="rotate(-180 49 33)" stroke="url(#paint0_radial_1608_40600)" stroke-width="2"/>
                        <rect x="-1" y="1" width="160" height="137" rx="38" transform="matrix(-1 0 0 1 19 -11)" stroke="url(#paint1_linear_1608_40600)" stroke-width="2"/>
                        <circle cx="11" cy="3" r="3" fill="#FFC700"/>
                        <circle cx="11" cy="33" r="3" fill="#8CB0FF"/>
                        <circle cx="29.6404" cy="171.64" r="47.8707" transform="rotate(120.833 29.6404 171.64)" fill="url(#paint2_linear_1608_40600)" fill-opacity="0.5"/>
                        <defs>
                        <radialGradient id="paint0_radial_1608_40600" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(72.5 13) rotate(76.5571) scale(105.387 555.808)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint1_linear_1608_40600" x1="-14.1978" y1="-25.4712" x2="73.3171" y2="55.7137" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint2_linear_1608_40600" x1="47.193" y1="38.7987" x2="29.6404" y2="219.511" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>
                </div>
                <div class="bg-icon-br">
                    <svg width="87" height="161" viewBox="0 0 87 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="1" y="131" width="173" height="148" rx="38" stroke="url(#paint0_linear_454_24505)" stroke-width="2"/>
                        <rect x="68" y="85" width="173" height="148" rx="38" stroke="url(#paint1_linear_454_24505)" stroke-width="2"/>
                        <path d="M100 -31V60.0432C100 71.0889 91.0457 80.0432 80 80.0432H70C58.9543 80.0432 50 88.9975 50 100.043V292" stroke="url(#paint2_radial_454_24505)" stroke-width="2"/>
                        <circle cx="50" cy="119" r="3" fill="#8CB0FF"/>
                        <circle cx="44" cy="131" r="3" fill="#FFC700"/>
                        <circle cx="77" cy="98" r="3" fill="#FFC700"/>
                        <circle cx="99.5395" cy="39.5394" r="47.8707" transform="rotate(14.8479 99.5395 39.5394)" fill="url(#paint3_linear_454_24505)" fill-opacity="0.5"/>
                        <circle cx="65.6404" cy="-2.35872" r="47.8707" transform="rotate(-59.1673 65.6404 -2.35872)" fill="url(#paint4_linear_454_24505)" fill-opacity="0.5"/>
                        <defs>
                        <linearGradient id="paint0_linear_454_24505" x1="-15.3371" y1="102.513" x2="79.1106" y2="190.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint1_linear_454_24505" x1="51.6629" y1="56.5131" x2="146.111" y2="144.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <radialGradient id="paint2_radial_454_24505" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(75 130.5) rotate(90) scale(161.5 113.013)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint3_linear_454_24505" x1="117.092" y1="-93.3019" x2="99.5395" y2="87.4101" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint4_linear_454_24505" x1="83.193" y1="-135.2" x2="65.6404" y2="45.512" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>                        
                </div>
            </div>
        </div>
        <div class="col container-card-2">
            <div class="container-card-2-wrapper">
                <svg class="icon-top" width="68" height="57" viewBox="0 0 68 57" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.5535 29.6865C11.129 28.8813 10.9062 27.9754 10.9062 27.0544C10.9062 26.1334 11.129 25.2276 11.5535 24.4224L21.559 5.44789C22.0048 4.60233 22.6573 3.8978 23.4487 3.40735C24.2401 2.91689 25.1417 2.65839 26.06 2.65865L45.7364 2.65864C46.6542 2.65887 47.5553 2.91761 48.3462 3.40803C49.1371 3.89846 49.7892 4.60274 50.2348 5.44789L60.2454 24.4224C60.6699 25.2276 60.8927 26.1334 60.8927 27.0544C60.8927 27.9754 60.6699 28.8813 60.2454 29.6865L50.2348 48.6609C49.7894 49.5057 49.1378 50.2097 48.3473 50.7001C47.5569 51.1905 46.6564 51.4495 45.739 51.4502L26.06 51.4502C25.1417 51.4504 24.2401 51.1919 23.4487 50.7015C22.6573 50.211 22.0048 49.5065 21.559 48.6609L11.5535 29.6865Z" stroke="url(#paint0_linear_1608_40591)" stroke-width="2"/>
                    <path d="M0.694361 29.8672C0.238969 29.0067 -2.83273e-07 28.0387 -2.94387e-07 27.0545C-3.05501e-07 26.0703 0.238969 25.1023 0.694361 24.2419L11.4275 3.96519C11.9058 3.0616 12.6057 2.30871 13.4546 1.7846C14.3036 1.26049 15.2708 0.984248 16.2558 0.984521L37.3633 0.98452C38.3478 0.984765 39.3144 1.26125 40.1629 1.78534C41.0113 2.30942 41.7108 3.06203 42.1888 3.96519L52.9275 24.2419C53.3829 25.1023 53.6218 26.0703 53.6218 27.0545C53.6218 28.0387 53.3829 29.0067 52.9275 29.8672L42.1888 50.1438C41.711 51.0466 41.012 51.7989 40.164 52.323C39.3161 52.847 38.3501 53.1238 37.366 53.1245L16.2558 53.1245C15.2708 53.1248 14.3036 52.8485 13.4546 52.3244C12.6057 51.8003 11.9058 51.0474 11.4275 50.1438L0.694361 29.8672Z" fill="url(#paint1_linear_1608_40591)"/>
                    <rect x="14.2222" y="19.6401" width="23.2728" height="16.2147" rx="2" stroke="white" stroke-width="2"/>
                    <path d="M16.8081 23.6938H20.6869" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <path d="M31.0303 31.8013H34.9091" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <ellipse cx="25.8583" cy="27.7474" rx="2.58587" ry="2.70245" stroke="white" stroke-width="2"/>
                    <defs>
                    <linearGradient id="paint0_linear_1608_40591" x1="62.5" y1="25" x2="14" y2="30.5" gradientUnits="userSpaceOnUse">
                    <stop stop-color="white"/>
                    <stop offset="1" stop-color="white" stop-opacity="0"/>
                    </linearGradient>
                    <linearGradient id="paint1_linear_1608_40591" x1="-24.6396" y1="61.5697" x2="52.7587" y2="22.5869" gradientUnits="userSpaceOnUse">
                    <stop offset="0.160122" stop-color="white" stop-opacity="0"/>
                    <stop offset="1" stop-color="white"/>
                    </linearGradient>
                    </defs>
                </svg>
        
               <p class="title">Payment behavior</p>  
               <p class="amount">13 days average</p>  
                <div class="bg-icon-tl">
                    <svg width="96" height="161" viewBox="0 0 96 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="49" y="33" width="173" height="148" rx="38" transform="rotate(-180 49 33)" stroke="url(#paint0_radial_1608_40600)" stroke-width="2"/>
                        <rect x="-1" y="1" width="160" height="137" rx="38" transform="matrix(-1 0 0 1 19 -11)" stroke="url(#paint1_linear_1608_40600)" stroke-width="2"/>
                        <circle cx="11" cy="3" r="3" fill="#FFC700"/>
                        <circle cx="11" cy="33" r="3" fill="#8CB0FF"/>
                        <circle cx="29.6404" cy="171.64" r="47.8707" transform="rotate(120.833 29.6404 171.64)" fill="url(#paint2_linear_1608_40600)" fill-opacity="0.5"/>
                        <defs>
                        <radialGradient id="paint0_radial_1608_40600" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(72.5 13) rotate(76.5571) scale(105.387 555.808)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint1_linear_1608_40600" x1="-14.1978" y1="-25.4712" x2="73.3171" y2="55.7137" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint2_linear_1608_40600" x1="47.193" y1="38.7987" x2="29.6404" y2="219.511" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>
                </div>
                <div class="bg-icon-br">
                    <svg width="87" height="161" viewBox="0 0 87 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="1" y="131" width="173" height="148" rx="38" stroke="url(#paint0_linear_454_24505)" stroke-width="2"/>
                        <rect x="68" y="85" width="173" height="148" rx="38" stroke="url(#paint1_linear_454_24505)" stroke-width="2"/>
                        <path d="M100 -31V60.0432C100 71.0889 91.0457 80.0432 80 80.0432H70C58.9543 80.0432 50 88.9975 50 100.043V292" stroke="url(#paint2_radial_454_24505)" stroke-width="2"/>
                        <circle cx="50" cy="119" r="3" fill="#8CB0FF"/>
                        <circle cx="44" cy="131" r="3" fill="#FFC700"/>
                        <circle cx="77" cy="98" r="3" fill="#FFC700"/>
                        <circle cx="99.5395" cy="39.5394" r="47.8707" transform="rotate(14.8479 99.5395 39.5394)" fill="url(#paint3_linear_454_24505)" fill-opacity="0.5"/>
                        <circle cx="65.6404" cy="-2.35872" r="47.8707" transform="rotate(-59.1673 65.6404 -2.35872)" fill="url(#paint4_linear_454_24505)" fill-opacity="0.5"/>
                        <defs>
                        <linearGradient id="paint0_linear_454_24505" x1="-15.3371" y1="102.513" x2="79.1106" y2="190.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint1_linear_454_24505" x1="51.6629" y1="56.5131" x2="146.111" y2="144.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <radialGradient id="paint2_radial_454_24505" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(75 130.5) rotate(90) scale(161.5 113.013)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint3_linear_454_24505" x1="117.092" y1="-93.3019" x2="99.5395" y2="87.4101" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint4_linear_454_24505" x1="83.193" y1="-135.2" x2="65.6404" y2="45.512" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>                        
                </div>
            </div>
        </div>
        <div class="col container-card-2">
            <div class="container-card-2-wrapper">
                <svg class="icon-top" width="68" height="57" viewBox="0 0 68 57" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.5535 29.6865C11.129 28.8813 10.9062 27.9754 10.9062 27.0544C10.9062 26.1334 11.129 25.2276 11.5535 24.4224L21.559 5.44789C22.0048 4.60233 22.6573 3.8978 23.4487 3.40735C24.2401 2.91689 25.1417 2.65839 26.06 2.65865L45.7364 2.65864C46.6542 2.65887 47.5553 2.91761 48.3462 3.40803C49.1371 3.89846 49.7892 4.60274 50.2348 5.44789L60.2454 24.4224C60.6699 25.2276 60.8927 26.1334 60.8927 27.0544C60.8927 27.9754 60.6699 28.8813 60.2454 29.6865L50.2348 48.6609C49.7894 49.5057 49.1378 50.2097 48.3473 50.7001C47.5569 51.1905 46.6564 51.4495 45.739 51.4502L26.06 51.4502C25.1417 51.4504 24.2401 51.1919 23.4487 50.7015C22.6573 50.211 22.0048 49.5065 21.559 48.6609L11.5535 29.6865Z" stroke="url(#paint0_linear_1608_40591)" stroke-width="2"/>
                    <path d="M0.694361 29.8672C0.238969 29.0067 -2.83273e-07 28.0387 -2.94387e-07 27.0545C-3.05501e-07 26.0703 0.238969 25.1023 0.694361 24.2419L11.4275 3.96519C11.9058 3.0616 12.6057 2.30871 13.4546 1.7846C14.3036 1.26049 15.2708 0.984248 16.2558 0.984521L37.3633 0.98452C38.3478 0.984765 39.3144 1.26125 40.1629 1.78534C41.0113 2.30942 41.7108 3.06203 42.1888 3.96519L52.9275 24.2419C53.3829 25.1023 53.6218 26.0703 53.6218 27.0545C53.6218 28.0387 53.3829 29.0067 52.9275 29.8672L42.1888 50.1438C41.711 51.0466 41.012 51.7989 40.164 52.323C39.3161 52.847 38.3501 53.1238 37.366 53.1245L16.2558 53.1245C15.2708 53.1248 14.3036 52.8485 13.4546 52.3244C12.6057 51.8003 11.9058 51.0474 11.4275 50.1438L0.694361 29.8672Z" fill="url(#paint1_linear_1608_40591)"/>
                    <rect x="14.2222" y="19.6401" width="23.2728" height="16.2147" rx="2" stroke="white" stroke-width="2"/>
                    <path d="M16.8081 23.6938H20.6869" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <path d="M31.0303 31.8013H34.9091" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <ellipse cx="25.8583" cy="27.7474" rx="2.58587" ry="2.70245" stroke="white" stroke-width="2"/>
                    <defs>
                    <linearGradient id="paint0_linear_1608_40591" x1="62.5" y1="25" x2="14" y2="30.5" gradientUnits="userSpaceOnUse">
                    <stop stop-color="white"/>
                    <stop offset="1" stop-color="white" stop-opacity="0"/>
                    </linearGradient>
                    <linearGradient id="paint1_linear_1608_40591" x1="-24.6396" y1="61.5697" x2="52.7587" y2="22.5869" gradientUnits="userSpaceOnUse">
                    <stop offset="0.160122" stop-color="white" stop-opacity="0"/>
                    <stop offset="1" stop-color="white"/>
                    </linearGradient>
                    </defs>
                </svg>
        
               <p class="title">Total turnover</p>  
               <p class="amount">€ 2.171,86 excl. tax</p>  
                <div class="bg-icon-tl">
                    <svg width="96" height="161" viewBox="0 0 96 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="49" y="33" width="173" height="148" rx="38" transform="rotate(-180 49 33)" stroke="url(#paint0_radial_1608_40600)" stroke-width="2"/>
                        <rect x="-1" y="1" width="160" height="137" rx="38" transform="matrix(-1 0 0 1 19 -11)" stroke="url(#paint1_linear_1608_40600)" stroke-width="2"/>
                        <circle cx="11" cy="3" r="3" fill="#FFC700"/>
                        <circle cx="11" cy="33" r="3" fill="#8CB0FF"/>
                        <circle cx="29.6404" cy="171.64" r="47.8707" transform="rotate(120.833 29.6404 171.64)" fill="url(#paint2_linear_1608_40600)" fill-opacity="0.5"/>
                        <defs>
                        <radialGradient id="paint0_radial_1608_40600" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(72.5 13) rotate(76.5571) scale(105.387 555.808)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint1_linear_1608_40600" x1="-14.1978" y1="-25.4712" x2="73.3171" y2="55.7137" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint2_linear_1608_40600" x1="47.193" y1="38.7987" x2="29.6404" y2="219.511" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>
                </div>
                <div class="bg-icon-br">
                    <svg width="87" height="161" viewBox="0 0 87 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="1" y="131" width="173" height="148" rx="38" stroke="url(#paint0_linear_454_24505)" stroke-width="2"/>
                        <rect x="68" y="85" width="173" height="148" rx="38" stroke="url(#paint1_linear_454_24505)" stroke-width="2"/>
                        <path d="M100 -31V60.0432C100 71.0889 91.0457 80.0432 80 80.0432H70C58.9543 80.0432 50 88.9975 50 100.043V292" stroke="url(#paint2_radial_454_24505)" stroke-width="2"/>
                        <circle cx="50" cy="119" r="3" fill="#8CB0FF"/>
                        <circle cx="44" cy="131" r="3" fill="#FFC700"/>
                        <circle cx="77" cy="98" r="3" fill="#FFC700"/>
                        <circle cx="99.5395" cy="39.5394" r="47.8707" transform="rotate(14.8479 99.5395 39.5394)" fill="url(#paint3_linear_454_24505)" fill-opacity="0.5"/>
                        <circle cx="65.6404" cy="-2.35872" r="47.8707" transform="rotate(-59.1673 65.6404 -2.35872)" fill="url(#paint4_linear_454_24505)" fill-opacity="0.5"/>
                        <defs>
                        <linearGradient id="paint0_linear_454_24505" x1="-15.3371" y1="102.513" x2="79.1106" y2="190.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint1_linear_454_24505" x1="51.6629" y1="56.5131" x2="146.111" y2="144.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <radialGradient id="paint2_radial_454_24505" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(75 130.5) rotate(90) scale(161.5 113.013)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint3_linear_454_24505" x1="117.092" y1="-93.3019" x2="99.5395" y2="87.4101" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint4_linear_454_24505" x1="83.193" y1="-135.2" x2="65.6404" y2="45.512" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>                        
                </div>
            </div>
        </div>
    </div>
    <div class="row row-2">
        <div class="col container-card-2 cstm">
            <div class="container-card-2-wrapper">
                <svg class="icon-top" width="68" height="57" viewBox="0 0 68 57" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M11.5535 29.6865C11.129 28.8813 10.9062 27.9754 10.9062 27.0544C10.9062 26.1334 11.129 25.2276 11.5535 24.4224L21.559 5.44789C22.0048 4.60233 22.6573 3.8978 23.4487 3.40735C24.2401 2.91689 25.1417 2.65839 26.06 2.65865L45.7364 2.65864C46.6542 2.65887 47.5553 2.91761 48.3462 3.40803C49.1371 3.89846 49.7892 4.60274 50.2348 5.44789L60.2454 24.4224C60.6699 25.2276 60.8927 26.1334 60.8927 27.0544C60.8927 27.9754 60.6699 28.8813 60.2454 29.6865L50.2348 48.6609C49.7894 49.5057 49.1378 50.2097 48.3473 50.7001C47.5569 51.1905 46.6564 51.4495 45.739 51.4502L26.06 51.4502C25.1417 51.4504 24.2401 51.1919 23.4487 50.7015C22.6573 50.211 22.0048 49.5065 21.559 48.6609L11.5535 29.6865Z" stroke="url(#paint0_linear_1608_40591)" stroke-width="2"/>
                    <path d="M0.694361 29.8672C0.238969 29.0067 -2.83273e-07 28.0387 -2.94387e-07 27.0545C-3.05501e-07 26.0703 0.238969 25.1023 0.694361 24.2419L11.4275 3.96519C11.9058 3.0616 12.6057 2.30871 13.4546 1.7846C14.3036 1.26049 15.2708 0.984248 16.2558 0.984521L37.3633 0.98452C38.3478 0.984765 39.3144 1.26125 40.1629 1.78534C41.0113 2.30942 41.7108 3.06203 42.1888 3.96519L52.9275 24.2419C53.3829 25.1023 53.6218 26.0703 53.6218 27.0545C53.6218 28.0387 53.3829 29.0067 52.9275 29.8672L42.1888 50.1438C41.711 51.0466 41.012 51.7989 40.164 52.323C39.3161 52.847 38.3501 53.1238 37.366 53.1245L16.2558 53.1245C15.2708 53.1248 14.3036 52.8485 13.4546 52.3244C12.6057 51.8003 11.9058 51.0474 11.4275 50.1438L0.694361 29.8672Z" fill="url(#paint1_linear_1608_40591)"/>
                    <rect x="14.2222" y="19.6401" width="23.2728" height="16.2147" rx="2" stroke="white" stroke-width="2"/>
                    <path d="M16.8081 23.6938H20.6869" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <path d="M31.0303 31.8013H34.9091" stroke="white" stroke-width="2" stroke-linecap="round"/>
                    <ellipse cx="25.8583" cy="27.7474" rx="2.58587" ry="2.70245" stroke="white" stroke-width="2"/>
                    <defs>
                    <linearGradient id="paint0_linear_1608_40591" x1="62.5" y1="25" x2="14" y2="30.5" gradientUnits="userSpaceOnUse">
                    <stop stop-color="white"/>
                    <stop offset="1" stop-color="white" stop-opacity="0"/>
                    </linearGradient>
                    <linearGradient id="paint1_linear_1608_40591" x1="-24.6396" y1="61.5697" x2="52.7587" y2="22.5869" gradientUnits="userSpaceOnUse">
                    <stop offset="0.160122" stop-color="white" stop-opacity="0"/>
                    <stop offset="1" stop-color="white"/>
                    </linearGradient>
                    </defs>
                </svg>
        
               <p class="title">Turnover this year</p>  
               <p class="amount">€ 667,20 excl. tax € 1.504,66 last year</p>  
                <div class="bg-icon-tl">
                    <svg width="96" height="161" viewBox="0 0 96 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="49" y="33" width="173" height="148" rx="38" transform="rotate(-180 49 33)" stroke="url(#paint0_radial_1608_40600)" stroke-width="2"/>
                        <rect x="-1" y="1" width="160" height="137" rx="38" transform="matrix(-1 0 0 1 19 -11)" stroke="url(#paint1_linear_1608_40600)" stroke-width="2"/>
                        <circle cx="11" cy="3" r="3" fill="#FFC700"/>
                        <circle cx="11" cy="33" r="3" fill="#8CB0FF"/>
                        <circle cx="29.6404" cy="171.64" r="47.8707" transform="rotate(120.833 29.6404 171.64)" fill="url(#paint2_linear_1608_40600)" fill-opacity="0.5"/>
                        <defs>
                        <radialGradient id="paint0_radial_1608_40600" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(72.5 13) rotate(76.5571) scale(105.387 555.808)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint1_linear_1608_40600" x1="-14.1978" y1="-25.4712" x2="73.3171" y2="55.7137" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint2_linear_1608_40600" x1="47.193" y1="38.7987" x2="29.6404" y2="219.511" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>
                </div>
                <div class="bg-icon-br">
                    <svg width="148" height="161" viewBox="0 0 148 161" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect x="1" y="131" width="173" height="148" rx="38" stroke="url(#paint0_linear_454_24576)" stroke-width="2"/>
                        <rect x="68" y="85" width="173" height="148" rx="38" stroke="url(#paint1_linear_454_24576)" stroke-width="2"/>
                        <path d="M100 -31V60.0432C100 71.0889 91.0457 80.0432 80 80.0432H70C58.9543 80.0432 50 88.9975 50 100.043V292" stroke="url(#paint2_radial_454_24576)" stroke-width="2"/>
                        <circle cx="50" cy="119" r="3" fill="#8CB0FF"/>
                        <circle cx="90" cy="81" r="3" fill="#8CB0FF"/>
                        <circle cx="44" cy="131" r="3" fill="#FFC700"/>
                        <circle cx="77" cy="98" r="3" fill="#FFC700"/>
                        <circle cx="99.5395" cy="39.5394" r="47.8707" transform="rotate(14.8479 99.5395 39.5394)" fill="url(#paint3_linear_454_24576)" fill-opacity="0.5"/>
                        <circle cx="65.6404" cy="-2.35872" r="47.8707" transform="rotate(-59.1673 65.6404 -2.35872)" fill="url(#paint4_linear_454_24576)" fill-opacity="0.5"/>
                        <defs>
                        <linearGradient id="paint0_linear_454_24576" x1="-15.3371" y1="102.513" x2="79.1106" y2="190.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint1_linear_454_24576" x1="51.6629" y1="56.5131" x2="146.111" y2="144.22" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FFC700"/>
                        <stop offset="1" stop-color="#FFC700" stop-opacity="0"/>
                        </linearGradient>
                        <radialGradient id="paint2_radial_454_24576" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(75 130.5) rotate(90) scale(161.5 113.013)">
                        <stop stop-color="#8B9BFF"/>
                        <stop offset="1" stop-color="#8B9BFF" stop-opacity="0"/>
                        </radialGradient>
                        <linearGradient id="paint3_linear_454_24576" x1="117.092" y1="-93.3019" x2="99.5395" y2="87.4101" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        <linearGradient id="paint4_linear_454_24576" x1="83.193" y1="-135.2" x2="65.6404" y2="45.512" gradientUnits="userSpaceOnUse">
                        <stop stop-color="white"/>
                        <stop offset="1" stop-color="white" stop-opacity="0"/>
                        </linearGradient>
                        </defs>
                    </svg>                                         
                </div>
            </div>
        </div>
    </div>


</div>