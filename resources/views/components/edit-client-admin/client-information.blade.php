<div class="container-card-1 client-information-container">
    <h2>{{ __('Client information') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="#company_name">{{ __('Company name') }}</label>
            <input type="text" name="company_name" id="company_name" value="{{ $user['CompanyName'] ? $user['CompanyName'] : '' }}">
        </div>
        <div class="form-group w-1/2">
            <label for="#company_legal_form">{{ __('Legal form') }}</label>
            <div class="select-wrapper">
                <select name="company_legal_form" id="company_legal_form">
                    <option value="">{{ __('Choose here') }}</option>
                    @if(count($legalForms) > 0)
                        @foreach ($legalForms as $legalForm)
                            <option value='{{ $legalForm->legal_form_code }}' {{ $userDB->userInformation->company_legal_form == $legalForm->legal_form_code ? 'selected' : '' }}>{{ $legalForm->legal_form_name }}</option>
                        @endforeach
                    @else
                        <option value="">{{ __('No legal form Available') }}</option>
                    @endif
                    {{-- <option value="Other or Unknown" {{ $userDB->userInformation->company_legal_form == 'Other or Unknown' ? 'selected="selected"' : '' }}>Other or Unknown</option>
                    <option value="Private company" {{ $userDB->userInformation->company_legal_form == 'Private company' ? 'selected="selected"' : '' }} >Private company</option>
                    <option value="Bv in formation" {{ $userDB->userInformation->company_legal_form == 'Bv in formation' ? 'selected="selected"' : '' }}>Bv in formation</option>
                    <option value="Cooperation" {{ $userDB->userInformation->company_legal_form == 'Cooperation' ? 'selected="selected"' : '' }}>Cooperation</option>
                    <option value="Limited partnership" {{ $userDB->userInformation->company_legal_form == 'Limited partnership' ? 'selected="selected"' : '' }}>Limited partnership</option>
                    <option value="Proprietorship" {{ $userDB->userInformation->company_legal_form == 'Proprietorship' ? 'selected="selected"' : '' }}>Proprietorship</option> --}}
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
        <div class="form-group w-1/2">
            <label for="#chamber_of_commerce">{{ __('Chamber of commerce number') }}</label>
            <input type="text" name="chamber_of_commerce" id="chamber_of_commerce" value="{{ $user['CompanyNumber'] ? $user['CompanyNumber'] : '' }}">
        </div>
        <div class="form-group w-1/2">
            <label for="#vat_number">{{ __('Vat number') }}</label>
            <input type="text" name="vat_number" id="vat_number" value="{{ $user["TaxNumber"] }}">
        </div>
        <div class="form-group w-1/3">
            <label for="#user_legal_form">{{ __('Legal form') }}</label>
            <div class="select-wrapper">
                <select name="user_legal_form" id="user_legal_form">
                    <option value="">{{ __('Choose here') }}</option>
                    <option value="d" {{ $userDB->userInformation->user_legal_form == 'd' ? 'selected' : '' }}>Dhr.</option>
                    <option value="m" {{ $userDB->userInformation->user_legal_form == 'm' ? 'selected' : '' }} >Mrs.</option>
                    <option value="f" {{ $userDB->userInformation->user_legal_form == 'f' ? 'selected' : '' }}>Afd.</option>
                    <option value="u" {{ $userDB->userInformation->user_legal_form == 'u' ? 'selected' : '' }}>Unknown</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
        <div class="form-group w-1/3">
            <label for="#first_name">{{ __('First name') }}</label>
            <input type="text" name="first_name" id="first_name" value="{{ $user['Initials'] != "" ? $user['Initials'] : '' }}" >
        </div>
        <div class="form-group w-1/3">
            <label for="#last_name">{{ __('Last name') }}</label>
            <input type="text" name="last_name" id="last_name" value="{{ $user["SurName"] != "" ? $user["SurName"] : '' }}" >
        </div>
        <div class="form-group w-1/2">
            <label for="#address">{{ __('Address') }}</label>
            <input type="text" name="address" id="address" value="{{ $user['Address'] != "" ? $user['Address'] : '' }}">
        </div>
        <div class="form-group w-1/2">
            <label for="#postcode">{{ __('Postcode') }}</label>
            <input type="text" name="postcode" id="postcode" value="{{ $user["ZipCode"] != "null" ? $user["ZipCode"] : '' }}">
        </div>
        <div class="form-group w-1/2">
            <label for="#place">{{ __('Place') }}</label>
            <input type="text" name="place" id="place" value="{{ $user["City"] != "" ? $user["City"] : '' }}" >
        </div>
        <div class="form-group w-1/2">
            <label for="#land">{{ __('Land') }}</label>
            <div class="select-wrapper">
                <select name="land" id="land">
                    <option value="">{{ __('Choose here') }}</option>
                    @if(count($countries) > 0)
                        @foreach ($countries as $country)
                            <option value='{{ $country->sortname }}' {{ $user['Country'] == $country->sortname ? 'selected' : ''}}>{{ $country->name }}</option>
                        @endforeach
                    @else
                        <option value="">{{ __('No Country Available') }}</option>
                    @endif
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
        <div class="form-group radio">
            <p class="label">Reseller</p>
            <input type="radio" id="yes" name="reseller" value="">
            <label for="yes">Yes</label><br>
            <input type="radio" id="no" name="reseller" value="">
            <label for="no">No</label><br>
        </div>
    </div>
</div>
