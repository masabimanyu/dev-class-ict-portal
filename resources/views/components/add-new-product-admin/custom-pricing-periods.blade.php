<div id="customPricingPeriod" class="container-card-1 card-custom-pricing-periods js-card-custom-pricing-period">
    <h2>{{ __('Custom pricing & periods') }}</h2>

    <div class="input-container form">

        <div class="form-group radio">
            <div class="radio-wrapper">
                <p class="label">Contract period</p>
                <input class="contract-period-radio" type="radio" id="contract_period_1" name="contract_period" value="1">
                <label for="contract_period_1">Always based on the default price</label><br>
                <input class="contract-period-radio" type="radio" id="contract_period_2" name="contract_period" value="0">
                <label for="contract_period_2">Custom prices for different periods</label><br>
            </div>
        </div>

        <div class="group-input period-contract js-period-contract {{ $product_type }}">
            <div class="input-container form">

                <p class="label">{{ __('Periods') }}</p>

                <div class="group-input">
                    <div class="form-group number">
                        <input type="text" name="custom_price[0][periods]" id="custom_period">
                    </div>

                    <div class="form-group month">
                        <div class="select-wrapper cstm-fit-content">
                            <select name="custom_price[0][periodic]" id="custom_period_time">
                                @foreach ($periods as $value => $period)
                                    @if($value == "empty")
                                        @php
                                            continue;
                                        @endphp
                                    @endif
                                    <option value="{{ $value }}">{{ $period }}</option>
                                @endforeach
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>

                    <div class="form-group price-excl">
                        <input type="number" name="custom_price[0][price_exclude]" id="custom_price_exclude" placeholder="Price excl. tax">
                        <p class="text">
                            <svg class="js-custom-pricing-period-delete-input" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 15L10 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M14 15L14 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M3 7H21V7C20.0681 7 19.6022 7 19.2346 7.15224C18.7446 7.35523 18.3552 7.74458 18.1522 8.23463C18 8.60218 18 9.06812 18 10V16C18 17.8856 18 18.8284 17.4142 19.4142C16.8284 20 15.8856 20 14 20H10C8.11438 20 7.17157 20 6.58579 19.4142C6 18.8284 6 17.8856 6 16V10C6 9.06812 6 8.60218 5.84776 8.23463C5.64477 7.74458 5.25542 7.35523 4.76537 7.15224C4.39782 7 3.93188 7 3 7V7Z" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M10.0681 3.37059C10.1821 3.26427 10.4332 3.17033 10.7825 3.10332C11.1318 3.03632 11.5597 3 12 3C12.4403 3 12.8682 3.03632 13.2175 3.10332C13.5668 3.17033 13.8179 3.26427 13.9319 3.37059" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            Per month
                        </p>
                    </div>
                </div>

                {{-- <div class="group-input">
                    <div class="form-group number">
                        <input type="text" name="custom_price[1][periods]" id="custom_period">
                    </div>

                    <div class="form-group month">
                        <div class="select-wrapper cstm-fit-content">
                            <select name="custom_price[1][periodic]" id="custom_period_time">
                                @foreach ($periods as $value => $period)
                                    @if($value == "empty")
                                        @php
                                            continue;
                                        @endphp
                                    @endif
                                    <option value="{{ $value }}">{{ $period }}</option>
                                @endforeach
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>

                    <div class="form-group price-excl">
                        <input type="number" name="custom_price[1][price_exclude]" id="custom_price_exclude" placeholder="Price excl. tax">
                        <p class="text">
                            <svg class="js-custom-pricing-period-delete-input" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 15L10 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M14 15L14 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M3 7H21V7C20.0681 7 19.6022 7 19.2346 7.15224C18.7446 7.35523 18.3552 7.74458 18.1522 8.23463C18 8.60218 18 9.06812 18 10V16C18 17.8856 18 18.8284 17.4142 19.4142C16.8284 20 15.8856 20 14 20H10C8.11438 20 7.17157 20 6.58579 19.4142C6 18.8284 6 17.8856 6 16V10C6 9.06812 6 8.60218 5.84776 8.23463C5.64477 7.74458 5.25542 7.35523 4.76537 7.15224C4.39782 7 3.93188 7 3 7V7Z" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M10.0681 3.37059C10.1821 3.26427 10.4332 3.17033 10.7825 3.10332C11.1318 3.03632 11.5597 3 12 3C12.4403 3 12.8682 3.03632 13.2175 3.10332C13.5668 3.17033 13.8179 3.26427 13.9319 3.37059" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            Per month
                        </p>
                    </div>
                </div> --}}

                <button data-index="0" type="button" class="btn btn--primary--transparent cstm-btn js-custom-pricing-period-add-input">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9 3.75V14.25M3.75 9H14.25" stroke="#951D81" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    Add period
                </button>
            </div>
        </div>

    </div>

</div>
