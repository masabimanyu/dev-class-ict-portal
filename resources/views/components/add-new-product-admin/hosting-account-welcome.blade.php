<div class="container-card-1 card-hosting-account-welcome">
    <h2>{{ __('Properties of the hosting product') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="hosting-email">{{ __('Hosting account email template') }}</label>
            <div class="select-wrapper">
                <select name="hosting_email_template" id="hosting-email">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group checkbox">
            <label for="#send">{{ __('Send hosting login details automatically after creating hosting account') }}</label>
            <input
                type="checkbox"
                name="send_hosting_direct"
                id="send"
                >
        </div>

        <div class="form-group w-1/2">
            <label for="hosting-pdf">{{ __('Hosting account PDF template for sending by post:') }}</label>
            <div class="select-wrapper">
                <select name="hosting_pdf_template" id="hosting-pdf">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

</div>
