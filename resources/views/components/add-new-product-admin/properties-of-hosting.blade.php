<div class="container-card-1 card-properties-of-hosting">
    <h2>{{ __('Properties of the hosting product') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="webhosting">{{ __('Webhosting type') }}</label>
            <div class="select-wrapper">
                <select name="webhosting" id="webhosting">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="server">{{ __('Server') }}</label>
            <div class="select-wrapper">
                <select name="hosting_server" id="server">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="hosting_template">{{ __('Template') }}</label>
            <div class="select-wrapper">
                <select name="hosting_template" id="template">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

</div>
