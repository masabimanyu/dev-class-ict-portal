<div class="container-card-1 card-general">
        <h2>{{ __('General') }}</h2>

        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="p-code">{{ __('Product code') }}</label>
                <input name="product_code" type="text" placeholder="P221">
            </div>

            <div class="form-group w-1/2">
                <label for="name">{{ __('Name') }}</label>
                <input name="product_name" type="text" placeholder="">
            </div>

            <div class="form-group w-1/2">
                <label for="inv-desc">{{ __('Invoice description') }}</label>
                <textarea name="invoice_description" type="text" placeholder=""></textarea>
            </div>

            <div class="form-group w-1/2 cstm-p-unit">
                <label for="p-unit">{{ __('Product unit') }}</label>
                <input name="product_unit" type="text" placeholder="">
                <p>(For example: hour, liter, cm, m³ or kg)</p>
            </div>

        </div>

</div>
