<div class="container-card-1 card-detailed-description">
    <h2>{{ __('Detailed description') }}</h2>

    <p class="desc">
        If you want to save additional information about this product, please use this field. This information will not be communicated 
        to clients or shown on invoices.
    </p>

    <div class="input-container form">

        <div class="form-group radio">
            <textarea name="" id="" cols="30" rows="10"></textarea>
        </div>

    </div>

</div>