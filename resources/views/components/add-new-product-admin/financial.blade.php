<div class="container-card-1 card-financial">
    <h2>{{ __('Financial') }}</h2>

    <div class="input-container form">

        <div class="group-input">

            <div class="input-container form">

                <div class="form-group w-1/2">
                    <label for="#price_exclude">{{ __('Price excl. tax') }}</label>
                    <input type="number" name="price_exclude" id="price_exclude">
                </div>

                <div class="form-group tax-rate">
                    <label for="">{{ __('Tax rate') }}</label>
                    <div class="select-wrapper cstm-fit-content">
                        <select name="tax" id="tax">
                            <option value="">{{ __('Tax') }}</option>
                            @if(count($taxes) > 0)
                                @foreach ($taxes as $tax)
                                    @if($tax["Visibility"] == "always")
                                        <option value="{{ $tax['TaxCode'] }}" {{ $tax["IsDefault"] == "yes" ? "selected" : "" }}>{{ $tax['Name'] }}</option>
                                    @endif
                                @endforeach
                            @else
                                <option value="">{{ __('There is no tax applied in wefact') }}</option>
                            @endif
                        </select>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                </div>

                <div class="form-group price">
                    <label for="price-per">{{ __('Price per') }}</label>
                    <div class="select-wrapper cstm-fit-content">
                        <select name="price_per" id="pricePer" class="price-per">
                            @foreach ($periods as $value => $period)
                                <option value="{{ $value }}">{{ $period }}</option>
                            @endforeach
                        </select>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                </div>

            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="#cost_price_exclude">{{ __('Cost price excl. tax') }}</label>
            <input type="number" name="cost_price_exclude" id="cost_price_exclude">
        </div>

    </div>
</div>
