<div class="container-card-1 card-properties-of-domain">
    <h2>{{ __('Properties of the domain product') }}</h2>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="#product_domain_tld">{{ __('TLD') }}</label>
                <input name="product_domain_tld" id="product_domain_tld" type="text" placeholder="com">
                {{-- <select name="tld" id="tld">
                    <option value="">{{ __('Select a tld') }}</option>
                    @if(count($tlds) > 0)
                        @foreach ($tlds as $tld)
                            <option value="{{ $tld->id }}">{{ $tld->tld }}</option>
                        @endforeach
                    @else
                        <option value="">{{ __('There is no available tld') }}</option>
                    @endif
                </select> --}}
            </div>

            <div class="form-group w-1/2 cstm-fit-content">
                <div class="submit-wrapper left">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.2197 16.2803C15.5126 16.5732 15.9874 16.5732 16.2803 16.2803C16.5732 15.9874 16.5732 15.5126 16.2803 15.2197L15.2197 16.2803ZM13.0178 11.9572C12.7249 11.6643 12.2501 11.6643 11.9572 11.9572C11.6643 12.2501 11.6643 12.7249 11.9572 13.0178L13.0178 11.9572ZM13.5 8.25C13.5 11.1495 11.1495 13.5 8.25 13.5V15C11.9779 15 15 11.9779 15 8.25H13.5ZM8.25 13.5C5.3505 13.5 3 11.1495 3 8.25H1.5C1.5 11.9779 4.52208 15 8.25 15V13.5ZM3 8.25C3 5.3505 5.3505 3 8.25 3V1.5C4.52208 1.5 1.5 4.52208 1.5 8.25H3ZM8.25 3C11.1495 3 13.5 5.3505 13.5 8.25H15C15 4.52208 11.9779 1.5 8.25 1.5V3ZM16.2803 15.2197L13.0178 11.9572L11.9572 13.0178L15.2197 16.2803L16.2803 15.2197Z" fill="#951D81"/>
                    </svg>
                    <button type="button" data-target="modal-hosting-list" class="btn btn--transparent js-modal-trigger">Search TLD</button>
                </div>
            </div>

        </div>
    </div>

    {{-- If tld already exist --}}
    <div class="data">
        <div class="data-item">
            <p class="title">Registrar</p>
            <p class="value">Openprovider</p>
        </div>
        <div class="data-item">
            <p class="title">Charge costs for changing domain owner</p>
            <p class="value">No cost</p>
        </div>
    </div>

    {{-- If tld doesn't exist --}}
    <h2>{{ __('Add new TLD') }}</h2>

    <div class="input-container form add-new-tld">

        <div class="form-group w-1/2">
            <label for="registrar">{{ __('Registrar') }}</label>
            <div class="select-wrapper">
                <select name="registrar" id="registrar">
                    <option value="">{{ __('Select a Registrar') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <p><span class="note">Note </span>: Select the registrar where you want to register the TLD.</p>
        </div>

        <div class="form-group w-1/2">
            <label for="charge">{{ __('Charge costs for changing domain owner') }}</label>
            <div class="select-wrapper">
                <select name="charge" id="charge">
                    <option value="">{{ __('Charge costs') }}</option>
                    @if(count($products) > 0)
                        @foreach ($products as $product)
                            <option value="{{ $product->id }}">{{ $product->product_code }} - {{ $product->product_name }} (€{{ $product->cost_price_exclude.__('excl. tax') }})</option>
                        @endforeach
                    @else
                        <option value="">{{ __('No product available') }}</option>
                    @endif
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <p><span class="note">Note </span>: When a client wants to change the domain owner information, you can charge for this action automatically. If you want to charge costs, select the product that should be used for the charging</p>
        </div>

        <div class="form-group w-1/2">
            <label for="whois">{{ __('Public WHOIS server') }}</label>
            <input id="whois" type="text" name="whois">
        </div>

        <div class="form-group w-1/2">
            <label for="check">{{ __('No Match check') }}</label>
            <input id="check" type="text" name="no_match_check">
        </div>

        <div class="toggle-action">
            <label class="switch">
                <input type="checkbox" name="need_auth_code">
                <span class="slider round"></span>
                <p class="yes">Yes</p>
                <p class="no">No</p>
            </label>
            <p>Authorization code needed for transfer?</p>
        </div>

    </div>

</div>
