<div class="row row-forms row-product-type">
    <div class="col">
        <div class="container-card-1">

            <h2>{{ __('Product type') }}</h2>

            <div class="service-menu">

                <div data-target="general_product" class="service-menu-item container-card-1 js-service-menu-item js-product-form">
                    <div class="checklist-icon">
                        <svg class="checked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="20" height="20" rx="10" fill="#E6007E"/>
                            <path d="M15.6445 5.82808C15.178 5.43705 14.337 5.34656 13.8832 5.82808C12.0691 7.75316 10.2549 9.67823 8.44082 11.6033C7.66474 10.7391 6.88865 9.87494 6.11256 9.01076C5.69772 8.54883 4.77564 8.58864 4.35125 9.01076C3.85781 9.50156 3.90872 10.1262 4.35125 10.619C5.41382 11.8022 6.47639 12.9853 7.53895 14.1685C7.94438 14.62 8.89283 14.6009 9.30028 14.1685C11.1521 12.2034 13.004 10.2383 14.8559 8.27314C15.1188 7.99419 15.3816 7.71524 15.6445 7.43629C16.0691 6.98577 16.1655 6.26476 15.6445 5.82808Z" fill="white"/>
                        </svg>
                        <svg class="unchecked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="0.65" y="0.65" width="18.7" height="18.7" rx="9.35" stroke="#E6007E" stroke-width="1.3"/>
                        </svg>
                    </div>
                    <div class="service-name-wrapper">
                        <svg width="73" height="73" viewBox="0 0 73 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="73" height="73" rx="20" fill="url(#paint0_linear_1622_40670)"/>
                            <path d="M51.625 44.0139V28.9863C51.625 28.7701 51.5675 28.5577 51.4583 28.371C51.3491 28.1844 51.1922 28.0301 51.0038 27.9241L37.5975 20.3831C37.4151 20.2805 37.2093 20.2266 37 20.2266C36.7907 20.2266 36.5849 20.2805 36.4025 20.3831L22.9962 27.9241C22.8078 28.0301 22.6509 28.1844 22.5417 28.371C22.4325 28.5577 22.375 28.7701 22.375 28.9863V44.0139C22.375 44.2301 22.4325 44.4425 22.5417 44.6292C22.6509 44.8158 22.8078 44.9701 22.9962 45.0761L36.4025 52.6171C36.5849 52.7197 36.7907 52.7737 37 52.7737C37.2093 52.7737 37.4151 52.7197 37.5975 52.6171L51.0038 45.0761C51.1922 44.9701 51.3491 44.8158 51.4583 44.6292C51.5675 44.4425 51.625 44.2301 51.625 44.0139Z" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M44.4682 40.2341V32.3122L29.6875 24.1602" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M51.457 28.3687L37.1446 36.4998L22.5438 28.3672" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M37.1446 36.5L37.0016 52.7736" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <defs>
                            <linearGradient id="paint0_linear_1622_40670" x1="36.5" y1="0" x2="36.5" y2="73" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>
                        <p>{{ __('General') }}</p>
                    </div>
                </div>

                <div data-target="domain_product" class="service-menu-item container-card-1 js-service-menu-item js-product-form" >
                    <div class="checklist-icon">
                        <svg class="checked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="20" height="20" rx="10" fill="#E6007E"/>
                            <path d="M15.6445 5.82808C15.178 5.43705 14.337 5.34656 13.8832 5.82808C12.0691 7.75316 10.2549 9.67823 8.44082 11.6033C7.66474 10.7391 6.88865 9.87494 6.11256 9.01076C5.69772 8.54883 4.77564 8.58864 4.35125 9.01076C3.85781 9.50156 3.90872 10.1262 4.35125 10.619C5.41382 11.8022 6.47639 12.9853 7.53895 14.1685C7.94438 14.62 8.89283 14.6009 9.30028 14.1685C11.1521 12.2034 13.004 10.2383 14.8559 8.27314C15.1188 7.99419 15.3816 7.71524 15.6445 7.43629C16.0691 6.98577 16.1655 6.26476 15.6445 5.82808Z" fill="white"/>
                        </svg>
                        <svg class="unchecked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="0.65" y="0.65" width="18.7" height="18.7" rx="9.35" stroke="#E6007E" stroke-width="1.3"/>
                        </svg>
                    </div>
                    <div class="service-name-wrapper">
                        <svg width="73" height="73" viewBox="0 0 73 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="73" height="73" rx="20" fill="url(#paint0_linear_1212_36862)"/>
                            <path d="M37 51.125C45.0772 51.125 51.625 44.5772 51.625 36.5C51.625 28.4228 45.0772 21.875 37 21.875C28.9228 21.875 22.375 28.4228 22.375 36.5C22.375 44.5772 28.9228 51.125 37 51.125Z" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M23.208 31.625H50.7921" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M23.2085 41.375H50.7925" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M37 50.7324C40.3655 50.7324 43.0938 44.3603 43.0938 36.5C43.0938 28.6396 40.3655 22.2676 37 22.2676C33.6345 22.2676 30.9062 28.6396 30.9062 36.5C30.9062 44.3603 33.6345 50.7324 37 50.7324Z" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                            <defs>
                            <linearGradient id="paint0_linear_1212_36862" x1="36.5" y1="0" x2="36.5" y2="73" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>
                        <p>{{ __('Domain') }}</p>
                    </div>
                </div>

                <div data-target="hosting_product" class="service-menu-item container-card-1 js-service-menu-item js-product-form">
                    <div class="checklist-icon">
                        <svg class="checked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="20" height="20" rx="10" fill="#E6007E"/>
                            <path d="M15.6445 5.82808C15.178 5.43705 14.337 5.34656 13.8832 5.82808C12.0691 7.75316 10.2549 9.67823 8.44082 11.6033C7.66474 10.7391 6.88865 9.87494 6.11256 9.01076C5.69772 8.54883 4.77564 8.58864 4.35125 9.01076C3.85781 9.50156 3.90872 10.1262 4.35125 10.619C5.41382 11.8022 6.47639 12.9853 7.53895 14.1685C7.94438 14.62 8.89283 14.6009 9.30028 14.1685C11.1521 12.2034 13.004 10.2383 14.8559 8.27314C15.1188 7.99419 15.3816 7.71524 15.6445 7.43629C16.0691 6.98577 16.1655 6.26476 15.6445 5.82808Z" fill="white"/>
                        </svg>
                        <svg class="unchecked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect x="0.65" y="0.65" width="18.7" height="18.7" rx="9.35" stroke="#E6007E" stroke-width="1.3"/>
                        </svg>
                    </div>
                    <div class="service-name-wrapper">
                        <svg width="73" height="73" viewBox="0 0 73 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="73" height="73" rx="20" fill="url(#paint0_linear_1212_36988)"/>
                            <ellipse cx="37" cy="28.375" rx="11.375" ry="4.875" stroke="white" stroke-width="3"/>
                            <path d="M25.625 38.125C25.625 38.125 25.625 41.9326 25.625 44.625C25.625 47.3174 30.7178 49.5 37 49.5C43.2822 49.5 48.375 47.3174 48.375 44.625C48.375 43.2811 48.375 38.125 48.375 38.125" stroke="white" stroke-width="3" stroke-linecap="square"/>
                            <path d="M25.625 28.375C25.625 28.375 25.625 33.8076 25.625 36.5C25.625 39.1924 30.7178 41.375 37 41.375C43.2822 41.375 48.375 39.1924 48.375 36.5C48.375 35.1561 48.375 28.375 48.375 28.375" stroke="white" stroke-width="3"/>
                            <defs>
                            <linearGradient id="paint0_linear_1212_36988" x1="36.5" y1="0" x2="36.5" y2="73" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>
                        <p>{{ __('Hosting') }}</p>
                    </div>
                </div>

            </div>

            <div class="explanation">
                <h3>{{ __('Product type') }}</h3>
                <p>{{ __('Here you can specify the type of product.
                Additional settings may appear to specify certain links.
                By entering these settings, HostFact will be able to perform
                (automated) tasks.') }}</p>
            </div>
        </div>
    </div>
</div>
