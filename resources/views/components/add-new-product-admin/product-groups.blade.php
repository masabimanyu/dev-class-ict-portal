<div class="container-card-1 card-product-groups">
    <h2>{{ __('Product groups') }}</h2>

    <p class="desc">
        Products can be linked to product groups. This is useful when you want to use the orderform or discount module.
    </p>

    <div class="input-container form">

        <div class="form-group checkbox">
            <label for="#domeinnamen">{{ __('Domeinnamen bestelformulier') }}</label>
            <input
                type="checkbox"
                name="is_login_and_order_form"
                id="domeinnamen"
                >
        </div>

        <div class="form-group checkbox">
            <label for="#hosting">{{ __('Hosting bestelformulier ') }}</label>
            <input
                type="checkbox"
                name="is_login_and_order_form"
                id="hosting"
                >
        </div>

        <div class="form-group checkbox">
            <label for="#onderhoud">{{ __('Onderhoud bestelformulier ') }}</label>
            <input
                type="checkbox"
                name="is_login_and_order_form"
                id="onderhoud"
                >
        </div>

        <div class="form-group checkbox">
            <label for="#opties">{{ __('Opties bestelformulier') }}</label>
            <input
                type="checkbox"
                name="is_login_and_order_form"
                id="opties"
                >
        </div>

    </div>

</div>