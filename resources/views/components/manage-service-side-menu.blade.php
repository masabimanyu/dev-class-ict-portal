<div class="action-menu">

    <div class="action-menu-group">

        <h2 class="action-menu-group-name js-show-hide-dropdown" data-target="domains">
            Domains
            <svg class="js-show-hide-dropdown-ic active" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </h2>

        <div class="menu-item-wrapper js-dropdown-content active" id="domains">

            <a href="{{ route('manage-service.registrar') }}" class="{{ (request()->is('manage-service/registrar*')) ? 'active' : '' }} menu-item move-effect btn btn--primary--transparent">
                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8.93753 16.25C11.8543 16.25 14.2188 13.8855 14.2188 10.9688C14.2188 8.052 11.8543 5.6875 8.93753 5.6875C6.02078 5.6875 3.65628 8.052 3.65628 10.9688C3.65628 13.8855 6.02078 16.25 8.93753 16.25Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M15.7841 5.88544C16.5105 5.68077 17.2723 5.63415 18.0182 5.74871C18.7642 5.86326 19.4769 6.13633 20.1084 6.54953C20.7399 6.96273 21.2755 7.50646 21.6792 8.1441C22.0829 8.78174 22.3452 9.49848 22.4486 10.246C22.5519 10.9936 22.4939 11.7547 22.2783 12.4779C22.0628 13.2011 21.6948 13.8698 21.199 14.4388C20.7033 15.0079 20.0914 15.464 19.4045 15.7767C18.7177 16.0893 17.9718 16.2511 17.2171 16.2512" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M1.6246 20.0481C2.44943 18.8749 3.54443 17.9173 4.81717 17.2562C6.08991 16.5952 7.50301 16.2501 8.93719 16.25C10.3714 16.2499 11.7845 16.595 13.0573 17.2559C14.3301 17.9169 15.4251 18.8744 16.2501 20.0475" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M17.2171 16.25C18.6514 16.249 20.0648 16.5936 21.3377 17.2546C22.6106 17.9156 23.7055 18.8737 24.5296 20.0475" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>                            
                {{ __('Registrars') }}
            </a>
    
            <a href="{{ route('manage-service.domain-contacts') }}" class="{{ (request()->is('manage-service/domain-contacts*')) ? 'active' : '' }} menu-item move-effect btn btn--primary--transparent">
                <svg width="26" height="26" style="min-width: 26px;" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M19 11V8.2C19 7.0799 19 6.51984 18.782 6.09202C18.5903 5.71569 18.2843 5.40973 17.908 5.21799C17.4802 5 16.9201 5 15.8 5H7.2C6.0799 5 5.51984 5 5.09202 5.21799C4.71569 5.40973 4.40973 5.71569 4.21799 6.09202C4 6.51984 4 7.07989 4 8.2V13.8C4 14.9201 4 15.4802 4.21799 15.908C4.40973 16.2843 4.71569 16.5903 5.09202 16.782C5.51984 17 6.07989 17 7.2 17H14" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M8 13H12" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M8 9H15" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="18" cy="15" r="1.25" stroke="currentColor" stroke-width="1.5"/>
                    <path d="M20 20C20 20 19.5 19 18 19C16.5 19 16 20 16 20" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                </svg>                            
                {{ __('Domain contacs') }}
            </a>
    
            <a href="{{ route('manage-service.tld') }}" class="{{ (request()->is('manage-service/tld*')) ? 'active' : '' }} menu-item move-effect btn btn--primary--transparent">
                <svg width="26" height="26" style="min-width: 26px;" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13 22.75C18.3848 22.75 22.75 18.3848 22.75 13C22.75 7.61522 18.3848 3.25 13 3.25C7.61522 3.25 3.25 7.61522 3.25 13C3.25 18.3848 7.61522 22.75 13 22.75Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M3.80518 9.75H22.1946" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M3.80566 16.25H22.195" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M13 22.4883C15.2437 22.4883 17.0625 18.2402 17.0625 13C17.0625 7.75976 15.2437 3.51172 13 3.51172C10.7563 3.51172 8.9375 7.75976 8.9375 13C8.9375 18.2402 10.7563 22.4883 13 22.4883Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>                            
                {{ __('TLDs') }}
            </a>

        </div>


    </div>

    <div class="action-menu-group">

        <h2 class="action-menu-group-name js-show-hide-dropdown" data-target="dnsManagement">
            DNS management
            <svg class="js-show-hide-dropdown-ic active" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </h2>

        <div class="menu-item-wrapper js-dropdown-content active" id="dnsManagement">

            <a href="{{ route('manage-service.nameserver') }}" class="{{ (request()->is('manage-service/nameserver*')) ? 'active' : '' }} menu-item move-effect btn btn--primary--transparent">
                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5625 13H21.9374" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M10.5625 6.5H21.9374" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M10.5624 19.5H21.9374" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M4.0625 6.09375L5.6875 5.28125V10.9681" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M4.17424 15.4935C4.25144 15.3108 4.366 15.1463 4.51056 15.0105C4.65511 14.8748 4.82647 14.7707 5.01362 14.7051C5.20076 14.6395 5.39957 14.6137 5.59726 14.6295C5.79495 14.6453 5.98715 14.7022 6.16153 14.7967C6.3359 14.8912 6.48859 15.021 6.60979 15.178C6.73099 15.335 6.81802 15.5156 6.86528 15.7082C6.91254 15.9008 6.919 16.1011 6.88424 16.2964C6.84947 16.4916 6.77425 16.6775 6.66341 16.8419L4.0625 20.3126H6.90625" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>                                                  
                {{ __('Nameserver groups') }}
            </a>

        </div>


    </div>
    <div class="action-menu-group">

        <h2 class="action-menu-group-name js-show-hide-dropdown" data-target="webHosting">
            Webhosting
            <svg class="js-show-hide-dropdown-ic active" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </h2>

        <div class="menu-item-wrapper js-dropdown-content active" id="webHosting">

            <a href="{{ route('manage-service.server') }}" class="{{ (request()->is('manage-service/server*')) ? 'active' : '' }} menu-item move-effect btn btn--primary--transparent">
                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M21.125 14.625H4.875C4.42627 14.625 4.0625 14.9888 4.0625 15.4375V20.3125C4.0625 20.7612 4.42627 21.125 4.875 21.125H21.125C21.5737 21.125 21.9375 20.7612 21.9375 20.3125V15.4375C21.9375 14.9888 21.5737 14.625 21.125 14.625Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M21.125 4.875H4.875C4.42627 4.875 4.0625 5.23877 4.0625 5.6875V10.5625C4.0625 11.0112 4.42627 11.375 4.875 11.375H21.125C21.5737 11.375 21.9375 11.0112 21.9375 10.5625V5.6875C21.9375 5.23877 21.5737 4.875 21.125 4.875Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M19.2344 8.125C19.2344 8.6514 18.8076 9.07812 18.2812 9.07812C17.7549 9.07812 17.3281 8.6514 17.3281 8.125C17.3281 7.5986 17.7549 7.17188 18.2812 7.17188C18.8076 7.17188 19.2344 7.5986 19.2344 8.125Z" fill="currentColor" stroke="currentColor" stroke-width="0.125"/>
                    <path d="M19.2344 17.875C19.2344 18.4014 18.8076 18.8281 18.2812 18.8281C17.7549 18.8281 17.3281 18.4014 17.3281 17.875C17.3281 17.3486 17.7549 16.9219 18.2812 16.9219C18.8076 16.9219 19.2344 17.3486 19.2344 17.875Z" fill="currentColor" stroke="currentColor" stroke-width="0.125"/>
                </svg>                                                                           
                {{ __('Servers') }}
            </a>
    
            <a href="{{ route('manage-service.package') }}" class="{{ (request()->is('manage-service/package*')) ? 'active' : '' }} menu-item move-effect btn btn--primary--transparent">
                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M22.75 18.0106V7.99219C22.75 7.84802 22.7116 7.70645 22.6389 7.582C22.5661 7.45755 22.4615 7.35471 22.3358 7.28403L13.3983 2.25669C13.2767 2.18828 13.1395 2.15234 13 2.15234C12.8605 2.15234 12.7233 2.18828 12.6017 2.25669L3.66416 7.28403C3.53851 7.35471 3.43393 7.45755 3.36114 7.582C3.28836 7.70645 3.25 7.84802 3.25 7.99219V18.0106C3.25 18.1547 3.28836 18.2963 3.36114 18.4207C3.43393 18.5452 3.53851 18.648 3.66416 18.7187L12.6017 23.7461C12.7233 23.8145 12.8605 23.8504 13 23.8504C13.1395 23.8504 13.2767 23.8145 13.3983 23.7461L22.3358 18.7187C22.4615 18.648 22.5661 18.5452 22.6389 18.4207C22.7116 18.2963 22.75 18.1547 22.75 18.0106Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M17.9788 15.4894V10.2081L8.125 4.77344" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M22.638 7.57914L13.0964 12.9998L3.36252 7.57812" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M13.0964 13L13.001 23.849" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                                                                
                {{ __('Package') }}
            </a>

        </div>

    </div>

</div>