<div class="container-card-1 card-login-sec">
    <h2>{{ __('Employee login') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="username">{{ __('Username') }}</label>
            <input id="username" type="text" placeholder="">
        </div>
        <div class="form-group w-1/2">
            <label for="current-password">{{ __('Last login') }}</label>
            <p class="loginsec-date">01/09/2022 at 09:01</p>
        </div>
        <div class="form-group w-1/2">
            <label for="new-password">{{ __('2-Step authentication') }}</label>
            <div class="label label-active">
                Active
            </div>
            {{-- If deactive --}}
            {{-- <div class="label label-deactive">
                Deactive
            </div> --}}
        </div>
        <div class="form-group w-1/2">
            <button class="btn btn--primary--outline">2-Step authentication setup</button>
        </div>
    </div>
</div>