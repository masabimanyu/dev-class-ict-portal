<div class="container-card-1 contact-information-container">
    <h2>{{ __('Company information') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="company-name">{{ __('Company name') }}</label>
            <input type="text" name="company-name" id="company-name" value="" >
        </div>
        <div class="form-group w-1/2">
            <label for="commerce-number">{{ __('Chamber of commerce number') }}</label>
            <input type="number" name="commerce-number" id="commerce-number" value="" >
        </div>
        <div class="form-group w-1/2">
            <label for="vat-number">{{ __('VAT number ') }}</label>
            <input type="text" name="vat-number" id="vat-number" value="">
        </div>
        <div class="form-group w-1/2">
            <label for="company-address">{{ __('Address') }}</label>
            <input type="text" name="company-address" id="company-address" value="" >
        </div>
        <div class="form-group w-1/2">
            <label for="zip-code">{{ __('ZIP code') }}</label>
            <input type="text" name="zip-code" id="zip-code" value="" >
        </div>
        <div class="form-group w-1/2">
            <label for="city">{{ __('City') }}</label>
            <input type="text" name="city" id="city" value="" >
        </div>
        <div class="form-group w-1/2">
            <label for="company-country">{{ __('Country') }}</label>
            <div class="select-wrapper">
                <select name="company-country" id="company-country">
                    <option value="">{{ __('Netherland') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
    </div>
</div>