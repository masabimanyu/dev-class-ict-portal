<div class="container-card-1 client-area-container">
    <h2>{{ __('Change password') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="username">{{ __('User name') }}</label>
            <input id="username" type="text" placeholder="">
        </div>
        <div class="form-group w-1/2">
            <label for="current-password">{{ __('Current password') }}</label>
            <input id="current-password" type="password">
        </div>
        <div class="form-group w-1/2">
            <label for="new-password">{{ __('New password') }}</label>
            <input id="new-password" type="password">
        </div>
        <div class="form-group w-1/2">
            <label for="repeat-password">{{ __('Repeat password') }}</label>
            <input id="repeat-password" type="password">
        </div>
    </div>
</div>