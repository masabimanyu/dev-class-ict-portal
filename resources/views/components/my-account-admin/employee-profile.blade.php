<div class="container-card-1 client-area-container">
    <h2>{{ __('Employee profile') }}</h2>
    <div class="input-container form">
        <div class="form-group w-1/2">
            <label for="employeeName">{{ __('Name') }}</label>
            <input id="employeeName" type="text" placeholder="John Doe">
        </div>
        <div class="form-group w-1/2">
            <label for="employeeEmail">{{ __('E-mail address') }}</label>
            <input id="employeeEmail" type="text" placeholder="example@mail.com">
        </div>
        <div class="form-group w-1/2">
            <label for="employeeLang">{{ __('Backoffice language') }}</label>
            <div class="select-wrapper">
                <select name="employeeLang" id="employeeLang">
                    <option value="">{{ __('English') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
    </div>
</div>