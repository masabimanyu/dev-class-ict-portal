<div class="comp-sidebar">
    <img class="sidebar-img" src="{{ asset('/assets/images/ict-sb-logo.webp') }}" alt="">

    <ul class="sidebar-menu-wrapper">

        <li class="sidebar-menu-item">
            <a class="{{ (request()->is('user*')) ? 'active' : '' }} move-effect" href="{{ URL::to('/user') }}">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.375 4.875H4.875V11.375H11.375V4.875Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M21.125 4.875H14.625V11.375H21.125V4.875Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M11.375 14.625H4.875V21.125H11.375V14.625Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M21.125 14.625H14.625V21.125H21.125V14.625Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                {{ __('components.sidebar.menu-1') }}
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ URL::to('/domain') }}" class="{{ (request()->is('domain*')) ? 'active' : '' }} move-effect">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 22.75C18.3848 22.75 22.75 18.3848 22.75 13C22.75 7.61522 18.3848 3.25 13 3.25C7.61522 3.25 3.25 7.61522 3.25 13C3.25 18.3848 7.61522 22.75 13 22.75Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M3.80518 9.75H22.1946" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M3.80566 16.25H22.195" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M13 22.4883C15.2437 22.4883 17.0625 18.2402 17.0625 13C17.0625 7.75976 15.2437 3.51172 13 3.51172C10.7563 3.51172 8.9375 7.75976 8.9375 13C8.9375 18.2402 10.7563 22.4883 13 22.4883Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                {{ __('components.sidebar.menu-2') }}
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ URL::to('/hosting') }}" class="{{ (request()->is('hosting*')) ? 'active' : '' }} move-effect">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M13 13C17.936 13 21.9375 10.8174 21.9375 8.125C21.9375 5.43261 17.936 3.25 13 3.25C8.06395 3.25 4.0625 5.43261 4.0625 8.125C4.0625 10.8174 8.06395 13 13 13Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M4.0625 8.125V13C4.0625 15.6924 8.06395 17.875 13 17.875C17.936 17.875 21.9375 15.6924 21.9375 13V8.125" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M4.0625 13V17.875C4.0625 20.5674 8.06395 22.75 13 22.75C17.936 22.75 21.9375 20.5674 21.9375 17.875V13" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                {{ __('components.sidebar.menu-3') }}
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ URL::to('/product') }}" class="{{ (request()->is('product*')) ? 'active' : '' }} move-effect">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3.24834 14.9432C2.71963 13.9928 2.45528 13.5175 2.45528 12.9987C2.45528 12.4799 2.71963 12.0046 3.24834 11.0542L4.80017 8.26453L6.44018 5.52577C6.99893 4.59268 7.2783 4.12613 7.72763 3.86671C8.17696 3.60729 8.72069 3.59861 9.80815 3.58127L13 3.53036L16.1918 3.58127C17.2793 3.59861 17.823 3.60729 18.2723 3.86671C18.7217 4.12613 19.0011 4.59268 19.5598 5.52577L21.1998 8.26453L22.7516 11.0542C23.2803 12.0046 23.5447 12.4799 23.5447 12.9987C23.5447 13.5175 23.2803 13.9928 22.7516 14.9432L21.1998 17.7329L19.5598 20.4716C19.0011 21.4047 18.7217 21.8713 18.2723 22.1307C17.823 22.3901 17.2793 22.3988 16.1918 22.4161L13 22.467L9.80815 22.4161C8.72069 22.3988 8.17696 22.3901 7.72763 22.1307C7.2783 21.8713 6.99893 21.4047 6.44018 20.4716L4.80017 17.7329L3.24834 14.9432Z" stroke="currentColor" stroke-width="2"/>
                        <circle cx="13" cy="13" r="3.25" stroke="currentColor" stroke-width="2"/>
                    </svg>
                </div>
                {{ __('Manage Product') }}
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ route('manage-service.registrar') }}" class="{{ (request()->is('manage-service*')) ? 'active' : '' }} move-effect">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.71875 11.375C9.73804 11.375 11.375 9.73804 11.375 7.71875C11.375 5.69946 9.73804 4.0625 7.71875 4.0625C5.69946 4.0625 4.0625 5.69946 4.0625 7.71875C4.0625 9.73804 5.69946 11.375 7.71875 11.375Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M18.2812 11.375C20.3005 11.375 21.9375 9.73804 21.9375 7.71875C21.9375 5.69946 20.3005 4.0625 18.2812 4.0625C16.262 4.0625 14.625 5.69946 14.625 7.71875C14.625 9.73804 16.262 11.375 18.2812 11.375Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M7.71875 21.9375C9.73804 21.9375 11.375 20.3005 11.375 18.2812C11.375 16.262 9.73804 14.625 7.71875 14.625C5.69946 14.625 4.0625 16.262 4.0625 18.2812C4.0625 20.3005 5.69946 21.9375 7.71875 21.9375Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M18.2812 15.4375V21.125" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M21.125 18.2812H15.4375" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
                {{ __('Manage services') }}
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a href="{{ URL::to('/invoice') }}" class="{{ (request()->is('invoice*')) ? 'active' : '' }} move-effect">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.20825 13.5415L16.7916 13.5415" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                        <path d="M9.20825 16.7915L13.5416 16.7915" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                        <path d="M5.95825 6.1915C5.95825 5.35142 5.95825 4.93139 6.12174 4.61052C6.26555 4.32828 6.49502 4.0988 6.77727 3.95499C7.09813 3.7915 7.51817 3.7915 8.35825 3.7915H13.6308C13.9977 3.7915 14.1811 3.7915 14.3538 3.83295C14.5068 3.86969 14.6531 3.9303 14.7873 4.01254C14.9387 4.10531 15.0684 4.23502 15.3279 4.49445L19.3386 8.50523C19.5981 8.76465 19.7278 8.89437 19.8205 9.04574C19.9028 9.17995 19.9634 9.32627 20.0001 9.47932C20.0416 9.65195 20.0416 9.8354 20.0416 10.2023V19.8082C20.0416 20.6483 20.0416 21.0683 19.8781 21.3892C19.7343 21.6714 19.5048 21.9009 19.2226 22.0447C18.9017 22.2082 18.4817 22.2082 17.6416 22.2082H8.35825C7.51817 22.2082 7.09813 22.2082 6.77727 22.0447C6.49502 21.9009 6.26555 21.6714 6.12174 21.3892C5.95825 21.0683 5.95825 20.6483 5.95825 19.8082V6.1915Z" stroke="currentColor" stroke-width="1.5"/>
                        <path d="M13.5417 3.7915V7.8915C13.5417 8.73158 13.5417 9.15162 13.7052 9.47249C13.849 9.75473 14.0785 9.9842 14.3608 10.128C14.6816 10.2915 15.1017 10.2915 15.9417 10.2915H20.0417" stroke="currentColor" stroke-width="1.5"/>
                    </svg>
                </div>
                {{ __('components.sidebar.menu-5') }}
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a class="move-effect" href="">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M16.5834 8.66683C16.5834 10.6458 14.9791 12.2502 13.0001 12.2502C11.0211 12.2502 9.41675 10.6458 9.41675 8.66683C9.41675 6.68781 11.0211 5.0835 13.0001 5.0835C14.9791 5.0835 16.5834 6.68781 16.5834 8.66683Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                        <path d="M5.34194 18.0524C6.04762 15.7268 8.4564 14.625 10.8867 14.625H15.1133C17.5436 14.625 19.9524 15.7268 20.6581 18.0524C20.8485 18.6798 21.0008 19.3761 21.0753 20.1263C21.1298 20.6759 20.6773 21.125 20.125 21.125H5.875C5.32272 21.125 4.87017 20.6759 4.92474 20.1263C4.99922 19.3761 5.15154 18.6798 5.34194 18.0524Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>
                {{ __('components.sidebar.menu-6') }}
            </a>
        </li>

        <li class="sidebar-menu-item">
            <a class="move-effect" href="{{ URL::to('testScheduleInvoice') }}">
                <div class="icon">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.20825 13.5415L16.7916 13.5415" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                        <path d="M9.20825 16.7915L13.5416 16.7915" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                        <path d="M5.95825 6.1915C5.95825 5.35142 5.95825 4.93139 6.12174 4.61052C6.26555 4.32828 6.49502 4.0988 6.77727 3.95499C7.09813 3.7915 7.51817 3.7915 8.35825 3.7915H13.6308C13.9977 3.7915 14.1811 3.7915 14.3538 3.83295C14.5068 3.86969 14.6531 3.9303 14.7873 4.01254C14.9387 4.10531 15.0684 4.23502 15.3279 4.49445L19.3386 8.50523C19.5981 8.76465 19.7278 8.89437 19.8205 9.04574C19.9028 9.17995 19.9634 9.32627 20.0001 9.47932C20.0416 9.65195 20.0416 9.8354 20.0416 10.2023V19.8082C20.0416 20.6483 20.0416 21.0683 19.8781 21.3892C19.7343 21.6714 19.5048 21.9009 19.2226 22.0447C18.9017 22.2082 18.4817 22.2082 17.6416 22.2082H8.35825C7.51817 22.2082 7.09813 22.2082 6.77727 22.0447C6.49502 21.9009 6.26555 21.6714 6.12174 21.3892C5.95825 21.0683 5.95825 20.6483 5.95825 19.8082V6.1915Z" stroke="currentColor" stroke-width="1.5"/>
                        <path d="M13.5417 3.7915V7.8915C13.5417 8.73158 13.5417 9.15162 13.7052 9.47249C13.849 9.75473 14.0785 9.9842 14.3608 10.128C14.6816 10.2915 15.1017 10.2915 15.9417 10.2915H20.0417" stroke="currentColor" stroke-width="1.5"/>
                    </svg>
                </div>
                {{ __('Test scheduling invoice') }}
            </a>
        </li>

        <li class="sidebar-menu-item logout">
            <a href="">
                <div class="icon">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.25 2.25V1.5H1.5V2.25H2.25ZM9.21967 10.2803C9.51256 10.5732 9.98744 10.5732 10.2803 10.2803C10.5732 9.98744 10.5732 9.51256 10.2803 9.21967L9.21967 10.2803ZM3 8.25V2.25H1.5V8.25H3ZM2.25 3H8.25V1.5H2.25V3ZM1.71967 2.78033L9.21967 10.2803L10.2803 9.21967L2.78033 1.71967L1.71967 2.78033Z" fill="currentColor"/>
                        <path d="M3 11.25V11.25C3 11.947 3 12.2955 3.05764 12.5853C3.29436 13.7753 4.22466 14.7056 5.41473 14.9424C5.70453 15 6.05302 15 6.75 15H9C11.8284 15 13.2426 15 14.1213 14.1213C15 13.2426 15 11.8284 15 9V6.75C15 6.05302 15 5.70453 14.9424 5.41473C14.7056 4.22466 13.7753 3.29436 12.5853 3.05764C12.2955 3 11.947 3 11.25 3V3" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>
                {{ __('components.navbar.logout') }}
            </a>
        </li>

    </ul>

    <div class="lang">
        <div class="lang__select">
            <ul class="lang__select-list js-lang-list">
                <li class="active"><img src="{{ asset('/assets/images/nl.svg') }}" alt="Lang NL" width="32"
                        height="22"> NL
                </li>
                <li><img src="{{ asset('/assets/images/en.svg') }}" alt="Lang EN" width="32" height="22"> EN
                </li>
            </ul>
        </div>
    </div>
</div>
