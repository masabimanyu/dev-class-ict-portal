<form action="#" id="domain_contact">
    <div class="modal-card modal-contact-information container-card-1">
        <div class="row row-contact-information">

            <div class="close">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M18.5303 6.53033C18.8232 6.23744 18.8232 5.76256 18.5303 5.46967C18.2374 5.17678 17.7626 5.17678 17.4697 5.46967L18.5303 6.53033ZM5.46967 17.4697C5.17678 17.7626 5.17678 18.2374 5.46967 18.5303C5.76256 18.8232 6.23744 18.8232 6.53033 18.5303L5.46967 17.4697ZM6.53033 5.46967C6.23744 5.17678 5.76256 5.17678 5.46967 5.46967C5.17678 5.76256 5.17678 6.23744 5.46967 6.53033L6.53033 5.46967ZM17.4697 18.5303C17.7626 18.8232 18.2374 18.8232 18.5303 18.5303C18.8232 18.2374 18.8232 17.7626 18.5303 17.4697L17.4697 18.5303ZM17.4697 5.46967L5.46967 17.4697L6.53033 18.5303L18.5303 6.53033L17.4697 5.46967ZM5.46967 6.53033L17.4697 18.5303L18.5303 17.4697L6.53033 5.46967L5.46967 6.53033Z" fill="#1A1A1A"/>
                </svg>
            </div>

            <div class="title">
                <h3>Contact information</h3>
            </div>

            <input type="hidden" name="client_id" id="contact_client_id" value="">

            <div class="container-card-1">
                <div class="input-container form">

                    <div class="form-group radio w-1/3">
                        <div class="radio-wrapper">
                            <p class="label">Client contact</p>
                            <input type="radio" name="client_contact" value="1" id="client_contact_yes">
                            <label for="#client_contact_yes">Contact for client <p id="contact_debtor_id">DB00001</p></label><br>
                            <input type="radio" name="client_contact" value="0" id="client_contact_no">
                            <label for="client_contact_no">General contact for all clients</label><br>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="provider">Registrar</label>
                        <div class="select-wrapper">
                            <select name="provider" id="provider" required>
                                <option value="">Select registrar</option>
                                <option value="op">Open Provider</option>
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                        <p><span>{{ __('Note : ') }}</span>{{ __('Select the registrar that you want to link this contact to') }}</p>
                    </div>

                    <div class="form-group radio w-1/3">
                        <div class="radio-wrapper">
                            <input type="radio" name="client_contact_new" value="1" id="client_contact_new">
                            <label for="client_contact_new">Create a new contact at registrar</label><br>
                            <input type="radio" name="client_contact_new" value="0" id="client_contact_exist">
                            <label for="client_contact_exist">Contact already exists at registrar</label><br>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="#contact_id_register">{{ __('Contact ID register') }}</label>
                        <input type="text" placeholder="Contact ID register" id="contact_id_register">
                    </div>

                </div>
            </div>

            <div class="container-card-1">
                <div class="input-container form">

                    <div class="form-group">
                        <label for="#contact_phone_number">{{ __('Phone number') }}</label>
                        <input type="text" placeholder="+31858085070" id="contact_phone_number" name="contact_phone_number">
                    </div>

                    <div class="form-group">
                        <label for="#contact_fax_number">{{ __('Fax number') }}</label>
                        <input type="text" name="contact_fax_number" id="contact_fax_number">
                    </div>

                    <div class="form-group">
                        <label for="#contact_email">{{ __('E-mail address') }}</label>
                        <input type="email" placeholder="example@mail.com" name="contact_email" id="contact_email">
                    </div>

                </div>
            </div>


        </div>

        <div class="row row-client-information">
            <div class="title">
                <h3>Client information</h3>
            </div>

        <div class="container-card-1 card-company-detail">
            <div class="title">
                <h4>Company details</h4>
                <p class="js-card-company-detail">Use info existing contact</p>
                <p style="display: none" class="js-card-company-detail">Don't Use info existing contact</p>
            </div>
            <div class="input-container form">

                <div style="display: none" class="group-input select-client js-card-company-detail-field">
                    <div class="input-container form">

                        <div class="form-group select">

                            <div class="select-wrapper">
                                <select name="select_client" id="select_client">
                                    <option value="">{{ __('Select a client') }}</option>
                                </select>
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </div>

                        </div>

                        <div class="form-group cstm-fit-content">
                            <div class="submit-wrapper left">
                                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M15.2197 16.2803C15.5126 16.5732 15.9874 16.5732 16.2803 16.2803C16.5732 15.9874 16.5732 15.5126 16.2803 15.2197L15.2197 16.2803ZM13.0178 11.9572C12.7249 11.6643 12.2501 11.6643 11.9572 11.9572C11.6643 12.2501 11.6643 12.7249 11.9572 13.0178L13.0178 11.9572ZM13.5 8.25C13.5 11.1495 11.1495 13.5 8.25 13.5V15C11.9779 15 15 11.9779 15 8.25H13.5ZM8.25 13.5C5.3505 13.5 3 11.1495 3 8.25H1.5C1.5 11.9779 4.52208 15 8.25 15V13.5ZM3 8.25C3 5.3505 5.3505 3 8.25 3V1.5C4.52208 1.5 1.5 4.52208 1.5 8.25H3ZM8.25 3C11.1495 3 13.5 5.3505 13.5 8.25H15C15 4.52208 11.9779 1.5 8.25 1.5V3ZM16.2803 15.2197L13.0178 11.9572L11.9572 13.0178L15.2197 16.2803L16.2803 15.2197Z" fill="#951D81"/>
                                </svg>
                                <button data-target="modal-client-list" class="btn btn--transparent js-modal-nested-trigger">Search client</button>
                            </div>
                        </div>

                    </div>
                </div>

                    <div class="form-group w-1/2">
                        <label for="#contact_company_name">{{ __('Company name') }}</label>
                        <input type="text" placeholder="Company Example" name="contact_company_name" id="contact_company_name">
                    </div>

                    <div class="form-group w-1/2">
                        <label for="contact_company_legal_form">{{ __('Company Legal form') }}</label>

                        <div class="select-wrapper">
                            <select name="contact_company_legal_form" id="contact_company_legal_form">
                                <option value="">{{ __('choose here') }}</option>
                                <option value="Other or Unknown">Other or Unknown</option>
                                <option value="Private company">Private company</option>
                                <option value="Bv in formation">Bv in formation</option>
                                <option value="Cooperation">Cooperation</option>
                                <option value="Limited partnership">Limited partnership</option>
                                <option value="Proprietorship">Proprietorship</option>
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>

                    </div>

                    <div class="form-group w-1/2">
                        <label for="#contact_chamber_of_commerce">{{ __('Chamber of commerce number') }}</label>
                        <input type="number" placeholder="80513859" name="contact_chamber_of_commerce" id="contact_chamber_of_commerce">
                    </div>

                    <div class="form-group w-1/2">
                        <label for="#contact_vat_number">{{ __('VAT number') }}</label>
                        <input type="text" placeholder="NL002937821B41" name="contact_vat_number" id="contact_vat_number">
                    </div>

                </div>
            </div>

            <div class="container-card-1 card-contact-person">
                <h4>Contact person</h4>
                <div class="input-container form">

                    <div class="form-group w-1/3">
                        <label for="#contact_user_legal_form">{{ __('Legal form') }}</label>

                        <div class="select-wrapper">
                            <select name="contact_user_legal_form" id="contact_user_legal_form">
                                <option value="">{{ __('Choose here') }}</option>
                                <option value="Dhr.">Dhr.</option>
                                <option value="Mrs.">Mrs.</option>
                                <option value="Afd.">Afd.</option>
                                <option value="Unknown">Unknown</option>
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>

                    </div>

                    <div class="form-group w-1/3">
                        <label for="#contact_first_name">{{ __('First name') }}</label>
                        <input type="text" placeholder="Frank" name="contact_first_name" id="contact_first_name">
                    </div>

                    <div class="form-group w-1/3">
                        <label for="#contact_last_name">{{ __('Last name') }}</label>
                        <input type="text" placeholder="Vlemmix" name="contact_last_name" id="contact_last_name">
                    </div>

                </div>
            </div>

            <div class="container-card-1 card-address-data">
                <h4>Address data</h4>
                <div class="input-container form">

                    <div class="form-group w-1/2">
                        <label for="#contact_address">{{ __('Address') }}</label>
                        <input type="text" placeholder="Adelaartlaan 192" name="contact_address" id="contact_address">
                    </div>

                    <div class="form-group w-1/2">
                        <label for="#contact_postcode">{{ __('ZIP code') }}</label>
                        <input type="text" placeholder="5665CS" name="contact_postcode" id="contact_postcode">
                    </div>

                    <div class="form-group w-1/2">
                        <label for="#contact_city">{{ __('City') }}</label>
                        <input type="text" placeholder="Geldrop" name="contact_city" id="contact_city">
                    </div>

                    <div class="form-group w-1/2">
                        <label for="">{{ __('Country') }}</label>

                        <div class="select-wrapper">
                            <select name="contact_country" id="contact_country">
                                <option value="">{{ __('Choose country') }}</option>
                                <option value="NL">Nederland</option>
                                <option value="BE">Belgie</option>
                                <option value="BG">Bulgarije</option>
                                <option value="CY">Cyprus</option>
                                <option value="DK">Denemarken</option>
                                <option value="THE">Duitsland</option>
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>

                    </div>

                </div>
            </div>


        </div>

        <div class="row row-submit">
            <button class="btn btn--primary--outline" type="button" >Cancel</button>
            <button class="btn btn--primary js-contact-add-customer" type="button" >Add client</button>
        </div>

    </div>

</form>
