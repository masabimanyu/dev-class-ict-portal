@if($modalType == 'contact-information')
    @include('components/modals/contact-information')



@elseif($modalType == 'domain-list')
    @include('components/modals/domain-list')



@elseif($modalType == 'hosting-list')
    @include('components/modals/hosting-list')



@elseif($modalType == 'client-list')
    @include('components/modals/client-list')


@endif