<div class="modal-card modal-hosting-list container-card-1">

    <div class="close">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18.5303 6.53033C18.8232 6.23744 18.8232 5.76256 18.5303 5.46967C18.2374 5.17678 17.7626 5.17678 17.4697 5.46967L18.5303 6.53033ZM5.46967 17.4697C5.17678 17.7626 5.17678 18.2374 5.46967 18.5303C5.76256 18.8232 6.23744 18.8232 6.53033 18.5303L5.46967 17.4697ZM6.53033 5.46967C6.23744 5.17678 5.76256 5.17678 5.46967 5.46967C5.17678 5.76256 5.17678 6.23744 5.46967 6.53033L6.53033 5.46967ZM17.4697 18.5303C17.7626 18.8232 18.2374 18.8232 18.5303 18.5303C18.8232 18.2374 18.8232 17.7626 18.5303 17.4697L17.4697 18.5303ZM17.4697 5.46967L5.46967 17.4697L6.53033 18.5303L18.5303 6.53033L17.4697 5.46967ZM5.46967 6.53033L17.4697 18.5303L18.5303 17.4697L6.53033 5.46967L5.46967 6.53033Z" fill="#1A1A1A"/>
        </svg>
    </div>
    
    <div class="title">
        <h3>Hosting list</h3>
        <p>Click on the row to use data</p>
    </div>

    <div class="table-wrapper">
        <table class="table-content">
            <thead>
                <th>{{ __('Username') }}</th>
                <th>{{ __('Domain') }}</th>
                <th>{{ __('Status') }}</th>
            </thead>
            <tbody>
                {{-- Loop this item --}}
                <tr>
                    <td>classict</td>
                    <td>classict.com</td>
                    <td>
                        {{-- If active --}}
                        <div class="label label-active">
                            Active
                        </div>
                        {{-- If deactive --}}
                        {{-- <div class="label label-deactive">
                            Deactive
                        </div> --}}
                    </td>
                </tr>
                {{-- Loop this item END --}}
            </tbody>
        </table>
    </div>

    <div class="pagination-wrapper">
        <div class="item-wrapper">

            <div class="item-pagination first">First</div>
            <div class="item-pagination prev">
                <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M13.125 17.0625L6.5625 10.5L13.125 3.9375" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>

        </div>
        
        <div class="item-wrapper">
            
            <div class="item-pagination current">1</div>
            <div class="item-pagination">2</div>
            <div class="item-pagination">3</div>
            <div class="item-pagination skip">.....</div>
            <div class="item-pagination">13</div>

        </div>
        
        <div class="item-wrapper">

            <div class="item-pagination next">
                <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7.875 3.9375L14.4375 10.5L7.875 17.0625" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
    
            <div class="item-pagination last">Last</div>
        
        </div>
    </div>
</div>