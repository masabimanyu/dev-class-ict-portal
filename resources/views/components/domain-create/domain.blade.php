
<div class="container-card-1 card-domain">
    <h2>{{ __('Domain') }}</h2>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <div class="search-wrapper">
                    <input type="search" name="search_domain_name" placeholder="Search domain" id="search_domain_name" required>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="11" cy="11" r="7" stroke="#33363F" stroke-width="1.5"/>
                        <path d="M20 20L17 17" stroke="#33363F" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>
            </div>

            <div class="form-group cstm-fit-content">
                <div class="select-wrapper cstm-fit-content">
                    <select name="select_domain_extension" id="select_domain_extension" required>
                        {{-- later this option will be dynamic --}}
                        <option value="">Select Extension</option>
                        <option value="be">{{ __('.be') }}</option>
                        <option value="co.uk">{{ __('.co.uk') }}</option>
                        <option value="nl">{{ __('.nl') }}</option>
                        <option value="com">{{ __('.com') }}</option>
                        <option value="de">{{ __('.de') }}</option>
                        <option value="eu">{{ __('.eu') }}</option>
                        <option value="fr">{{ __('.fr') }}</option>
                        <option value="info">{{ __('.info') }}</option>
                        <option value="it">{{ __('.it') }}</option>
                        <option value="net">{{ __('.net') }}</option>
                        <option value="nl">{{ __('.nl') }}</option>
                        <option value="shop">{{ __('.shop') }}</option>
                        <option value="space">{{ __('.space') }}</option>
                        <option value="uk">{{ __('.uk') }}</option>
                    </select>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>

            <div class="form-group w-1/2 cstm-fit-content">
                <div class="submit-wrapper">
                    <input class="btn btn--primary" type="button" id="checkDomain" value="Check">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_1212_9015)">
                        <path d="M6.75 3.375L12.375 9L6.75 14.625" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </g>
                        <defs>
                        <clipPath id="clip0_1212_9015">
                        <rect width="18" height="18" fill="currentColor"/>
                        </clipPath>
                        </defs>
                    </svg>
                </div>
            </div>

            <div class="m-y-auto">
                <span class="js-domain-status"></span>
                @include('components/loading-spin', ['class' => 'd-none js-loading-spin-domain'])
            </div>
        </div>
    </div>

    <div class="group-input js-authorization-code" style="display: none">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="#auth">{{ __('Authorization code') }}</label>
                <input type="text" name="auth" id="auth">
                <p><span>{{ __('Note : ') }}</span>{{ __('You have to enter the authorization code to transfer a domain.') }}</p>
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="">{{ __('Hosting') }}</label>
                <div class="select-wrapper">
                    <select name="hosting" id="hosting">
                        <option value="">{{ __('Select hosting') }}</option>
                    </select>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>

            <div class="form-group w-1/2 cstm-fit-content">
                <div class="submit-wrapper left">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.2197 16.2803C15.5126 16.5732 15.9874 16.5732 16.2803 16.2803C16.5732 15.9874 16.5732 15.5126 16.2803 15.2197L15.2197 16.2803ZM13.0178 11.9572C12.7249 11.6643 12.2501 11.6643 11.9572 11.9572C11.6643 12.2501 11.6643 12.7249 11.9572 13.0178L13.0178 11.9572ZM13.5 8.25C13.5 11.1495 11.1495 13.5 8.25 13.5V15C11.9779 15 15 11.9779 15 8.25H13.5ZM8.25 13.5C5.3505 13.5 3 11.1495 3 8.25H1.5C1.5 11.9779 4.52208 15 8.25 15V13.5ZM3 8.25C3 5.3505 5.3505 3 8.25 3V1.5C4.52208 1.5 1.5 4.52208 1.5 8.25H3ZM8.25 3C11.1495 3 13.5 5.3505 13.5 8.25H15C15 4.52208 11.9779 1.5 8.25 1.5V3ZM16.2803 15.2197L13.0178 11.9572L11.9572 13.0178L15.2197 16.2803L16.2803 15.2197Z" fill="#951D81"/>
                    </svg>
                    <button data-target="modal-hosting-list" class="btn btn--transparent js-modal-trigger">Search hosting</button>
                </div>
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="">{{ __('Status') }}</label>
                <div class="select-wrapper">
                    <select name="status" id="status">
                        @if(count($domainStatus) > 0 )
                            @foreach ($domainStatus as $statusIndex => $status)
                                <option value="{{ $statusIndex }}"> {{ $status }} </option>
                            @endforeach
                        @else
                            <option value="">{{ __('there is no domain status set') }}</option>
                        @endif
                    </select>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <div class="form-group checkbox">
                    <label for="#domain_registration_direct">{{ __('Start domain registration directly') }}</label>
                    <input type="checkbox" name="domain_registration_direct" id="domain_registration_direct">
                </div>
            </div>

        </div>
    </div>

</div>
