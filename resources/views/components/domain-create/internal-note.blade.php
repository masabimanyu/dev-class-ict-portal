<div class="container-card-1 card-internal-note">
    <h2>{{ __('Internal note') }}</h2>

    <div class="input-container form">

        <div class="form-group">
            <textarea name="internal_note" id="" cols="30" rows="10"></textarea>
        </div>

    </div>

</div>
