<div class="container-card-1 card-financial">
    <h2>{{ __('Financial') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="">{{ __('Product') }}</label>

            <div class="select-wrapper">
                <select name="search_product" id="searchProductFinancial">
                    @if(count($products))
                        @foreach($products as $product)
                            <option value="{{ $product['id'] }}" data-period="{{ $product->price_per }}" data-price-exclude="{{ $product->cost_price_exclude }}" >
                                {{ $product['product_code']." ".$product['product_name'] }}
                                ( €{{ $product->cost_price_exclude }}/{{ $product->period }} )
                            </option>
                        @endforeach
                    @else
                        <option value="">{{ __('there is no products for now') }}</option>
                    @endif
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>

        </div>

        <div class="form-group w-1/2">
            <label for="">{{ __('Description') }}</label>
            <input type="text" name="description" id="desc" placeholder="Domeinnaam - .nl">
        </div>

        <div class="group-input">
            <div class="input-container form">

                <div class="form-group price">
                    <label for="#price_exclude">{{ __('Price excl. tax') }}</label>
                    <input type="text" name="price_exclude" id="price_exclude">
                    <p>{{ __('per') }} <span class="periodic-text">Year</span>  € <span class="product_incl_tax">5,99</span> {{ __('incl. tax') }}</p>
                </div>

                <div class="form-group tax-rate">
                    <label for="">{{ __('Tax rate') }}</label>
                    <div class="select-wrapper cstm-fit-content">
                        <select name="tax" id="tax">
                            @if(count($taxes) > 0)
                                @foreach ($taxes as $tax)
                                    @if($tax["Visibility"] == "always")
                                        <option data-rate="{{ $tax['Rate'] }}" value="{{ $tax["TaxCode"] }}" {{ $tax['IsDefault'] == "yes" ? "selected" : ""}}>{{ $tax["Name"] }}</option>
                                    @endif
                                @endforeach
                            @else
                                <option value="">{{ __('there is no taxes for now') }}</option>
                            @endif
                        </select>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                </div>

                <div class="form-group discount">
                    <label for="">{{ __('Discount on recurring profile') }}</label>
                    <input type="text" name="discount_recurring" id="desc">
                    <p>{{ __('%') }}</p>
                </div>

            </div>
        </div>

    </div>
</div>
