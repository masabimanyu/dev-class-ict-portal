<div class="container-card-1 card-billing-period">
    <h2>{{ __('Billing Period') }}</h2>

    <div class="input-container form">

        <div class="form-group">

            <div class="group-input billing-cycle">
                <div class="input-container form">

                    <p class="label">{{ __('Billing Cycle') }}</p>

                    <div class="form-group number">
                        <input type="number" name="billing_cycle_length" id="billing_cycle_length">
                    </div>

                    <div class="form-group month">
                        <div class="select-wrapper cstm-fit-content">
                            <select name="billing_cycle_per" id="billingCyclePer">
                                @if(count($periodic) > 0)
                                    @foreach ($periodic as $key => $period)
                                        <option value="{{ $key }}"> {{ $period }} </option>
                                    @endforeach
                                @else
                                    <option value="">{{ __( 'there is no period from wefact') }}</option>
                                @endif
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>

                </div>
            </div>

            <div class="group-input billing-period">
                <div class="input-container form">

                    <p class="label">{{ __('Billing period') }}</p>

                    <div class="form-group w-1/2">
                        <input type="date" name="billing_period_start" value="{{ date('Y-m-d') }}" id="fromDate">
                    </div>

                    
                    <div class="form-group date">
                        <p class="till">
                            Till 
                            <span class="end-date"></span>
                        </p>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>
