<div class="container-card-1 card-account-information">
    <h2>{{ __('Account information') }}</h2>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="">{{ __('Domain name') }}</label>
                <input type="text" name="domain_name" id="domain_name_hosting">
            </div>

            <div class="form-group w-1/2 cstm-fit-content">
                <div class="submit-wrapper left">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.2197 16.2803C15.5126 16.5732 15.9874 16.5732 16.2803 16.2803C16.5732 15.9874 16.5732 15.5126 16.2803 15.2197L15.2197 16.2803ZM13.0178 11.9572C12.7249 11.6643 12.2501 11.6643 11.9572 11.9572C11.6643 12.2501 11.6643 12.7249 11.9572 13.0178L13.0178 11.9572ZM13.5 8.25C13.5 11.1495 11.1495 13.5 8.25 13.5V15C11.9779 15 15 11.9779 15 8.25H13.5ZM8.25 13.5C5.3505 13.5 3 11.1495 3 8.25H1.5C1.5 11.9779 4.52208 15 8.25 15V13.5ZM3 8.25C3 5.3505 5.3505 3 8.25 3V1.5C4.52208 1.5 1.5 4.52208 1.5 8.25H3ZM8.25 3C11.1495 3 13.5 5.3505 13.5 8.25H15C15 4.52208 11.9779 1.5 8.25 1.5V3ZM16.2803 15.2197L13.0178 11.9572L11.9572 13.0178L15.2197 16.2803L16.2803 15.2197Z" fill="#951D81"/>
                    </svg>
                    <button type="button" data-target="modal-domain-list" class="btn btn--transparent js-modal-trigger">Search domain</button>
                </div>
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group checkbox">
                <label for="#alt_invoicing_and_payment">
                    <p>{{ __('Add domain in HostFact') }}</p>
                    <p><span>{{ __('Note : ') }}</span>{{ __('After creating the hosting account you will be redirected to add the domain') }}</p>
                </label>
                <input type="checkbox" name="direct_create_domain" id="direct_create_domain">
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="">{{ __('Password') }}</label>
                <input type="password" name="random_password" id="currentPassword">
            </div>

            <div class="form-group w-1/2 cstm-fit-content">
                <div class="submit-wrapper left">
                    <button class="btn btn--transparent cstm-btn" type="button" id="generateRandomPassword">Use random password</button>
                </div>
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="hosting_account_name">{{ __('Account name') }}</label>
                <input type="text" name="hosting_account_name" id="hosting_account_name">
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="">{{ __('Status') }}</label>
                <div class="select-wrapper cstm-fit-content">
                    <select name="status" id="status">
                        @if (count($hostingStatus) > 0)
                            @foreach ($hostingStatus as $key => $hs)
                                <option value="{{$key}}">{{ $hs }}</option>
                            @endforeach
                        @else
                            <option value=""> {{ __('there is no hosting status') }} </option>
                        @endif
                    </select>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>

        </div>
    </div>

    <div class="group-input">
        <div class="input-container form">

            <div class="form-group checkbox">
                <label for="#direct_create_hosting">{{ __('Create account directly on the server') }}</label>
                <input type="checkbox" name="direct_create_hosting" id="direct_create_hosting">
            </div>

        </div>
    </div>

</div>
