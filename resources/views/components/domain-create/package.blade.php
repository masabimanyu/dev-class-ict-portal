<div class="container-card-1 card-package">
    <h2>{{ __('Package') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="">{{ __('Server') }}</label>

            <div class="select-wrapper">
                <select name="hosting_server" id="hostingServer">
                    <option value="">{{ __('Choose here') }}</option>
                    {{-- @foreach($users["debtors"] as $user)
                        <option value="{{ $user['DebtorCode'] }}">{{ $user['DebtorCode'].' '.$user['CompanyName'] }}</option>
                    @endforeach --}}
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>

        </div>

        <div class="form-group w-1/2">
            <label for="">{{ __('Package') }}</label>

            <div class="select-wrapper">
                <select name="hosting_package" id="hostingPackage">
                    <option value="">{{ __('Choose here') }}</option>
                    {{-- @foreach($users["debtors"] as $user)
                        <option value="{{ $user['DebtorCode'] }}">{{ $user['DebtorCode'].' '.$user['CompanyName'] }}</option>
                    @endforeach --}}
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>

        </div>

    </div>
</div>
