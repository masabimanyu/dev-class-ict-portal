<div class="container-card-1 card-domain-contacts">
    <h2>{{ __('Domain contacts') }}</h2>

    <div class="input-container form">

        <div class="group-input w-1/3">
            <div class="form-group radio">
                <div class="radio-wrapper">
                    <p class="label">Tech contact</p>
                    <input type="radio" id="techYes" checked name="tech_contact" value="1">
                    <label for="techYes">Use client information <button  type="button" class="btn btn--transparent js-modal-trigger" data-target="modal-contact-information" >Change</button></label><br>
                    <input type="radio" id="techNo" name="tech_contact" value="0">
                    <label for="techNo">Use different contact</label><br>
                </div>
            </div>
            @include('../../components/domain-create/choose-owner')
        </div>
        <div class="group-input w-1/3">
            <div class="form-group radio">
                <div class="radio-wrapper">
                    <p class="label">Admin contact</p>
                    <input type="radio" id="adminYes" checked name="admin_contact" value="1">
                    <label for="adminYes">Use client information</label><br>
                    <input type="radio" id="adminNo" name="admin_contact" value="0">
                    <label for="adminNo">Use different contact</label><br>
                </div>
            </div>
        </div>
        <div class="group-input w-1/3">
            <div class="form-group radio">
                <div class="radio-wrapper">
                    <p class="label">Domain Owner</p>
                    <input type="radio" id="ownerYes" checked name="domain_owner" value="1">
                    <label for="ownerYes">Use client information</label><br>
                    <input type="radio" id="ownerNo" name="domain_owner" value="0">
                    <label for="ownerNo">Use different contact</label><br>
                </div>
            </div>
        </div>
    </div>

</div>
