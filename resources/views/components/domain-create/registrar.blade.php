<div class="container-card-1 card-registrar">
    <div class="card-registrar__left">
        <h2>{{ __('Registrar and nameservers') }}</h2>

        <div class="input-container form">

            <div class="form-group w-1/2">
                <label for="">{{ __('Registrar') }}</label>
                <div class="select-wrapper cstm-fit-content">
                    <select name="registrar" id="registrar" required>
                        <option value="">{{ __('Select Registrar') }}</option>
                        <option value="open_provider">Open Provider</option>
                    </select>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>

            <div class="form-group w-1/2">
                <label for="">{{ __('DNS template') }}</label>
                <div class="select-wrapper cstm-fit-content">
                    <select name="dns_template" id="dns_template">
                        <option value="">{{ __('Choose template') }}</option>
                        @foreach ($dnsTemplates['results'] as $template)
                            <option value="{{ $template["id"] }}"> {{ $template["name"] }} </option>
                        @endforeach
                    </select>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>

        </div>

    </div>

    <div class="card-registrar__right">
        <h2>{{ __('Server name') }}</h2>

        <div class="input-container form">

            <div class="group-input">

                <div class="form-group w-1/2">
                    <label for="">{{ __('Server name 1') }}</label>
                    <input type="text" name="name_server_1" id="name_server_1" placeholder="jon.quicns.com">
                </div>

                <div class="form-group w-1/2 js-ip">
                    <label for="ip_1">{{ __('IP address') }}</label>
                    <input type="text" name="ip_1" id="ip_1">
                </div>

            </div>

            <div class="group-input">

                <div class="form-group w-1/2">
                    <label for="">{{ __('Server name 2') }}</label>
                    <input type="text" name="name_server_2" id="name_server_2" placeholder="kevin.quicns.com">
                </div>

                <div class="form-group w-1/2 js-ip">
                    <label for="ip_2">{{ __('IP address') }}</label>
                    <input type="text" name="ip_2" id="ip_2">
                </div>

            </div>

            <div class="group-input">

                <div class="form-group w-1/2">
                    <label for="">{{ __('Server name 3') }}</label>
                    <input type="text" name="name_server_3" id="name_server_3">
                </div>

                <div class="form-group w-1/2 js-ip">
                    <label for="ip_3">{{ __('IP address') }}</label>
                    <input type="text" name="ip_3" id="ip_3">
                </div>

            </div>

            <div class="toggle-action">
                <label class="switch">
                    <input type="checkbox" class="js-ip-toggle">
                    <span class="slider round"></span>
                    <p class="yes">Yes</p>
                    <p class="no">No</p>
                </label>
                <p>Show IP addresses and nameservers</p>
            </div>

        </div>

    </div>

</div>
