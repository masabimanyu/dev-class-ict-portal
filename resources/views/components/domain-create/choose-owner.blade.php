<div class="form-group choose-owner js-choose-owner d-none">
    <label for="search_account_contact">Choose owner</label>
    <div class="search-wrapper">
        <input type="search" name="search_account_contact" placeholder="Search contact" id="search_account_contact">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="11" cy="11" r="7" stroke="#33363F" stroke-width="1.5"/>
            <path d="M20 20L17 17" stroke="#33363F" stroke-width="1.5" stroke-linecap="round"/>
        </svg>
    </div>
    <svg class="js-modal-trigger" data-target="modal-contact-information" width="64" height="38" viewBox="0 0 64 38" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M23 13.75V24.25M17.75 19H28.25" stroke="#951D81" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        <g clip-path="url(#clip0_1496_39698)">
        <path d="M41 21.25C43.4853 21.25 45.5 19.2353 45.5 16.75C45.5 14.2647 43.4853 12.25 41 12.25C38.5147 12.25 36.5 14.2647 36.5 16.75C36.5 19.2353 38.5147 21.25 41 21.25Z" stroke="#951D81" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M34.1787 25.1868C34.8703 23.9898 35.8647 22.9959 37.062 22.3048C38.2593 21.6138 39.6174 21.25 40.9999 21.25C42.3823 21.25 43.7404 21.6138 44.9377 22.3049C46.135 22.996 47.1294 23.9899 47.821 25.187" stroke="#951D81" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        </g>
        <defs>
        <clipPath id="clip0_1496_39698">
        <rect width="18" height="18" fill="white" transform="translate(32 10)"/>
        </clipPath>
        </defs>
    </svg>
</div>
