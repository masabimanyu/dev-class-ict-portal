<div id="hostingBilling" class="container-card-1 card-preferences js-row-service">
    <h2>{{ __('Preferences') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="">{{ __('Send a notification before a new billing cycle will be invoiced') }}</label>

            <div class="select-wrapper">
                <select name="client_send_notification" id="client_send_notification">
                    <option value="2">{{ __('Use default settings (Do not send)') }}</option>
                    <option value="1">{{ __('Send always') }}</option>
                    <option value="0">{{ __('Never Send') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>

        </div>

        <div class="form-group w-1/2">
            <label for="">{{ __('Billing preferences') }}</label>

            <div class="select-wrapper">
                <select name="bill_direct_debit" id="bill_direct_debit">
                    <option value="1">{{ __('Use client settings') }}</option>
                    <option value="0">{{ __('Never a direct debit') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>

        </div>

    </div>

    <div class="email-reminder">
        <h3>{{ __('Email reminder') }}</h3>
        <p>{{ __('You can notify a client that a new invoice will
         be sent for his recurring profile soon. This way,
          you can give the client an early opportunity to
          terminate the recurring profile.') }}</p>
        <h3>{{ __('Direct debit') }}</h3>
        <p>{{ __('It is possible to make an exception in
         the case that a recurring profile has a direct
         debit, depending on the default client settings.
         .') }}</p>
    </div>

</div>
