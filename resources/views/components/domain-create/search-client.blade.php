<div class="col col-search-client">
    <div class="container-card-1">
        <h2>{{ __('Search client') }}</h2>

        <div class="input-container form">

            <div class="form-group w-1/2">
                <div class="select-wrapper">
                    <select name="search_client" id="searchClient">
                        <option value class="d-none">{{ __('Select a client') }}</option>
                        @foreach($users["debtors"] as $user)
                            <option value="{{ $user['DebtorCode'] }}">{{ $user['DebtorCode'].' '.$user['CompanyName'] }}</option>
                        @endforeach
                    </select>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </div>
            </div>

            <div class="form-group w-1/2">
                <div class="submit-wrapper">
                    <button class="btn btn--primary" id="searchClientButton" type="button">Select & continue</button>
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_1212_9015)">
                        <path d="M6.75 3.375L12.375 9L6.75 14.625" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </g>
                        <defs>
                        <clipPath id="clip0_1212_9015">
                        <rect width="18" height="18" fill="currentColor"/>
                        </clipPath>
                        </defs>
                    </svg>
                </div>
            </div>

        </div>
        <div class="client-information js-client-information d-none">
            <h2>{{ __('Client information') }}</h2>
            <div class="client-information-item-wrapper">
                <div class="client-information-item">
                    <h3>{{ __('Company name') }}</h3>
                    <p class="js-company-name">{{ __('ClassICT') }}</p>
                </div>

                <div class="client-information-item">
                    <h3>{{ __('Name') }}</h3>
                    <p class="js-name">{{ __('Remco Straaten') }}</p>
                </div>

                <div class="client-information-item">
                    <h3>{{ __('Address') }}</h3>
                    <p class="js-address">{{ __('Hommerter Allee 92B') }}</p>
                </div>

                <div class="client-information-item">
                    <h3>{{ __('City & ZIP code') }}</h3>
                    <p class="js-city-zipcode" >{{ __('6436AN Amstenrade') }}</p>
                </div>

                <div class="client-information-item">
                    <h3>{{ __('Country') }}</h3>
                    <p class="js-country">{{ __('Netherlands') }}</p>
                </div>
            </div>


        </div>
    </div>
</div>
