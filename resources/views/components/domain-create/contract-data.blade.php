<div class="container-card-1 card-contract-data">
    <h2>{{ __('Contract data') }}</h2>

    <div class="input-container form">

        <div class="form-group radio">
            <div class="radio-wrapper">
                <p class="label">Contract period</p>
                <input type="radio" class="contract-data-radio" id="contractDataYes" name="contract_period" value="1" checked>
                <label for="yes">Equal to the billing period</label><br>
                <input type="radio" class="contract-data-radio no" id="contractDataNo" name="contract_period" value="0">
                <label for="yes">Different from the billing period</label><br>
            </div>
        </div>

        {{-- js-display/domainCreateContractData.js --}}
        <div class="group-input period-contract js-period-contract d-none">
            <div class="input-container form">

                <p class="label">{{ __('Different contract period') }}</p>

                <div class="form-group number">
                    <input type="text" class="contract-length" name="contract_length" id="contract_length">
                </div>

                <div class="form-group month">
                    <div class="select-wrapper cstm-fit-content">
                        <select name="contract_per" class="contract-per" id="contractPer">
                            @if(count($periodic) > 0)
                                @foreach ($periodic as $key => $period)
                                    <option value="{{ $key }}"> {{ $period }} </option>
                                @endforeach
                            @else
                                <option value="">{{ __( 'there is no period from wefact') }}</option>
                            @endif
                        </select>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                </div>

            </div>
        </div>

        {{-- js-display/domainCreateContractData.js --}}
        <div class="group-input period-billing js-period-billing d-none">
            <div class="input-container form">

                <p class="label">{{ __('Contract start date and end date') }}</p>

                <div class="form-group date">
                    <input type="date" class="contract-date-start" name="contract_date_start" value="{{ date('Y-m-d') }}" id="fromDate">
                </div>

                <div class="form-group date">
                    <p class="till">
                        Till 
                        <span class="contract-date-end"></span>
                    </p>
                </div>

            </div>
        </div>

    </div>

    <div class="explanation">
        <h3>{{ __('Explanation contract data') }}</h3>
        <p>{{ __('HostFact assumes by default that the
        billing period is equal to the contract period.
        If another contract period is agreed, you can
        be enter this period here.') }}</p>
    </div>

</div>
