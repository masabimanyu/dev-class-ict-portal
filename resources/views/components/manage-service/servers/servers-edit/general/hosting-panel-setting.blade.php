<div class="container-card-1 card-hosting-panel-setting">
    <h2>{{ __('Hosting panel settings') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="hostingPanelLoc">Hosting panel location</label>
            <input id="hostingPanelLoc" name="hosting_panel_loc" type="text">
        </div>

        <div class="form-group w-1/4">
            <input class="w-1/2" id="hostingPanelPort" name="hosting_panel_port" type="text" placeholder="Port">
        </div>

        <div class="form-group w-1/2">
            <label for="hostingPanelUsername">Username</label>
            <input id="hostingPanelUsername" name="hosting_panel_username" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="hostingPanelPassword">Password</label>
            <input id="hostingPanelPassword" name="hosting_panel_password" type="text">
        </div>

    </div>

</div>
