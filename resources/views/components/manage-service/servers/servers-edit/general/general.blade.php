<div class="container-card-1 card-general">
    <h2>{{ __('General') }}</h2>

    <button class="btn btn--primary" type="button">Hosting panel isn’t yet available</button>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="generalServerName">Server name</label>
            <input id="generalServerName" name="general_sever_name" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalHostingPanel">Hosting panel</label>
            <div class="select-wrapper">
                <select name="general_hosting_panel" id="generalHostingPanel">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <p class="note"><span class="note">Note : </span>Select the hosting panel, if you want to create a direct connection between HostFact and your server.</p>
        </div>

    </div>

</div>
