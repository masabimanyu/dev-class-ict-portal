<div class="container-card-1 card-nameservers">
    <h2>{{ __('Nameservers') }}</h2>

    <p class="desc">When a domain will be registered for a hosting account on this server, use the following name servers.</p>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="nameserver1">Name server 1</label>
            <input id="nameserver1" name="nameserver_1" type="text">
        </div>

        <div class="w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="nameserver2">Name server 2</label>
            <input id="nameserver2" name="nameserver_2" type="text">
        </div>

        <div class="w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="nameserver3">Name server 3</label>
            <input id="nameserver3" name="nameserver_3" type="text">
        </div>

        <div class="w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="nameServerPreferred">Preferred DNS template</label>
            <div class="select-wrapper">
                <select name="nameserver_preferred" id="nameServerPreferred">
                    <option value="">{{ __('No DNS template') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

</div>
