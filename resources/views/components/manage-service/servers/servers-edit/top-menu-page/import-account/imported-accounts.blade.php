<div class="card-table container-card-1 card-clients import-table">
    <div class="table-action-wrapper">
        <div class="counter-wrapper">
            <h3>Imported accounts</h3>
            <p class="count">13</p>
        </div>
    </div>
    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>{{ __('Account') }}</th>
                <th>{{ __('Domain') }}</th>
                <th>{{ __('Client') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td>amdvisu</td>
                    <td>amdvisualcreations.nl</td>
                    <td>AMD Visual Creations</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>