<div class="card-table container-card-1 card-clients import-table">
    <div class="table-action-wrapper">
        <div class="counter-wrapper">
            <h3>New accounts</h3>
            <p class="count">0</p>
        </div>
    </div>
    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>{{ __('Account') }}</th>
                <th>{{ __('Domain') }}</th>
                <th>{{ __('Hosting type') }}</th>
                <th>{{ __('Package') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td colspan="4">No result found</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>