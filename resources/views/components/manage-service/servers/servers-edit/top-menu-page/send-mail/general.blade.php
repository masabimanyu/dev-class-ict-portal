<div class="container-card-1 import-card card-general">
    <h2>{{ __('General') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="generalTemplate">Template</label>
            <div class="select-wrapper">
                <select name="general_template" id="generalTemplate">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="generalSubject">Subject</label>
            <input id="generalSubject" name="general_subject" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalSenderName">Sender name</label>
            <input id="generalSenderName" name="general_sender_name" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalSenderEmail">Sender E-mail address</label>
            <input id="generalSenderEmail" name="general_sender_email" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalCC">CC</label>
            <input id="generalCC" name="general_cc" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalBCC">BCC</label>
            <input id="generalBCC" name="general_bcc" type="text">
        </div>

        <div class="form-group w-1/2 checkbox">
            <p class="label">Default BCC</p>

            <div class="checkbox-wrapper">
                <label for="generalBCCOnlyOnce">BCC only once</label>
                <input
                    type="checkbox"
                    name="general_bcc_only_once"
                    id="generalBCCOnlyOnce"
                >
            </div>

        </div>

        <div class="form-group">

            <button type="button" class="btn btn--primary">
                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M2.8125 9H15.1875" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    <path d="M9 2.8125V15.1875" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                {{ __('Add e-mail address to the mailing') }}
            </button>

        </div>

    </div>


</div>
