<div class="card-table container-card-1 import-table import-card card-add-email-address">
    <h2>{{ __('Add e-mail address to mailing list') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="addEmailClientGroup">Client group</label>
            <div class="select-wrapper">
                <select name="add_email_client_group" id="addEmailClientGroup">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="addEmailRecurringProfile">Recurring profile of product</label>
            <div class="select-wrapper">
                <select name="add_email_recurring_profile" id="addEmailRecurringProfile">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="addEmailServer">Server</label>
            <div class="select-wrapper">
                <select name="add_email_server" id="addEmailServer">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

    <div class="table-action-wrapper">

        <div class="counter-wrapper">
            <h3>Clients</h3>
            <p class="count">10</p>
        </div>

        <div class="input-container form">
            <p class="body-1">Below you can add individual clients to the mailing list. You can also search for clients.</p>
            <div class="form-group w-1/2">
                <div class="search-wrapper">
                    <input type="search" name="search_client" placeholder="Search client" id="searchClient" required>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="11" cy="11" r="7" stroke="#33363F" stroke-width="1.5"/>
                        <path d="M20 20L17 17" stroke="#33363F" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>
            </div>
        </div>

    </div>
    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>
                    <div class="form-group checkbox">
            
                            <label for="selectContactCheckboxHead"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_head"
                                id="selectContactCheckboxHead"
                                class="js-select-contact-checkbox-head"
                            >
            
                    </div>
                </th>
                <th>{{ __('Client no.') }}</th>
                <th>{{ __('Company name') }}</th>
                <th>{{ __('Contact person') }}</th>
                <th>{{ __('E-mail address') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="form-group checkbox">
            
                            <label for="selectContactCheckbox1"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_1"
                                id="selectContactCheckbox1"
                                class="js-select-contact-checkbox-1"
                            >
            
                        </div>
                    </td>
                    <td>DB00001</td>
                    <td>Fotoccasion</td>
                    <td>Vlemmix, Frank</td>
                    <td>example@mail.com</td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="button-wrapper">
        <button type="submit" class="btn btn--primary"> {{ __('Select contact') }} </button>
    </div>

</div>
