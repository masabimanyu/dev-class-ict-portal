<div class="card-table container-card-1 card-clients import-table">
    <div class="table-action-wrapper">
        <div class="counter-wrapper">
            <h3>Clients</h3>
            <p class="count">10</p>
        </div>
    </div>
    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>
                    <div class="form-group checkbox">
            
                            <label for="selectContactCheckboxHead"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_head"
                                id="selectContactCheckboxHead"
                                class="js-select-contact-checkbox-head"
                            >
            
                    </div>
                </th>
                <th>{{ __('Client no.') }}</th>
                <th>{{ __('Company name') }}</th>
                <th>{{ __('Contact person') }}</th>
                <th>{{ __('E-mail address') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="form-group checkbox">
            
                            <label for="selectContactCheckbox1"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_1"
                                id="selectContactCheckbox1"
                                class="js-select-contact-checkbox-1"
                            >
            
                        </div>
                    </td>
                    <td>DB00001</td>
                    <td>Fotoccasion</td>
                    <td>Vlemmix, Frank</td>
                    <td>example@mail.com</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>