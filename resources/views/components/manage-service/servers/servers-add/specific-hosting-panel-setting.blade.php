<div class="container-card-1 card-specific-hosting-panel-setting">
    <h2>{{ __('Specific hosting panel settings') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="hostingPanelLoc">IP address (shared)</label>
            <input id="hostingPanelLoc" name="hosting_panel_loc" type="text">
            <p class="note"><span class="note">Note : </span>Fill in the shared IP address that will be used for creating hosting accounts on this server.</p>
        </div>

        <div class="form-group w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="hostingPanelUsername">Additional IP address (IPv6)</label>
            <input id="hostingPanelUsername" name="hosting_panel_username" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="hostingPanelWebstat">Webstat</label>
            <div class="select-wrapper">
                <select name="hosting_panel_webstat" id="hostingPanelWebstat">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group radio w-1/2">
            <div class="radio-wrapper">
                <p class="label">Additional domain names :</p>
                <input class="" type="radio" id="hostingPanelWebstatAdditional1" name="addtional_information" value="1">
                <label for="hostingPanelWebstatAdditional1">create as additional</label><br>
                <p class="label">Domain :</p>
                <input class="" type="radio" id="hostingPanelWebstatAdditional2" name="addtional_information" value="0">
                <label for="hostingPanelWebstatAdditional2">create as pointer</label><br>
                <input class="" type="radio" id="hostingPanelWebstatAdditional3" name="addtional_information" value="0">
                <label for="hostingPanelWebstatAdditional3">create as alias</label><br>
                <p class="label note"><span class="note">Note : </span>Choose how additional domains must be added on the server.</p>
            </div>
        </div>

    </div>

</div>
