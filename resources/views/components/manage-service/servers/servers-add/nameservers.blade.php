<div class="container-card-1 card-nameservers">
    <h2>{{ __('Nameservers') }}</h2>

    <p class="desc">When a domain will be registered for a hosting account on this server, use the following name servers.</p>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="nameserver1">Name server 1</label>
            <input id="nameserver1" name="nameserver_1" type="text">
        </div>

        <div class="w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="nameserver2">Name server 2</label>
            <input id="nameserver2" name="nameserver_2" type="text">
        </div>

        <div class="w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="nameserver3">Name server 3</label>
            <input id="nameserver3" name="nameserver_3" type="text">
        </div>

    </div>

</div>
