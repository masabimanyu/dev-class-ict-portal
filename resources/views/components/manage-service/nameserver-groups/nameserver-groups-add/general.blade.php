<div class="container-card-1 card-general">
    <h2>{{ __('General') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="generalName">Name</label>
            <input id="generalName" name="general_name" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalNameServerManager">Nameserver manager</label>
            <div class="select-wrapper">
                <select name="general_name_server_manager" id="generalNameServerManager">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="generalNameServer1">Name server 1</label>
            <input id="generalNameServer1" name="general_name_server_1" type="text">
        </div>

        <div class="form-group w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="generalNameNameServer2">Name server 2</label>
            <input id="generalNameNameServer2" name="general_name_server_2" type="text">
        </div>

        <div class="form-group w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="generalNameNameServer3">Name server 3</label>
            <input id="generalNameNameServer3" name="general_name_server_3" type="text">
        </div>

    </div>

    <div class="explanation">
        <h3>{{ __('Manage nameserver group') }}</h3>
        <p>A nameserver group determines who manages the nameservers and takes care of the DNS. This could be your hosting control paneel, your registrar or external DNS software.
            <br><br>
            In a nameserver group you can have one or more DNS templates. When adding a domain name you can choose which DNS template you want to use. To make sure domains which a registered automatically are provisioned correctly, you can set a default DNS template in your registrar or server settings page in HostFact.
            <br><br>
            Use the variable [domain] for the domain name in the DNS record value field.
            Example: [domain] will be 'example.com'</p>
    </div>

</div>
