<div class="container-card-1 card-default-domain-contacts">
    <h2>{{ __('Default domain contacts') }}</h2>

    <p class="desc">Use the following default administrative - and technical contact. Client information will be used for the domain owner.</p>

    <div class="input-container form">

        <div class="group-input w-1/2">
            <div class="form-group radio">
                <div class="radio-wrapper">
                    <p class="label">Admin contact</p>
                    <input type="radio" id="yes" name="tech_contact" value="1">
                    <label for="yes">Use client information</label><br>
                    <input type="radio" id="no" name="tech_contact" value="0">
                    <label for="no">Use a fixed contact</label><br>
                </div>
            </div>
            @include('../../components/domain-create/choose-owner')
        </div>

        <div class="group-input w-1/2">
            <div class="form-group radio">
                <div class="radio-wrapper">
                    <p class="label">Tech contact</p>
                    <input type="radio" id="yes" name="tech_contact" value="1">
                    <label for="yes">Use client information</label><br>
                    <input type="radio" id="no" name="tech_contact" value="0">
                    <label for="no">Use a fixed contact</label><br>
                </div>
            </div>
            @include('../../components/domain-create/choose-owner')
        </div>

    </div>

</div>
