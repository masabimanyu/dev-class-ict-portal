<div class="container-card-1 card-hostfact">
    <h2>{{ __('If you have any questions about the link, please contact') }}</h2>

    @include('../../components/application-logo') 

    <div class="info-wrapper">
        <p class="body-1">ClassICT</p>
        <a class="body-1 font-1" href="https://www.classict.nl/">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21Z" stroke="#0E0E0E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M3.5127 9H20.4875" stroke="#0E0E0E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M3.5127 15H20.4874" stroke="#0E0E0E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M12 20.759C14.0711 20.759 15.75 16.8377 15.75 12.0006C15.75 7.16346 14.0711 3.24219 12 3.24219C9.92893 3.24219 8.25 7.16346 8.25 12.0006C8.25 16.8377 9.92893 20.759 12 20.759Z" stroke="#0E0E0E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>                
            https://www.classict.nl/
        </a>
        <a class="body-1 font-1" href="mailto:contact@classict.nl">
            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22 6C22 4.9 21.1 4 20 4H4C2.9 4 2 4.9 2 6M22 6V18C22 19.1 21.1 20 20 20H4C2.9 20 2 19.1 2 18V6M22 6L12 13L2 6" stroke="#0E0E0E" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>                
            contact@classict.nl
        </a>
    </div>

    <h3 class="heading-3 primary-1">Do you want to use a DNS template from Openprovider?</h3>
    <p class="body-1">Enter as nameservers: ns1.openprovider.nl, ns2.openprovider.be and ns3.openprovider.eu</p>

</div>
