<div class="container-card-1 card-nameservers">
    <h2>{{ __('Nameservers') }}</h2>

    <p class="desc">Use the following default administrative - and technical contact. Client information will be used for the domain owner.</p>

    <div class="input-container form">

        <div class="form-group">
            <label for="nameserver1">Name server 1</label>
            <input id="nameserver1" name="nameserver_1" type="text">
        </div>

        <div class="form-group">
            <label for="nameserver2">Name server 2</label>
            <input id="nameserver2" name="nameserver_2" type="text">
        </div>

        <div class="form-group">
            <label for="nameserver3">Name server 3</label>
            <input id="nameserver3" name="nameserver_3" type="text">
        </div>

        <div class="form-group checkbox">

            <div class="checkbox-wrapper">
                <label for="nameServersRegistrar">Registrar manages the DNS over these name servers</label>
                <input
                    type="checkbox"
                    name="name_servers_registrar"
                    id="nameServersRegistrar"
                >
            </div>

            <p class="note"><span class="note">Note : </span>When the registrar manages the entered name servers, a name server group is created under DNS management. DNS templates can be managed under the nameserver group.</p>

        </div>

    </div>

</div>
