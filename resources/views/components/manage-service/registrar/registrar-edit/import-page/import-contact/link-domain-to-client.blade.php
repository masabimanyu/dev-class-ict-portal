<div class="container-card-1 card-link-domain-to-client">
    <h2>{{ __('Link domain to client') }}</h2>
    <p class="body-1">Below you can link domains to a specific client.</p>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="linkToCient">Link to client</label>
            <div class="select-wrapper">
                <select name="link_to_client" id="linkToCient">
                    <option value="">{{ __('Select a client') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

</div>
