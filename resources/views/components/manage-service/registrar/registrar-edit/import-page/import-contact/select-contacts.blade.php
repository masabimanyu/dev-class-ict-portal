<div class="card-table container-card-1 card-select-contacts import-table">
    <div class="table-action-wrapper">
        <div class="counter-wrapper">
            <h3>Select contacts</h3>
        </div>
    </div>
    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>
                    <div class="form-group checkbox">
            
                            <label for="selectContactCheckboxHead"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_head"
                                id="selectContactCheckboxHead"
                                class="js-select-contact-checkbox-head"
                            >
            
                    </div>
                </th>
                <th>{{ __('Contact') }}</th>
                <th>{{ __('Contact person') }}</th>
                <th>{{ __('E-mail address') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="form-group checkbox">
            
                            <label for="selectContactCheckbox1"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_1"
                                id="selectContactCheckbox1"
                                class="js-select-contact-checkbox-1"
                            >
            
                        </div>
                    </td>
                    <td>BH1022163-NL</td>
                    <td>B500</td>
                    <td>emailb500@gmail.com</td>
                </tr>
                <tr>
                    <td>
                        <div class="form-group checkbox">
            
                            <label for="selectContactCheckbox2"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_2"
                                id="selectContactCheckbox2"
                                class="js-select-contact-checkbox-2"
                            >
            
                        </div>
                    </td>
                    <td>HA911999-NL</td>
                    <td>Haditax Taxibedrijf V.O.F.</td>
                    <td>contact@apts.nl</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>