<div class="card-table container-card-1 card-select-contacts import-table">
    <div class="table-action-wrapper">
        <div class="counter-wrapper">
            <h3>Select domains</h3>
        </div>
    </div>
    <div class="table-filter select-status">
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M5 12L5 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <path d="M19 20L19 18" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <path d="M5 20L5 16" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <path d="M19 12L19 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <path d="M12 7L12 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <path d="M12 20L12 12" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <circle cx="5" cy="14" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <circle cx="12" cy="9" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        <circle cx="19" cy="15" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
        </svg>
        <select class="filter-dropdown" name="status" id="selectDomainBtn">
            <option value="">{{ __('Select domains with domain owner') }}</option>
            <option value="ACT"> {{ __('ClassICT') }} </option>
            <option value="REQ"> {{ __('GM Academy') }} </option>
        </select>
    </div>
    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>
                    <div class="form-group checkbox">
            
                            <label for="selectContactCheckboxHead"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_head"
                                id="selectContactCheckboxHead"
                                class="js-select-contact-checkbox-head"
                            >
            
                    </div>
                </th>
                <th>{{ __('Domain') }}</th>
                <th>{{ __('Owner') }}</th>
                <th>{{ __('Renewal date') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="form-group checkbox">
            
                            <label for="selectContactCheckbox1"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_1"
                                id="selectContactCheckbox1"
                                class="js-select-contact-checkbox-1"
                            >
            
                        </div>
                    </td>
                    <td>hosting-amsterdam.nl</td>
                    <td>ClassICT</td>
                    <td>01/06/2023</td>
                </tr>
                <tr>
                    <td>
                        <div class="form-group checkbox">
            
                            <label for="selectContactCheckbox1"></label>
                            <input
                                type="checkbox"
                                name="select_contact_checkbox_1"
                                id="selectContactCheckbox1"
                                class="js-select-contact-checkbox-1"
                            >
            
                        </div>
                    </td>
                    <td>hosting-amsterdam.nl</td>
                    <td>ClassICT</td>
                    <td>01/06/2023</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>