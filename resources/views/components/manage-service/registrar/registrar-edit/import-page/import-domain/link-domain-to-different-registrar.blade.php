<div class="card-table import-table container-card-1">
    <div class="table-action-wrapper">
        <div class="counter-wrapper">
            <h3>Link domains to different registrar</h3>
        </div>
    </div>

    <p class="text body-1">The domain names listed below are present 
        in HostFact but have a different registrar. If you import these 
        domain names, they will be moved (in HostFact) from the current 
        registrar to registrar Openprovider.</p>

    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>{{ __('Domain') }}</th>
                <th>{{ __('Client') }}</th>
                <th>{{ __('Registered') }}</th>
                <th>{{ __('Renewal date') }}</th>
                <th>{{ __('Registrar') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td>hosting-amsterdam.nl</td>
                    <td>ClassICT</td>
                    <td>05/11/2022</td>
                    <td>05/11/2023</td>
                    <td>Openprovider</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>