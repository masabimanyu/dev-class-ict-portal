<div class="card-table import-table container-card-1">
    <div class="table-action-wrapper">
        <div class="counter-wrapper">
            <h3>Imported domains</h3>
        </div>
    </div>

    <div class="table-wrapper">
        <table class="users-table-content table-content">
            <thead>
                <th>{{ __('Domain') }}</th>
                <th>{{ __('Client') }}</th>
                <th>{{ __('Registered') }}</th>
                <th>{{ __('Renewal date') }}</th>
                <th>{{ __('Registrar') }}</th>
            </thead>
            <tbody>
                <tr>
                    <td>hosting-amsterdam.nl</td>
                    <td>ClassICT</td>
                    <td>05/11/2022</td>
                    <td>05/11/2023</td>
                    <td>Openprovider</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>