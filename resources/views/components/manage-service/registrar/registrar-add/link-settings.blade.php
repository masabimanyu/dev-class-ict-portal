<div class="container-card-1 card-link-setting">
    <h2>{{ __('Link Settings') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="linkUsername">Username</label>
            <input id="linkUsername" name="link_username" type="text">
        </div>

        <div class="form-group w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="linkPasswordType">Password type</label>
            <div class="select-wrapper">
                <select name="link_password_type" id="linkPasswordType">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="linkPassword">Password</label>
            <input id="linkPassword" name="link_password" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="linkRegistrant">Registrant accepts</label>
            <div class="select-wrapper">
                <select name="link_registrant" id="linkRegistrant">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <p class="note"><span class="note">Note :</span> Registrant accepts additional terms and conditions for extensions. Currently in use for: .es</p>
        </div>
        
        <div class="form-group w-1/2">
            <label for="linkTestMode">Test mode</label>
            <div class="select-wrapper">
                <select name="link_test_mode" id="linkTestMode">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

    <div class="desc">
        <h3>Do you want to use the DNS template?</h3>
        <p>Enter as nameservers: ns1.openprovider.nl, ns2.openprovider.be and ns3.openprovider.eu</p>
    </div>

</div>
