<div class="container-card-1 card-general">
    <h2>{{ __('General') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="generalName">Name</label>
            <input id="generalName" name="services_name" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalRegistrar">Registrar integration (API)</label>
            <div class="select-wrapper">
                <select name="general_registrar" id="generalRegistrar">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group checkbox">
            <p class="label">Services available for this registrar</p>

            <div class="checkbox-wrapper">
                <label for="generalDomainName">Domain names</label>
                <input
                    type="checkbox"
                    name="general_domain_name"
                    id="generalDomainName"
                >
            </div>

            <div class="checkbox-wrapper">
                <label for="generalSSLCertificates">SSL certificates</label>
                <input
                    type="checkbox"
                    name="general_ssl_certificates"
                    id="generalSSLCertificates"
                >
            </div>

        </div>

    </div>

    <div class="explanation">
        <h3>{{ __('Why do I have to add a registrar?') }}</h3>
        <p>If you sell domains, you must add at least one registrar 
        where one or more TLDs can be registered. HostFact will be 
        able to automate tasks for you. <br><br>

        Even if you manage your domains manually, 
        you need to add a fictitious registrar. 
        Later, you only have to link the registrar 
        to its corresponding API and you can use 
        all automated options without having to 
        edit all domains.</p>
    </div>

</div>
