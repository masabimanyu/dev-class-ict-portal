<div class="container-card-1 card-default-domain-contacts">
    <h2>{{ __('Default domain contacts') }}</h2>

    <p class="desc">Use the following default administrative - and technical contact. Client information will be used for the domain owner.</p>

    <div class="input-container form">

        <div class="form-group radio">
            <div class="radio-wrapper">
                <p class="label">Admin contact</p>
                <input class="" type="radio" id="adminContact1" name="admin_contact" value="1">
                <label for="adminContact1">Use client information</label><br>
                <input class="" type="radio" id="adminContact2" name="admin_contact" value="0">
                <label for="adminContact2">Use default contact</label><br>
            </div>
        </div>

        <div class="form-group radio">
            <div class="radio-wrapper">
                <p class="label">Tech contact</p>
                <input class="" type="radio" id="techContact1" name="tech_contact" value="1">
                <label for="techContact1">Use client information</label><br>
                <input class="" type="radio" id="techContact2" name="tech_contact" value="0">
                <label for="techContact2">Use default contact</label><br>
            </div>
        </div>

    </div>

</div>
