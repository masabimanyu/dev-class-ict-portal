<div class="container-card-1 card-public-whois-server">
    <h2>{{ __('Public WHOIS server') }}</h2>

    <button class="btn btn--primary" type="button">Get WHOIS server from HostFact</button>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="PWSName">Public WHOIS server</label>
            <input id="PWSName" name="pws_name" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="PWSMatchCheck">No Match check</label>
            <input id="PWSMatchCheck" name="pws_match_check" type="text">
        </div>

    </div>

</div>
