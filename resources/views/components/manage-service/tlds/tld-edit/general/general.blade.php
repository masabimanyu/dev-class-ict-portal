<div class="container-card-1 card-general">
    <h2>{{ __('General') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="generalTLD">TLD</label>
            <input id="generalTLD" name="general_tld" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="generalRegistrar">Registrar</label>
            <div class="select-wrapper">
                <select name="general_registrar" id="generalRegistrar">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <p class="note"><span class="note">Note :</span>Select the registrar that you want to link this contact to</p>
        </div>

    </div>

</div>
