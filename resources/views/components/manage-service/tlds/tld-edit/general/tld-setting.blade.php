<div class="container-card-1 card-tld-setting">
    <h2>{{ __('TLD settings') }}</h2>

    <div class="input-container form">

        <div class="form-group toggle-action">
            <label class="switch">
                <input type="checkbox" id="TLDSettingsAuthCode">
                <span class="slider round"></span>
                <p class="yes">Yes</p>
                <p class="no">No</p>
            </label>
            <p>Authorization code needed for transfer?</p>
        </div>

        <div class="form-group">
            <label for="TLDSettingsCharge">Charge costs for changing domain owner</label>
            <div class="select-wrapper w-1/2">
                <select name="tld_settings_charge" id="TLDSettingsCharge">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <p class="note"><span class="note">Note :</span> When a client wants to change the domain owner information, you can charge for this action automatically. If you want to charge costs, select the product that should be used for the charging</p>
        </div>

        <div class="form-group checkbox">

            <div class="checkbox-wrapper">
                <label for="tldSettingTransferCost">Tranfer cost</label>
                <input
                    type="checkbox"
                    name="tld_setting_transfer_cost"
                    id="tldSettingTransferCost"
                >
            </div>

        </div>

    </div>

</div>
