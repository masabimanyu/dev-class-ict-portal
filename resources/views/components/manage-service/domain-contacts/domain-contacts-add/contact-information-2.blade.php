<div class="container-card-1 card-contact-information-2">
    <h2>{{ __('Contact Information') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="contactInformationPhone">Phone number</label>
            <input id="contactInformationPhone" name="contact_information_phone" type="number">
        </div>

        <div class="form-group w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationFax">Fax number</label>
            <input id="contactInformationFax" name="contact_information_fax" type="number">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationEmail">Email address</label>
            <input id="contactInformationEmail" name="contact_information_email" type="email">
        </div>

    </div>

</div>
