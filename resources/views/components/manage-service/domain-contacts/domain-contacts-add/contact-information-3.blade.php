<div class="container-card-1 card-contact-information-3">
    <div class="title-wrapper">
        <h2>{{ __('Contact Information') }}</h2>
        <p class="button-1 primary-1">Use info existing contact</p>
    </div>

    <div class="input-container form">

        <div class="group-input search-client">
            <div class="input-container form">
    
                <div class="form-group w-3/4">
                    <div class="select-wrapper">
                        <select name="contact_information_search_client" id="contactInformationSearchClient">
                            <option value="">{{ __('Select client') }}</option>
                        </select>
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                </div>
    
                <div class="form-group search-btn">
                    <div class="submit-wrapper left">
                        <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.2197 16.2803C15.5126 16.5732 15.9874 16.5732 16.2803 16.2803C16.5732 15.9874 16.5732 15.5126 16.2803 15.2197L15.2197 16.2803ZM13.0178 11.9572C12.7249 11.6643 12.2501 11.6643 11.9572 11.9572C11.6643 12.2501 11.6643 12.7249 11.9572 13.0178L13.0178 11.9572ZM13.5 8.25C13.5 11.1495 11.1495 13.5 8.25 13.5V15C11.9779 15 15 11.9779 15 8.25H13.5ZM8.25 13.5C5.3505 13.5 3 11.1495 3 8.25H1.5C1.5 11.9779 4.52208 15 8.25 15V13.5ZM3 8.25C3 5.3505 5.3505 3 8.25 3V1.5C4.52208 1.5 1.5 4.52208 1.5 8.25H3ZM8.25 3C11.1495 3 13.5 5.3505 13.5 8.25H15C15 4.52208 11.9779 1.5 8.25 1.5V3ZM16.2803 15.2197L13.0178 11.9572L11.9572 13.0178L15.2197 16.2803L16.2803 15.2197Z" fill="#951D81"/>
                        </svg>
                        <button type="button" class="btn btn--transparent js-modal-trigger">Search client</button>
                    </div>
                </div>
    
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationCompanyName">Company name</label>
            <input id="contactInformationCompanyName" name="contact_information_company_name" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationCompanyNumber">Company number</label>
            <input id="contactInformationCompanyNumber" name="contact_information_company_number" type="number">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationTaxNumber">Tax number</label>
            <input id="contactInformationTaxNumber" name="contact_information_tax_number" type="number">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationLegalform">Legalform</label>
            <div class="select-wrapper">
                <select name="contact_information_legalform" id="contactInformationLegalform">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/3">
            <label for="contactInformationContactPersonSel">Contact person</label>
            <div class="select-wrapper">
                <select name="contact_information_contact_person_sel" id="contactInformationContactPersonSel">
                    <option value="">{{ __('Dhr.') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>

        </div>
        <div class="form-group w-1/3">
            <input id="contactInformationContactPersonDep" 
            name="contact_information_contact_person_dep" 
            type="text"
            placeholder="First name">
        </div>

        <div class="form-group w-1/3">
            <input id="contactInformationContactPersonName" 
            name="contact_information_contact_person_name" 
            type="text"
            placeholder="Last name">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationAddress">Address</label>
            <input id="contactInformationAddress" name="contact_information_address" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationZip">Zip code</label>
            <input id="contactInformationZip" name="contact_information_zip" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationCity">City</label>
            <input id="contactInformationCity" name="contact_information_city" type="text">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationCountry">Country</label>
            <div class="select-wrapper">
                <select name="contact_information_country" id="contactInformationCountry">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

</div>
