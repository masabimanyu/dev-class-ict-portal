<div class="container-card-1 card-contact-information-1">
    <h2>{{ __('Contact information') }}</h2>

    <div class="input-container form">

        <div class="form-group radio">
            <div class="radio-wrapper">
                <p class="label">Client contact</p>
                <input class="" type="radio" id="contactInformationGeneral" name="contact_information" value="1">
                <label for="contactInformationGeneral">General contact for all clients</label><br>
                <input class="" type="radio" id="contactInformationOneClient" name="contact_information" value="0">
                <label for="contactInformationOneClient">Contact for one client</label><br>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationClient">Client</label>
            <div class="select-wrapper">
                <select name="contact_information_client" id="contactInformationClient">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

        <div class="form-group w-1/2">
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationRegistrar">Registrar</label>
            <div class="select-wrapper">
                <select name="contact_information_registrar" id="contactInformationRegistrar">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
            <p class="note"><span class="note">Note :</span> Select the registrar that you want to link this contact to</p>
        </div>

        <div class="form-group radio">
            <div class="radio-wrapper">
                <input class="" type="radio" id="contactInformationExtend1" name="contact_information_extend" value="1">
                <label for="contactInformationExtend1">It concerns a new contact</label><br>
                <input class="" type="radio" id="contactInformationExtend2" name="contact_information_extend" value="0">
                <label for="contactInformationExtend2">Contact already exists at registrar</label><br>
            </div>
        </div>

        <div class="form-group w-1/2">
            <label for="contactInformationRegistryId">Registry ID</label>
            <input type="text" id="contactInformationRegistryId" name="contact_information_registry_id" placeholder="Registry ID">
        </div>

        <div class="form-group checkbox">

            <div class="checkbox-wrapper">
                <label for="generalUpdateContact">Also update contact details at registrar</label>
                <input
                    type="checkbox"
                    name="general_update_contact"
                    id="generalUpdateContact"
                >
            </div>

        </div>

    </div>

</div>
