<div class="container-card-1 card-hosting-account">
    <h2>{{ __('Hosting account welcome message with login credentials') }}</h2>

    <div class="input-container form">

        <div class="form-group w-1/2">
            <label for="packageProduct">Hosting account email template</label>
            <div class="select-wrapper">
                <select name="package_product" id="packageProduct">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>
        <div class="form-group checkbox">
            <label for="IDNSupportDomainNames">{{ __('Send hosting login details automatically after creating hosting account') }}</label>
            <input
                type="checkbox"
                name="idn_support_domain_names"
                id="IDNSupportDomainNames"
                >
        </div>

        <div class="form-group w-1/2">
            <label for="packageWebhostingType">Hosting account PDF template for sending by post:</label>
            <div class="select-wrapper">
                <select name="package_webhosting_type" id="packageWebhostingType">
                    <option value="">{{ __('Choose here') }}</option>
                </select>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
            </div>
        </div>

    </div>

</div>
