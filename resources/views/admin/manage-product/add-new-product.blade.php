@extends('layouts.app')
@section('content')

<div class="product-add">

    @if($message = Session::get('success') )
        <div class="success">
            {{ $message }}
        </div>
    @endif

    @if($message = Session::get('error'))
        <div class="error">
            {{ $message }}
        </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li> {{ $error }} </li>
            @endforeach
        </div>
    @endif

    <div class="row breadcrumb">
        <div class="breadcrumb-wrapper">
            <h1>{{ __('Add new product') }}</h1>
        </div>
    </div>

    <div class="row row-action">
        <div class="col">
            <x-back-button redirect="{{ route('product.index') }}" text="{{ __('back') }}"/>
        </div>
    </div>

    <div class="row row-forms row-domain-information">
        <div class="col">

            <input type="hidden" name="client_id" id="client_id">

            <input id="is_billed_formDomain" type="hidden" name="is_billed" value="0">

            <div class="menu-wrapper">
                <div data-target="general" class="menu-item js-menu-item domain btn btn--primary--outline active">
                    General
                </div>
                <div data-target="detailed-description" class="menu-item js-menu-item domain btn btn--primary--outline">
                    Detailed description
                </div>
                {{-- <div data-target="product-groups" class="menu-item js-menu-item domain btn btn--primary--outline">
                    Product groups
                </div> --}}
            </div>

            <div id="general" class="general js-domain-information-forms active">

                @include('../../components/add-new-product-admin/product-type')

                <form action="{{ route('product.store') }}" id="general_product" class="product-form js-row-service" method="POST">
                    @csrf

                    <input type="hidden" value="general" name="product_type">

                    @include('../../components/add-new-product-admin/general')

                    @include('../../components/add-new-product-admin/financial')

                    @include('../../components/add-new-product-admin/custom-pricing-periods', [ 'product_type' => 'general' ])

                    <button type="submit" class="btn btn--primary"> {{ __('Add') }} </button>
                </form>

                <form action="{{ route('product.store') }}" id="domain_product" class="product-form js-row-service" method="POST">
                    @csrf

                    <input type="hidden" value="domain" name="product_type">

                    @include('../../components/add-new-product-admin/general')

                    @include('../../components/add-new-product-admin/properties-of-domain')

                    @include('../../components/add-new-product-admin/financial')

                    @include('../../components/add-new-product-admin/custom-pricing-periods',[ 'product_type' => 'domain' ])

                    <button type="submit" class="btn btn--primary"> {{ __('Add') }} </button>
                </form>

                <form action="{{ route('product.store') }}" id="hosting_product" class="product-form js-row-service" method="POST">
                    @csrf

                    <input type="hidden" value="hosting" name="product_type">

                    @include('../../components/add-new-product-admin/general')

                    @include('../../components/add-new-product-admin/properties-of-hosting')

                    @include('../../components/add-new-product-admin/hosting-account-welcome')

                    @include('../../components/add-new-product-admin/financial')

                    @include('../../components/add-new-product-admin/custom-pricing-periods', [ 'product_type' => 'hosting' ])

                    <button type="submit" class="btn btn--primary"> {{ __('Add') }} </button>
                </form>

                {{-- @include('../../components/add-new-product-admin/general')

                @include('../../components/add-new-product-admin/financial')

                @include('../../components/add-new-product-admin/custom-pricing-periods')

                @include('../../components/add-new-product-admin/properties-of-domain')

                @include('../../components/add-new-product-admin/properties-of-hosting')

                @include('../../components/add-new-product-admin/hosting-account-welcome') --}}

            </div>

            <div id="detailed-description" class="internal-note js-domain-information-forms">

                @include('../../components/add-new-product-admin/detailed-description')

            </div>

            <div id="product-groups" class="internal-note js-domain-information-forms">

                @include('../../components/add-new-product-admin/product-groups')

            </div>

        </div>
    </div>

    {{-- <button type="submit" class="btn btn--primary"> {{ __('Add') }} </button> --}}

</div>

@endsection
