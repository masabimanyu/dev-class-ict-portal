@extends('layouts.app')
@section('content')
    <div class="product-overview">
        <div class="row">
            <h1 class="title-page">{{ __('Product overview') }}</h1>
        </div>
        <div class="row table-action">
            <div class="col title-col all-client">
                <h2 class="title-table">{{ __('All products') }}</h2>
                <p class="count total">
                    4
                </p>
            </div>
            <div class="col action-col">
                <div class="search-wrapper">
                    <input type="text" placeholder="Search Product" class="search-client" id="searchProductBtn">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="11" cy="11" r="7" stroke="#33363F" stroke-width="1.5"/>
                        <path d="M20 20L17 17" stroke="#33363F" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>

                <a href="{{ route('product.create') }}" class="btn btn--primary">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.8125 9H15.1875" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M9 2.8125V15.1875" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    {{ __('Add new product') }}
                </a>
            </div>
        </div>
        <div class="row row-table">
            <div class="col">
                <div class="table-filter select-status">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 12L5 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M19 20L19 18" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M5 20L5 16" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M19 12L19 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M12 7L12 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M12 20L12 12" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="5" cy="14" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="12" cy="9" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="19" cy="15" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                    <select class="filter-dropdown" name="status" id="selectProductBtn">
                        <option value="">{{ __('Product group') }}</option>
                    </select>
                </div>
            </div>
            <div class="col table-col">
                <table class="users-table-content table-content">
                    <thead>
                        <th>{{ __('Product no.') }}</th>
                        <th>{{ __('Name') }}</th>
                        <th>{{ __('Product type') }}</th>
                        <th>{{ __('Price excl. tax') }}</th>
                        <th>{{ __('Price incl. tax') }}</th>
                        <th>{{ __('Sold') }}</th>
                        <th>{{ __('Product groups') }}</th>
                    </thead>
                    <tbody>
                        @if(count($products) > 0)
                            @foreach ($products as $product)
                                <tr>
                                    <td>
                                        <a href="{{ route('product.edit', $product->id) }}">{{ $product->product_code }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('product.edit', $product->id) }}">{{ $product->product_name }}</a>
                                    </td>
                                    <td>{{ $product->product_type }}</td>
                                    <td>€ {{ $product->price_exclude  }} per {{ $product->price_per }}</td>
                                    <td>
                                        €
                                        {{
                                            $product->price_exclude + ($product->price_exclude * ( (float) $taxes[$product->tax]["Rate"]))
                                        }}
                                        per
                                        {{ $product->price_per }}
                                    </td>
                                    <td>2x</td>
                                    <td> - </td>
                                </tr>
                            @endforeach
                        @endif
                        {{-- <tr>
                            <td>P018</td>
                            <td>WP Onderhoud starts</td>
                            <td>General</td>
                            <td>€ 19.95 per month</td>
                            <td>€ 24.14</td>
                            <td>74x</td>
                            <td>Onderhoud bestelformulier</td>
                        </tr> --}}
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
