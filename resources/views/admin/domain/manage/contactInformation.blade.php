@extends('layouts.app')
@section('content')
    <div class="container">
        <form action="">
            <div class="container client-information-container">
                <h2 class="container-title">{{ __('pages.client-create.information-clients') }}</h2>
                <div class="input-container form">
                    <div class="form-group w-1/2">
                        <label for="#company_name">{{ __('pages.client-create.company-clients') }}</label>
                        <input type="text" name="company_name" id="company_name" value="{{ $contacts->owner_handle->company_name }}" >
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#company_legal_form">{{ __('pages.client-create.legal-form') }}</label>
                        <div class="select-wrapper">
                            <select name="company_legal_form" id="company_legal_form">
                                <option value="">{{ __('pages.client-create.choose-here') }}</option>
                                <option value="Other or Unknown">Other or Unknown</option>
                                <option value="Private company">Private company</option>
                                <option value="Bv in formation">Bv in formation</option>
                                <option value="Cooperation">Cooperation</option>
                                <option value="Limited partnership">Limited partnership</option>
                                <option value="Proprietorship">Proprietorship</option>
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#chamber_of_commerce">{{ __('Chamber of commerce number') }}</label>
                        <input type="text" name="chamber_of_commerce" id="chamber_of_commerce" >
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#vat_number">{{ __('Vat number') }}</label>
                        <input type="text" name="vat_number" id="vat_number" value="{{ $contacts->owner_handle->vat }}" >
                    </div>
                </div>
            </div>

            <div class="container client-information-container">
                <h2 class="container-title">{{ __('Contact') }}</h2>
                <div class="input-container form">
                    <div class="form-group w-1/3">
                        <label for="#user_legal_form">{{ __('Legal form') }}</label>
                        <div class="select-wrapper">
                            <select name="user_legal_form" id="user_legal_form">
                                <option value="">{{ __('Choose here') }}</option>
                                <option value="Dhr.">Dhr.</option>
                                <option value="Mrs.">Mrs.</option>
                                <option value="Afd.">Afd.</option>
                                <option value="Unknown">Unknown</option>
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>
                    <div class="form-group w-1/3">
                        <label for="#first_name">{{ __('First name') }}</label>
                        <input type="text" name="first_name" id="first_name" value="{{ $contacts->owner_handle->name->first_name }}">
                    </div>
                    <div class="form-group w-1/3">
                        <label for="#last_name">{{ __('Last name') }}</label>
                        <input type="text" name="last_name" id="last_name" value="{{ $contacts->owner_handle->name->last_name }}">
                    </div>
                </div>
            </div>

            <div class="container client-information-container">
                <h2 class="container-title">{{ __('Address data') }}</h2>
                <div class="input-container form">
                    <div class="form-group w-1/2">
                        <label for="#address">{{ __('Address') }}</label>
                        <input type="text" name="address" id="address" >
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#postcode">{{ __('Postcode') }}</label>
                        <input type="text" name="postcode" id="postcode" >
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#place">{{ __('Place') }}</label>
                        <input type="text" name="place" id="place" >
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#land">{{ __('Land') }}</label>
                        <div class="select-wrapper">
                            <select name="land" id="land">
                                <option value="">{{ __('Choose here') }}</option>
                                <option value="NL">Nederland</option>
                                <option value="BE">Belgie</option>
                                <option value="BG">Bulgarije</option>
                                <option value="CY">Cyprus</option>
                                <option value="DK">Denemarken</option>
                                <option value="THE">Duitsland</option>
                            </select>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container client-information-container">
                <h2 class="container-title">{{ __('Contact Details') }}</h2>
                <div class="input-container form">
                    <div class="form-group w-1/2">
                        <label for="#email">{{ __('E-mail address') }}</label>
                        <input type="email" name="email" id="email">
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#phone_number">{{ __('Phone number') }}</label>
                        <input type="text" name="phone_number" id="phone_number">
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#mobile_number">{{ __('Mobile number') }}</label>
                        <input type="text" name="mobile_number" id="mobile_number">
                    </div>
                    <div class="form-group w-1/2">
                        <label for="#fax_number">{{ __('Fax number') }}</label>
                        <input type="text" name="fax_number" id="fax_number">
                    </div>
                </div>
            </div>

            <div class="form-group checkbox">
                <label for="#data_for_other_contact">{{ __('I want to use this data for Administrative Contacts and Technical Contract') }}</label>
                <input type="checkbox" name="data_for_other_contact" id="data_for_other_contact">
            </div>

            <div class="row row-submit">
                <div class="col col-submit">
                    {{-- Save Button --}}
                    <div class="button-container">
                        <button type="submit" class="btn btn--primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
