@extends('layouts.app')
@section('content')

<div class="domain-add">

    <div class="row breadcrumb">
        <div class="breadcrumb-wrapper">
            <h1>{{ __('Add new service') }}</h1>
        </div>
    </div>

    <div class="row row-action">
        <div class="col">
            <x-back-button redirect="{{ route('domain.index') }}" text="{{ __('back') }}"/>
        </div>
    </div>

    <div class="row row-forms row-search-client">
        @include('../../components/domain-create/search-client')
    </div>

    <div class="row row-forms row-choose-service js-row-choose-service d-none">
        <div class="col">
            <div class="container-card-1">

                <h2>{{ __('Choose service') }}</h2>

                <div class="service-menu">

                    <div data-target="formDomain" class="service-menu-item container-card-1 js-service-menu-item">
                        <div class="checklist-icon">
                            <svg class="checked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="20" height="20" rx="10" fill="#E6007E"/>
                                <path d="M15.6445 5.82808C15.178 5.43705 14.337 5.34656 13.8832 5.82808C12.0691 7.75316 10.2549 9.67823 8.44082 11.6033C7.66474 10.7391 6.88865 9.87494 6.11256 9.01076C5.69772 8.54883 4.77564 8.58864 4.35125 9.01076C3.85781 9.50156 3.90872 10.1262 4.35125 10.619C5.41382 11.8022 6.47639 12.9853 7.53895 14.1685C7.94438 14.62 8.89283 14.6009 9.30028 14.1685C11.1521 12.2034 13.004 10.2383 14.8559 8.27314C15.1188 7.99419 15.3816 7.71524 15.6445 7.43629C16.0691 6.98577 16.1655 6.26476 15.6445 5.82808Z" fill="white"/>
                            </svg>
                            <svg class="unchecked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.65" y="0.65" width="18.7" height="18.7" rx="9.35" stroke="#E6007E" stroke-width="1.3"/>
                            </svg>
                        </div>
                        <div class="service-name-wrapper">
                            <svg width="73" height="73" viewBox="0 0 73 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="73" height="73" rx="20" fill="url(#paint0_linear_1212_36862)"/>
                                <path d="M37 51.125C45.0772 51.125 51.625 44.5772 51.625 36.5C51.625 28.4228 45.0772 21.875 37 21.875C28.9228 21.875 22.375 28.4228 22.375 36.5C22.375 44.5772 28.9228 51.125 37 51.125Z" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23.208 31.625H50.7921" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M23.2085 41.375H50.7925" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M37 50.7324C40.3655 50.7324 43.0938 44.3603 43.0938 36.5C43.0938 28.6396 40.3655 22.2676 37 22.2676C33.6345 22.2676 30.9062 28.6396 30.9062 36.5C30.9062 44.3603 33.6345 50.7324 37 50.7324Z" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                <defs>
                                <linearGradient id="paint0_linear_1212_36862" x1="36.5" y1="0" x2="36.5" y2="73" gradientUnits="userSpaceOnUse">
                                <stop stop-color="#E10080"/>
                                <stop offset="1" stop-color="#991A81"/>
                                </linearGradient>
                                </defs>
                            </svg>
                            <p>{{ __('Domain') }}</p>
                        </div>
                    </div>

                    <div data-target="formHosting" class="service-menu-item container-card-1 js-service-menu-item">
                        <div class="checklist-icon">
                            <svg class="checked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="20" height="20" rx="10" fill="#E6007E"/>
                                <path d="M15.6445 5.82808C15.178 5.43705 14.337 5.34656 13.8832 5.82808C12.0691 7.75316 10.2549 9.67823 8.44082 11.6033C7.66474 10.7391 6.88865 9.87494 6.11256 9.01076C5.69772 8.54883 4.77564 8.58864 4.35125 9.01076C3.85781 9.50156 3.90872 10.1262 4.35125 10.619C5.41382 11.8022 6.47639 12.9853 7.53895 14.1685C7.94438 14.62 8.89283 14.6009 9.30028 14.1685C11.1521 12.2034 13.004 10.2383 14.8559 8.27314C15.1188 7.99419 15.3816 7.71524 15.6445 7.43629C16.0691 6.98577 16.1655 6.26476 15.6445 5.82808Z" fill="white"/>
                            </svg>
                            <svg class="unchecked" width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="0.65" y="0.65" width="18.7" height="18.7" rx="9.35" stroke="#E6007E" stroke-width="1.3"/>
                            </svg>
                        </div>
                        <div class="service-name-wrapper">
                            <svg width="73" height="73" viewBox="0 0 73 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="73" height="73" rx="20" fill="url(#paint0_linear_1212_36988)"/>
                                <ellipse cx="37" cy="28.375" rx="11.375" ry="4.875" stroke="white" stroke-width="3"/>
                                <path d="M25.625 38.125C25.625 38.125 25.625 41.9326 25.625 44.625C25.625 47.3174 30.7178 49.5 37 49.5C43.2822 49.5 48.375 47.3174 48.375 44.625C48.375 43.2811 48.375 38.125 48.375 38.125" stroke="white" stroke-width="3" stroke-linecap="square"/>
                                <path d="M25.625 28.375C25.625 28.375 25.625 33.8076 25.625 36.5C25.625 39.1924 30.7178 41.375 37 41.375C43.2822 41.375 48.375 39.1924 48.375 36.5C48.375 35.1561 48.375 28.375 48.375 28.375" stroke="white" stroke-width="3"/>
                                <defs>
                                <linearGradient id="paint0_linear_1212_36988" x1="36.5" y1="0" x2="36.5" y2="73" gradientUnits="userSpaceOnUse">
                                <stop stop-color="#E10080"/>
                                <stop offset="1" stop-color="#991A81"/>
                                </linearGradient>
                                </defs>
                            </svg>
                            <p>{{ __('Hosting') }}</p>
                        </div>
                    </div>

                </div>

                <div class="toggle-action">
                    <p>Would you like to bill this service?</p>
                    <label class="switch">
                        <input type="checkbox" id="service-checkbox">
                        <span class="slider round"></span>
                        <p class="yes">Yes</p>
                        <p class="no">No</p>
                    </label>
                </div>
            </div>
        </div>
    </div>

    <form id="formDomain" class="js-row-service" action="{{ route('domain.store') }}" method="POST" enctype="application/x-www-form-urlencoded">

        @csrf
        @method('POST')

        <div class="row row-forms row-domain-information">
            <div class="col">
                <h2>{{ __('Domain information') }}</h2>

                <input type="hidden" name="client_id" id="client_id">

                <input id="is_billed" type="hidden" name="is_billed" value="0">

                <div class="menu-wrapper">
                    <div data-target="domain-general" class="menu-item js-menu-item domain btn btn--primary--outline active">
                        General
                    </div>
                    <div data-target="domain-internal-note" class="menu-item js-menu-item domain btn btn--primary--outline">
                        Internal note
                    </div>
                </div>

                <div id="domain-general" class="general js-domain-information-forms active">

                    @include('../../components/domain-create/domain')

                    @include('../../components/domain-create/registrar')

                    @include('../../components/domain-create/domain-contacts')

                </div>

                <div id="domain-internal-note" class="internal-note js-domain-information-forms">

                    @include('../../components/domain-create/internal-note')

                </div>

            </div>
        </div>

        <div class="row row-forms row-billing-information js-row-billing-information">
            <div class="col">
                <h2>{{ __('Information about the billing (recurring profile)') }}</h2>

                <div class="menu-wrapper">
                    <div data-target="new-profile" class="menu-item js-billing-menu-item btn btn--primary--outline active">
                        New profile
                    </div>
                    <div data-target="existing-profile" class="menu-item js-billing-menu-item btn btn--primary--outline">
                        Existing profile
                    </div>
                </div>

                <div id="new-profile" class="new-profile js-billing-forms active">

                    @include('../../components/domain-create/financial')

                    @include('../../components/domain-create/billing-period')

                    @include('../../components/domain-create/contract-data')

                    {{-- @include('../../components/domain-create/preferences') --}}

                </div>

                <div id="existing-profile" class="existing-profile js-billing-forms">

                    @include('../../components/domain-create/existing-profile')

                    @include('../../components/domain-create/information-recurring')

                </div>
            </div>
        </div>

        <button type="submit" class="btn btn--primary"> {{ __('Add') }} </button>

    </form>

    <form id="formHosting" class="js-row-service" action="{{ route('hosting.store') }}" method="POST" enctype="application/x-www-form-urlencoded">

        @csrf
        @method('POST')

        <div class="row row-forms row-hosting-information">
            <div class="col">
                <h2>{{ __('Hosting account information') }}</h2>

                <input type="hidden" name="client_id" id="client_id" required>

                <input id="is_billed" type="hidden" name="is_billed" value="0">

                <div class="menu-wrapper">
                    <div data-target="hosting-general" class="menu-item js-menu-item hosting btn btn--primary--outline active">
                        General
                    </div>
                    <div data-target="hosting-internal-note" class="menu-item js-menu-item hosting btn btn--primary--outline">
                        Internal note
                    </div>
                </div>

                <div id="hosting-general" class="general js-hosting-information-forms active">

                    @include('../../components/domain-create/account-information')

                    @include('../../components/domain-create/package')

                    @include('../../components/domain-create/hosting-properties')

                </div>

                <div id="hosting-internal-note" class="internal-note js-hosting-information-forms">

                    @include('../../components/domain-create/internal-note')

                </div>

            </div>
        </div>

        <div class="row row-forms row-billing-information js-row-billing-information">
            <div class="col">
                <h2>{{ __('Information about the billing (recurring profile)') }}</h2>

                <div class="menu-wrapper">
                    <div data-target="hosting-new-profile" class="menu-item js-billing-menu-item btn btn--primary--outline active">
                        New profile
                    </div>
                    <div data-target="hosting-existing-profile" class="menu-item js-billing-menu-item btn btn--primary--outline">
                        Existing profile
                    </div>
                </div>

                <div id="hosting-new-profile" class="new-profile js-billing-forms active">

                    @include('../../components/domain-create/financial')

                    @include('../../components/domain-create/billing-period')

                    @include('../../components/domain-create/contract-data')

                    @include('../../components/domain-create/preferences')

                </div>

                <div id="hosting-existing-profile" class="existing-profile js-billing-forms">

                    @include('../../components/domain-create/existing-profile')

                    @include('../../components/domain-create/information-recurring')

                </div>
            </div>
        </div>

        <button type="submit" class="btn btn--primary"> {{ __('Add') }} </button>

    </form>

</div>

@endsection

@section('modal')

    @include('../../components/modals/_modals', ['modalType' => 'contact-information'])

    @include('../../components/modals/_modals', ['modalType' => 'hosting-list'])

    @include('../../components/modals/_modals', ['modalType' => 'domain-list'])

    @include('../../components/modals/_modals', ['modalType' => 'client-list'])

@endsection
