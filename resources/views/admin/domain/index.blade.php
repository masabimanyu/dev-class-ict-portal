@extends('layouts.app')
@section('content')
    <div class="domain-overview">
        <div class="row">
            <h1 class="title-page">{{ __('Domain') }}</h1>
        </div>
        <div class="row table-action">
            <div class="col title-col all-client">
                <h2 class="title-table">{{ __('All domain') }}</h2>
                <p class="count total">
                    @if(isset($domains->results))
                        {{ count($domains->results) }}
                    @else
                        0
                    @endif
                </p>
            </div>
            <div class="col action-col">
                <div class="search-wrapper">
                    <input type="text" placeholder="Search Domain" class="search-client" id="searchDomainBtn">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="11" cy="11" r="7" stroke="#33363F" stroke-width="1.5"/>
                        <path d="M20 20L17 17" stroke="#33363F" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>

                <a href="{{ route('domain.create') }}" class="btn btn--primary">
                    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2.8125 9H15.1875" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M9 2.8125V15.1875" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    {{ __('Add new domain') }}
                </a>
            </div>
        </div>
        <div class="row row-table">
            <div class="col">
                <div class="table-filter select-status">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 12L5 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M19 20L19 18" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M5 20L5 16" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M19 12L19 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M12 7L12 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M12 20L12 12" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="5" cy="14" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="12" cy="9" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="19" cy="15" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                    <select class="filter-dropdown" name="status" id="selectDomainBtn">
                        <option value="">{{ __('All status') }}</option>
                        <option value="ACT"> {{ __('Active') }} </option>
                        <option value="REQ"> {{ __('Deactive') }} </option>
                    </select>
                </div>
            </div>
            <div class="col table-col">
                <table class="users-table-content table-content">
                    <thead>
                        <th>{{ __('Domain') }}</th>
                        <th>{{ __('Next domain') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                        {{-- @php
                            $invoiceStatus = [
                                0 => "Draft invoice",
                                2 => "Unpaid", //Sent
                                3 => "Paid in part",
                                4 => "Paid",
                                8 => "Credit invoice",
                                9 => "Cancelled"
                            ];
                        @endphp --}}
                        @if(isset($domains->results))
                            @foreach ($domains->results as $key => $domain)
                                <tr>
                                    <td>{{ $domain->domain->name.".".$domain->domain->extension }}</td>
                                    <td>
                                        @php
                                            $date = new DateTime($domain->renewal_date);
                                            $date = $date->format('d/m/Y');
                                        @endphp
                                        {{ $date }}
                                    </td>
                                    <td>
                                        @if($domain->status == "ACT")
                                        <div class="label label-active">
                                            Active
                                        </div>
                                        @else
                                        <div class="label label-deactive">
                                            Deactive
                                        </div>
                                        @endif
                                    </td>
                                    <td><a href="{{ route('domain.show', $domain->id) }}">
                                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip0_1574_40613)">
                                            <path d="M0.666656 8.00033C0.666656 8.00033 3.33332 2.66699 7.99999 2.66699C12.6667 2.66699 15.3333 8.00033 15.3333 8.00033C15.3333 8.00033 12.6667 13.3337 7.99999 13.3337C3.33332 13.3337 0.666656 8.00033 0.666656 8.00033Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                            <path d="M7.99999 10.0003C9.10456 10.0003 9.99999 9.10489 9.99999 8.00033C9.99999 6.89576 9.10456 6.00033 7.99999 6.00033C6.89542 6.00033 5.99999 6.89576 5.99999 8.00033C5.99999 9.10489 6.89542 10.0003 7.99999 10.0003Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                            </g>
                                            <defs>
                                            <clipPath id="clip0_1574_40613">
                                            <rect width="16" height="16" fill="currentColor"/>
                                            </clipPath>
                                            </defs>
                                        </svg>                                            
                                        {{ __('Detail') }}
                                    </a></td>
                                    <td><a href="{{ route('domain.nameserver', $domain->id) }}">
                                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip0_1574_40615)">
                                            <path d="M12.0642 6.73263L11.4818 6.44402L12.0642 6.73263C12.293 6.27099 12.293 5.72901 12.0642 5.26737C11.9422 5.0211 11.7309 4.81061 11.5138 4.59439C11.4958 4.57643 11.4777 4.55844 11.4596 4.54038C11.4416 4.52232 11.4236 4.50425 11.4056 4.48623C11.1894 4.26915 10.9789 4.05782 10.7326 3.93578C10.271 3.70702 9.72902 3.70702 9.26739 3.93578C9.02112 4.05782 8.81062 4.26915 8.5944 4.48623C8.57644 4.50426 8.55845 4.52232 8.54039 4.54038L3.93211 9.14867C3.92 9.16077 3.90788 9.17286 3.89575 9.18494C3.73735 9.34283 3.57907 9.50061 3.46714 9.6983C3.35522 9.89598 3.30136 10.1129 3.24746 10.3299C3.24334 10.3466 3.23921 10.3632 3.23506 10.3798L2.90486 11.7006C2.90238 11.7105 2.89985 11.7206 2.8973 11.7308C2.85896 11.8837 2.8144 12.0615 2.79952 12.2136C2.78285 12.3839 2.78332 12.7022 3.04054 12.9595C3.29776 13.2167 3.61609 13.2172 3.78641 13.2005C3.93856 13.1856 4.11633 13.141 4.26924 13.1027C4.27942 13.1002 4.2895 13.0976 4.29944 13.0951L5.62023 12.7649C5.63684 12.7608 5.65345 12.7567 5.67007 12.7525C5.88713 12.6986 6.10403 12.6448 6.30171 12.5329C6.4994 12.4209 6.65718 12.2627 6.81507 12.1043C6.82715 12.0921 6.83924 12.08 6.85134 12.0679L11.4596 7.45962C11.4777 7.44156 11.4958 7.42356 11.5138 7.40561C11.7309 7.18939 11.9422 6.9789 12.0642 6.73263Z" stroke="currentColor" stroke-width="1.3"/>
                                            <path d="M8.33334 5.00033L10.3333 3.66699L12.3333 5.66699L11 7.66699L8.33334 5.00033Z" fill="currentColor"/>
                                            </g>
                                            <defs>
                                            <clipPath id="clip0_1574_40615">
                                            <rect width="16" height="16" fill="currentColor"/>
                                            </clipPath>
                                            </defs>
                                        </svg>                                            
                                        {{ __('Edit name server') }}
                                    </a></td>
                                    <td><a href="{{ route('domain.information', $domain->id) }}">
                                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <g clip-path="url(#clip0_1574_40615)">
                                            <path d="M12.0642 6.73263L11.4818 6.44402L12.0642 6.73263C12.293 6.27099 12.293 5.72901 12.0642 5.26737C11.9422 5.0211 11.7309 4.81061 11.5138 4.59439C11.4958 4.57643 11.4777 4.55844 11.4596 4.54038C11.4416 4.52232 11.4236 4.50425 11.4056 4.48623C11.1894 4.26915 10.9789 4.05782 10.7326 3.93578C10.271 3.70702 9.72902 3.70702 9.26739 3.93578C9.02112 4.05782 8.81062 4.26915 8.5944 4.48623C8.57644 4.50426 8.55845 4.52232 8.54039 4.54038L3.93211 9.14867C3.92 9.16077 3.90788 9.17286 3.89575 9.18494C3.73735 9.34283 3.57907 9.50061 3.46714 9.6983C3.35522 9.89598 3.30136 10.1129 3.24746 10.3299C3.24334 10.3466 3.23921 10.3632 3.23506 10.3798L2.90486 11.7006C2.90238 11.7105 2.89985 11.7206 2.8973 11.7308C2.85896 11.8837 2.8144 12.0615 2.79952 12.2136C2.78285 12.3839 2.78332 12.7022 3.04054 12.9595C3.29776 13.2167 3.61609 13.2172 3.78641 13.2005C3.93856 13.1856 4.11633 13.141 4.26924 13.1027C4.27942 13.1002 4.2895 13.0976 4.29944 13.0951L5.62023 12.7649C5.63684 12.7608 5.65345 12.7567 5.67007 12.7525C5.88713 12.6986 6.10403 12.6448 6.30171 12.5329C6.4994 12.4209 6.65718 12.2627 6.81507 12.1043C6.82715 12.0921 6.83924 12.08 6.85134 12.0679L11.4596 7.45962C11.4777 7.44156 11.4958 7.42356 11.5138 7.40561C11.7309 7.18939 11.9422 6.9789 12.0642 6.73263Z" stroke="currentColor" stroke-width="1.3"/>
                                            <path d="M8.33334 5.00033L10.3333 3.66699L12.3333 5.66699L11 7.66699L8.33334 5.00033Z" fill="currentColor"/>
                                            </g>
                                            <defs>
                                            <clipPath id="clip0_1574_40615">
                                            <rect width="16" height="16" fill="currentColor"/>
                                            </clipPath>
                                            </defs>
                                        </svg>   
                                        {{ __('Edit information') }}</a></td>
                                </tr>
                            @endforeach
                        @elseif((int) $domains->total == 0 )
                            <tr>
                                <td colspan="6">There is no domain for now</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
