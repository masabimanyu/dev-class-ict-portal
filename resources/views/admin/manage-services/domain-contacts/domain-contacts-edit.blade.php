@extends('layouts.app')
@section('content')
    <div class="manage-services domain-contacts-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="card-import-wrapper">
                    <a href="" class="container-card-3">
                        <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="46" height="46" rx="10" fill="url(#paint0_linear_2107_53427)"/>
                            <path d="M32 25.4925V31.5C32 32.0967 31.7629 32.669 31.341 33.091C30.919 33.5129 30.3467 33.75 29.75 33.75H14C13.4033 33.75 12.831 33.5129 12.409 33.091C11.9871 32.669 11.75 32.0967 11.75 31.5V15.75C11.75 15.1533 11.9871 14.581 12.409 14.159C12.831 13.7371 13.4033 13.5 14 13.5H20.0075M29.75 11.25L34.25 15.75L23 27H18.5V22.5L29.75 11.25Z" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <defs>
                            <linearGradient id="paint0_linear_2107_53427" x1="23" y1="0" x2="23" y2="46" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>                            
                        <p class="text">Update contact at register</p>
                    </a>
                </div>

                <div class="notice">
                    <svg width="39" height="39" viewBox="0 0 39 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="39" height="39" rx="10" fill="#E2F7FF"/>
                        <path d="M15.9062 28.8438H23.0938" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M19.5 25.9688V20.9375" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M16.625 18.0625L19.5 20.9375L22.375 18.0625" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M15.071 23.005C14.2164 22.3375 13.5242 21.4849 13.0464 20.5114C12.5686 19.5379 12.3178 18.4688 12.3126 17.3844C12.2911 13.4885 15.4319 10.25 19.3267 10.1583C20.8359 10.122 22.3183 10.5618 23.5636 11.4153C24.8088 12.2689 25.7537 13.4929 26.2641 14.9136C26.7746 16.3344 26.8248 17.8799 26.4075 19.3307C25.9902 20.7816 25.1267 22.0643 23.9394 22.9968C23.6779 23.1994 23.4659 23.4588 23.3194 23.7554C23.1729 24.052 23.0957 24.378 23.0938 24.7088L23.0938 25.2501C23.0938 25.4407 23.018 25.6235 22.8832 25.7583C22.7484 25.8931 22.5656 25.9688 22.375 25.9688H16.625C16.4344 25.9688 16.2515 25.8931 16.1168 25.7583C15.982 25.6235 15.9062 25.4407 15.9062 25.2501L15.9062 24.7082C15.9054 24.3795 15.8298 24.0554 15.6851 23.7603C15.5404 23.4652 15.3304 23.2069 15.071 23.005V23.005Z" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    
                    <div class="text-wrapper">
                        <p>Warning!</p>
                        <ul>
                            <li>If you edit this contact, it will be adjusted at 4 domain names.</li>
                            <li>If you only want to change the details of one domain name, you must edit the relevant domain name.</li>
                        </ul>
                    </div>
                </div>

                <div class="form-menu-tab">
                    <h2>Edit contact</h2>
                    <div class="menu-wrapper">
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="generalContent" class="btn btn--primary--outline menu-item active js-menu-item">
                            General
                        </div>
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="linkedDataContent" class="btn btn--primary--outline menu-item js-menu-item">
                            Linked data
                        </div>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="generalContent" class="js-manage-service-edit-menu-content active"> 

                    @include('../../components/manage-service/domain-contacts/domain-contacts-edit/general/contact-information-1')
                    @include('../../components/manage-service/domain-contacts/domain-contacts-edit/general/contact-information-2')
                    @include('../../components/manage-service/domain-contacts/domain-contacts-edit/general/contact-information-3')

                    <div class="button-wrapper">
                        <button type="submit" class="btn btn--primary"> {{ __('Save data') }} </button>
                        <button type="submit" class="btn btn--primary--outline"> 
                            <svg class="" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 15L10 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M14 15L14 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M3 7H21V7C20.0681 7 19.6022 7 19.2346 7.15224C18.7446 7.35523 18.3552 7.74458 18.1522 8.23463C18 8.60218 18 9.06812 18 10V16C18 17.8856 18 18.8284 17.4142 19.4142C16.8284 20 15.8856 20 14 20H10C8.11438 20 7.17157 20 6.58579 19.4142C6 18.8284 6 17.8856 6 16V10C6 9.06812 6 8.60218 5.84776 8.23463C5.64477 7.74458 5.25542 7.35523 4.76537 7.15224C4.39782 7 3.93188 7 3 7V7Z" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M10.0681 3.37059C10.1821 3.26427 10.4332 3.17033 10.7825 3.10332C11.1318 3.03632 11.5597 3 12 3C12.4403 3 12.8682 3.03632 13.2175 3.10332C13.5668 3.17033 13.8179 3.26427 13.9319 3.37059" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            {{ __('Delete') }} 
                        </button>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="linkedDataContent" class="js-manage-service-edit-menu-content">

                    @include('../../components/manage-service/domain-contacts/domain-contacts-edit/linked-data/linked-domain-names')

                </div>


            </div>
        </div>
    </div>


@endsection
