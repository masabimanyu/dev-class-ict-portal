@extends('layouts.app')
@section('content')
    <div class="manage-services domain-contacts-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="form-menu-tab">
                    <h2>Add new domain contacts</h2>
                    <div class="menu-wrapper">
                        <div class="btn btn--primary--outline menu-item active">
                            General
                        </div>
                    </div>
                </div>

                @include('../../components/manage-service/domain-contacts/domain-contacts-add/contact-information-1')
                @include('../../components/manage-service/domain-contacts/domain-contacts-add/contact-information-2')
                @include('../../components/manage-service/domain-contacts/domain-contacts-add/contact-information-3')

                <button type="submit" class="btn btn--primary"> {{ __('Add contact') }} </button>
            </div>
        </div>
    </div>


@endsection
