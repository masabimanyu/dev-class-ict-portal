@extends('layouts.app')
@section('content')
    <div class="manage-services server-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="card-import-wrapper">
                    <a href="{{ route('manage-service.server.edit.send-mail') }}" class="container-card-3">
                        <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="46" height="46" rx="10" fill="url(#paint0_linear_2109_53428)"/>
                            <path d="M15.125 22.5V18.625C15.125 16.7394 15.125 15.7966 15.7108 15.2108C16.2966 14.625 17.2394 14.625 19.125 14.625H20.2181C21.0356 14.625 21.4444 14.625 21.8119 14.7772C22.1795 14.9295 22.4685 15.2185 23.0466 15.7966L24.0784 16.8284C24.6565 17.4065 24.9455 17.6955 25.3131 17.8478C25.6806 18 26.0894 18 26.9069 18H29.125C31.0106 18 31.9534 18 32.5392 18.5858C33.125 19.1716 33.125 20.1144 33.125 22V26.375C33.125 28.2606 33.125 29.2034 32.5392 29.7892C31.9534 30.375 31.0106 30.375 29.125 30.375H27.5" stroke="white" stroke-width="2"/>
                            <path d="M14 27.5625H24.125M24.125 27.5625L20.1875 23.625M24.125 27.5625L20.1875 31.5" stroke="white" stroke-width="2"/>
                            <defs>
                            <linearGradient id="paint0_linear_2109_53428" x1="23" y1="0" x2="23" y2="46" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>                                                     
                        <p class="text">Send mail</p>
                    </a>

                    <a href="{{ route('manage-service.server.edit.import-account') }}" class="container-card-3">
                        <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="46" height="46" rx="10" fill="url(#paint0_linear_2109_53432)"/>
                            <path d="M23 24.75L22.2929 25.4571L23 26.1642L23.7071 25.4571L23 24.75ZM24 14.625C24 14.0727 23.5523 13.625 23 13.625C22.4477 13.625 22 14.0727 22 14.625L24 14.625ZM16.6679 19.8321L22.2929 25.4571L23.7071 24.0429L18.0821 18.4179L16.6679 19.8321ZM23.7071 25.4571L29.3321 19.8321L27.9179 18.4179L22.2929 24.0429L23.7071 25.4571ZM24 24.75L24 14.625L22 14.625L22 24.75L24 24.75Z" fill="white"/>
                            <path d="M15.125 27L15.125 28.125C15.125 29.3676 16.1324 30.375 17.375 30.375L28.625 30.375C29.8676 30.375 30.875 29.3676 30.875 28.125V27" stroke="white" stroke-width="2"/>
                            <defs>
                            <linearGradient id="paint0_linear_2109_53432" x1="23" y1="0" x2="23" y2="46" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>                                                
                        <p class="text">Import account</p>
                    </a>
                </div>

                <div class="form-menu-tab">
                    <h2>Edit server</h2>
                    <div class="menu-wrapper">
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="generalContent" class="btn btn--primary--outline menu-item active js-menu-item">
                            General
                        </div>
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="linkedDataContent" class="btn btn--primary--outline menu-item js-menu-item">
                            Linked data
                        </div>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="generalContent" class="js-manage-service-edit-menu-content active"> 

                    @include('../../components/manage-service/servers/servers-edit/general/general')
                    @include('../../components/manage-service/servers/servers-edit/general/nameservers')
                    @include('../../components/manage-service/servers/servers-edit/general/hosting-panel-setting')
                    @include('../../components/manage-service/servers/servers-edit/general/specific-hosting-panel-setting')
                    @include('../../components/manage-service/servers/servers-edit/general/hostfact')

                    <div class="button-wrapper">
                        <button type="submit" class="btn btn--primary"> {{ __('Save data') }} </button>
                        <button type="submit" class="btn btn--primary--outline"> 
                            <svg class="" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 15L10 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M14 15L14 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M3 7H21V7C20.0681 7 19.6022 7 19.2346 7.15224C18.7446 7.35523 18.3552 7.74458 18.1522 8.23463C18 8.60218 18 9.06812 18 10V16C18 17.8856 18 18.8284 17.4142 19.4142C16.8284 20 15.8856 20 14 20H10C8.11438 20 7.17157 20 6.58579 19.4142C6 18.8284 6 17.8856 6 16V10C6 9.06812 6 8.60218 5.84776 8.23463C5.64477 7.74458 5.25542 7.35523 4.76537 7.15224C4.39782 7 3.93188 7 3 7V7Z" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M10.0681 3.37059C10.1821 3.26427 10.4332 3.17033 10.7825 3.10332C11.1318 3.03632 11.5597 3 12 3C12.4403 3 12.8682 3.03632 13.2175 3.10332C13.5668 3.17033 13.8179 3.26427 13.9319 3.37059" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            {{ __('Delete') }} 
                        </button>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="linkedDataContent" class="js-manage-service-edit-menu-content">

                    @include('../../components/manage-service/servers/servers-edit/linked-data/accounts')
                    @include('../../components/manage-service/servers/servers-edit/linked-data/packages')
                    @include('../../components/manage-service/servers/servers-edit/linked-data/clients')

                </div>


            </div>
        </div>
    </div>


@endsection
