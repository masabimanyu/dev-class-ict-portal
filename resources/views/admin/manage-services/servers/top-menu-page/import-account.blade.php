@extends('layouts.app')
@section('content')
    <div class="manage-services server-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                <div class="row row-action btn-import">
                    <div class="col">
                        <x-back-button redirect="{{ route('manage-service.server.edit') }}" text="{{ __('back') }}"/>
                    </div>
                </div>
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="form-menu-tab">
                    <h2>Import accounts from server001.classict.nl</h2>

                    <div class="notice">
                        <svg width="39" height="39" viewBox="0 0 39 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="39" height="39" rx="10" fill="#E2F7FF"/>
                            <path d="M15.9062 28.8438H23.0938" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M19.5 25.9688V20.9375" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M16.625 18.0625L19.5 20.9375L22.375 18.0625" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M15.071 23.005C14.2164 22.3375 13.5242 21.4849 13.0464 20.5114C12.5686 19.5379 12.3178 18.4688 12.3126 17.3844C12.2911 13.4885 15.4319 10.25 19.3267 10.1583C20.8359 10.122 22.3183 10.5618 23.5636 11.4153C24.8088 12.2689 25.7537 13.4929 26.2641 14.9136C26.7746 16.3344 26.8248 17.8799 26.4075 19.3307C25.9902 20.7816 25.1267 22.0643 23.9394 22.9968C23.6779 23.1994 23.4659 23.4588 23.3194 23.7554C23.1729 24.052 23.0957 24.378 23.0938 24.7088L23.0938 25.2501C23.0938 25.4407 23.018 25.6235 22.8832 25.7583C22.7484 25.8931 22.5656 25.9688 22.375 25.9688H16.625C16.4344 25.9688 16.2515 25.8931 16.1168 25.7583C15.982 25.6235 15.9062 25.4407 15.9062 25.2501L15.9062 24.7082C15.9054 24.3795 15.8298 24.0554 15.6851 23.7603C15.5404 23.4652 15.3304 23.2069 15.071 23.005V23.005Z" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        
                        <div class="text-wrapper">
                            <p>Notice</p>
                            <p>the imported data has been cached. <a href="">click here to do a new import</a></p>
                        </div>
                    </div>
                    <div class="menu-wrapper">
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="newAccounts" class="btn btn--primary--outline menu-item active js-menu-item">
                            New accounts (0)
                        </div>

                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="importedAccounts" class="btn btn--primary--outline menu-item js-menu-item">
                            Imported accounts (229)
                        </div>

                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="newAccounts" class="js-manage-service-edit-menu-content active"> 

                    @include('../../components/manage-service/servers/servers-edit/top-menu-page/import-account/new-accounts')

                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="importedAccounts" class="js-manage-service-edit-menu-content"> 

                    @include('../../components/manage-service/servers/servers-edit/top-menu-page/import-account/Imported-accounts')

                </div>

            </div>
        </div>
    </div>


@endsection
