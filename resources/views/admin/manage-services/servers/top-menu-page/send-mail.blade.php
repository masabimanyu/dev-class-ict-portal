@extends('layouts.app')
@section('content')
    <div class="manage-services server-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                <div class="row row-action btn-import">
                    <div class="col">
                        <x-back-button redirect="{{ route('manage-service.server.edit') }}" text="{{ __('back') }}"/>
                    </div>
                </div>
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="form-menu-tab">
                    <h2>Send mailing</h2>

                    <div class="notice">
                        <svg width="39" height="39" viewBox="0 0 39 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="39" height="39" rx="10" fill="#E2F7FF"/>
                            <path d="M19.5 17.3438V20.9375" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M18.2557 11.5924L10.3516 25.2479C10.2251 25.4664 10.1584 25.7144 10.1582 25.9669C10.158 26.2193 10.2243 26.4674 10.3504 26.6861C10.4765 26.9048 10.658 27.0865 10.8766 27.2128C11.0952 27.3391 11.3432 27.4056 11.5957 27.4056H27.4039C27.6564 27.4056 27.9044 27.3391 28.123 27.2128C28.3416 27.0865 28.5231 26.9048 28.6493 26.6861C28.7754 26.4674 28.8417 26.2193 28.8414 25.9669C28.8412 25.7144 28.7745 25.4664 28.6481 25.2479L20.7439 11.5924C20.6177 11.3742 20.4362 11.1931 20.2179 11.0672C19.9995 10.9413 19.7519 10.875 19.4998 10.875C19.2478 10.875 19.0001 10.9413 18.7818 11.0672C18.5634 11.1931 18.382 11.3742 18.2557 11.5924V11.5924Z" stroke="#13A6DF" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M20.3359 24.1719C20.3359 24.6336 19.9617 25.0078 19.5 25.0078C19.0383 25.0078 18.6641 24.6336 18.6641 24.1719C18.6641 23.7102 19.0383 23.3359 19.5 23.3359C19.9617 23.3359 20.3359 23.7102 20.3359 24.1719Z" fill="#13A6DF" stroke="#13A6DF" stroke-width="0.125"/>
                        </svg>                            
                        
                        <div class="text-wrapper">
                            <p>Warning</p>
                            <p>Client DB10007 (Pietersma, Danny) has indicated to not want to receive any mailings</p>
                        </div>
                    </div>

                </div>

                <div> 

                    @include('../../components/manage-service/servers/servers-edit/top-menu-page/send-mail/general')
                    @include('../../components/manage-service/servers/servers-edit/top-menu-page/send-mail/add-email-address')
                    @include('../../components/manage-service/servers/servers-edit/top-menu-page/send-mail/clients')

                    <div class="button-wrapper">
                        <button type="submit" class="btn btn--primary"> {{ __('Send mail') }} </button>
                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection
