@extends('layouts.app')
@section('content')
    <div class="manage-services nameserver-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="form-menu-tab">
                    <h2>Add new nameserver group</h2>
                    <div class="menu-wrapper">
                        <div class="btn btn--primary--outline menu-item active">
                            General
                        </div>
                    </div>
                </div>

                @include('../../components/manage-service/nameserver-groups/nameserver-groups-add/general')
                @include('../../components/manage-service/nameserver-groups/nameserver-groups-add/dns-template')

                <button type="submit" class="btn btn--primary"> {{ __('Add Nameserver') }} </button>
            </div>
        </div>
    </div>


@endsection
