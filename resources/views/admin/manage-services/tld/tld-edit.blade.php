@extends('layouts.app')
@section('content')
    <div class="manage-services tld-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="form-menu-tab">
                    <h2>Edit TLD</h2>
                    <div class="menu-wrapper">
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="generalContent" class="btn btn--primary--outline menu-item active js-menu-item">
                            General
                        </div>
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="linkedDataContent" class="btn btn--primary--outline menu-item js-menu-item">
                            Linked data
                        </div>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="generalContent" class="js-manage-service-edit-menu-content active"> 

                    @include('../../components/manage-service/tlds/tld-edit/general/general')
                    @include('../../components/manage-service/tlds/tld-edit/general/public-whois-server')
                    @include('../../components/manage-service/tlds/tld-edit/general/tld-setting')
                    @include('../../components/manage-service/tlds/tld-edit/general/idn-support')

                    <div class="button-wrapper">
                        <button type="submit" class="btn btn--primary"> {{ __('Save data') }} </button>
                        <button type="submit" class="btn btn--primary--outline"> 
                            <svg class="" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 15L10 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M14 15L14 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M3 7H21V7C20.0681 7 19.6022 7 19.2346 7.15224C18.7446 7.35523 18.3552 7.74458 18.1522 8.23463C18 8.60218 18 9.06812 18 10V16C18 17.8856 18 18.8284 17.4142 19.4142C16.8284 20 15.8856 20 14 20H10C8.11438 20 7.17157 20 6.58579 19.4142C6 18.8284 6 17.8856 6 16V10C6 9.06812 6 8.60218 5.84776 8.23463C5.64477 7.74458 5.25542 7.35523 4.76537 7.15224C4.39782 7 3.93188 7 3 7V7Z" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M10.0681 3.37059C10.1821 3.26427 10.4332 3.17033 10.7825 3.10332C11.1318 3.03632 11.5597 3 12 3C12.4403 3 12.8682 3.03632 13.2175 3.10332C13.5668 3.17033 13.8179 3.26427 13.9319 3.37059" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            {{ __('Delete') }} 
                        </button>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="linkedDataContent" class="js-manage-service-edit-menu-content">

                    @include('../../components/manage-service/tlds/tld-edit/linked-data/linked-domain-names')

                </div>


            </div>
        </div>
    </div>


@endsection
