@extends('layouts.app')
@section('content')
    <div class="manage-services registrar-add">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Manage services') }}</h1>
                <p>{{ __('Overview / Manage services') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row row-action">
            <div class="col col-side-menu">
                @include('../../components/manage-service-side-menu')
            </div>
            <div class="col col-forms">

                <div class="card-import-wrapper">
                    <a href="{{ route('manage-service.registrar.edit.import-contact') }}" class="container-card-3">
                        <svg class="icon" width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="46" height="46" rx="10" fill="url(#paint0_linear_2092_62094)"/>
                            <path d="M28.0625 14.9062H33.125" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M30.5938 12.375V17.4375" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M12.7684 31.7803C13.8057 29.9847 15.2973 28.4938 17.0933 27.4572C18.8893 26.4207 20.9265 25.875 23.0001 25.875C25.0738 25.875 27.1109 26.4207 28.9069 27.4573C30.7029 28.4939 32.1944 29.9849 33.2317 31.7805" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M29.3335 21.4646C28.8823 22.6828 28.089 23.7449 27.0488 24.5232C26.0086 25.3015 24.7659 25.7629 23.4698 25.852C22.1738 25.9412 20.8796 25.6542 19.7426 25.0256C18.6057 24.397 17.6745 23.4535 17.0608 22.3085C16.4471 21.1635 16.177 19.8656 16.2831 18.5708C16.3891 17.276 16.8667 16.0394 17.6585 15.0095C18.4503 13.9796 19.5227 13.2002 20.7467 12.7649C21.9707 12.3297 23.2944 12.2571 24.5587 12.5559" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <defs>
                            <linearGradient id="paint0_linear_2092_62094" x1="23" y1="0" x2="23" y2="46" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>
                        <p class="text">Import contact</p>
                    </a>

                    <a href="{{ route('manage-service.registrar.edit.import-domain') }}" class="container-card-3">
                        <svg class="icon" width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="46" height="46" rx="10" fill="url(#paint0_linear_2092_62102)"/>
                            <path d="M23 32.625C28.5919 32.625 33.125 28.0919 33.125 22.5C33.125 16.9081 28.5919 12.375 23 12.375C17.4081 12.375 12.875 16.9081 12.875 22.5C12.875 28.0919 17.4081 32.625 23 32.625Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M13.4516 19.125H32.5483" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M13.452 25.875H32.5485" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M23 32.3548C25.33 32.3548 27.2188 27.9434 27.2188 22.5016C27.2188 17.0599 25.33 12.6484 23 12.6484C20.67 12.6484 18.7812 17.0599 18.7812 22.5016C18.7812 27.9434 20.67 32.3548 23 32.3548Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <defs>
                            <linearGradient id="paint0_linear_2092_62102" x1="23" y1="0" x2="23" y2="46" gradientUnits="userSpaceOnUse">
                            <stop stop-color="#E10080"/>
                            <stop offset="1" stop-color="#991A81"/>
                            </linearGradient>
                            </defs>
                        </svg>                            
                        <p class="text">Import domain</p>
                    </a>
                </div>

                <div class="form-menu-tab">
                    <h2>Edit registrar</h2>
                    <div class="menu-wrapper">
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="generalContent" class="btn btn--primary--outline menu-item active js-menu-item">
                            General
                        </div>
                        {{-- manageServiceEditMenu.js --}}
                        <div data-target="linkedDataContent" class="btn btn--primary--outline menu-item js-menu-item">
                            Linked data
                        </div>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="generalContent" class="js-manage-service-edit-menu-content active"> 

                    @include('../../components/manage-service/registrar/registrar-edit/general/general')
                    @include('../../components/manage-service/registrar/registrar-edit/general/default-domain-contacts')
                    @include('../../components/manage-service/registrar/registrar-edit/general/nameservers')
                    @include('../../components/manage-service/registrar/registrar-edit/general/link-settings')
                    @include('../../components/manage-service/registrar/registrar-edit/general/hostfact')

                    <div class="button-wrapper">
                        <button type="submit" class="btn btn--primary"> {{ __('Save data') }} </button>
                        <button type="submit" class="btn btn--primary--outline"> 
                            <svg class="" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10 15L10 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M14 15L14 12" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M3 7H21V7C20.0681 7 19.6022 7 19.2346 7.15224C18.7446 7.35523 18.3552 7.74458 18.1522 8.23463C18 8.60218 18 9.06812 18 10V16C18 17.8856 18 18.8284 17.4142 19.4142C16.8284 20 15.8856 20 14 20H10C8.11438 20 7.17157 20 6.58579 19.4142C6 18.8284 6 17.8856 6 16V10C6 9.06812 6 8.60218 5.84776 8.23463C5.64477 7.74458 5.25542 7.35523 4.76537 7.15224C4.39782 7 3.93188 7 3 7V7Z" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                                <path d="M10.0681 3.37059C10.1821 3.26427 10.4332 3.17033 10.7825 3.10332C11.1318 3.03632 11.5597 3 12 3C12.4403 3 12.8682 3.03632 13.2175 3.10332C13.5668 3.17033 13.8179 3.26427 13.9319 3.37059" stroke="#951D81" stroke-width="2" stroke-linecap="round"/>
                            </svg>
                            {{ __('Delete') }} 
                        </button>
                    </div>
                </div>

                {{-- manageServiceEditMenu.js --}}
                <div id="linkedDataContent" class="js-manage-service-edit-menu-content">

                    @include('../../components/manage-service/registrar/registrar-edit/linked-data/linked-domain-names')
                    @include('../../components/manage-service/registrar/registrar-edit/linked-data/linked-contacts')

                </div>


            </div>
        </div>
    </div>


@endsection
