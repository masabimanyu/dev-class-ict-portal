<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    @vite(['resources/scss/style.scss', 'resources/js/app.js'])

    <style>

        @page{
            margin: 0;
        }
        body{
            background-color: #F8FBFF;
        }
        .section{
            padding: 0 34px;
        }

        /* Section Heading */
        .section-heading{
            margin-bottom: 47px;
        }
        .section-heading > .bg{
            width: 100%;

            position: absolute;
            top: 0;
            left: 0;

            z-index: -1;

            max-height: 120px;

            object-fit: cover;
        }
        .section-heading > .row{
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
        .section-heading > .row .logo > img.logo{
            position: absolute;
            top: 0;

            transform: translateX(-13%);

            width: 30%;

        }
        .section-heading > .row .title{
            text-align: right;
        }
        .section-heading > .row .title .title-text{
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            font-size: 24px;
            line-height: 38px;

            margin-bottom: 0;

            color: #0E0E0E;
        }
        .section-heading > .row .title .title-text span{
            color: #951D81;
        }
        .section-heading > .row .title .title-sub{
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-size: 10px;
            line-height: 14px;

            color: #951D81;
        }
        /* Section Heading END */

        /* Section Client Information */
        .section-client-information{
            margin-bottom: 22px;
        }
        .section-client-information > .row > .col .info-wrapper .info-title{
            font-family: 'Calibri';
            font-style: normal;
            font-weight: 700;
            font-size: 12px;
            line-height: 15px;
        }
        .section-client-information > .row > .col .info-wrapper .info-desc{
            font-family: 'Calibri';
            font-style: normal;
            font-weight: 400;
            font-size: 10px;
            line-height: 12px;
        }
        .section-client-information > .row > .col table{
            width: 100%;
        }
        .section-client-information > .row > .col table td{
            width: 25%;

            padding-top: 40px;
        }
        /* Section Client Information END */

        /* Section Products */
        .section-products{
            margin-bottom: 30px;
        }
        .section-products table{
            background: white;

            width: 100%;

            border-radius: 10px;
        }
        .section-products table thead th{
            padding: 3px 23px;

            text-align: left;

            font-family: 'Calibri';
            font-style: normal;
            font-weight: 700;
            font-size: 12px;
            line-height: 24px;

            color: #951D81;

            border-bottom: 2px solid #F3E2F0;
        }
        .section-products table tbody tr td{
            padding: 10px 23px;

            text-align: left;

            font-family: 'Calibri';
            font-style: normal;
            font-weight: 400;
            font-size: 10px;
            line-height: 12px;

            color: #0E0E0E;

            border: none;
            border-bottom: 2px solid #F3E2F0;
        }
        .section-products table tbody tr td:nth-child(3),
        .section-products table thead th:nth-child(3){
            text-align: right;
        }
        .section-products table tbody tr:nth-child(odd){
            background-color: #FFFAFE;
        }
        /* Section Products END*/

        /* Section Total*/
        .section-total .row .col .condition-wrapper .title{
            font-family: 'Calibri';
            font-style: normal;
            font-weight: 700;
            font-size: 12px;
            line-height: 20px;

            color: #951D81;
        }
        .section-total .row .col .condition-wrapper .text{
            font-family: 'Calibri';
            font-style: normal;
            font-weight: 400;
            font-size: 10px;
            line-height: 20px;

            color: #0E0E0E;
        }
        .section-total .row .col .table-wrapper td.divider{
            border-right: 1px solid #F3E2F0;

            padding-right: 15px
        }
        .section-total .row .col .table-wrapper td:last-child{
            width: 70px;

            padding-left: 15px
        }
        .section-total .row .col .table-total{
            width: 100%;
        }
        .section-total .row .col .table-total tbody tr td{
            font-family: 'Calibri';
            font-style: normal;
            font-weight: 400;
            font-size: 10px;
            line-height: 12px;

            color: #0E0E0E;

            white-space: nowrap;
        }
        .section-total .row .col .table-total tbody tr:not(:last-child) td{
            padding-bottom: 10px;
        }
        .section-total .row .col .table-total tbody tr td:last-child{
            text-align: right;
        }
        /* Section Total END*/

        /* Section Footer */
        .section-footer {
            position: absolute;
            bottom: 10px;
            left: 50%;

            transform: translateX(-50%);
        }
        .section-footer .row{
            position: relative;

            height: 47px;
        }
        .section-footer table{
            margin: auto;

            transform: translateY(25%);
        }
        .section-footer .bg img{
            position: absolute;
            left: 50%;

            transform: translateX(-50%);

            z-index: -1;
        }
        .section-footer table td{
            vertical-align: middle;
        }
        .section-footer table td:not(:first-child){
            padding-left: 15px;
        }
        .section-footer table td img{
            max-height: 28px;
        }
        .section-footer table td li{
            list-style: disc;
            list-style-position: inside;

            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            font-size: 8px;
            line-height: 11px;
            text-transform: uppercase;

            color: white;
        }
        .section-footer table td li::marker{
            margin-right: 0;
        }
        /* Section Footer END */
    </style>
</head>
<body>
    {{-- @php
        $invoiceStatus = [
            0 => "Draft invoice",
            2 => "Unpaid", //Sent
            3 => "Paid in part",
            4 => "Paid",
            8 => "Credit invoice",
            9 => "Cancelled"
        ];
    @endphp --}}

    <section class="section section-heading">
        <img class="bg" src="assets/images/ict-pdf-header-bg.webp" alt="bg">
        <div class="row">
            <div class="col col-left">
                <div class="logo">
                    <img class="logo" src="assets/images/ict-pdf-header-logo.png" alt="bg">
                </div>
            </div>
            <div class="col col-right">
                <div class="title">
                    <p class="title-text">Invoice<span>.</span></p>
                    <p class="title-sub">www.classict.nl</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-client-information">
        <div class="row row-client-information">
            {{-- <div class="col col-client-information">
                <div class="info-wrapper">
                    <div class="info-title">Client name</div>
                    <div class="info-desc">Mr. Frank Vlemmix</div>
                </div>
                <div class="info-wrapper">
                    <div class="info-title">Company name</div>
                    <div class="info-desc">Fotoccasion</div>
                </div>
                <div class="info-wrapper">
                    <div class="info-title">Address</div>
                    <div class="info-desc">Adelaartlaan 192 5665CS Geldrop Netherlands</div>
                </div>
                <div class="info-wrapper">
                    <div class="info-title">Client no.</div>
                    <div class="info-desc">DB10000</div>
                </div>
                <div class="info-wrapper">
                    <div class="info-title">Invoice no</div>
                    <div class="info-desc">F2022-0305</div>
                </div>
                <div class="info-wrapper">
                    <div class="info-title">Invoice date</div>
                    <div class="info-desc">01/01/2022</div>
                </div>
                <div class="info-wrapper">
                    <div class="info-title">Term of payment</div>
                    <div class="info-desc">30 days</div>
                </div>
                <div class="info-wrapper">
                    <div class="info-title">Reference</div>
                    <div class="info-desc">According to estimate 13</div>
                </div>
            </div> --}}

            <div class="col col-client-information">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Client name</div>
                                    <div class="info-desc">Mr. Frank Vlemmix</div>
                                </div>
                            </td>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Company name</div>
                                    <div class="info-desc">Mr. Frank Vlemmix</div>
                                </div>
                            </td>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Address</div>
                                    <div class="info-desc">Adelaartlaan 192 5665CS Geldrop Netherlands</div>
                                </div>
                            </td>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Client no.</div>
                                    <div class="info-desc">DB10000</div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Invoice no.</div>
                                    <div class="info-desc">F2022-0305</div>
                                </div>
                            </td>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Invoice date</div>
                                    <div class="info-desc">01/01/2022</div>
                                </div>
                            </td>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Term of payment</div>
                                    <div class="info-desc">30 days</div>
                                </div>
                            </td>
                            <td>
                                <div class="info-wrapper">
                                    <div class="info-title">Reference</div>
                                    <div class="info-desc">According to estimate 13</div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="section section-products">
        <div class="invoice-container">
            <table class="user-detail">
                <thead>
                    <th>
                        Qty
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Price per unit
                    </th>
                    <th>
                        Total
                    </th>
                </thead>
                <tbody>
                    <tr>
                        <td>12</td>
                        <td>Hosting Starter Periode: 28-12-2022 tot 28-12-2023</td>
                        <td>€ 5,00</td>
                        <td>€ 60,00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Managed WordPress Hosting Start
                            01/10/2022 - 01/01/2023  (3 months )</td>
                        <td>€ 10,00</td>
                        <td>€ 20,00</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Development of “stars” in accordance
                            with agreements</td>
                        <td>€ 25,00</td>
                        <td>€ 25,00</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>

    <section class="section section-total">
        <div class="row">
            <div class="col">
                <table class="table-wrapper">
                    <tbody>
                        <tr>
                            <td>
                                <div class="condition-wrapper">
                                    <p class="title">
                                        Conditions
                                    </p>
                                    <p class="text">
                                        The amount due will be transferred within 30 days
                                        to 23.112.453.11 Attn. Company LLC stating the
                                        company name and credit note number.
                                        NOTE: If this concerns a credit note for a
                                        reversal of an invoice that has not yet been paid.
                                        This credit invoice will be canceled against the
                                        outstanding amount. This cancels the above conditions.
                                        Our general terms and conditions apply to all services.
                                        You can download this from our website www.classict.nl.
                                        For questions you can contact us by phone or e-mail.
                                    </p>
                                </div>
                            </td>
                            <td class="divider"></td>
                            <td>
                                <table class="table-total">
                                    <tbody>
                                        <tr>
                                            <td>Amount excl. BTW</td>
                                            <td>€ 95,00</td>
                                        </tr>
                                        <tr>
                                            <td>21% BTW</td>
                                            <td>€ 19,95</td>
                                        </tr>
                                        <tr>
                                            <td>Amount Incl. BTW</td>
                                            <td style="font-weight: 700; font-size: 12px;">€ 114,95</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="section section-footer">
        <div class="row">
            <div class="bg">
                <img src="assets/images/ict-pdf-footer-bg.png" alt="">
            </div>
            <div class="content-wrapper">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <img src="assets/images/ClassICT_logo.png" alt="">
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        KVk: 86206915
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        btw: nl863895232b01
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>
                                        iban: nl72 ingb 0009 0948 99
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</body>
</html>
