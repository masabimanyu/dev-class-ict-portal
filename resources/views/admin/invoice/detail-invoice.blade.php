@extends('layouts.app')
@section('content')
    @php
        $invoice = $invoice["invoice"];

        $invoiceStatus = [
            0 => "Draft invoice",
            2 => "Unpaid", //Sent
            3 => "Paid in part",
            4 => "Paid",
            8 => "Credit invoice",
            9 => "Cancelled"
        ];
    @endphp
    <div class="invoice-overview">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>
                    {{ __('Invoices')." ". $invoice["InvoiceCode"] ."-"." ". ($invoice["Authorisation"] == "yes" ? "Direct debit" : "") }}
                </h1>
                <p>{{ __('Overview / Invoices') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}

        <div class="row row-action">
            <div class="col">
                <x-back-button redirect="{{ route('domain.index') }}" text="{{ __('back') }}"/>
            </div>
        </div>

        <div class="row row-content container-card-1">

            <a href="{{ URL::to('invoiceDownload/'.$invoice['InvoiceCode']) }}"class="btn btn--primary fit-content" type="button">
                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M9 12L8.64645 12.3536L9 12.7071L9.35355 12.3536L9 12ZM9.5 3C9.5 2.72386 9.27614 2.5 9 2.5C8.72386 2.5 8.5 2.72386 8.5 3L9.5 3ZM4.14645 7.85355L8.64645 12.3536L9.35355 11.6464L4.85355 7.14645L4.14645 7.85355ZM9.35355 12.3536L13.8536 7.85355L13.1464 7.14645L8.64645 11.6464L9.35355 12.3536ZM9.5 12L9.5 3L8.5 3L8.5 12L9.5 12Z" fill="white"/>
                    <path d="M3.75 15.75H14.25" stroke="white"/>
                </svg>
                {{ __('Download Invoice') }}
            </a>

            <div class="data-info">
                <div class="data-info-wrapper name">
                    <p class="title">Name</p>
                    <p class="value">Mr. {{ $invoice["Initials"]." ".$invoice["SurName"] }}</p>
                </div>
                <div class="data-info-wrapper email">
                    <p class="title">E-mail</p>
                    <p class="value">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M4.91807 7L11.8719 12.5978L18.8257 7H4.91807ZM4 7.73193V17.3679H19.744V7.73193L12.232 13.7799L12.2321 13.7801C12.1299 13.8618 12.003 13.9064 11.8721 13.9064C11.7413 13.9064 11.6143 13.8618 11.5121 13.7801L4 7.73193Z" fill="#0E0E0E"/>
                        </svg>
                        {{ $invoice['EmailAddress'] }}
                    </p>
                </div>
                <div class="data-info-wrapper address">
                    <p class="title">Address</p>
                    <p class="value">{{ $invoice['Address'] }}</p>
                </div>
                <div class="data-info-wrapper inv-num">
                    <p class="title">Invoice no.</p>
                    <p class="value">{{ $invoice['InvoiceCode'] }}</p>
                </div>
                <div class="data-info-wrapper client-num">
                    <p class="title">Client no. </p>
                    <p class="value">{{ $invoice['DebtorCode'] }}</p>
                </div>
                <div class="data-info-wrapper inv-date">
                    <p class="title">Invoice date</p>
                    <p class="value">{{ $invoice['Created'] }}</p>
                    {{-- date format want 01/01/2022 --}}
                </div>
                <div class="data-info-wrapper status">
                    <p class="title">Status</p>
                    <div class="value">
                        @if((int) $invoice['Status'] == 2)
                            <div class="label label-deactive">
                                Unpaid
                            </div>
                        @else
                            <div class="label label-active">
                                {{ $invoiceStatus[ (int) $invoice['Status'] ]  }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col table-col">
                <table class="users-table-content table-content">
                    <thead>
                        <th>{{ __('Date') }}</th>
                        <th>{{ __('Qty') }}</th>
                        <th>{{ __('Product') }}</th>
                        <th>{{ __('Description') }}</th>
                        <th>{{ __('Tax') }}</th>
                        <th class="end">{{ __('Price per unit') }}</th>
                        <th class="end">{{ __('Amount excl. tax') }}</th>
                    </thead>
                    <tbody>
                        @if(isset($invoice['InvoiceLines']))
                            @foreach($invoice['InvoiceLines'] as $lineItem)
                                <tr>
                                    <td>{{ $lineItem["Date"] }}</td>
                                    <td>{{ $lineItem["Number"] }}</td>
                                    <td>
                                        @if($lineItem["ProductCode"] == "")
                                            -
                                        @else
                                            {{ $lineItem["ProductCode"] }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $lineItem["Description"] != "" ? $lineItem["Description"] : "-" }}
                                    </td>
                                    <td>{{ $lineItem["TaxPercentage"] }}%</td>
                                    <td class="end">€ {{ (int) $lineItem["PriceExcl"] }}</td>
                                    <td class="end">€ {{ $lineItem["PriceExcl"] * $lineItem["Number"]}}</td>
                                </tr>
                            @endforeach
                        @else
                        <tr>
                            <td colspan="7">{{ __('There is no line item int this invoices') }}</td>
                        </tr>
                        @endif
                        {{-- <tr>
                            <td>01/01/2022</td>
                            <td>1</td>
                            <td>P015</td>
                            <td>
                                Managed WordPress Hosting Start || 01/10/2022 - 01/01/2023  (3 months )
                            </td>
                            <td>21%</td>
                            <td class="end">€ 9,95</td>
                            <td class="end">€ 29,85</td>
                        </tr> --}}
                    </tbody>
                </table>
            </div>

            <div class="total">
                <div class="total-item subtotal">
                    <div class="text">Subtotal</div>
                    <div class="value">€ {{ $invoice['AmountExcl'] }}</div>
                </div>
                @foreach($taxes as $key => $tax)
                    <div class="total-item tax">
                        <div class="text">{{ $tax["name"] }}</div>
                        <div class="value">€ {{ number_format($tax["value"], 2) }}</div>
                    </div>
                @endforeach
                <div class="total-item amount">
                    <div class="text">Amount incl. tax</div>
                    <div class="value">€ {{ $invoice['AmountIncl'] }}</div>
                </div>
            </div>
        </div>
    </div>
@endsection

