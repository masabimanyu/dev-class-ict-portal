@extends('layouts.app')
@section('content')
    <div class="invoice-overview">
        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Invoices Overview') }}</h1>
                <p>{{ __('Overview / Invoices') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row table-action">
            <div class="col title-col all-invoice">
                <h2 class="title-table">{{ __('All invoices') }}</h2>
                <p class="count total">
                    @if(isset($invoices["invoices"]))
                        {{ $invoices['totalresults'] }}
                    @else
                        0
                    @endif
                </p>
            </div>
            <div class="col action-col">
                <div class="search-wrapper">
                    <input type="text" placeholder="Search Invoice" class="search-client" id="searchInvoiceBtn">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="11" cy="11" r="7" stroke="#33363F" stroke-width="1.5"/>
                        <path d="M20 20L17 17" stroke="#33363F" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                </div>
            </div>
        </div>
        <div class="row row-content container-card-1">
            <div class="col">
                <div class="table-filter select-status">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 12L5 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M19 20L19 18" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M5 20L5 16" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M19 12L19 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M12 7L12 4" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <path d="M12 20L12 12" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="5" cy="14" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="12" cy="9" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    <circle cx="19" cy="15" r="2" stroke="#951D81" stroke-width="1.5" stroke-linecap="round"/>
                    </svg>
                    <select class="filter-dropdown" name="status" id="selectInvoiceBtn">
                        <option value="">{{ __('All status') }}</option>
                        <option value="0"> {{ __('Draft Invoices') }} </option>
                        <option value="2"> {{ __('Unpaid') }} </option>
                        <option value="4"> {{ __('Paid') }} </option>
                    </select>
                </div>
            </div>
            <div class="col table-col">
                <table class="invoices-table-content table-content">
                    <thead>
                        <th>{{ __('ID Invoice') }}</th>
                        <th>{{ __('Date') }}</th>
                        <th>{{ __('Amount') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @if(isset($invoices["invoices"]))
                            @php
                                $invoiceStatus = [
                                    0 => "Draft invoice",
                                    2 => "Unpaid", //Sent
                                    3 => "Paid in part",
                                    4 => "Paid",
                                    8 => "Credit invoice",
                                    9 => "Cancelled"
                                ];
                            @endphp
                            @foreach ($invoices['invoices'] as $key => $invoice)
                                <tr>
                                    <td>{{ $invoice['InvoiceCode'] }}</td>
                                    <td>{{ $invoice['Date'] }}</td>
                                    <td>&euro;{{ $invoice['AmountIncl'] }}</td>
                                    <td>
                                        @if((int) $invoice['Status'] == 2)
                                            <div class="label label-deactive">
                                                Unpaid
                                            </div>
                                        @else
                                            <div class="label label-active">
                                                {{ $invoiceStatus[ (int) $invoice['Status'] ]  }}
                                            </div>
                                        @endif
                                        {{-- <span>{{ $invoiceStatus[ (int) $invoice['Status'] ]  }}</span></td> --}}
                                    <td>
                                        <a href="{{ route('invoice.show', $invoice['InvoiceCode']) }}">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0_1574_40613)">
                                                <path d="M0.666656 8.00033C0.666656 8.00033 3.33332 2.66699 7.99999 2.66699C12.6667 2.66699 15.3333 8.00033 15.3333 8.00033C15.3333 8.00033 12.6667 13.3337 7.99999 13.3337C3.33332 13.3337 0.666656 8.00033 0.666656 8.00033Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                                <path d="M7.99999 10.0003C9.10456 10.0003 9.99999 9.10489 9.99999 8.00033C9.99999 6.89576 9.10456 6.00033 7.99999 6.00033C6.89542 6.00033 5.99999 6.89576 5.99999 8.00033C5.99999 9.10489 6.89542 10.0003 7.99999 10.0003Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                                </g>
                                                <defs>
                                                <clipPath id="clip0_1574_40613">
                                                <rect width="16" height="16" fill="currentColor"/>
                                                </clipPath>
                                                </defs>
                                            </svg>
                                            {{ __('Detail') }}
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">{{ __('there is no invoices') }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
