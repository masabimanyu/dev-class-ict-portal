@extends('layouts.app')
@section('content')
    <div class="container">
        @if($message = Session::get('success') )
            <div class="success">
                {{ $message }}
            </div>
        @endif

        @if($message = Session::get('error'))
            <div class="error">
                {{ $message }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </div>
        @endif

        <form action="{{ route('user.store') }}" method="POST" enctype="application/x-www-form-urlencoded">
            @csrf

            <div class="client-add">
            {{-- Client Information --}}
                <div class="row">
                    <h1>{{ __('pages.client-create.title') }}</h1>
                </div>
                <div class="row content-forms">
                    {{-- Actions column --}}
                    <div class="col col-action">
                        <div class="action-menu">
                            <div class="back-wrapper">
                                <x-back-button redirect="{{ route('user.index') }}" text="{{ __('back') }}"/>
                                {{-- <a class="back btn btn--primary--outline" href="">
                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M13.4666 14.6022L8.64209 9.99989L13.4666 5.39754C13.6421 5.22386 13.7444 5.00677 13.7444 4.76073C13.7444 4.5147 13.6567 4.28313 13.4813 4.10946C13.3058 3.93578 13.0865 3.83447 12.838 3.83447L12.8234 3.83447C12.5895 3.83447 12.3555 3.92131 12.1801 4.09498L6.6684 9.34861C6.49296 9.52228 6.39062 9.75385 6.39062 9.99989C6.39062 10.2459 6.49296 10.492 6.6684 10.6512L12.1947 15.9048C12.3702 16.064 12.5895 16.1653 12.838 16.1653C13.0865 16.1653 13.3204 16.064 13.4959 15.8903C13.6713 15.7166 13.759 15.4851 13.759 15.239C13.759 14.993 13.6567 14.7614 13.4666 14.6022Z" fill="currentColor"/>
                                    </svg>
                                    {{ __('pages.client-create.back') }}
                                </a> --}}
                            </div>
                            <div data-target="client-information" class="menu-item move-effect btn btn--primary--transparent active">
                                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.08301 13.0002L0.412187 12.6648C0.306615 12.8759 0.306615 13.1244 0.412187 13.3356L1.08301 13.0002ZM24.9163 13.0002L25.5872 13.3356C25.6927 13.1244 25.6927 12.8759 25.5872 12.6648L24.9163 13.0002ZM1.08301 13.0002C1.75383 13.3356 1.75373 13.3358 1.75365 13.3359C1.75364 13.3359 1.75358 13.3361 1.75357 13.3361C1.75354 13.3361 1.75359 13.3361 1.7537 13.3358C1.75391 13.3354 1.75439 13.3345 1.75513 13.333C1.75661 13.3301 1.75913 13.3251 1.76268 13.3182C1.7698 13.3044 1.78106 13.2827 1.79643 13.2536C1.82719 13.1955 1.87437 13.1081 1.9377 12.9955C2.06442 12.7702 2.25546 12.4449 2.50861 12.0529C3.01583 11.2675 3.76747 10.2222 4.74474 9.17978C6.71298 7.08033 9.50764 5.0835 12.9997 5.0835V3.5835C8.90838 3.5835 5.7447 5.91999 3.65044 8.15387C2.59647 9.27811 1.79081 10.3995 1.24855 11.2391C0.976958 11.6596 0.770216 12.0114 0.630335 12.2601C0.560366 12.3845 0.507042 12.4832 0.470616 12.552C0.452401 12.5865 0.438404 12.6134 0.428657 12.6324C0.423784 12.6419 0.419972 12.6493 0.417226 12.6548C0.415853 12.6575 0.414746 12.6597 0.413906 12.6613C0.413486 12.6622 0.413133 12.6629 0.412846 12.6634C0.412703 12.6637 0.412538 12.6641 0.412467 12.6642C0.412319 12.6645 0.412187 12.6648 1.08301 13.0002ZM12.9997 5.0835C16.4917 5.0835 19.2864 7.08033 21.2546 9.17978C22.2319 10.2222 22.9835 11.2675 23.4907 12.0529C23.7439 12.4449 23.9349 12.7702 24.0616 12.9955C24.125 13.1081 24.1722 13.1955 24.2029 13.2536C24.2183 13.2827 24.2296 13.3044 24.2367 13.3182C24.2402 13.3251 24.2427 13.3301 24.2442 13.333C24.245 13.3345 24.2454 13.3354 24.2457 13.3358C24.2458 13.3361 24.2458 13.3361 24.2458 13.3361C24.2458 13.3361 24.2457 13.3359 24.2457 13.3359C24.2456 13.3358 24.2455 13.3356 24.9163 13.0002C25.5872 12.6648 25.587 12.6645 25.5869 12.6642C25.5868 12.6641 25.5866 12.6637 25.5865 12.6634C25.5862 12.6629 25.5859 12.6622 25.5854 12.6613C25.5846 12.6597 25.5835 12.6575 25.5821 12.6548C25.5794 12.6493 25.5756 12.6419 25.5707 12.6324C25.5609 12.6134 25.5469 12.5865 25.5287 12.552C25.4923 12.4832 25.439 12.3845 25.369 12.2601C25.2291 12.0114 25.0224 11.6596 24.7508 11.2391C24.2085 10.3995 23.4029 9.27811 22.3489 8.15387C20.2546 5.91999 17.091 3.5835 12.9997 3.5835V5.0835ZM24.9163 13.0002C24.2455 12.6648 24.2456 12.6646 24.2457 12.6644C24.2457 12.6644 24.2458 12.6642 24.2458 12.6642C24.2458 12.6642 24.2458 12.6643 24.2457 12.6645C24.2454 12.6649 24.245 12.6659 24.2442 12.6673C24.2427 12.6702 24.2402 12.6752 24.2367 12.6821C24.2296 12.6959 24.2183 12.7176 24.2029 12.7467C24.1722 12.8048 24.125 12.8923 24.0616 13.0049C23.9349 13.2301 23.7439 13.5555 23.4907 13.9474C22.9835 14.7328 22.2319 15.7781 21.2546 16.8205C19.2864 18.92 16.4917 20.9168 12.9997 20.9168V22.4168C17.091 22.4168 20.2546 20.0803 22.3489 17.8465C23.4029 16.7222 24.2085 15.6008 24.7508 14.7612C25.0224 14.3407 25.2291 13.9889 25.369 13.7403C25.439 13.6159 25.4923 13.5171 25.5287 13.4483C25.5469 13.4139 25.5609 13.3869 25.5707 13.3679C25.5756 13.3585 25.5794 13.351 25.5821 13.3456C25.5835 13.3429 25.5846 13.3407 25.5854 13.339C25.5859 13.3382 25.5862 13.3375 25.5865 13.3369C25.5866 13.3366 25.5868 13.3363 25.5869 13.3361C25.587 13.3358 25.5872 13.3356 24.9163 13.0002ZM12.9997 20.9168C9.50764 20.9168 6.71298 18.92 4.74474 16.8205C3.76747 15.7781 3.01583 14.7328 2.50861 13.9474C2.25546 13.5555 2.06442 13.2301 1.9377 13.0049C1.87437 12.8923 1.82719 12.8048 1.79643 12.7467C1.78106 12.7176 1.7698 12.6959 1.76268 12.6821C1.75913 12.6752 1.75661 12.6702 1.75513 12.6673C1.75439 12.6659 1.75391 12.6649 1.7537 12.6645C1.75359 12.6643 1.75354 12.6642 1.75357 12.6642C1.75358 12.6642 1.75364 12.6644 1.75365 12.6644C1.75373 12.6646 1.75383 12.6648 1.08301 13.0002C0.412187 13.3356 0.412319 13.3358 0.412467 13.3361C0.412538 13.3363 0.412703 13.3366 0.412846 13.3369C0.413133 13.3375 0.413486 13.3382 0.413906 13.339C0.414746 13.3407 0.415853 13.3429 0.417226 13.3456C0.419972 13.351 0.423784 13.3585 0.428657 13.3679C0.438404 13.3869 0.452401 13.4139 0.470616 13.4483C0.507042 13.5171 0.560366 13.6159 0.630335 13.7403C0.770216 13.9889 0.976958 14.3407 1.24855 14.7612C1.79081 15.6008 2.59647 16.7222 3.65044 17.8465C5.7447 20.0803 8.90838 22.4168 12.9997 22.4168V20.9168ZM15.4997 13.0002C15.4997 14.3809 14.3804 15.5002 12.9997 15.5002V17.0002C15.2088 17.0002 16.9997 15.2093 16.9997 13.0002H15.4997ZM12.9997 15.5002C11.619 15.5002 10.4997 14.3809 10.4997 13.0002H8.99967C8.99967 15.2093 10.7905 17.0002 12.9997 17.0002V15.5002ZM10.4997 13.0002C10.4997 11.6195 11.619 10.5002 12.9997 10.5002V9.00016C10.7905 9.00016 8.99967 10.791 8.99967 13.0002H10.4997ZM12.9997 10.5002C14.3804 10.5002 15.4997 11.6195 15.4997 13.0002H16.9997C16.9997 10.791 15.2088 9.00016 12.9997 9.00016V10.5002Z" fill="currentColor"/>
                                </svg>
                                {{ __('pages.client-create.information-clients') }}
                            </div>
                            <div data-target="menu-bank-and-direct" class="menu-item move-effect btn btn--primary--transparent">
                                <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.70858 11.3752C5.7059 10.4158 5.89254 9.46538 6.25781 8.57828C6.62308 7.69119 7.1598 6.88489 7.83722 6.20557C8.51464 5.52626 9.31944 4.98729 10.2055 4.61954C11.0916 4.2518 12.0415 4.0625 13.0009 4.0625C13.9602 4.0625 14.9101 4.2518 15.7962 4.61954C16.6823 4.98729 17.4871 5.52626 18.1645 6.20557C18.8419 6.88489 19.3786 7.69119 19.7439 8.57828C20.1092 9.46538 20.2958 10.4158 20.2931 11.3752V11.3752C20.2931 15.0127 21.0542 17.1235 21.7245 18.2772C21.7967 18.4004 21.8351 18.5406 21.8359 18.6834C21.8366 18.8263 21.7997 18.9668 21.7288 19.0909C21.6579 19.2149 21.5556 19.3181 21.4321 19.3899C21.3086 19.4618 21.1684 19.4998 21.0255 19.5002H4.97536C4.83248 19.4998 4.69222 19.4618 4.56874 19.3899C4.44526 19.318 4.34292 19.2148 4.27204 19.0908C4.20117 18.9667 4.16426 18.8262 4.16505 18.6833C4.16584 18.5404 4.2043 18.4003 4.27654 18.277C4.94718 17.1233 5.70857 15.0125 5.70857 11.3752H5.70858Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M9.75 19.5V20.3125C9.75 21.1745 10.0924 22.0011 10.7019 22.6106C11.3114 23.2201 12.138 23.5625 13 23.5625C13.862 23.5625 14.6886 23.2201 15.2981 22.6106C15.9076 22.0011 16.25 21.1745 16.25 20.3125V19.5" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M18.6299 2.43652C20.2832 3.48024 21.6159 4.9605 22.481 6.71389" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M3.51953 6.71389C4.38459 4.9605 5.71734 3.48024 7.37063 2.43652" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                {{ __('pages.client-create.debit-clients') }}
                            </div>
                        </div>
                    </div>
                    {{-- Actions column END--}}

                    {{-- Client information Forms --}}
                    <div class="col js-col-forms col-forms active" id="client-information">

                        {{-- Client Information --}}
                        <div class="container-card-1 client-information-container">
                            <h2 class="container-title">{{ __('pages.client-create.information-clients') }}</h2>
                            <div class="input-container form">
                                <div class="form-group w-1/2">
                                    <label for="#company_name">{{ __('pages.client-create.company-clients') }}</label>
                                    <input type="text" name="company_name" id="company_name" value="{{ old('company_name') }}" >
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#company_legal_form">{{ __('pages.client-create.legal-form') }}</label>
                                    <div class="select-wrapper">
                                        <select name="company_legal_form" id="company_legal_form">
                                            <option value="">{{ __('pages.client-create.choose-here') }}</option>
                                            @if(count($legalForms) > 0)
                                                @foreach ($legalForms as $legalForm)
                                                    <option value='{{ $legalForm->legal_form_code }}' {{ old('company_legal_form') == $legalForm->legal_form_code ? 'selected' : ''}}>{{ $legalForm->legal_form_name }}</option>
                                                @endforeach
                                            @else
                                                <option value="">{{ __('No legal form Available') }}</option>
                                            @endif
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#chamber_of_commerce">{{ __('Chamber of commerce number') }}</label>
                                    <input type="text" name="chamber_of_commerce" id="chamber_of_commerce" value="{{ old('chamber_of_commerce') }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#vat_number">{{ __('Vat number') }}</label>
                                    <input type="text" name="vat_number" id="vat_number" value="{{ old('vat_number') }}">
                                </div>
                                <div class="form-group w-1/3">
                                    <label for="#user_legal_form">{{ __('Legal form') }}</label>
                                    <div class="select-wrapper">
                                        <select name="user_legal_form" id="user_legal_form">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="m" {{ old('user_legal_form') == 'm' ? 'selected' : ''}} >Dhr.</option>
                                            <option value="f" {{ old('user_legal_form') == 'f' ? 'selected' : ''}} >Mrs.</option>
                                            <option value="d" {{ old('user_legal_form') == 'd' ? 'selected' : ''}} >Afd.</option>
                                            <option value="u" {{ old('user_legal_form') == 'u' ? 'selected' : ''}} >Unknown</option>
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="form-group w-1/3">
                                    <label for="#first_name">{{ __('First name') }}</label>
                                    <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}">
                                </div>
                                <div class="form-group w-1/3">
                                    <label for="#last_name">{{ __('Last name') }}</label>
                                    <input type="text" name="last_name" id="last_name" value="{{ old("last_name") }}" >
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#address">{{ __('Address') }}</label>
                                    <input type="text" name="address" id="address" value="{{ old("address") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#postcode">{{ __('Postcode') }}</label>
                                    <input type="text" name="postcode" id="postcode" value="{{ old("postcode") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#place">{{ __('Place') }}</label>
                                    <input type="text" name="place" id="place" value="{{ old("place") }}" >
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#land">{{ __('Land') }}</label>
                                    <div class="select-wrapper">
                                        <select name="land" id="land">
                                            <option value="">{{ __('Choose here') }}</option>
                                            @if(count($countries) > 0)
                                                @foreach ($countries as $country)
                                                    <option value='{{ $country->sortname }}'  {{ old('land') == $country->sortname ? 'selected' : ''}}>{{ $country->name }}</option>
                                                @endforeach
                                            @else
                                                <option value="">{{ __('No Country Available') }}</option>
                                            @endif
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Client Information END --}}

                        {{-- Contact Information --}}
                        <div class="container-card-1 contact-information-container">
                            <h2>{{ __('Contact information') }}</h2>
                            <div class="input-container form">
                                <div class="form-group w-1/2">
                                    <label for="#email">{{ __('E-mail address') }}</label>
                                    <input type="email" name="email" id="email" value="{{ old("email") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#phone_number">{{ __('Phone number') }}</label>
                                    <input type="text" name="phone_number" id="phone_number" value="{{ old("phone_number") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#mobile_number">{{ __('Mobile number') }}</label>
                                    <input type="text" name="mobile_number" id="mobile_number" value="{{ old("mobile_number") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#fax_number">{{ __('Fax number') }}</label>
                                    <input type="text" name="fax_number" id="fax_number" value="{{ old("fax_number") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#website">{{ __('Website') }}</label>
                                    <input type="text" name="website" id="website" value="{{ old("website") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#is_mailing">{{ __('Receive mailing') }}</label>
                                    <div class="select-wrapper">
                                        <select name="is_mailing" id="is_mailing">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="0" {{ old('is_mailing') == '0' ? 'selected' : ''}} >{{ __('Well received') }}</option>
                                            <option value="1" {{ old('is_mailing') == '1' ? 'selected' : ''}} >{{ __('Not received') }}</option>
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Contact Information END --}}

                        {{-- Invoicing and payment --}}
                        <div class="container-card-1 invoicing-and-payment-container">
                            <h2>{{ __('Invoicing and payment') }}</h2>
                            <div class="input-container form">
                                <div class="form-group checkbox">
                                    <label for="#alt_invoicing_and_payment">{{ __('Alternate Invoice Address') }}</label>
                                    <input type="checkbox" name="alt_invoicing_and_payment" id="alt_invoicing_and_payment" {{ old('alt_invoicing_and_payment') == 'on' ? 'checked' : ''}}>
                                </div>
                            </div>
                        </div>
                        {{-- Invoicing and payment END --}}

                        {{-- Client Area --}}
                        <div class="container-card-1 client-area-container">
                            <h2>{{ __('Client Area') }}</h2>
                            <div class="input-container form">
                                <div class="form-group checkbox">
                                    <label for="#is_login_and_order_form">{{ __('Activate login for client area and order form') }}</label>
                                    <input type="checkbox" name="is_login_and_order_form" id="is_login_and_order_form" {{ old('is_login_and_order_form') == 'on' ? 'checked' : ''}}>
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#username">{{ __('Username') }}</label>
                                    <input type="text" name="username" id="username" value="{{ old("username") }}">
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#temporary_password">{{ __('Temporary password') }}</label>
                                    <input type="text" name="temporary_password" id="temporary_password" value="{{ old("username") }}"  >
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#language">{{ __('Language') }}</label>
                                    <div class="select-wrapper">
                                        <select name="language" id="language">
                                            <option value="">{{ __('Choose here') }}</option>
                                            @if(count($languageCodes) > 0)
                                                @foreach ($languageCodes as $languageCode)
                                                    <option value='{{ $languageCode['LanguageCode'] }}' {{ old('alt_invoicing_and_payment') == $languageCode['LanguageCode'] ? 'selected' : ''}} >{{ $languageCode['Name'] }}</option>
                                                @endforeach
                                            @else
                                                <option value="">{{ __('No Country Available') }}</option>
                                            @endif
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#language">{{ __('Profile') }}</label>
                                    <div class="select-wrapper">
                                        <select name="profile" id="profile">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="true" {{ old('profile') == 'true' ? 'selected' : ''}}>Yes</option>
                                            <option value="false" {{ old('profile') == 'false' ? 'selected' : ''}} >No</option>
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#send_welcome_page">{{ __('Send welcome message') }}</label>
                                    <div class="select-wrapper">
                                        <select name="send_welcome_page" id="send_welcome_page">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="true" {{ old('send_welcome_page') == 'true' ? 'selected' : ''}} >Send</option>
                                            <option value="false" {{ old('send_welcome_page') == 'false' ? 'selected' : ''}} >Do not send</option>
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Client Area END --}}

                        {{-- Report Area --}}
                        <div class="container-card-1 client-area-container">
                            <h2>{{ __('Report Area') }}</h2>
                            <div class="input-container form">
                                <div class="form-group w-1/2">
                                    <label for="#processing_agreement">{{ __('Verwerkersovereenkomst') }}</label>
                                    <div class="select-wrapper">
                                        <input name="processing_agreement" id="processing_agreement" value={{ old('processing_agreement') }}>
                                        {{-- <select name="processing_agreement" id="processing_agreement">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="true">Yes</option>
                                            <option value="false">No</option>
                                        </select> --}}
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#term_and_condition">{{ __('Algemene Voorwaarden') }}</label>
                                    <div class="select-wrapper">
                                        <input name="term_and_condition" id="term_and_condition" value="{{ old('term_and_condition') }}" >
                                        {{-- <select name="term_and_condition" id="term_and_condition">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="true">Yes</option>
                                            <option value="false">No</option>
                                        </select> --}}
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="form-group w-1/2">
                                    <label for="#privacy_policy">{{ __('Privacy Policy') }}</label>
                                    <div class="select-wrapper">
                                        <input name="privacy_policy" id="privacy_policy" value="{{ old('privacy_policy') }}">
                                        {{-- <select name="privacy_policy" id="privacy_policy">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="true">Send</option>
                                            <option value="false">Do not send</option>
                                        </select> --}}
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- Report Area END --}}
                    </div>
                    {{-- Client information Forms END --}}

                    {{-- Bank and direct debit Forms --}}
                    <div class="col js-col-forms col-forms" id="menu-bank-and-direct">
                        {{-- Bank Account Information --}}
                        <div class="container-card-1 bank-account-information-container">
                            <h2>{{ __('Bank Account Information') }}</h2>
                            <div class="input-container form">
                                <div class="form-group">
                                    <label for="#bank_account_number">{{ __('Bank account number') }}</label>
                                    <input type="number" name="bank_account_number" id="bank_account_number" value={{ old('bank_account_number') }}>
                                </div>
                                <div class="form-group">
                                    <label for="#account_holder">{{ __('Account Holder') }}</label>
                                    <input type="text" name="account_holder" id="account_holder" value={{ old('bank_account_number') }}>
                                </div>
                                <div class="form-group">
                                    <label for="#bank">{{ __('Bank') }}</label>
                                    <input type="text" name="bank" id="bank" value={{ old('bank_account_number') }}>
                                </div>
                                <div class="form-group">
                                    <label for="#bank_city">{{ __('Land') }}</label>
                                    <input type="text" name="bank_city" id="bank_city" value={{ old('bank_account_number') }}>
                                    {{-- <div class="select-wrapper">
                                        <select name="bank_city" id="bank_city">
                                            <option value="">{{ __('Choose here') }}</option>
                                            <option value="NL">Nederland</option>
                                            <option value="BE">Belgie</option>
                                            <option value="BG">Bulgarije</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="DK">Denemarken</option>
                                            <option value="THE">Duitsland</option>
                                        </select>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6 9L12 15L18 9" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                        </svg>
                                    </div> --}}
                                </div>
                                <div class="form-group">
                                    <label for="#bank_code">{{ __('Bank Code (BIC)') }}</label>
                                    <input type="text" name="bank_code" id="bank_code" value="{{ old('bank_code') }}" >
                                </div>
                            </div>
                        </div>
                        {{-- Bank Account Information END --}}

                        {{-- Direct Debit --}}
                        <div class="container-card-1 direct-debit">
                            <h2>{{ __('Direct Debit') }}</h2>
                            <div class="input-container form">
                                <div class="form-group checkbox">
                                    <label for="#direct_debit">{{ __('This client wants to pay by direct debit') }}</label>
                                    <input type="checkbox" name="direct_debit" id="direct_debit" {{ old('direct_debit') == 'on' ? 'checked' : ''}}>
                                </div>
                                <div class="form-group">
                                    <label for="#mandate_reference">{{ __('Mandate reference') }}</label>
                                    <input type="text" name="mandate_reference" id="mandate_reference" value={{ old('mandate_reference') }}>
                                </div>
                                <div class="form-group">
                                    <label for="#mandate_sign_date">{{ __('Mandate sign date') }}</label>
                                    <input type="date" name="mandate_sign_date" id="mandate_sign_date" value={{ old('mandate_sign_date') }}>
                                </div>
                            </div>
                        </div>
                        {{-- Direct Debit END --}}

                    </div>
                    {{-- Bank and direct debit Forms END --}}

                </div>
                <div class="row row-submit">
                    <div class="col col-submit">
                        {{-- Save Button --}}
                        <div class="button-container">
                            <button type="submit" class="btn btn--primary">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
