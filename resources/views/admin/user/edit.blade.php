@extends('layouts.app')
@section('content')
    <div class="container client-edit">
        @if(Session::get('success') )
            <div class="success">
                {{ Session::get('success') }}
            </div>
        @endif

        @if(Session::get('error'))
            <div class="error">
                {{ $message }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                @endforeach
            </div>
        @endif

        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Clients - Fotoccasion') }}</h1>
                <p>{{ __('Overview / Client - Fotoccasion') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}

        <form action="{{ route('user.update', $user['DebtorCode']) }}" method="POST" enctype="application/x-www-form-urlencoded">
            @csrf
            @method('PUT')
            <div class="row">

                {{-- Action menu column --}}
                <div class="col col-action">
                    <div class="action-menu">
                        <div class="back-wrapper">
                            <x-back-button redirect="{{ route('user.index') }}" text="{{ __('back') }}"/>
                        </div>
                        <div data-target="client-information" class="menu-item move-effect btn btn--primary--transparent active">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.08301 13.0002L0.412187 12.6648C0.306615 12.8759 0.306615 13.1244 0.412187 13.3356L1.08301 13.0002ZM24.9163 13.0002L25.5872 13.3356C25.6927 13.1244 25.6927 12.8759 25.5872 12.6648L24.9163 13.0002ZM1.08301 13.0002C1.75383 13.3356 1.75373 13.3358 1.75365 13.3359C1.75364 13.3359 1.75358 13.3361 1.75357 13.3361C1.75354 13.3361 1.75359 13.3361 1.7537 13.3358C1.75391 13.3354 1.75439 13.3345 1.75513 13.333C1.75661 13.3301 1.75913 13.3251 1.76268 13.3182C1.7698 13.3044 1.78106 13.2827 1.79643 13.2536C1.82719 13.1955 1.87437 13.1081 1.9377 12.9955C2.06442 12.7702 2.25546 12.4449 2.50861 12.0529C3.01583 11.2675 3.76747 10.2222 4.74474 9.17978C6.71298 7.08033 9.50764 5.0835 12.9997 5.0835V3.5835C8.90838 3.5835 5.7447 5.91999 3.65044 8.15387C2.59647 9.27811 1.79081 10.3995 1.24855 11.2391C0.976958 11.6596 0.770216 12.0114 0.630335 12.2601C0.560366 12.3845 0.507042 12.4832 0.470616 12.552C0.452401 12.5865 0.438404 12.6134 0.428657 12.6324C0.423784 12.6419 0.419972 12.6493 0.417226 12.6548C0.415853 12.6575 0.414746 12.6597 0.413906 12.6613C0.413486 12.6622 0.413133 12.6629 0.412846 12.6634C0.412703 12.6637 0.412538 12.6641 0.412467 12.6642C0.412319 12.6645 0.412187 12.6648 1.08301 13.0002ZM12.9997 5.0835C16.4917 5.0835 19.2864 7.08033 21.2546 9.17978C22.2319 10.2222 22.9835 11.2675 23.4907 12.0529C23.7439 12.4449 23.9349 12.7702 24.0616 12.9955C24.125 13.1081 24.1722 13.1955 24.2029 13.2536C24.2183 13.2827 24.2296 13.3044 24.2367 13.3182C24.2402 13.3251 24.2427 13.3301 24.2442 13.333C24.245 13.3345 24.2454 13.3354 24.2457 13.3358C24.2458 13.3361 24.2458 13.3361 24.2458 13.3361C24.2458 13.3361 24.2457 13.3359 24.2457 13.3359C24.2456 13.3358 24.2455 13.3356 24.9163 13.0002C25.5872 12.6648 25.587 12.6645 25.5869 12.6642C25.5868 12.6641 25.5866 12.6637 25.5865 12.6634C25.5862 12.6629 25.5859 12.6622 25.5854 12.6613C25.5846 12.6597 25.5835 12.6575 25.5821 12.6548C25.5794 12.6493 25.5756 12.6419 25.5707 12.6324C25.5609 12.6134 25.5469 12.5865 25.5287 12.552C25.4923 12.4832 25.439 12.3845 25.369 12.2601C25.2291 12.0114 25.0224 11.6596 24.7508 11.2391C24.2085 10.3995 23.4029 9.27811 22.3489 8.15387C20.2546 5.91999 17.091 3.5835 12.9997 3.5835V5.0835ZM24.9163 13.0002C24.2455 12.6648 24.2456 12.6646 24.2457 12.6644C24.2457 12.6644 24.2458 12.6642 24.2458 12.6642C24.2458 12.6642 24.2458 12.6643 24.2457 12.6645C24.2454 12.6649 24.245 12.6659 24.2442 12.6673C24.2427 12.6702 24.2402 12.6752 24.2367 12.6821C24.2296 12.6959 24.2183 12.7176 24.2029 12.7467C24.1722 12.8048 24.125 12.8923 24.0616 13.0049C23.9349 13.2301 23.7439 13.5555 23.4907 13.9474C22.9835 14.7328 22.2319 15.7781 21.2546 16.8205C19.2864 18.92 16.4917 20.9168 12.9997 20.9168V22.4168C17.091 22.4168 20.2546 20.0803 22.3489 17.8465C23.4029 16.7222 24.2085 15.6008 24.7508 14.7612C25.0224 14.3407 25.2291 13.9889 25.369 13.7403C25.439 13.6159 25.4923 13.5171 25.5287 13.4483C25.5469 13.4139 25.5609 13.3869 25.5707 13.3679C25.5756 13.3585 25.5794 13.351 25.5821 13.3456C25.5835 13.3429 25.5846 13.3407 25.5854 13.339C25.5859 13.3382 25.5862 13.3375 25.5865 13.3369C25.5866 13.3366 25.5868 13.3363 25.5869 13.3361C25.587 13.3358 25.5872 13.3356 24.9163 13.0002ZM12.9997 20.9168C9.50764 20.9168 6.71298 18.92 4.74474 16.8205C3.76747 15.7781 3.01583 14.7328 2.50861 13.9474C2.25546 13.5555 2.06442 13.2301 1.9377 13.0049C1.87437 12.8923 1.82719 12.8048 1.79643 12.7467C1.78106 12.7176 1.7698 12.6959 1.76268 12.6821C1.75913 12.6752 1.75661 12.6702 1.75513 12.6673C1.75439 12.6659 1.75391 12.6649 1.7537 12.6645C1.75359 12.6643 1.75354 12.6642 1.75357 12.6642C1.75358 12.6642 1.75364 12.6644 1.75365 12.6644C1.75373 12.6646 1.75383 12.6648 1.08301 13.0002C0.412187 13.3356 0.412319 13.3358 0.412467 13.3361C0.412538 13.3363 0.412703 13.3366 0.412846 13.3369C0.413133 13.3375 0.413486 13.3382 0.413906 13.339C0.414746 13.3407 0.415853 13.3429 0.417226 13.3456C0.419972 13.351 0.423784 13.3585 0.428657 13.3679C0.438404 13.3869 0.452401 13.4139 0.470616 13.4483C0.507042 13.5171 0.560366 13.6159 0.630335 13.7403C0.770216 13.9889 0.976958 14.3407 1.24855 14.7612C1.79081 15.6008 2.59647 16.7222 3.65044 17.8465C5.7447 20.0803 8.90838 22.4168 12.9997 22.4168V20.9168ZM15.4997 13.0002C15.4997 14.3809 14.3804 15.5002 12.9997 15.5002V17.0002C15.2088 17.0002 16.9997 15.2093 16.9997 13.0002H15.4997ZM12.9997 15.5002C11.619 15.5002 10.4997 14.3809 10.4997 13.0002H8.99967C8.99967 15.2093 10.7905 17.0002 12.9997 17.0002V15.5002ZM10.4997 13.0002C10.4997 11.6195 11.619 10.5002 12.9997 10.5002V9.00016C10.7905 9.00016 8.99967 10.791 8.99967 13.0002H10.4997ZM12.9997 10.5002C14.3804 10.5002 15.4997 11.6195 15.4997 13.0002H16.9997C16.9997 10.791 15.2088 9.00016 12.9997 9.00016V10.5002Z" fill="currentColor"/>
                            </svg>
                            {{ __('pages.client-create.information-clients') }}
                        </div>
                        <div data-target="bank-and-direct" class="menu-item move-effect btn btn--primary--transparent">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.70858 11.3752C5.7059 10.4158 5.89254 9.46538 6.25781 8.57828C6.62308 7.69119 7.1598 6.88489 7.83722 6.20557C8.51464 5.52626 9.31944 4.98729 10.2055 4.61954C11.0916 4.2518 12.0415 4.0625 13.0009 4.0625C13.9602 4.0625 14.9101 4.2518 15.7962 4.61954C16.6823 4.98729 17.4871 5.52626 18.1645 6.20557C18.8419 6.88489 19.3786 7.69119 19.7439 8.57828C20.1092 9.46538 20.2958 10.4158 20.2931 11.3752V11.3752C20.2931 15.0127 21.0542 17.1235 21.7245 18.2772C21.7967 18.4004 21.8351 18.5406 21.8359 18.6834C21.8366 18.8263 21.7997 18.9668 21.7288 19.0909C21.6579 19.2149 21.5556 19.3181 21.4321 19.3899C21.3086 19.4618 21.1684 19.4998 21.0255 19.5002H4.97536C4.83248 19.4998 4.69222 19.4618 4.56874 19.3899C4.44526 19.318 4.34292 19.2148 4.27204 19.0908C4.20117 18.9667 4.16426 18.8262 4.16505 18.6833C4.16584 18.5404 4.2043 18.4003 4.27654 18.277C4.94718 17.1233 5.70857 15.0125 5.70857 11.3752H5.70858Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M9.75 19.5V20.3125C9.75 21.1745 10.0924 22.0011 10.7019 22.6106C11.3114 23.2201 12.138 23.5625 13 23.5625C13.862 23.5625 14.6886 23.2201 15.2981 22.6106C15.9076 22.0011 16.25 21.1745 16.25 20.3125V19.5" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M18.6299 2.43652C20.2832 3.48024 21.6159 4.9605 22.481 6.71389" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M3.51953 6.71389C4.38459 4.9605 5.71734 3.48024 7.37063 2.43652" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            {{ __('pages.client-create.debit-clients') }}
                        </div>
                        <div data-target="invoices" class="menu-item move-effect btn btn--primary--transparent">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.75 13.0005H16.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M9.75 16.2505H16.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M5.6875 4.06299H20.3125C20.528 4.06299 20.7347 4.14859 20.887 4.30096C21.0394 4.45334 21.125 4.66 21.125 4.87549V20.313C21.125 20.9595 20.8682 21.5794 20.4111 22.0366C19.954 22.4937 19.334 22.7505 18.6875 22.7505H7.3125C6.66603 22.7505 6.04605 22.4937 5.58893 22.0366C5.13181 21.5794 4.875 20.9595 4.875 20.313V4.87549C4.875 4.66 4.9606 4.45334 5.11298 4.30096C5.26535 4.14859 5.47201 4.06299 5.6875 4.06299Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M8.125 2.43799V5.68799" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M13 2.43799V5.68799" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M17.875 2.43799V5.68799" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            {{ __('Invoices') }}
                        </div>
                        <div data-target="reports" class="menu-item move-effect btn btn--primary--transparent">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.75 7.5835L14.0833 7.5835" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                                <path d="M9.75 16.25L13 16.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                                <path d="M9.75 11.9165L16.25 11.9165" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                                <path d="M20.5834 11.9167V9.25C20.5834 6.42157 20.5834 5.00736 19.7047 4.12868C18.8261 3.25 17.4118 3.25 14.5834 3.25H11.4168C8.58832 3.25 7.17411 3.25 6.29543 4.12868C5.41675 5.00736 5.41675 6.42157 5.41675 9.25V16.75C5.41675 19.5784 5.41675 20.9926 6.29543 21.8713C7.17411 22.75 8.58832 22.75 11.4167 22.75H13.0001" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                                <circle cx="18.9583" cy="18.9583" r="2.70833" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                                <path d="M22.75 22.75L21.125 21.125" stroke="currentColor" stroke-linecap="round"/>
                            </svg>
                            {{ __('Reports') }}
                        </div>
                        <div data-target="services" class="menu-item move-effect btn btn--primary--transparent">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M22.75 18.0091V7.99072C22.75 7.84655 22.7116 7.70498 22.6389 7.58054C22.5661 7.45609 22.4615 7.35325 22.3358 7.28257L13.3983 2.25522C13.2767 2.18681 13.1395 2.15088 13 2.15088C12.8605 2.15088 12.7233 2.18681 12.6017 2.25522L3.66416 7.28257C3.53851 7.35325 3.43393 7.45609 3.36114 7.58054C3.28836 7.70498 3.25 7.84655 3.25 7.99072V18.0091C3.25 18.1533 3.28836 18.2948 3.36114 18.4193C3.43393 18.5437 3.53851 18.6466 3.66416 18.7172L12.6017 23.7446C12.7233 23.813 12.8605 23.8489 13 23.8489C13.1395 23.8489 13.2767 23.813 13.3983 23.7446L22.3358 18.7172C22.4615 18.6466 22.5661 18.5437 22.6389 18.4193C22.7116 18.2948 22.75 18.1533 22.75 18.0091Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M17.9788 15.4894V10.2081L8.125 4.77344" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M22.638 7.57914L13.0964 12.9998L3.36255 7.57812" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M13.0963 13L13.001 23.849" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                            {{ __('Services') }}
                        </div>
                    </div>
                </div>
                {{-- Action menu column END --}}

                <div id="client-information" class="col js-col-forms col-forms active">

                    {{-- Client Information --}}

                    @include('../../components/edit-client-admin/client-information')

                    {{-- Client Information END --}}

                    {{-- Contact Information --}}

                    @include('../../components/edit-client-admin/contact-information')

                    {{-- Contact Information END --}}

                    {{-- Invoicing and payment --}}

                    @include('../../components/edit-client-admin/invoicing-payment')

                    {{-- Invoicing and payment END --}}

                    {{-- Client Area --}}

                    @include('../../components/edit-client-admin/client-area')

                    {{-- Client Area END --}}

                    {{-- Report Area --}}

                    @include('../../components/edit-client-admin/report-area')

                    {{-- Report Area END --}}

                </div>

                <div id="bank-and-direct" class="col js-col-forms col-forms">

                    {{-- Bank Account Information --}}

                    @include('../../components/edit-client-admin/bank-account-information')

                    {{-- Bank Account Information END --}}

                    {{-- Direct Debit --}}

                    @include('../../components/edit-client-admin/direct-debit')

                    {{-- Direct Debit END --}}

                </div>

                <div id="invoices" class="col js-col-forms col-forms">

                    @include('../../components/edit-client-admin/invoices')

                </div>

                <div id="reports" class="col js-col-forms col-forms">

                    @include('../../components/edit-client-admin/reports')

                </div>

                <div id="services" class="col js-col-forms col-forms">

                    @include('../../components/edit-client-admin/service')

                </div>
                {{-- Form column END --}}

                {{-- Company card column --}}

                @include('../../components/edit-client-admin/company-card')

                {{-- Company card column END --}}
            </div>

            <div class="row-submit">
                <div class="col-submit">

                    {{-- Submit Button --}}
                    <div class="button-container">
                        <button type="submit" class="btn btn--primary">{{ __('Save Update') }}</button>
                    </div>
                    {{-- Submit Button END --}}

                </div>
            </div>

        </form>
    </div>
@endsection

