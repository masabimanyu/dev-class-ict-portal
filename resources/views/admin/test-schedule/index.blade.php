@extends('layouts.app')
@section('content')
<div class="col col-search-client">
    <div class="container-card-1">
        <h2>{{ __('Search client') }}</h2>

        <form action="{{ route('invoice.test') }}" method="POST" >
            @csrf
            <div class="input-container form">

                <div class="form-group w-1/2">
                    <div class="select-wrapper">
                        <select name="schedule_date" id="scheduleDate" required>
                            <option value class="d-none">{{ __('Select a date that invoice will be generated') }}</option>
                            @foreach ($invoiceDate as $date)
                                <option value="{{ $date }}"> {{ $date }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group w-1/2">
                    <div class="submit-wrapper">
                        <button class="btn btn--primary" id="scheduleDateButton">Generate Invoice</button>
                    </div>
                </div>

            </div>
        </form>
    </div>

    @if(Session::get('generatedInvoice') !== null )

        @php
            $generatedInvoices = Session::get('generatedInvoice');
        @endphp

        <div class="container-card-1">
            <div class="row">
                <h1 class="title-page">{{ __('Generated Invoice') }}</h1>
            </div>
            <div class="col table-col">
                <table class="users-table-content table-content">
                    <thead>
                        <th>{{ __('ID') }}</th>
                        <th>{{ __('Debtor Code') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Amount Exclude') }}</th>
                        <th>{{ __('Amount Include') }}</th>
                        <th>{{ __('Tax') }}</th>
                        <th>{{ __('Period') }}</th>
                    </thead>
                    <tbody>
                        @if(count($generatedInvoices) > 0)
                            @foreach ($generatedInvoices as $invoice)
                                <tr>
                                    <td> {{ $invoice['invoice']['InvoiceCode'] }} </td>
                                    <td> {{ $invoice['invoice']['DebtorCode'] }} </td>
                                    <td> {{ $invoice['invoice']['Status'] }} </td>
                                    <td> {{ $invoice['invoice']['AmountExcl'] }} </td>
                                    <td> {{ $invoice['invoice']['AmountIncl'] }} </td>
                                    <td> {{ $invoice['invoice']['AmountTax'] }} </td>
                                    <td>
                                        @foreach ($invoice['invoice']['InvoiceLines'] as $invoiceLine)
                                            <span> {{ $invoiceLine['StartDate'] }} - {{ $invoiceLine['EndDate'] }} </span>
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">{{ __('No invoices generated on this date') }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    <div class="container-card-1">
        <div class="row">
            <h1 class="title-page">{{ __('Transaction') }}</h1>
        </div>
        <div class="col table-col">
            <table class="users-table-content table-content">
                <thead>
                    <th>{{ __('ID') }}</th>
                    <th>{{ __('Product') }}</th>
                    <th>{{ __('Price Exclude') }}</th>
                    <th>{{ __('Contract Period Start') }}</th>
                    <th>{{ __('Billing per') }}</th>
                    <th>{{ __('Billing length') }}</th>
                    <th>{{ __('Next Billing Date') }}</th>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $transaction->id }}</td>
                            <td>{{ $transaction->product->product_name }}</td>
                            <td>{{ $transaction->price_exclude }}</td>
                            <td>{{ date('Y-m-d', strtotime($transaction->contractPeriod->contract_period_start)) }}</td>
                            <td>{{ $transaction->period }}</td>
                            <td>{{ $transaction->billing_cycle_length }}</td>
                            <td>{{ $transaction->next_date ? $transaction->next_date : "First time billing,date same as contract period"}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
