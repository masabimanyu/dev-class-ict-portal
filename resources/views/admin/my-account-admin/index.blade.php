@extends('layouts.app')
@section('content')
    <div class="container my-account">

        {{-- Title/Breadcrumb --}}
        <div class="row breadcrumb">
            <div class="breadcrumb-wrapper">
                <h1>{{ __('Clients - Fotoccasion') }}</h1>
                <p>{{ __('Overview / Client - Fotoccasion') }}</p>
            </div>
        </div>
        {{-- Title/Breadcrumb END --}}
        <div class="row">

            {{-- Action menu column --}}
            <div class="col col-action">
                <div class="action-menu">
                    <div class="back-wrapper">
                        <x-back-button redirect="{{ route('user.index') }}" text="{{ __('back') }}"/>
                    </div>
                    <div data-target="general-data" class="menu-item move-effect btn btn--primary--transparent active">
                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M9.75 7.58398L14.0833 7.58398" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M9.75 16.25L13 16.25" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M9.75 11.916L16.25 11.916" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M20.5833 11.9167V9.25C20.5833 6.42157 20.5833 5.00736 19.7047 4.12868C18.826 3.25 17.4118 3.25 14.5833 3.25H11.4167C8.58824 3.25 7.17402 3.25 6.29534 4.12868C5.41666 5.00736 5.41666 6.42157 5.41666 9.25V16.75C5.41666 19.5784 5.41666 20.9926 6.29534 21.8713C7.17402 22.75 8.58824 22.75 11.4167 22.75H13" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                            <circle cx="18.9583" cy="18.9583" r="2.70833" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                            <path d="M22.75 22.75L21.125 21.125" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"/>
                        </svg>
                        {{ __('General data') }}
                    </div>
                    <div data-target="company-information" class="menu-item move-effect btn btn--primary--transparent">
                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.625 21.9355H24.375" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M14.6244 21.9355V4.06055C14.6244 3.84506 14.5388 3.6384 14.3864 3.48602C14.2341 3.33365 14.0274 3.24805 13.8119 3.24805H4.0619C3.84642 3.24805 3.63975 3.33365 3.48738 3.48602C3.33501 3.6384 3.2494 3.84506 3.2494 4.06055V21.9355" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M22.7494 21.9355V10.5605C22.7494 10.3451 22.6638 10.1384 22.5114 9.98602C22.3591 9.83365 22.1524 9.74805 21.9369 9.74805H14.6244" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M6.4994 7.31055H9.7494" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M8.1244 13.8105H11.3744" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M6.4994 17.873H9.7494" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M17.8744 17.873H19.4994" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M17.8744 13.8105H19.4994" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        {{ __('Company information') }}
                    </div>
                    <div data-target="change-password" class="menu-item move-effect btn btn--primary--transparent">
                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.9994 16.25C14.1212 16.25 15.0307 15.3406 15.0307 14.2188C15.0307 13.0969 14.1212 12.1875 12.9994 12.1875C11.8776 12.1875 10.9682 13.0969 10.9682 14.2188C10.9682 15.3406 11.8776 16.25 12.9994 16.25Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M12.9994 16.25V18.6875" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M21.1244 8.9375H4.8744C4.42567 8.9375 4.0619 9.30127 4.0619 9.75V21.125C4.0619 21.5737 4.42567 21.9375 4.8744 21.9375H21.1244C21.5731 21.9375 21.9369 21.5737 21.9369 21.125V9.75C21.9369 9.30127 21.5731 8.9375 21.1244 8.9375Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M9.34315 8.9375V5.28125C9.34315 4.31155 9.72837 3.38157 10.414 2.69589C11.0997 2.01021 12.0297 1.625 12.9994 1.625C13.9691 1.625 14.8991 2.01021 15.5848 2.69589C16.2704 3.38157 16.6557 4.31155 16.6557 5.28125V8.9375" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        {{ __('Change password') }}
                    </div>
                    <div data-target="login-auth" class="menu-item move-effect btn btn--primary--transparent">
                        <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.10804 10.127H3.23304V5.25195" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M6.68023 19.3198C7.93016 20.5697 9.52267 21.4209 11.2564 21.7658C12.9901 22.1106 14.7871 21.9336 16.4202 21.2572C18.0533 20.5807 19.4492 19.4352 20.4313 17.9654C21.4133 16.4956 21.9375 14.7677 21.9375 13C21.9375 11.2323 21.4133 9.50436 20.4313 8.03459C19.4492 6.56483 18.0533 5.41929 16.4202 4.74283C14.7871 4.06637 12.9901 3.88938 11.2564 4.23424C9.52267 4.57909 7.93016 5.4303 6.68023 6.68024L3.23309 10.1274" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        {{ __('Login & auth') }}
                    </div>
                </div>
            </div>
            {{-- Action menu column END --}}

            <div id="general-data" class="col js-col-forms col-forms active">

                @include('../../components/my-account-admin/employee-profile')

                @include('../../components/my-account-admin/bank-account-information')

            </div>

            <div id="company-information" class="col js-col-forms col-forms">

                @include('../../components/my-account-admin/company-information')
                
            </div>
            
            <div id="change-password" class="col js-col-forms col-forms">
                
                @include('../../components/my-account-admin/change-password')
                
            </div>
            
            <div id="login-auth" class="col js-col-forms col-forms">
                
                @include('../../components/my-account-admin/loginsec')

            </div>

            {{-- Form column END --}}

        </div>

        <div class="row-submit">
            <div class="col-submit">

                {{-- Submit Button --}}
                <div class="button-container">
                    <button type="submit" class="btn btn--primary">{{ __('Save Update') }}</button>
                </div>
                {{-- Submit Button END --}}

            </div>
        </div>

    </div>
@endsection

