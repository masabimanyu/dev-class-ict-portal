<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.partials.header')
</head>
<body>
    <div class="page-container">
        {{-- Sidebar --}}
        <div class="sidebar">
            @include('../components.sidebar')
        </div>
        {{-- Content --}}
        <div class="content">
            {{-- Navbar --}}
            @include('../components.navbar')
            {{-- @include('../pages.client-overview') --}}

            @yield('content')
        </div>

        {{-- Modal --}}
        <div class="modal js-modal">
            <div class="modal-dialog js-modal-dialog"></div>
            @yield('modal')
        </div>
    </div>

    @include('layouts.partials.footer')

</body>
</html>
