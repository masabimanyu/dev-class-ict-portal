@include('layouts.partials.header')

<div class="auth">
    <div class="auth__start">
        <div class="auth__start-wrapper">
            <div class="auth__start-logo">
                <img src="{{ asset('/assets/images/ClassICT_logo.png') }}" alt="Logo Class ICT" height="80"
                    width="235">
                <div></div>
            </div>
            <h1 class="auth__start-title h2 white">
                What’s new
            </h1>
            <div class="auth__posts js-auth__posts">
                <div class="post-container">
                    <p class="post-date white">Jul 4, 2022</p>
                    <h2 class="post-title white">Space for title</h2>
                    <p class="post-desc white">Lorem ipsum dolor sit amet, 
                        consectetur adipiscing elit, 
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                        nisi ut aliquip ex ea commodo consequat
                    </p>
                </div>
                <div class="post-container">
                    <p class="post-date white">Jul 5, 2022</p>
                    <h2 class="post-title white">Space for title</h2>
                    <p class="post-desc white">Lorem ipsum dolor sit amet, 
                        consectetur adipiscing elit, 
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
                        nisi ut aliquip ex ea commodo consequat
                    </p>
                </div>
            </div>
        </div>
        <div class="bg bg-tl">
            <img src="{{ asset('/assets/images/ict-auth-bg-tl.svg') }}" alt="">
        </div>
        <div class="bg bg-tr">
            <img src="{{ asset('/assets/images/ict-auth-bg-tr.svg') }}" alt="">
        </div>
        <div class="bg bg-bl">
            <img src="{{ asset('/assets/images/ict-auth-bg-bl.svg') }}" alt="">
        </div>
        <div class="bg bg-br">
            <img src="{{ asset('/assets/images/ict-auth-bg-br.svg') }}" alt="">
        </div>
    </div>
    <div class="auth__end">
        @yield('content')
        <div class="lang">
            <div class="lang__title">
                <span>Language: </span>
            </div>
            <div class="lang__select">
                <ul class="lang__select-list js-lang-list">
                    <li class="active"><img src="{{ asset('/assets/images/nl.svg') }}" alt="Lang NL" width="32"
                            height="22"> NL
                    </li>
                    <li><img src="{{ asset('/assets/images/en.svg') }}" alt="Lang EN" width="32" height="22"> EN
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

@include('layouts.partials.footer')
