{{-- Title/Breadcrumb --}}
<div class="row breadcrumb">
    <div class="breadcrumb-wrapper">
        <h1>{{ __('Clients - Fotoccasion') }}</h1>
        <p>{{ __('Overview / Client - Fotoccasion') }}</p>
    </div>
</div>
{{-- Title/Breadcrumb END --}}
